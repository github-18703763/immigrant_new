//
//  AlipayHeader.h
//  IntegratedAlipay
//
//  Created by Winann on 15/1/9.
//  Copyright (c) 2015年 Winann. All rights reserved.
//

/**
 *  1. 将本工程中的IntegratedAlipay文件夹导入工程中，记得选copy；
 *  2. 点击项目名称,点击“Build Settings”选项卡,在搜索框中,以关键字“search” 搜索,对“Header Search Paths”增加头文件路径:“$(SRCROOT)/项目名称/IntegratedAlipay/AlipayFiles”（注意：不包括引号，如果不是放到项目根目录下，请在项目名称后面加上相应的目录名）；
 *  3. 点击项目名称,点击“Build Phases”选项卡,在“Link Binary with Librarles” 选项中,新增“AlipaySDK.framework”和“SystemConfiguration.framework” 两个系统库文件。如果项目中已有这两个库文件,可不必再增加；
 *  4. 在本头文件中设置kPartnerID、kSellerAccount、kNotifyURL、kAppScheme和kPrivateKey的值（所有的值在支付宝回复的邮件里面：注意，建议除appScheme以外的字段都从服务器请求）；
 *  5. 点击项目名称,点击“Info”选项卡，在URL types里面添加一项，Identifier可以不填，URL schemes必须和appScheme的值相同，用于支付宝处理回到应用的事件；
 *  6. 在需要用的地方导入“AlipayHeader.h”，并使用“[AlipayRequestConfig alipayWithPartner:...”方法进行支付；
 *  7. 在AppDelegate中处理事件回调（可直接复制下面内容）：
 - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
 //如果极简 SDK 不可用,会跳转支付宝钱包进行支付,需要将支付宝钱包的支付结果回传给 SDK if ([url.host isEqualToString:@"safepay"]) {
 [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
 NSLog(@"result = %@",resultDic);
 }];
 if ([url.host isEqualToString:@"platformapi"]){//支付宝钱包快登授权返回 authCode
 [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
 NSLog(@"result = %@",resultDic);
 }];
 }
 return YES;
 }
 */

#ifndef IntegratedAlipay_AlipayHeader_h
#define IntegratedAlipay_AlipayHeader_h


#import <AlipaySDK/AlipaySDK.h>     // 导入AlipaySDK
#import "AlipayRequestConfig.h"     // 导入支付类
#import "Order.h"                   // 导入订单类
#import "DataSigner.h"              // 生成signer的类：获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循 RSA 签名规范, 并将签名字符串 base64 编码和 UrlEncode

#import <Foundation/Foundation.h>   // 导入Foundation，防止某些类出现类似：“Cannot find interface declaration for 'NSObject', superclass of 'Base64'”的错误提示


/**
 *  partner:合作身份者ID,以 2088 开头由 16 位纯数字组成的字符串。
 *
 */
#define kPartnerID @"2019012163132252"


/**
 *  seller:支付宝收款账号,手机号码或邮箱格式。
 */
#define kSellerAccount @"ceo@letsqyq.com"

/**
 *  支付宝服务器主动通知商户 网站里指定的页面 http 路径。
 */
#define kNotifyURL @"http://xkpay.ht118.cc/Pay/AliPay"


/**
 *  appSckeme:应用注册scheme,在Info.plist定义URLtypes，处理支付宝回调
 */
#define kAppScheme @"IntegratedAlipay"
//2016042901351083

/**
 *  private_key:商户方的私钥,pkcs8 格式。
 */
#define kPrivateKey @"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDG8wQLsSXRcUWYkoXes/NKJdw1t3k5P8/XAezI4sV7+SAHP/Lb7s3BDy2Vx6Jbixvz9wghjccIc91C9gudS9/ehzoj/bTetEtW/iU7CrKq7v8hhatSdOePbQROtc+a4sGJqegXn00ETlenvsPEj66pZBjOo/l3UlkQ0y7a9cVGAybieic7jCKn8eLxG2Y6K2M6E/vzQ/4syM9A8aGnBQqACp48YYAYR0kkrSXFPP07T4qm8Q5pdwFkLs/9E6kuvd5pnnwXP1WzuZQJrsuKAW0Kgx+BiCl/7gmissMJ17frFC9V1LvjeK45me0qe18UlvfZezl1QNrgMZ9a/RGh1KF/AgMBAAECggEAf2kU1zQNBzPmoU59A2H1VjAm9hPZQ6u1PHoUHZb1aG7kbyAqNWWbSZ+hzwF53eyZ60TyZSK6K5SgSAjw6Ty1U3sA5hnN4q5ObZj3WPeB8ux6YgqL7JPH1Fe54XqYSRYdYj94zuALX82YX2pRT6nA7uSW6oei9SK8vYSxxjKNs0aiO0hoayG00zRd9q3wT+RzYy3zd7Y0qdAZMkhJQ0rRrnTd6UwvwAA5KZLDguLxMJAgtNi7fsWj0ASsqPIzEX5jUN1l3GEFaZR9dI38J82x8g8bQlX9t1rnigzZe5sIWQhwQmWGWOX6FzVgLviGrNFOrth1XKi8dSe2zp/meLpU8QKBgQDp0FFX96gRsOk4ZG4pIr+3HWE3fSpPst4VJCStpMI4k4ebH3kChtx5Qybe3huRlT/X7g1pAofxu9ok/9s2efCaZAx5Bx4iX6bVVZZdInfx11wfdFB9A8vJRty6uxXkErzTHn/BOQ8sM34VEPq2rS3NZpN14S7nD0IrjUxgOpCotwKBgQDZ08nkThURy4gKPMZG5PL39gUG2Yw4h/QEMz/uORg1nJGsSUfz496oazOlrzCSMbGviElBNy4v2K8nvMZKakg1FrlIKj+NeYACoG8pPnuIiOLJwfqvX7mRM3xfloa2QZnLLBeMDLDK0EWtiRmRLNaAYhBaw8X4iayoUuK7drE1eQKBgFENvVUwblwLpx28ew/Zkvpove3dfxB34lMT338usYGpawYUh/+fPsjuxYfXGVmDkslJJe70SKo9Q+c1pwmghg8Phd9i30htPkmQTPrmWwxEaxQgX1KnQTr6TMSS3IjfEgRh+0nBfQgGpbup0jAHouwklhP0Ry0kdfzhE8v8IW/LAoGAI4HliCS+RH9T9q8/dvD9NjRRT3OI6vImccTU6AVAkASsrlPnubbsZbHL4nczSLF3EHD71/ihu8TrkGOyQxEeIiFJJJ1WbL5ggo0acbOwuWnu5gqLzaih/CxTWtWTsgPxiO/wCrFK5FSc9xPBKiwP7y+NEO6Wx9PRZELWmM5ZRqkCgYEA2aYTriTPdNhOtT6NvGt3y7L0dgoDjkD5LmJ0/Io6g0+UQGIu39swHIFGn9vitb8fhdSj1FwmLIbhvPPLVhL/W34JtZOAKHcx6ECm3K3ZOHCTWJ3LNsojk/g8l9SXOTwzi/XWKVo1KZ92BBEc5kOmpcahm6likn8oBL0QoK9NGf4="


#endif
