#import "ZPCAshapelGradientView.h"
#define degreesToRadians(x) (M_PI*(x)/180.0) //把角度转换成PI的方式
#define DEFAULT_STARTANGLE   -270  //默认开始度数
#define DEFAULT_ENDANGLE     90  //默认结束度数
#define MAX_PROGRESS_LINE_WIDTH 45 //弧线的最大宽度**

@interface ZPCAshapelGradientView ()
{
    UISlider *_slider;
    CALayer *_gradientLayer;
    
    UILabel *_smallerLabel;
}
@end
@implementation ZPCAshapelGradientView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    //默认线宽 15
    _progressLineWidth = 12;
    _trackLayer = [CAShapeLayer layer];//创建一个track shape layer
    _trackLayer.frame = self.bounds;
    [self.layer addSublayer:_trackLayer];
    _trackLayer.fillColor = [[UIColor clearColor] CGColor];
//    #F5F7F9
    _trackLayer.strokeColor = [ [UIColor getColor:@"F5F7F9"] CGColor];
//    _trackLayer.opacity = .25;
    _trackLayer.lineCap = kCALineCapRound;
    _trackLayer.lineWidth = _progressLineWidth;//线的宽度
    //
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(frame.size.width/2, frame.size.width/2) radius:(frame.size.width - _progressLineWidth)/2 startAngle:degreesToRadians(DEFAULT_STARTANGLE) endAngle:degreesToRadians(DEFAULT_ENDANGLE) clockwise:YES];//上面说明过了用来构建圆形
    _trackLayer.path =[path CGPath]; //把path传递給layer，然后layer会处理相应的渲染，整个逻辑和CoreGraph是一致的。
    
    _progressLayer = [CAShapeLayer layer];
    _progressLayer.frame = self.bounds;
    _progressLayer.fillColor =  [[UIColor clearColor] CGColor];
    _progressLayer.strokeColor  = [[UIColor redColor] CGColor];
    _progressLayer.lineCap = kCALineCapRound;
    _progressLayer.opacity = 1;
    _progressLayer.lineWidth = _progressLineWidth;
    _progressLayer.path = [path CGPath];
    _progressLayer.strokeEnd = 0;
    
    
    _gradientLayer = [CALayer layer];
    
    CAGradientLayer *gradientLayerLeft =  [CAGradientLayer layer];
    gradientLayerLeft.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    //黄上篮下
    [gradientLayerLeft setColors:[NSArray arrayWithObjects:(id)[[UIColor getColor:@"FDBC3F"] CGColor],(id)[[UIColor getColor:@"E65C80"] CGColor],(id)[[UIColor getColor:@"B06CD4"] CGColor],(id)[[UIColor getColor:@"30E9E5"] CGColor], nil]];

    [_gradientLayer addSublayer:gradientLayerLeft];

    [_gradientLayer setMask:_progressLayer]; //用progressLayer来截取渐变层
    [self.layer addSublayer:_gradientLayer];
    
    
    return self;
}
//设置线宽
- (void)setProgressLineWidth:(CGFloat)progressLineWidth{
    
    if (_progressLineWidth != progressLineWidth) {
        _progressLineWidth = progressLineWidth;
        
        if (_progressLineWidth <= MAX_PROGRESS_LINE_WIDTH) {
            _trackLayer.lineWidth = _progressLineWidth;//线的宽度
            //从新定义 轨迹  宽
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:(self.frame.size.width - _progressLineWidth)/2 startAngle:degreesToRadians(DEFAULT_STARTANGLE) endAngle:degreesToRadians(DEFAULT_ENDANGLE) clockwise:YES];
            _trackLayer.path =[path CGPath];
            
            //动态图像路径  跟上重新绘制的  轨迹路径  一样（不然不能完全重合）
            _progressLayer.lineWidth = _progressLineWidth;
            _progressLayer.path = [path CGPath];
            [_gradientLayer setMask:_progressLayer]; //用progressLayer来截取渐变层
            [self.layer addSublayer:_gradientLayer];
            
        }else{
            
            _trackLayer.lineWidth = MAX_PROGRESS_LINE_WIDTH;//最大线的宽度
            //从新定义 轨迹  宽
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:(self.frame.size.width - MAX_PROGRESS_LINE_WIDTH)/2 startAngle:degreesToRadians(DEFAULT_STARTANGLE) endAngle:degreesToRadians(DEFAULT_ENDANGLE) clockwise:YES];
            _trackLayer.path =[path CGPath];
            
            //动态图像路径  跟上重新绘制的  轨迹路径  一样（不然不能完全重合）
            _progressLayer.lineWidth = MAX_PROGRESS_LINE_WIDTH;
            _progressLayer.path = [path CGPath];
            [_gradientLayer setMask:_progressLayer]; //用progressLayer来截取渐变层
            [self.layer addSublayer:_gradientLayer];
            
            
        }
        
    }
    
}

//设置 圆环的角度
- (void)setStartAngle:(CGFloat)startAngle{
    
    if ([NSString stringWithFormat:@"%f",_endAngle] == nil) {
        
        if (_startAngle != startAngle) {
            _startAngle = startAngle;
            //从新定义 开始 角度
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:(self.frame.size.width - MAX_PROGRESS_LINE_WIDTH)/2 startAngle:degreesToRadians(_startAngle) endAngle:degreesToRadians(DEFAULT_ENDANGLE) clockwise:YES];
            _trackLayer.path =[path CGPath];
            _progressLayer.path = [path CGPath];
            
            
        }
    }else{
        
        if (_startAngle != startAngle) {
            _startAngle = startAngle;
            //从新定义 开始 角度
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:(self.frame.size.width - MAX_PROGRESS_LINE_WIDTH)/2 startAngle:degreesToRadians(_startAngle) endAngle:degreesToRadians(_endAngle) clockwise:YES];
            _trackLayer.path =[path CGPath];
            _progressLayer.path = [path CGPath];
            
            
        }
    }
    
}
- (void)setEndAngle:(CGFloat)endAngle{
    
    if ([NSString stringWithFormat:@"%f",_startAngle] == nil) {
        
        if (_endAngle != endAngle) {
            _endAngle = endAngle;
            //从新定义 结束 角度
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:(self.frame.size.width - MAX_PROGRESS_LINE_WIDTH)/2 startAngle:degreesToRadians(DEFAULT_STARTANGLE) endAngle:degreesToRadians(_endAngle) clockwise:YES];
            _trackLayer.path =[path CGPath];
            _progressLayer.path = [path CGPath];
        }
        
    }else{
        
        if (_endAngle != endAngle) {
            _endAngle = endAngle;
            //从新定义 结束 角度
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:(self.frame.size.width - MAX_PROGRESS_LINE_WIDTH)/2 startAngle:degreesToRadians(_startAngle) endAngle:degreesToRadians(_endAngle) clockwise:YES];
            _trackLayer.path =[path CGPath];
            _progressLayer.path = [path CGPath];
        }
        
    }
}

- (void)setPercent:(NSInteger)percent {
    _progressLayer.strokeEnd = percent / 100.0f;
}

@end
