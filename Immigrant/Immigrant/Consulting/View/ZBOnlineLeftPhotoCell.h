//
//  ZBOnlineLeftPhotoCell.h
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBOnlineLeftPhotoCell : UITableViewCell

@property (nonatomic,strong) NSDictionary  *dict;

@end

NS_ASSUME_NONNULL_END
