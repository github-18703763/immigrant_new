//
//  ZBConsultingResultView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBConsultingResultView : UIView
@property (weak, nonatomic) IBOutlet UIButton *allConsultingBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UILabel *labFirstLinkUpTime;//首先时间
@property (weak, nonatomic) IBOutlet UILabel *labSecondLinkUpTime;//次选时间

@end
