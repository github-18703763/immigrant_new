//
//  ZBOnlineLeftPhotoCell.m
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBOnlineLeftPhotoCell.h"

@interface ZBOnlineLeftPhotoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIImageView *adimnImage;


@end

@implementation ZBOnlineLeftPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.photoImg.layer.cornerRadius = 6;
    self.photoImg.layer.masksToBounds = YES;
}

-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
    [self.adimnImage sd_setImageWithURL:[NSURL URLWithString:_dict[@"adminmessage"]] placeholderImage:[UIImage imageNamed:@"collect2"]];
     self.photoImg.image = [UIImage imageNamed:@"denglu_logo"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
