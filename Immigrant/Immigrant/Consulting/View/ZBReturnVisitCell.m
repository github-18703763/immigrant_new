//
//  ZBReturnVisitCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBReturnVisitCell.h"
#import "WSDatePickerView.h"
@interface ZBReturnVisitCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic,strong)NSDate *selectDateAll;//首先开始时间
@end

@implementation ZBReturnVisitCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius =15;
    self.containerView.layer.masksToBounds = YES;
}
- (IBAction)btnStartTimeClick:(UIButton *)sender {
    switch (sender.tag) {
        case 10:
        {
            //年-月-日-时-分
            WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
                self.selectDateAll = selectDate;
                NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
                NSLog(@"选择的日期：%@",dateString);
                self.startTimeTxt.text = dateString;
                self.endTimeTxt.text = @"";
            }];
            datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
            datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
            datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
            [datepicker show];
        }
            break;
        case 11:
        {
            if (self.startTimeTxt.text.length>0) {
                //年-月-日-时-分
                WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
                    NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
                    NSLog(@"选择的日期：%@",dateString);
                    self.endTimeTxt.text = dateString;
                }];
                datepicker.minLimitDate = [NSDate dateWithTimeInterval:60*60 sinceDate:self.selectDateAll];
                datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
                datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
                datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
                [datepicker show];
            }else{
                [[UIApplication sharedApplication].delegate.window makeToast:@"请先选择开始时间" duration:0.8 position:CSToastPositionBottom];
            }
        }
            break;
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
