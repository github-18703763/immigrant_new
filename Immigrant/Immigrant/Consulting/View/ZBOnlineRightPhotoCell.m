//
//  ZBOnlineRightPhotoCell.m
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBOnlineRightPhotoCell.h"

@interface ZBOnlineRightPhotoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;


@end

@implementation ZBOnlineRightPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_dict[@"useravatar"]] placeholderImage:[UIImage imageNamed:@"wanshanziliao_xingming"]];
     [self.userImage sd_setImageWithURL:[NSURL URLWithString:_dict[@"userimage"]] placeholderImage:[UIImage imageNamed:@"collect2"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
