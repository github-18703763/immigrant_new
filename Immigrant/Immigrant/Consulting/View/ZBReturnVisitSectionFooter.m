//
//  ZBReturnVisitSectionFooter.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBReturnVisitSectionFooter.h"


@interface ZBReturnVisitSectionFooter()
//@property (weak, nonatomic) IBOutlet UIButton *addressBtn;

@property (weak, nonatomic) IBOutlet UIButton *moneyBtn;
@property (weak, nonatomic) IBOutlet UILabel *markLab;
//@property (weak, nonatomic) IBOutlet ZBSearchOptionView *seraView;

@end

@implementation ZBReturnVisitSectionFooter

-(void)awakeFromNib{
    [super awakeFromNib];
//    self.addressBtn.layer.cornerRadius = 22.5;
//    self.addressBtn.layer.masksToBounds = YES;
    self.moneyBtn.layer.cornerRadius = 22.5;
    self.moneyBtn.layer.masksToBounds = YES;
    self.moneyBtn.enabled = NO;

}

-(void)setType:(NSInteger)type{
    _type = type;
    if (_type == 1) {
        self.addressLab.hidden = NO;
        self.moneyBtn.hidden = NO;
        self.markLab.hidden = NO;
    }else{
        self.addressLab.hidden = YES;
        self.moneyBtn.hidden = YES;
        self.markLab.hidden = YES;
    }
}

- (IBAction)addressAction:(id)sender {

}

@end
