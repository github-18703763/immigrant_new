//
//  ZBOnlineRightCell.m
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBOnlineRightCell.h"

@interface ZBOnlineRightCell ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;

@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@end

@implementation ZBOnlineRightCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.photoImg.layer.cornerRadius = 6;
    self.photoImg.layer.masksToBounds = YES;
    
}

-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_dict[@"useravatar"]] placeholderImage:[UIImage imageNamed:@"wanshanziliao_xingming"]];
     self.contentLab.text = _dict[@"usermessage"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
