//
//  ZBConsultingView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBConsultingView.h"

@interface ZBConsultingView()
//试图容器
@property (weak, nonatomic) IBOutlet UIView *containerView;
//电话咨询
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
//在线咨询
@property (weak, nonatomic) IBOutlet UIButton *onlineBtn;
//回访
@property (weak, nonatomic) IBOutlet UIButton *visitBtn;


@end

@implementation ZBConsultingView
/**
 关闭事件
 */
- (IBAction)closeAction:(id)sender {
    [self removeFromSuperview];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.phoneBtn.layer.cornerRadius = 25.5;
    self.phoneBtn.layer.masksToBounds = YES;
    self.phoneBtn.layer.borderWidth = 0.5;
    self.phoneBtn.layer.borderColor = COLOR(166, 166, 166, 1).CGColor;
    self.onlineBtn.layer.cornerRadius = 25.5;
    self.onlineBtn.layer.masksToBounds = YES;
    self.visitBtn.layer.cornerRadius = 25.5;
    self.visitBtn.layer.masksToBounds = YES;
    self.visitBtn.layer.borderWidth = 0.5;
    self.visitBtn.layer.borderColor = COLOR(166, 166, 166, 1).CGColor;
}

/**
 电话咨询事件
 */
- (IBAction)phoneAction:(id)sender {
    if (self.phoneBlock) {
        self.phoneBlock();
    }
}

/**
 在线咨询事件
 */
- (IBAction)onlineAction:(id)sender {
    if (self.onlineBlock) {
        [self removeFromSuperview];
        self.onlineBlock();
    }
}

/**
 回访事件
 */
- (IBAction)visitiAction:(id)sender {
    if (self.visitiBlock) {
        [self removeFromSuperview];
        self.visitiBlock();
    }
}


@end
