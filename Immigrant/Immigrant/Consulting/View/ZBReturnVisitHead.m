//
//  ZBReturnVisitHead.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBReturnVisitHead.h"

@interface ZBReturnVisitHead()

@property (weak, nonatomic) IBOutlet UIView *topLine;

@property (weak, nonatomic) IBOutlet UIButton *onlineBtn;
@property (weak, nonatomic) IBOutlet UIButton *conversetionBtn;
@property (weak, nonatomic) IBOutlet UIButton *visitBtn;


@end

@implementation ZBReturnVisitHead

-(void)awakeFromNib{
    [super awakeFromNib];
    self.topLine.layer.masksToBounds = NO;
    self.topLine.layer.shadowColor = COLOR(252, 252, 252, 1).CGColor;
    self.topLine.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.topLine.layer.shadowOpacity = 0.5f;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, SystemScreenWidth, 15)];
    self.topLine.layer.shadowPath = shadowPath.CGPath;

    
}

- (IBAction)onlineAction:(id)sender {
    [self.conversetionBtn setImage:[UIImage imageNamed:@"gongsimiantan_weixuanzhong"] forState:UIControlStateNormal];
   [self.visitBtn setImage:[UIImage imageNamed:@"gongsimiantan_weixuanzhong"] forState:UIControlStateNormal];
    [self.onlineBtn setImage:[UIImage imageNamed:@"gongsimiantan_xuanzhong"] forState:UIControlStateNormal];
    if (self.onlineBlock) {
        self.onlineBlock();
    }
}

- (IBAction)conversetionAction:(id)sender {
    [self.conversetionBtn setImage:[UIImage imageNamed:@"gongsimiantan_xuanzhong"] forState:UIControlStateNormal];
    [self.visitBtn setImage:[UIImage imageNamed:@"gongsimiantan_weixuanzhong"] forState:UIControlStateNormal];
    [self.onlineBtn setImage:[UIImage imageNamed:@"gongsimiantan_weixuanzhong"] forState:UIControlStateNormal];
    if (self.conversetionBlock) {
        self.conversetionBlock();
    }
}

- (IBAction)visitAction:(id)sender {
    [self.conversetionBtn setImage:[UIImage imageNamed:@"gongsimiantan_weixuanzhong"] forState:UIControlStateNormal];
    [self.visitBtn setImage:[UIImage imageNamed:@"gongsimiantan_xuanzhong"] forState:UIControlStateNormal];
    [self.onlineBtn setImage:[UIImage imageNamed:@"gongsimiantan_weixuanzhong"] forState:UIControlStateNormal];
    if (self.visitiBlock) {
        self.visitiBlock();
    }
}

@end
