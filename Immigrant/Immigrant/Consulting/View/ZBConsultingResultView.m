//
//  ZBConsultingResultView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBConsultingResultView.h"

@interface ZBConsultingResultView()
@property (weak, nonatomic) IBOutlet UIView *containerView;


@end

@implementation ZBConsultingResultView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.allConsultingBtn.layer.cornerRadius = 25;
    self.allConsultingBtn.layer.masksToBounds = YES;
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
}

- (IBAction)closeAction:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)sureAction:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)allConsultingAction:(id)sender {
}

@end
