//
//  ZBReturnVisitSectionFooter.h
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBReturnVisitSectionFooter : UIView
@property (weak, nonatomic) IBOutlet UILabel *oneTitleLab;

@property (nonatomic,assign) NSInteger type;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@end
