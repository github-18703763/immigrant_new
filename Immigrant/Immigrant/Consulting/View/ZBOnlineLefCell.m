//
//  ZBOnlineLefCell.m
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBOnlineLefCell.h"

@interface ZBOnlineLefCell ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;


@end

@implementation ZBOnlineLefCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.photoImg.layer.cornerRadius = 6;
    self.photoImg.layer.masksToBounds = YES;
}

-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
    self.contentLab.text = _dict[@"adminmessage"];
    self.photoImg.image = [UIImage imageNamed:@"denglu_logo"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
