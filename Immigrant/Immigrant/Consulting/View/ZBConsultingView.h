//
//  ZBConsultingView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBConsultingView : UIView

@property (nonatomic, copy) void(^onlineBlock)(void);

@property (nonatomic, copy) void(^phoneBlock)(void);

@property (nonatomic, copy) void(^visitiBlock)(void);

@end
