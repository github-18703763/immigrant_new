//
//  ZBReturnVisitController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBReturnVisitController.h"
#import "ZBReturnVisitHead.h"
#import "ZBReturnVisitSectionFooter.h"
#import "ZBReturnVisitCell.h"
#import "ZBConsultingResultView.h"
#import "ZBOptionView.h"
#import "ZBProviceObject.h"
#import "ZBAreaPickerView.h"
@interface ZBReturnVisitController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
//确认按钮
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (weak, nonatomic) IBOutlet UITableView *contentTable;
@property (nonatomic,strong)NSArray *aryCompanyAddressList;
//上门回访选择地址
@property (nonatomic, nonatomic)UITextField *addressLab;
@property (nonatomic,strong) NSMutableArray  *addressArr;
//沟通时间
@property (nonatomic,strong)NSString  *firstSatrtTime;
@property (nonatomic,strong)NSString  *firstEndTime;
@property (nonatomic,strong)NSString  *secondSatrtTime;
@property (nonatomic,strong)NSString  *secondEndTime;
@end

@implementation ZBReturnVisitController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"要求回访";
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
    //公司地址的请求
//    WS(weakSELF);
    [QSNetworkManager appCompanyaddressFindListSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.aryCompanyAddressList = responseModel.data[@"bussData"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
        [self tableConfig];
    [self loadArear];
    
}

-(void)loadArear{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"province" ofType:@"json"];
    NSString *jsonStr = [NSString stringWithContentsOfFile:path usedEncoding:nil error:nil];
    self.addressArr = [NSJSONSerialization JSONObjectWithData:[((NSString *)jsonStr) dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    self.addressArr = [ZBProviceObject mj_objectArrayWithKeyValuesArray:self.addressArr];
}

-(void)tableConfig{
    self.contentTable.showsVerticalScrollIndicator = NO;
    self.contentTable.showsHorizontalScrollIndicator = NO;
    self.contentTable.dataSource = self;
    self.contentTable.delegate =  self;
    
    self.contentTable.separatorColor = [UIColor whiteColor];
    self.contentTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.contentTable registerNib:[UINib nibWithNibName:@"ZBReturnVisitCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    WS(weakSELF);
    ZBReturnVisitHead *head = [[[NSBundle mainBundle] loadNibNamed:@"ZBReturnVisitHead" owner:self options:nil] firstObject];
    head.frame = CGRectMake(0, 0, SystemScreenWidth, 102);
    self.contentTable.tableHeaderView = head;
    head.onlineBlock = ^{
//        ZBLog(@"线上");
        weakSELF.contentTable.tableFooterView = nil;
    };
    head.conversetionBlock = ^{
//        ZBLog(@"公司交谈");
         ZBReturnVisitSectionFooter *footer = [[[NSBundle mainBundle] loadNibNamed:@"ZBReturnVisitSectionFooter" owner:self options:nil] firstObject];
        footer.oneTitleLab.text = @"公司地址";
        ZBOptionView *viewT = [[ZBOptionView alloc] initWithFrame:CGRectMake(100, 23, 300, 40)];
        viewT.dataSource = self.aryCompanyAddressList;
        viewT.selectedBlock = ^(ZBOptionView * _Nonnull optionView, NSString * _Nonnull selectedId) {
            NSLog(@"%@",optionView);
            NSLog(@"%@",selectedId);
        };
        [footer addSubview:viewT];
        weakSELF.contentTable.tableFooterView = footer;
        footer.type = 2;
    };
    head.visitiBlock = ^{
//        ZBLog(@"上门拜访");
        ZBReturnVisitSectionFooter *footer = [[[NSBundle mainBundle] loadNibNamed:@"ZBReturnVisitSectionFooter" owner:self options:nil] firstObject];
        footer.oneTitleLab.text = @"您的城市";
        self.addressLab = [[UITextField alloc]initWithFrame:CGRectMake(100, 23, 300, 40)];
        self.addressLab.borderStyle = UITextBorderStyleRoundedRect;
        self.addressLab.text = @"";
        self.addressLab.placeholder = @"请选择您所在的城市";
        self.addressLab.delegate = self;
        self.addressLab.rightViewMode = UITextFieldViewModeAlways;
        UIImageView *pwdRightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dongxialingyin_xiala"]];
        pwdRightView.frame =CGRectMake(-20,0, 22,22);
        self.addressLab.rightView = pwdRightView;
        [footer addSubview:self.addressLab];
        weakSELF.contentTable.tableFooterView = footer;
        footer.type = 1;
    };
}
//请选择您所在的城市
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.addressLab) {
        ZBAreaPickerView *picker = [[[NSBundle mainBundle] loadNibNamed:@"ZBAreaPickerView" owner:self options:nil] firstObject];
        picker.frame = [UIScreen mainScreen].bounds;
        picker.dataArray = self.addressArr;
        WS(weakSELF);
        picker.filishActionBlock = ^(NSString * _Nonnull proviceStr, NSString * _Nonnull cityStr, NSString * _Nonnull areaStr) {
            weakSELF.addressLab.text = [NSString stringWithFormat:@"%@-%@-%@",proviceStr,cityStr,areaStr];
        };
        [[UIApplication sharedApplication].delegate.window addSubview:picker];
        return NO;
    }else{
        return YES;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
/**
 确认事件
 */
- (IBAction)sureAction:(id)sender {
    ZBConsultingResultView *result = [[[NSBundle mainBundle] loadNibNamed:@"ZBConsultingResultView" owner:self options:nil] firstObject];
    result.labFirstLinkUpTime.text = [NSString stringWithFormat:@"%@到\n%@",self.firstSatrtTime,self.firstEndTime];
    result.labSecondLinkUpTime.text = [NSString stringWithFormat:@"%@到\n%@",self.secondSatrtTime,self.secondEndTime];
    result.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:result];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBReturnVisitCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0){
        cell.leftImgView.image = [UIImage imageNamed:@"shangmenbaifang_shijian"];
        cell.titleLab.text = @"首选沟通时间";
        cell.startTimeTxt.placeholder = @"请选择首选开始沟通的时间";
        cell.endTimeTxt.placeholder = @"请选择首选结束沟通的时间";
    }else{
         cell.leftImgView.image = [UIImage imageNamed:@"shangmenbaifang_shijian_b"];
        cell.titleLab.text = @"次选沟通时间";
        cell.startTimeTxt.placeholder = @"请选择次选开始沟通的时间";
        cell.endTimeTxt.placeholder = @"请选择次选结束沟通的时间";
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
