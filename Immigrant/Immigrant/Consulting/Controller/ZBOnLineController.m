//
//  ZBOnLineController.m
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBOnLineController.h"
#import "ZBOnlineLefCell.h"
#import "ZBOnlineLeftPhotoCell.h"
#import "ZBOnlineRightCell.h"
#import "ZBOnlineRightPhotoCell.h"


@interface ZBOnLineController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,NSURLSessionDelegate>

@property (weak, nonatomic) IBOutlet UITableView *onlineTableView;

@property (nonatomic,strong) NSArray  *messageArray;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (nonatomic,strong) UIImagePickerController  *picker;

@end

@implementation ZBOnLineController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"客服";
    self.onlineTableView.delegate = self;
    self.onlineTableView.dataSource = self;
    self.textfield.delegate = self;
    [self.onlineTableView registerNib:[UINib nibWithNibName:@"ZBOnlineLefCell" bundle:nil] forCellReuseIdentifier:@"leftWord"];
    [self.onlineTableView registerNib:[UINib nibWithNibName:@"ZBOnlineLeftPhotoCell" bundle:nil] forCellReuseIdentifier:@"leftPhoto"];
    [self.onlineTableView registerNib:[UINib nibWithNibName:@"ZBOnlineRightCell" bundle:nil] forCellReuseIdentifier:@"rightWord"];
    [self.onlineTableView registerNib:[UINib nibWithNibName:@"ZBOnlineRightPhotoCell" bundle:nil] forCellReuseIdentifier:@"rightPhoto"];
    self.onlineTableView.separatorColor = [UIColor clearColor];
    self.onlineTableView.estimatedRowHeight = 300;
    self.onlineTableView.rowHeight = UITableViewAutomaticDimension;
//    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;

    [self loadData];
}

-(void)loadData{
    [QSNetworkManager lookForcallcentergmtCreatedGreaterThan:@"" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.messageArray = responseModel.data[@"bussData"];
            [self.onlineTableView reloadData];
            [self.onlineTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.messageArray.count-1 inSection:0]  atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [QSNetworkManager addOnlineASession:[QSUserManager shareInstance].userModel.id imagekey:@"" message:textField.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            [self loadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    self.textfield.text = @"";
    [self.view endEditing:YES];
    return YES;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    NSDictionary *dict = self.messageArray[indexPath.row];
    if ([[dict allKeys] containsObject:@"usermessage"]) {//用户
        NSString *userMsg =  [dict objectForKey:@"usermessage"];
        if (userMsg.length > 0) {//文本
            ZBOnlineRightCell *rightWordCell = [tableView dequeueReusableCellWithIdentifier:@"rightWord" forIndexPath:indexPath];
            rightWordCell.dict = self.messageArray[indexPath.row];
            rightWordCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return rightWordCell;
        }else{//图片
            ZBOnlineRightPhotoCell *rightPhotoCell = [tableView dequeueReusableCellWithIdentifier:@"rightPhoto" forIndexPath:indexPath];
            rightPhotoCell.dict = self.messageArray[indexPath.row];
            rightPhotoCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return rightPhotoCell;
        }
    }else{//客服
        NSString *adminMsg =  [dict objectForKey:@"adminmessage"];
        if (adminMsg.length > 0) {//文本
            ZBOnlineLefCell *leftWordCell = [tableView dequeueReusableCellWithIdentifier:@"leftWord" forIndexPath:indexPath];
            leftWordCell.dict = self.messageArray[indexPath.row];
            leftWordCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return leftWordCell;
        }else{//图片
            ZBOnlineLeftPhotoCell *leftPhotoCell = [tableView dequeueReusableCellWithIdentifier:@"leftPhoto" forIndexPath:indexPath];
            leftPhotoCell.dict = self.messageArray[indexPath.row];
            leftPhotoCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return leftPhotoCell;
        }
       
    }
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = self.messageArray[indexPath.row];
    if ([[dict allKeys] containsObject:@"usermessage"]) {//用户
        NSString *userMsg =  [dict objectForKey:@"usermessage"];
        if (userMsg.length > 0) {//文本
            return UITableViewAutomaticDimension;
        }else{//图片
            return 172.5;
        }
    }else{//客服
        NSString *adminMsg =  [dict objectForKey:@"adminmessage"];
        if (adminMsg.length > 0) {//文本
            return UITableViewAutomaticDimension;
        }else{//图片
            return 172.5;
        }
    }
    return 0.0;
}


- (IBAction)choosePhotoAction:(id)sender {
    
    //访问相册
    if (!self.picker) {
        self.picker = [[UIImagePickerController alloc]init];
    }
    self.picker.view.backgroundColor = [UIColor orangeColor];
    UIImagePickerControllerSourceType sourcheType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.picker.sourceType = sourcheType;
    self.picker.delegate = self;
    self.picker.allowsEditing = NO;
    [self presentViewController:self.picker animated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
 
    [QSNetworkManager getOSSUploadUrl:@"png" contentType:@"image/png" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if(responseModel.status == 200) {

            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:responseModel.data[@"bussData"][@"uploadUrl"]]];
            [request setHTTPMethod:@"PUT"];
            [request setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
            NSURLSessionUploadTask *task = [session uploadTaskWithRequest:request fromData:UIImagePNGRepresentation(img) completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error) {
                        
                        [QSNetworkManager addOnlineASession:[QSUserManager shareInstance].userModel.id imagekey:responseModel.data[@"bussData"][@"fileKey"] message:@"" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                            if (responseModel.status == 200) {
                                [self loadData];
                            }else{//失败
                                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                            }
                            
                        } failBlock:^(NSError * _Nullable error) {
                            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
                        }];
                        
                    } else {
                          [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
                    }
                });
            }];
            
            [task resume];
    }else{//失败
        [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
    }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

@end
