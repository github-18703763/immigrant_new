//
//  AppDelegate.m
//  Immigrant
//
//  Created by 张波 on 2018/12/10.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "AppDelegate.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>
#import "OrderPayTool.h"
#import <UMShare/UMShare.h>
#import <UMCommon/UMCommon.h>

@interface AppDelegate ()<WXApiDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight)];
    [self.window makeKeyAndVisible];
    XKTabBarController *tabBar = [[XKTabBarController alloc] init];
    self.window.rootViewController = tabBar;
    
    //向微信注册
    BOOL result = [WXApi registerApp:@"wxc2766aa68de7b550" enableMTA:YES];
    if (result) {
//        ZBLog(@"注册微信成功");
    }else{
//        ZBLog(@"注册微信失败");
    }
    
    NSString *uShareAppkey = @"5c6b6d74f1f55679580000bb";
    [UMConfigure initWithAppkey:uShareAppkey channel:@"App Store"];
    
    [self configUSharePlatforms];
    [self confitUShareSettings];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
}


- (void)applicationWillEnterForeground:(UIApplication *)application {

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
}


- (void)applicationWillTerminate:(UIApplication *)application {
}

-(void)onReq:(BaseReq*)req{
    
//    ZBLog(@"%@",req);
    
}

-(void)onResp:(BaseResp*)resp{
    
//    ZBLog(@"%@",resp);
//    ZBLog(@"%@",resp.errStr);
//    ZBLog(@"%i",resp.errCode);
//    ZBLog(@"%i",resp.type);
    
    if ([resp isKindOfClass:[PayResp class]]) {
        PayResp *r=(PayResp *)resp;
        
        if (resp.errCode==0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifactionWeixinPaySuccess object:nil];

//            [[NSNotificationCenter defaultCenter]postNotificationName:@"alipaySuccess" object:nil];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifactionWeixinPayFail object:nil];

//            [[NSNotificationCenter defaultCenter]postNotificationName:@"alipayfailure" object:nil];
            
        }
        
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"alipaySuccess" object:nil];
        
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"alipayfailure" object:nil];
        
//        ZBLog(@"%@",r.returnKey);
    }
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        
        SendAuthResp *aresp = (SendAuthResp *)resp;
        
        if (aresp.errCode==0) {
            NSString *code = aresp.code;
            
//            NSNotificationCenter *defaut=[NSNotificationCenter defaultCenter];
//
//            [defaut postNotificationName:@"sendCode" object:code];
            
        }
    }
}

//微信  --->iOS 9.2只有已经没用
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
        if ([url.host isEqualToString:@"safepay"]) {
            // 支付跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
                [[OrderPayTool shared] alipayResult:resultDic];
            }];
            
            // 授权跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
                // 解析 auth code
                NSString *result = resultDic[@"result"];
                NSString *authCode = nil;
                if (result.length>0) {
                    NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                    for (NSString *subResult in resultArr) {
                        if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                            authCode = [subResult substringFromIndex:10];
                            break;
                        }
                    }
                }
                NSLog(@"授权结果 authCode = %@", authCode?:@"");
            }];
        }
        return [WXApi handleOpenURL:url delegate:self];
    }
    return result;
}


// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            [[OrderPayTool shared] alipayResult:resultDic];
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }
    return [WXApi handleOpenURL:url delegate:self];
}

#pragma mark - Umeng

- (void)confitUShareSettings
{
    /*
     * 打开图片水印
     */
    //[UMSocialGlobal shareInstance].isUsingWaterMark = YES;
    /*
     * 关闭强制验证https，可允许http图片分享，但需要在info.plist设置安全域名
     <key>NSAppTransportSecurity</key>
     <dict>
     <key>NSAllowsArbitraryLoads</key>
     <true/>
     </dict>
     */
    //[UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
}

- (void)configUSharePlatforms
{
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wxc2766aa68de7b550" appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    //[[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"101546502"/*设置QQ平台的appID*/  appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    /* 设置新浪的appKey和appSecret */
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:@"3921700954"  appSecret:@"04b48b094faeb16683c32669824ebdad" redirectURL:@"https://sns.whalecloud.com/sina2/callback"];
    
}

@end
