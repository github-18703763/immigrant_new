//
//  ZBUserMessageCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBUserMessageCell.h"
#import "GYLoginRegisterVC.h"
#import "ZBExchangeAlertView.h"
@interface ZBUserMessageCell()
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UIButton *outBtn;

@end

@implementation ZBUserMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImg.layer.cornerRadius = 65/2;
    self.headImg.layer.masksToBounds = YES;
    self.outBtn.layer.cornerRadius = 25;
    self.outBtn.layer.masksToBounds = YES;
    
    [self.outBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)btnClick: (UIButton *)btn {
    
    ZBExchangeAlertView *alert = [[[NSBundle mainBundle] loadNibNamed:@"ZBExchangeAlertView" owner:self options:nil] firstObject];
    alert.noticeStr=@"退出登录?";
    alert.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:alert];
    alert.sureBlock = ^{
        [[QSUserManager shareInstance] exitLogin];
        XKNavigationController *nav = [[XKNavigationController alloc]  initWithRootViewController:[GYLoginRegisterVC new]];
        [[self currentViewController] presentViewController:nav animated:YES completion:nil];
        
    };
}


@end
