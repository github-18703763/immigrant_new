//
//  GYUserModel.h
//  Immigrant
//
//  Created by jlc on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBEarnPointsModel.h"
#import "XLCouponModel.h"
@interface GYUserModel : NSObject

/**
 "avatar": "avatar.jpg",
 */
@property (nonatomic,copy) NSString  *avatar;
/**
 "avatarkey": "avatar.jpg",
 */
@property (nonatomic,copy) NSString  *avatarkey;
/**
 "creator": "string",
 */
@property (nonatomic,copy) NSString  *creator;
/**
 "email": "1106988868@qq.com",
 */
@property (nonatomic,copy) NSString  *email;
/**
 "gmtCreated": "2018-12-28T01:52:41.812Z",
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
 "gmtModified": "2018-12-28T01:52:41.812Z",
 */
@property (nonatomic,copy) NSString  *gmtModified;
/**
 "id": 0,
 */
@property (nonatomic,copy) NSString  *id;
/**
 "isDeleted": "string",
 */
@property (nonatomic,copy) NSString  *isDeleted;
/**
 "isblacklist": false,
 */
@property (nonatomic,assign) BOOL  isblacklist;
/**
 "isblacklisttext": "正常",
 */
@property (nonatomic,copy) NSString  *isblacklisttext;
/**
 "issalesman": false,
 */
@property (nonatomic,assign) BOOL  issalesman;
/**
 "issalesmantext": "是",
 */
@property (nonatomic,copy) NSString  *issalesmantext;
/**
 "mobile": "15814520421",
 */
@property (nonatomic,copy) NSString  *mobile;
/**
 "modifier": "string",
 */
@property (nonatomic,copy) NSString  *modifier;
/**
 "nickname": "一二三四",
 */
@property (nonatomic,copy) NSString  *nickname;
/**
 "password": "$11$oZMEnFV./9P0tes4xLsKGuMU93Cqcj1I8S3BcFAlXLgsDRqDfpF2e",
 */
@property (nonatomic,copy) NSString  *password;
/**
 "sign": "人生如戏，戏如人生",
 */
@property (nonatomic,copy) NSString  *sign;
/**
 "tag": "移民,购房",
 */
@property (nonatomic,copy) NSString  *tag;
/**
 "username": "张三",
 */
@property (nonatomic,copy) NSString  *username;
/**
 "userrole": "member",
 */
@property (nonatomic,copy) NSString  *userrole;
/**
 "userroletext": "会员",
 */
@property (nonatomic,copy) NSString  *userroletext;
/**
 "userstatus": "enable",
 */
@property (nonatomic,copy) NSString  *userstatus;
/**
 "userstatustext": "禁用",
 */
@property (nonatomic,copy) NSString  *userstatustext;
/**
 "weixin": "qq1106988868"
 */
@property (nonatomic,copy) NSString  *weixin;
/**
 "积分"
 */
@property (nonatomic,strong) ZBEarnPointsModel *point;
/**
 "优惠券"
 */
@property (nonatomic,strong) XLCouponModel *coupon;


@end
