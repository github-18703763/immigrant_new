//
//  ZBUserResetPasswordController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBUserResetPasswordController.h"
#import "GYLoginRegisterVC.h"

@interface ZBUserResetPasswordController ()

@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIView *containerOne;
@property (weak, nonatomic) IBOutlet UIView *containerTwo;
@property (weak, nonatomic) IBOutlet UIView *containerThree;
@property (weak, nonatomic) IBOutlet UIView *containerFour;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;
@property (weak, nonatomic) IBOutlet UITextField *codeTxt;
@property (weak, nonatomic) IBOutlet UITextField *firstPwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *secondPwdTxt;

@property (nonatomic, assign)NSUInteger codeNumberInt;
@property (strong,nonatomic)NSTimer *timer;
@property (strong,nonatomic)NSString *strMsgCode;//手机验证码
@end

@implementation ZBUserResetPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"忘记密码";
    self.phoneTxt.keyboardType = UIKeyboardTypeNumberPad;
    self.codeNumberInt = 60;
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
    self.containerOne.layer.cornerRadius = 24;
    self.containerOne.layer.masksToBounds = YES;
    self.containerTwo.layer.cornerRadius = 24;
    self.containerTwo.layer.masksToBounds = YES;
    self.containerThree.layer.cornerRadius = 24;
    self.containerThree.layer.masksToBounds = YES;
    self.containerFour.layer.cornerRadius = 24;
    self.containerFour.layer.masksToBounds = YES;
    self.codeBtn.layer.cornerRadius = 24;
    self.codeBtn.layer.masksToBounds = YES;
}
- (IBAction)codeAction:(UIButton *)sender {
    //1、判断手机号码的格式是否正确
    if (![self checkPhoneNumInput:self.phoneTxt.text]) {
        return;
    }
    sender.userInteractionEnabled = NO;
    //按钮开始倒计时
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshBtnTitle:) userInfo:nil repeats:YES];
    
    [self requestCode];
    
}
//请求验证码
- (void)requestCode{
    WS(weakSELF);
    [QSNetworkManager getMobileCodeByResetPwd:self.phoneTxt.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.strMsgCode = responseModel.data[@"bussData"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            self.codeBtn.userInteractionEnabled = YES;
            self.codeNumberInt = 0;
            [self refreshBtnTitle:weakSELF.timer];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        self.codeBtn.userInteractionEnabled = YES;
        self.codeNumberInt = 0;
        [self refreshBtnTitle:weakSELF.timer];
    }];
}

//检查是否为手机号的方法
-(BOOL)checkPhoneNumInput:(NSString *)phoneStr
{
    NSString *photoRange = @"^1(3[0-9]|4[0-9]|5[0-9]|7[0-9]|8[0-9])\\d{8}$";//正则表达式
    NSPredicate *regexMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",photoRange];
    BOOL result = [regexMobile evaluateWithObject:phoneStr];
    if (result) {
        return YES;
    } else {
        [[UIApplication sharedApplication].delegate.window makeToast:@"手机号码格式不正确" duration:0.8 position:CSToastPositionBottom];
        return NO;
    }
}
//获取验证码
- (void)refreshBtnTitle:(NSTimer *)aTimer{
    if (self.codeNumberInt <= 0) {
        [aTimer invalidate];
        [self.codeBtn setEnabled:YES];
        self.codeNumberInt = 59;
        [self.codeBtn setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
        [self.codeBtn setTitle:[NSString stringWithFormat:@"重新获取"] forState:UIControlStateNormal];
        self.codeBtn.userInteractionEnabled = YES;
        return;
    }
    [self.codeBtn setEnabled:NO];
    [self.codeBtn setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
    self.codeNumberInt--;
}

- (IBAction)sureAction:(id)sender {
    if (![self.codeTxt.text isEqualToString:self.strMsgCode]) {
        [[UIApplication sharedApplication].delegate.window makeToast:@"验证码不正确" duration:0.8 position:CSToastPositionBottom];
        return;
    }else if (self.firstPwdTxt.text.length < 6 || self.firstPwdTxt.text.length > 16){
        [[UIApplication sharedApplication].delegate.window makeToast:@"密码必须为6-16位" duration:0.8 position:CSToastPositionBottom];
        return;
    }else if (![self.secondPwdTxt.text isEqualToString:self.firstPwdTxt.text]){
        [[UIApplication sharedApplication].delegate.window makeToast:@"两次密码不一致" duration:0.8 position:CSToastPositionBottom];
        return;
    }

    [QSNetworkManager postUserLogin:self.phoneTxt.text pwd:self.firstPwdTxt.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSString *sesstionId = responseModel.data[@"bussData"];
            [QSNetworkManager postUserDetail:sesstionId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {
                    [[QSUserManager shareInstance] userLoginWithsessionId:sesstionId user:responseModel.data[@"bussData"]];
                    XKNavigationController *nav = [[XKNavigationController alloc]  initWithRootViewController:[GYLoginRegisterVC new]];
//                    GYLoginRegisterVC *login = [GYLoginRegisterVC new];
                    [self presentViewController:nav animated:YES completion:nil];
                }else{
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
                
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
