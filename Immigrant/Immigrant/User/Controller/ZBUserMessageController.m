//
//  ZBUserMessageController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBUserMessageController.h"
#import "ZBUserMessageCell.h"
#import "ZBUserResetPasswordController.h"
@interface ZBUserMessageController ()

@end

@implementation ZBUserMessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人信息";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBUserMessageCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    self.tableView.estimatedRowHeight = 500;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBUserMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //重置登录密码
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapResetPwdClick)];
    [cell.resetPwdView addGestureRecognizer:tap];
    return cell;
}

//重置支付密码
- (void)tapResetPwdClick{
    ZBUserResetPasswordController *vc = [ZBUserResetPasswordController new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
