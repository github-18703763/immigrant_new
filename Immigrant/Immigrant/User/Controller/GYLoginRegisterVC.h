//
//  GYLoginRegisterVC.h
//  Immigrant
//
//  Created by jlc on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYBaseVC.h"
//登录或者注册
@interface GYLoginRegisterVC : GYBaseVC
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;


@property (weak, nonatomic) IBOutlet UIView *leftView;

//@property (weak, nonatomic) IBOutlet UIView *rightView;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

//@property (weak, nonatomic) IBOutlet UIView *loginUserBackView;
//@property (weak, nonatomic) IBOutlet UIView *loginPassBackView;

@property (weak, nonatomic) IBOutlet UITextField *loginTF;

@property (weak, nonatomic) IBOutlet UITextField *loginPassTF;

//右边的注册视图
//@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
//@property (weak, nonatomic) IBOutlet UITextField *codeTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnCodeResfresh;
@property (weak, nonatomic) IBOutlet UITextField *fisrtPwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *secondPwdTxt;
@property (weak, nonatomic) IBOutlet UIButton *reigisterBtn;

//2.19 修复
@property (weak, nonatomic) IBOutlet UIButton *forgetPswBtn;
@property (weak, nonatomic) IBOutlet UITextField *invitCodeTF;
@property (weak, nonatomic) IBOutlet UIButton *loginViewBtn;
@property (weak, nonatomic) IBOutlet UIButton *reigisterViewBtn;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *invitLB;



@end
