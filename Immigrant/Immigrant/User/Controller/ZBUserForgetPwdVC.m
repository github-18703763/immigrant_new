//
//  ZBUserForgetPwdVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/13.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBUserForgetPwdVC.h"

@interface ZBUserForgetPwdVC ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UITextField *codeTxt;
@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;

@property (weak, nonatomic) IBOutlet UITextField *firstPwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *secondPwdTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnSure;
@property (nonatomic, assign)NSUInteger codeNumberInt;
@property (strong,nonatomic)NSTimer *timer;
@property (strong,nonatomic)NSString *strMsgCode;//手机验证码

@end

@implementation ZBUserForgetPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.phoneTxt.keyboardType = UIKeyboardTypeNumberPad;
    self.codeNumberInt = 60;
    [self setFrameView];
}
//修改界面的元素
- (void)setFrameView{
    //
    ViewShadowRadius(self.bgView, 15, 00, 0, UIColorFromRGB(0x8DADD4), 0.3, 40);
    //
    //设置左边视图的宽度(占位)
    self.phoneTxt.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    //设置显示模式为永远显示(默认不显示 必须设置 否则没有效果)
    self.phoneTxt.leftViewMode = UITextFieldViewModeAlways;
    self.phoneTxt.layer.cornerRadius = 24.0;
    self.phoneTxt.layer.masksToBounds = YES;
    
    self.codeTxt.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    //设置显示模式为永远显示(默认不显示 必须设置 否则没有效果)
    self.codeTxt.leftViewMode = UITextFieldViewModeAlways;
    self.codeTxt.layer.cornerRadius = 24.0;
    self.codeTxt.layer.masksToBounds = YES;
    //
    self.codeBtn.layer.cornerRadius = 20.0;
    self.codeBtn.layer.masksToBounds = YES;
    //
    self.firstPwdTxt.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    //设置显示模式为永远显示(默认不显示 必须设置 否则没有效果)
    self.firstPwdTxt.leftViewMode = UITextFieldViewModeAlways;
    self.firstPwdTxt.layer.cornerRadius = 24.0;
    self.firstPwdTxt.layer.masksToBounds = YES;
    
    self.secondPwdTxt.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    //设置显示模式为永远显示(默认不显示 必须设置 否则没有效果)
    self.secondPwdTxt.leftViewMode = UITextFieldViewModeAlways;
    self.secondPwdTxt.layer.cornerRadius = 24.0;
    self.secondPwdTxt.layer.masksToBounds = YES;
    //
    ViewShadowRadius(self.btnSure, 25, 0, 10, UIColorFromRGB(0x267BFF), 0.18, 14);
    
    //获取验证码事件
    [self.codeBtn addTarget:self action:@selector(btnCodeResfreshAction:) forControlEvents:UIControlEventTouchUpInside];
}
//确定
- (IBAction)btnSureClick:(id)sender {
    if (![self.codeTxt.text isEqualToString:self.strMsgCode]) {
        [[UIApplication sharedApplication].delegate.window makeToast:@"验证码不正确" duration:0.8 position:CSToastPositionBottom];
        return;
    }else if (self.firstPwdTxt.text.length < 6 || self.firstPwdTxt.text.length > 16){
        [[UIApplication sharedApplication].delegate.window makeToast:@"密码必须为6-16位" duration:0.8 position:CSToastPositionBottom];
        return;
    }else if (![self.secondPwdTxt.text isEqualToString:self.firstPwdTxt.text]){
        [[UIApplication sharedApplication].delegate.window makeToast:@"两次密码不一致" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    WS(weakSELF);
    [QSNetworkManager postUserLogin:self.phoneTxt.text pwd:self.firstPwdTxt.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSString *sesstionId = responseModel.data[@"bussData"];
            [QSNetworkManager postUserDetail:sesstionId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {
                    [[QSUserManager shareInstance] userLoginWithsessionId:sesstionId user:responseModel.data[@"bussData"]];
                    [weakSELF dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                }else{
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
                
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}
- (IBAction)backBtnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//获取验证码
- (void)btnCodeResfreshAction:(UIButton *)sender {
    //1、判断手机号码的格式是否正确
    if (![self checkPhoneNumInput:self.phoneTxt.text]) {
        return;
    }
    sender.userInteractionEnabled = NO;
    //按钮开始倒计时
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshBtnTitle:) userInfo:nil repeats:YES];
    
    [self requestCode];
    
}
//请求验证码
- (void)requestCode{
    WS(weakSELF);
    [QSNetworkManager getMobileCodeByResetPwd:self.phoneTxt.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.strMsgCode = responseModel.data[@"bussData"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            self.codeBtn.userInteractionEnabled = YES;
            self.codeNumberInt = 0;
            [self refreshBtnTitle:weakSELF.timer];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        self.codeBtn.userInteractionEnabled = YES;
        self.codeNumberInt = 0;
        [self refreshBtnTitle:weakSELF.timer];
    }];
}

//检查是否为手机号的方法
-(BOOL)checkPhoneNumInput:(NSString *)phoneStr
{
    NSString *photoRange = @"^1(3[0-9]|4[0-9]|5[0-9]|7[0-9]|8[0-9])\\d{8}$";//正则表达式
    NSPredicate *regexMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",photoRange];
    BOOL result = [regexMobile evaluateWithObject:phoneStr];
    if (result) {
        return YES;
    } else {
        [[UIApplication sharedApplication].delegate.window makeToast:@"手机号码格式不正确" duration:0.8 position:CSToastPositionBottom];
        return NO;
    }
}
//获取验证码
- (void)refreshBtnTitle:(NSTimer *)aTimer{
    if (self.codeNumberInt <= 0) {
        [aTimer invalidate];
        [self.codeBtn setEnabled:YES];
        self.codeNumberInt = 59;
        [self.codeBtn setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
        [self.codeBtn setTitle:[NSString stringWithFormat:@"重新获取"] forState:UIControlStateNormal];
        self.codeBtn.userInteractionEnabled = YES;
        return;
    }
    [self.codeBtn setEnabled:NO];
    [self.codeBtn setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
    self.codeNumberInt--;
}
@end
