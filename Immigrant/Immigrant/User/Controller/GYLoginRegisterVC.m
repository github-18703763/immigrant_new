//
//  GYLoginRegisterVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYLoginRegisterVC.h"
//#import "FSSegmentTitleView.h"
#import "ZBPerfectInformationController.h"
#import "ZBUserForgetPwdVC.h"
#import "ZBEarnPointsModel.h"

@interface GYLoginRegisterVC ()
{
    UIButton *_lastBtn;
}
//@property (nonatomic,strong) FSSegmentTitleView *titleView;
//@property (assign,nonatomic)NSInteger flag;
@property (nonatomic, assign)NSUInteger codeNumberInt;
@property (strong,nonatomic)NSTimer *timer;
@property (strong,nonatomic)NSString *strMsgCode;//手机验证码
@end

@implementation GYLoginRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.codeNumberInt = 60;

    ViewShadowRadius(self.leftView, 15, 00, 0, UIColorFromRGB(0x8DADD4), 0.3, 40);
    
    
    self.loginTF.leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    self.loginTF.leftViewMode=UITextFieldViewModeAlways;
    self.loginPassTF.leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    self.loginPassTF.leftViewMode=UITextFieldViewModeAlways;
    self.fisrtPwdTxt.leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    self.fisrtPwdTxt.leftViewMode=UITextFieldViewModeAlways;
    self.secondPwdTxt.leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    self.secondPwdTxt.leftViewMode=UITextFieldViewModeAlways;
    self.invitCodeTF.rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    self.invitCodeTF.rightViewMode=UITextFieldViewModeAlways;
    ViewRadius(self.loginTF, 25);
    ViewRadius(self.loginPassTF, 25);
    ViewRadius(self.fisrtPwdTxt, 25);
    ViewRadius(self.secondPwdTxt, 25);
    ViewRadius(self.invitCodeTF, 25);
    ViewRadius(self.lineView, 1.5);
    //重新获取
    ViewRadius(self.btnCodeResfresh, 24);
    ViewRadius(self.invitLB, 24);
    
    //登录状态下
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(250);
    }];
    self.reigisterBtn.hidden=YES;
    self.fisrtPwdTxt.hidden=YES;
    self.secondPwdTxt.hidden=YES;
    self.invitCodeTF.hidden=YES;
    self.invitLB.hidden=YES;
    self.btnCodeResfresh.hidden=YES;
    
    _lastBtn=self.loginViewBtn;
    [self.loginViewBtn addTarget:self action:@selector(changeLoginOrRegist:) forControlEvents:UIControlEventTouchUpInside];
    [self.reigisterViewBtn addTarget:self action:@selector(changeLoginOrRegist:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.loginBtn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    //登录
     ViewShadowRadius(self.loginBtn, 25, 0, 10, UIColorFromRGB(0x267BFF), 0.18, 14);
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.leftView.mas_centerX);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(50);
        make.top.equalTo(self.leftView.mas_bottom).offset(-25);
    }];
    //注册
     ViewShadowRadius(self.reigisterBtn, 25, 0, 10, UIColorFromRGB(0x267BFF), 0.18, 14);
    [self.reigisterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.leftView.mas_centerX);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(50);
        make.top.equalTo(self.leftView.mas_bottom).offset(-25);
    }];
   
    
    [self.reigisterBtn addTarget:self action:@selector(reigisterBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCodeResfresh addTarget:self action:@selector(btnCodeResfreshAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    [IQKeyboardManager sharedManager].enable = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden=NO;
    [IQKeyboardManager sharedManager].enable = NO;

}

#pragma mark - method
-(void)loginAction:(UIButton*)btn{
    if (self.loginTF.text.length == 0 || self.loginPassTF.text.length == 0){
        [[UIApplication sharedApplication].delegate.window makeToast:@"信息不能为空" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    WS(weakSELF);
    [QSNetworkManager postUserLogin:self.loginTF.text pwd:self.loginPassTF.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
             NSString *sesstionId = responseModel.data[@"bussData"];
            [QSNetworkManager postUserDetail:sesstionId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {
                    [[QSUserManager shareInstance] userLoginWithsessionId:sesstionId user:responseModel.data[@"bussData"]];
                    [self requestUserPoint];
                    [weakSELF dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                }else{
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
                
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}
//注册
-(void)reigisterBtnAction:(UIButton*)btn{
    if (![self.loginPassTF.text isEqualToString:self.strMsgCode]) {
         [[UIApplication sharedApplication].delegate.window makeToast:@"验证码不正确" duration:0.8 position:CSToastPositionBottom];
        return;
    }else if (self.fisrtPwdTxt.text.length < 6 || self.fisrtPwdTxt.text.length > 16){
        [[UIApplication sharedApplication].delegate.window makeToast:@"密码必须为6-16位" duration:0.8 position:CSToastPositionBottom];
        return;
    }else if (![self.secondPwdTxt.text isEqualToString:self.fisrtPwdTxt.text]){
        [[UIApplication sharedApplication].delegate.window makeToast:@"两次密码不一致" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    ZBPerfectInformationController *detail = [ZBPerfectInformationController new];
    detail.phoneNum = self.loginTF.text;
    detail.strPassWord = self.fisrtPwdTxt.text;
    [self.navigationController pushViewController:detail animated:YES];
}
//更改成登录或者注册
-(void)changeLoginOrRegist:(UIButton *)btn{
    if (_lastBtn.tag==btn.tag) {
        return;
    }
     [btn setTitleColor:UIColorFromRGB(0x144681) forState:UIControlStateNormal];
     [_lastBtn setTitleColor:UIColorFromRGB(0xb2bace) forState:UIControlStateNormal];
    
    if (btn.tag==0) {
//        self.leftView.height=250;
        [self.view setNeedsUpdateConstraints];
        [self.leftView mas_updateConstraints:^(MASConstraintMaker *make) {
             make.height.mas_equalTo(250);
        }];
        [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(60);
        }];
      
        [self.view layoutIfNeeded];
        
        self.reigisterBtn.hidden=YES;
        self.fisrtPwdTxt.hidden=YES;
        self.secondPwdTxt.hidden=YES;
        self.invitCodeTF.hidden=YES;
        self.invitLB.hidden=YES;
        self.btnCodeResfresh.hidden=YES;
        self.loginBtn.hidden=NO;
        self.loginPassTF.placeholder=@"请输入密码";
        self.forgetPswBtn.hidden=NO;
    }else{
        [self.view setNeedsUpdateConstraints];
        [self.leftView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(400);
        }];
        [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(SystemScreenWidth-28-28-30-60);
        }];
        [self.view layoutIfNeeded];
        
        [self.leftView setNeedsUpdateConstraints];
        self.reigisterBtn.hidden=NO;
        self.fisrtPwdTxt.hidden=NO;
        self.secondPwdTxt.hidden=NO;
        self.invitCodeTF.hidden=NO;
        self.invitLB.hidden=NO;
        self.btnCodeResfresh.hidden=NO;
        self.loginBtn.hidden=YES;
        self.loginPassTF.placeholder=@"验证码";
        self.forgetPswBtn.hidden=YES;
    }
    _lastBtn=btn;
}
//忘记密码
- (IBAction)btnForgetPwdClick:(id)sender {

    ZBUserForgetPwdVC *vc = [ZBUserForgetPwdVC new];
    [self presentViewController:vc animated:YES completion:nil];
}
- (IBAction)backBtnClick:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

//获取验证码
- (void)btnCodeResfreshAction:(UIButton *)sender {
    //1、判断手机号码的格式是否正确
    if (![self checkPhoneNumInput:self.loginTF.text]) {
        return;
    }
    sender.userInteractionEnabled = NO;
    //按钮开始倒计时
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshBtnTitle:) userInfo:nil repeats:YES];
 
        [self requestCode];

}
//请求验证码
- (void)requestCode{
    WS(weakSELF);
    [QSNetworkManager getMobileCodeByRegister:self.loginTF.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.strMsgCode = responseModel.data[@"bussData"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            self.btnCodeResfresh.userInteractionEnabled = YES;
            self.codeNumberInt = 0;
            [self refreshBtnTitle:weakSELF.timer];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        self.btnCodeResfresh.userInteractionEnabled = YES;
        self.codeNumberInt = 0;
        [self refreshBtnTitle:weakSELF.timer];
    }];
}

//检查是否为手机号的方法
-(BOOL)checkPhoneNumInput:(NSString *)phoneStr
{
    NSString *photoRange = @"^1(3[0-9]|4[0-9]|5[0-9]|7[0-9]|8[0-9])\\d{8}$";//正则表达式
    NSPredicate *regexMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",photoRange];
    BOOL result = [regexMobile evaluateWithObject:phoneStr];
    if (result) {
        return YES;
    } else {
     [[UIApplication sharedApplication].delegate.window makeToast:@"手机号码格式不正确" duration:0.8 position:CSToastPositionBottom];
        return NO;
    }
}
//获取验证码
- (void)refreshBtnTitle:(NSTimer *)aTimer{
    if (self.codeNumberInt <= 0) {
        [aTimer invalidate];
        [self.btnCodeResfresh setEnabled:YES];
        self.codeNumberInt = 59;
        [self.btnCodeResfresh setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
        [self.btnCodeResfresh setTitle:[NSString stringWithFormat:@"重新获取"] forState:UIControlStateNormal];
        self.btnCodeResfresh.userInteractionEnabled = YES;
        return;
    }
    [self.btnCodeResfresh setEnabled:NO];
    [self.btnCodeResfresh setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
    self.codeNumberInt--;
}
#pragma 获取用户积分
- (void)requestUserPoint{
    [QSNetworkManager appPointFinddetailSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            ZBEarnPointsModel *myEarnPointsModel =  [ZBEarnPointsModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            [[NSUserDefaults standardUserDefaults]setObject:myEarnPointsModel.currentpoint forKey:@"CURRENTPOINT"];

        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        
    }];
}

//override,避免重复弹出
- (void)presentLogin {
    
}
@end
