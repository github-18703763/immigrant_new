//
//  ZBPerfectInformationController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBPerfectInformationController.h"

@interface ZBPerfectInformationController ()
@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UIButton *filishBtn;
@property (weak, nonatomic) IBOutlet UIView *containerOne;
@property (weak, nonatomic) IBOutlet UIView *containerTwo;
@property (weak, nonatomic) IBOutlet UIView *containerThree;
@property (weak, nonatomic) IBOutlet UIView *containerFour;
@property (weak, nonatomic) IBOutlet UIView *containerFive;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *userNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;
@property (weak, nonatomic) IBOutlet UITextField *weiXinNumTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;

@end

@implementation ZBPerfectInformationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"完善资料";
    self.phoneTxt.text = _phoneNum;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
    
//    self.filishBtn.layer.cornerRadius = 25;
//    self.filishBtn.layer.masksToBounds = YES;
    ViewShadowRadius(self.filishBtn, 25, 0, 10, UIColorFromRGB(0x267BFF), 0.18, 14);
    self.containerOne.layer.cornerRadius = 24;
    self.containerOne.layer.masksToBounds = YES;
    self.containerTwo.layer.cornerRadius = 24;
    self.containerTwo.layer.masksToBounds = YES;
    self.containerThree.layer.cornerRadius = 24;
    self.containerThree.layer.masksToBounds = YES;
    self.containerFour.layer.cornerRadius = 24;
    self.containerFour.layer.masksToBounds = YES;
    self.containerFive.layer.cornerRadius = 24;
    self.containerFive.layer.masksToBounds = YES;
    
    self.phoneTxt.text = self.phoneNum;
}

- (IBAction)filishAction:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.phoneTxt.text.length == 0 || self.nickNameTxt.text.length == 0|| self.userNameTxt.text.length == 0|| self.weiXinNumTxt.text.length == 0|| self.emailTxt.text.length == 0){
         [[UIApplication sharedApplication].delegate.window makeToast:@"信息不能为空" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    //注册
    WS(weakSELF);
    [QSNetworkManager postUserRegister:self.phoneTxt.text pwd:self.strPassWord nickname:self.nickNameTxt.text username:self.userNameTxt.text weixin:self.weiXinNumTxt.text email:self.emailTxt.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
              [[UIApplication sharedApplication].delegate.window makeToast:@"注册成功" duration:0.8 position:CSToastPositionBottom];
              [weakSELF dismissViewControllerAnimated:YES completion:^{
              }];
//            NSString *sesstionId = responseModel.data[@"bussData"];
//            [QSNetworkManager postUserDetail:sesstionId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
//                if (responseModel.status == 200) {
//                    [[QSUserManager shareInstance] userLoginWithsessionId:sesstionId user:responseModel.data[@"bussData"]];
//                    [weakSELF dismissViewControllerAnimated:YES completion:^{
//
//                    }];
//                }else{
//                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
//                }
//            }];
        }else{
           [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
