//
//  ZBInspectionRouteCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBInspectionObject.h"
#import "ZBCampObject.h"
#import "ZBMyCollectionModel.h"

@interface ZBInspectionRouteCell : UITableViewCell
@property (nonatomic,strong) ZBInspectionObject  *model;
@property (nonatomic,strong) ZBCampObject  *modelCamp;
//我的收藏
@property (nonatomic,strong) ZBLineDTOModel  *modelLineDto;
@property (nonatomic,strong) ZBCampDTOModel  *modelCampDto;
@end
