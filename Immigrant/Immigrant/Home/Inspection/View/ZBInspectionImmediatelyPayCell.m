//
//  ZBInspectionImmediatelyPayCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionImmediatelyPayCell.h"
#import "ZBPayMethodView.h"

@interface ZBInspectionImmediatelyPayCell()
@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@end

@implementation ZBInspectionImmediatelyPayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.orderView.layer.cornerRadius = 25;
    self.orderView.layer.masksToBounds = YES;
    [self.orderLab setAttributedText:[NSMutableAttributedString createColorString:@"订单编号：2018101610048575" withExcision:@"订单编号：" dainmaicColor:[UIColor getColor:@"4A4A4A"] excisionColor:[UIColor getColor:@"267BFF"]]];
    self.containerView.backgroundColor = [UIColor whiteColor];
    [self.containerView addShadow];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
    self.payBtn.layer.cornerRadius = 25;
    self.payBtn.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 25;
    self.pointView.layer.masksToBounds = YES;
}
- (IBAction)payAction:(id)sender {
    
    ZBPayMethodView *pay = [[[NSBundle mainBundle] loadNibNamed:@"ZBPayMethodView" owner:self options:nil] firstObject];
    
    pay.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:pay];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
