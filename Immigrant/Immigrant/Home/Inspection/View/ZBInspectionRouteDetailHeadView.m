//
//  ZBInspectionRouteDetailHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionRouteDetailHeadView.h"

@interface ZBInspectionRouteDetailHeadView()
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *containerOne;
@property (weak, nonatomic) IBOutlet UIView *containerTwo;
@property (weak, nonatomic) IBOutlet UIView *containerThree;
@property (weak, nonatomic) IBOutlet UIView *containerFour;
@property (weak, nonatomic) IBOutlet UIView *containerFive;
@property (weak, nonatomic) IBOutlet UIView *pointOne;
@property (weak, nonatomic) IBOutlet UIView *pointTwo;
@property (weak, nonatomic) IBOutlet UIView *pointThree;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *manCountLab;
@property (weak, nonatomic) IBOutlet UILabel *fullManLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoTwoImg;
@property (weak, nonatomic) IBOutlet UILabel *projectInformationLab;//项目信息

@property (weak, nonatomic) IBOutlet UILabel *costDescriptionLab;//费用说明
@end

@implementation ZBInspectionRouteDetailHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 355) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = path.bounds;
    maskLayer.path=path.CGPath;
    self.photoImg.layer.mask=maskLayer;
    self.containerOne.backgroundColor = [UIColor whiteColor];
    [self.containerOne addShadow];
    self.containerTwo.backgroundColor = [UIColor whiteColor];
    [self.containerTwo addShadow];
    self.containerThree.backgroundColor = [UIColor whiteColor];
    [self.containerThree addShadow];
    
    self.containerFour.backgroundColor = [UIColor whiteColor];
    [self.containerFour addShadow];
    self.containerFive.backgroundColor = [UIColor whiteColor];
    [self.containerFive addShadow];

    self.pointOne.layer.cornerRadius = 4;
    self.pointOne.layer.masksToBounds = YES;
    self.pointTwo.layer.cornerRadius = 4;
    self.pointTwo.layer.masksToBounds = YES;
    self.pointThree.layer.cornerRadius = 4;
    self.pointThree.layer.masksToBounds = YES;
    
}

-(void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
    //这里可能是轮播图
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_dicAll[@"cover"]] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    NSArray *aryStart = [_dicAll[@"gmtCreated"] componentsSeparatedByString:@" "];
    NSArray *aryEnd = [_dicAll[@"gmtModified"] componentsSeparatedByString:@" "];
    self.timeLab.text = [NSString stringWithFormat:@"%@到%@",aryStart[0],aryEnd[0]];
    //当前人数
    NSInteger allCount = [_dicAll[@"totalnum"] integerValue];
    NSInteger currentCount = [_dicAll[@"currentnum"] integerValue];
    if (currentCount == allCount) {
        self.fullManLab.hidden = NO;
    }
    self.manCountLab.text = [NSString stringWithFormat:@"%li人",currentCount];
    self.projectInformationLab.text = _dicAll[@"info"];
    self.costDescriptionLab.text = _dicAll[@"feeinclude"];
    
//    "bussData": {
//    currentnum  //当前人数
//        "country": "美国",
//        "countryId": 2,
//        "cover": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545896678&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=e15FfoFJBfAH%2BDgwojUC2HWe%2B8k%3D",
//        "creator": "2",
//        "depositfee": 12888900,
//        "depositfeeyuan": 128889,
//        "edate": "2018-10-20 00:00:00",
//        "feeinclude": "费用包含。。。",
//        "gmtCreated": "2018-12-27 12:15:34",
//        "gmtModified": "2018-12-27 12:54:32",
//        "goal": "实地考察以便偷渡",
//        "id": 7,
//        "images": [
//                   "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545896678&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=e15FfoFJBfAH%2BDgwojUC2HWe%2B8k%3D",
//                   "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545896678&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=e15FfoFJBfAH%2BDgwojUC2HWe%2B8k%3D",
//                   "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545896678&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=e15FfoFJBfAH%2BDgwojUC2HWe%2B8k%3D",
//                   "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545896678&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=e15FfoFJBfAH%2BDgwojUC2HWe%2B8k%3D"
//                   ],
//        "imageskey": "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg,icebartech-migrate/9.jpg,icebartech-migrate/9.jpg",
//        "info": "项目信息。。。",
//        "introduce": "行程介绍。。。",
//        "isDeleted": "n",
//        "modifier": "[SYS]",
//        "name": "美国哈佛大学考察",
//        "sdate": "2018-10-10 00:00:00",
//        "totalnum": 30,
//        "totalprice": 23463000,
//        "totalpriceyuan": 234630
//    }
}
@end
