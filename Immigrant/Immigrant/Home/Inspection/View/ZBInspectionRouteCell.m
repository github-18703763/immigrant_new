//
//  ZBInspectionRouteCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionRouteCell.h"

@interface ZBInspectionRouteCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLabe;
@property (weak, nonatomic) IBOutlet UILabel *surplusNumLab;

@end

@implementation ZBInspectionRouteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.backgroundColor = [UIColor whiteColor];
    [self.containerView addShadow];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
//    ￥8888元/人
    [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:@"￥8888元/人" withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
}
//定制考察
-(void)setModel:(ZBInspectionObject *)model{
    _model = model;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _model.name;
    self.timeLabe.text = _model.gmtCreated;
    int allCount = _modelCamp.totalnum.intValue;
    int currentCount = _modelCamp.currentnum.intValue;
    self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%i人",allCount-currentCount];
    NSString *strMoney = [NSString stringWithFormat:@"￥%@元/人",_model.totalpriceyuan];
     [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:strMoney withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
}
//冬夏令营列表
-(void)setModelCamp:(ZBCampObject *)modelCamp{
    _modelCamp = modelCamp;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelCamp.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelCamp.name;
    self.timeLabe.text = _modelCamp.gmtCreated;
    int allCount = _modelCamp.totalnum.intValue;
    int currentCount = _modelCamp.currentnum.intValue;
    self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%i人",allCount-currentCount];
    NSString *strMoney = [NSString stringWithFormat:@"￥%@元/人",_modelCamp.totalpriceyuan];
    [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:strMoney withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
}
//我的收藏---
-(void)setModelCampDto:(ZBCampDTOModel *)modelCampDto{
    _modelCampDto = modelCampDto;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelCampDto.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelCampDto.name;
    self.timeLabe.text = _modelCampDto.gmtCreated;
    int allCount = _modelCampDto.totalnum.intValue;
    int currentCount = _modelCampDto.currentnum.intValue;
    self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%i人",allCount-currentCount];
    NSString *strMoney = [NSString stringWithFormat:@"￥%@元/人",_modelCampDto.totalpriceyuan];
    [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:strMoney withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
}
-(void)setModelLineDto:(ZBLineDTOModel *)modelLineDto{
    _modelLineDto = modelLineDto;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelLineDto.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelLineDto.name;
    self.timeLabe.text = _modelLineDto.gmtCreated;
    int allCount = _modelLineDto.totalnum.intValue;
    int currentCount = _modelLineDto.currentnum.intValue;
    self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%i人",allCount-currentCount];
    NSString *strMoney = [NSString stringWithFormat:@"￥%@元/人",_modelLineDto.totalpriceyuan];
    [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:strMoney withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
