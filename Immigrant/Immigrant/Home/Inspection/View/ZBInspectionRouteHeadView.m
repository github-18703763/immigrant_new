//
//  ZBInspectionRouteHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionRouteHeadView.h"

@interface ZBInspectionRouteHeadView()

@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnThree;
@property (weak, nonatomic) IBOutlet UIButton *btnFour;

@end

@implementation ZBInspectionRouteHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.btnOne.layer.cornerRadius = 17.5;
    self.btnOne.layer.masksToBounds = YES;
    self.btnTwo.layer.cornerRadius = 17.5;
    self.btnTwo.layer.masksToBounds = YES;
    self.btnThree.layer.cornerRadius = 17.5;
    self.btnThree.layer.masksToBounds = YES;
    self.btnFour.layer.cornerRadius = 17.5;
    self.btnFour.layer.masksToBounds = YES;

}

@end
