//
//  ZBInspectionRouteDetailController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBInspectionObject.h"

@interface ZBInspectionRouteDetailController : UIViewController

@property (nonatomic,strong) ZBInspectionObject  *model;

@end
