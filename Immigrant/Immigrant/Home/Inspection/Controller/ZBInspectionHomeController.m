//
//  ZBInspectionHomeController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionHomeController.h"
#import "ZBInspectionRouteController.h"

@interface ZBInspectionHomeController ()

@end

@implementation ZBInspectionHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"定制考察";
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
}

- (IBAction)routeAction:(id)sender {
    ZBInspectionRouteController *route = [ZBInspectionRouteController new];
    [self.navigationController pushViewController:route animated:YES];
    
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
