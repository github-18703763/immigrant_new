//
//  ZBInspectionRouteController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionRouteController.h"
#import "ZBInspectionRouteHeadView.h"
#import "ZBInspectionRouteCell.h"
#import "ZBInspectionRouteDetailController.h"
#import "ZBInspectionObject.h"
#import "LineDetailController.h"

@interface ZBInspectionRouteController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;
@property (weak, nonatomic) IBOutlet UITableView *routeTable;
@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;

@end

@implementation ZBInspectionRouteController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"经典路线";
    self.searchTxt.layer.cornerRadius = 6;
    self.searchTxt.layer.masksToBounds = YES;
    self.routeTable.delegate = self;
    self.routeTable.dataSource  = self;
    self.routeTable.showsHorizontalScrollIndicator = NO;
    self.routeTable.showsVerticalScrollIndicator = NO;
    self.routeTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0   );
    self.routeTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.routeTable.backgroundColor = COLOR(252, 252, 252, 1);
    ZBInspectionRouteHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBInspectionRouteHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 120);
    self.routeTable.tableHeaderView = headView;
    
    [self.routeTable registerNib:[UINib nibWithNibName:@"ZBInspectionRouteCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.routeTable.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.routeTable.mj_footer = footer;
    
    [self loadData:YES];
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
   [QSNetworkManager inspectionNameLinek:@"" withPage:self.pageIndex withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.routeTable.mj_footer endRefreshing];
            [weakSELF.routeTable.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBInspectionObject mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.routeTable reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.routeTable.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBInspectionObject mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.routeTable.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.routeTable.mj_footer endRefreshing];
            [weakSELF.routeTable.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.routeTable.mj_footer endRefreshing];
        [weakSELF.routeTable.mj_header endRefreshing];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.newsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBInspectionRouteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.newsArray[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 145;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    ZBInspectionRouteDetailController *detial = [ZBInspectionRouteDetailController new];
//    detial.model = self.newsArray[indexPath.row];
//    [self.navigationController pushViewController:detial animated:YES];
    
    LineDetailController *vc = [[LineDetailController alloc] init];
    ZBInspectionObject *obj = self.newsArray[indexPath.row];
    vc.lineId = [obj.id integerValue];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
