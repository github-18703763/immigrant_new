//
//  ZBInspectionRouteDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBInspectionRouteDetailController.h"
#import "ZBInspectionRouteDetailHeadView.h"
#import "ZBInspectionRouteSureController.h"
#import "LineSignupController.h"
@interface ZBInspectionRouteDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *detailTable;
@property (weak, nonatomic) IBOutlet UIButton *willingBtn;

@property (strong,nonatomic)NSDictionary *dicAll;
@property (strong,nonatomic)ZBInspectionRouteDetailHeadView *headView;
@end

@implementation ZBInspectionRouteDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.detailTable.showsVerticalScrollIndicator = NO;
    self.detailTable.showsHorizontalScrollIndicator = NO;
    self.headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBInspectionRouteDetailHeadView" owner:self options:nil] firstObject];
    self.detailTable.tableHeaderView = self.headView;
    self.detailTable.tableFooterView = [UIView new];
    self.detailTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.detailTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.detailTable.estimatedSectionHeaderHeight = 250;
    self.detailTable.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.detailTable.rowHeight = UITableViewAutomaticDimension;
    self.detailTable.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    self.detailTable.delegate = self;
    self.detailTable.dataSource = self;
    [self.detailTable registerNib:[UINib nibWithNibName:@"ZBInspectionRouteDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    self.willingBtn.layer.cornerRadius = 25;
    self.willingBtn.layer.masksToBounds = YES;
    [QSNetworkManager inspectionDetial:self.model.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.dicAll = responseModel.data[@"bussData"];
            NSString *str = [self stringByDecodingHTMLEntitiesInString:self.dicAll[@"feeinclude"]];
//            ZBLog(@"%@",str);
            self.headView.dicAll = self.dicAll;
            [self.detailTable reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

- (NSString *)stringByDecodingHTMLEntitiesInString:(NSString *)input
{
    NSMutableString *results = [NSMutableString string];
    NSScanner *scanner = [NSScanner scannerWithString:input];
    [scanner setCharactersToBeSkipped:nil];
    while (![scanner isAtEnd]) {
        NSString *temp;
        if ([scanner scanUpToString:@"&" intoString:&temp]) {
            [results appendString:temp];
        }
        if ([scanner scanString:@"&" intoString:NULL]) {
            BOOL valid = YES;
            unsigned c = 0;
            NSUInteger savedLocation = [scanner scanLocation];
            if ([scanner scanString:@"#" intoString:NULL]) {
                // it's a numeric entity
                if ([scanner scanString:@"x" intoString:NULL]) {
                    // hexadecimal
                    unsigned int value;
                    if ([scanner scanHexInt:&value]) {
                        c = value;
                    } else {
                        valid = NO;
                    }
                } else {
                    // decimal
                    int value;
                    if ([scanner scanInt:&value] && value >= 0) {
                        c = value;
                    } else {
                        valid = NO;
                    }
                }
                if (![scanner scanString:@";" intoString:NULL]) {
                    // not ;-terminated, bail out and emit the whole entity
                    valid = NO;
                }
            } else {
                if (![scanner scanUpToString:@";" intoString:&temp]) {
                    // &; is not a valid entity
                    valid = NO;
                } else if (![scanner scanString:@";" intoString:NULL]) {
                    // there was no trailing ;
                    valid = NO;
                } else if ([temp isEqualToString:@"amp"]) {
                    c = '&';
                } else if ([temp isEqualToString:@"quot"]) {
                    c = '"';
                } else if ([temp isEqualToString:@"lt"]) {
                    c = '<';
                } else if ([temp isEqualToString:@"gt"]) {
                    c = '>';
                } else if ([temp isEqualToString:@"nbsp"]){
                    c = ' ';
                    
                }else{
                    // unknown entity
                    valid = NO;
                }
            }
            if (!valid) {
                // we errored, just emit the whole thing raw
                [results appendString:[input substringWithRange:NSMakeRange(savedLocation, [scanner scanLocation]-savedLocation)]];
            } else {
                [results appendFormat:@"%C", c];
            }
        }
    }
    return results;
}


- (IBAction)willingAction:(id)sender {
//    ZBInspectionRouteSureController *sure = [ZBInspectionRouteSureController new];
//    [self.navigationController pushViewController:sure animated:YES];
    
    LineSignupController *vc = [[LineSignupController alloc] init];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    self.headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBInspectionRouteDetailHeadView" owner:self options:nil] firstObject];
////    headView.dicAll = self.dicAll;
//    return self.headView;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

@end
