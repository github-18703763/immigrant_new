//
//  LineSignupController.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LineSignupController : UIViewController
@property (nonatomic,strong) LineModel *model;

@end

NS_ASSUME_NONNULL_END
