//
//  LineDetailController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "LineDetailController.h"
#import "StudyModel.h"
#import "QSNetworkManager+order.h"
#import <YYModel.h>
#import "LineSignupController.h"
#import "XRCarouselView.h"
#import "SingleLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import <WebKit/WebKit.h>
#import "PointWebContainCell.h"
#import "WebContainCell.h"
#import "QSNetworkManager+Order.h"
#import "QSNetworkManager+JLC.h"
#import "ZBConsultingView.h"
#import "ZBReturnVisitController.h"
#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>
#import <MBProgressHUD.h>
#import "LineModel.h"
#import "ImgTitleContainCell.h"
#import "ZBOnLineController.h"

@interface LineDetailController ()<UITableViewDataSource, UITableViewDelegate>{
    CGFloat _webCellHeight;
    CGFloat _infoWebCellHeight;
    CGFloat _feeWebCellHeight;
}

@property (nonatomic, strong) LineModel *detailModel;

@property (nonatomic, strong) XRCarouselView *cycleView;

@property (nonatomic, strong) UIView *headView;

@property (nonatomic, strong) UIButton *collectBtn;

@property (nonatomic, strong) UIButton *shareBtn;

@property (nonatomic, strong) UILabel *priceLabel;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) WKWebView *introduceWebView;

@property (nonatomic, strong) UIScrollView *introduceScrollView;

@property (nonatomic, strong) WKWebView *feeWebView;

@property (nonatomic, strong) UIScrollView *feeScrollView;

@property (nonatomic, strong) WKWebView *infoWebView;

@property (nonatomic, strong) UIScrollView *infoScrollView;

@property (nonatomic, strong) UIButton *talkBtn;

@property (nonatomic, strong) UIButton *applyBtn;

@property (nonatomic, strong) UIView *applyShadowView;

@end

@implementation LineDetailController

- (void)dealloc {
    [self.introduceWebView.scrollView removeObserver:self forKeyPath:@"contentSize"];
    [self.feeWebView.scrollView removeObserver:self forKeyPath:@"contentSize"];
    [self.infoWebView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    [self.navigationController.navigationBar setBackgroundImage: [UIImage createImageWithColor:UIColor.clearColor withRect:CGRectMake(0, 0, 1, 1)] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = UIColor.clearColor;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    self.navigationItem.leftBarButtonItem.tintColor = UIColor.whiteColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableHeaderView = self.headView;
    [self.headView addSubview:self.cycleView];
    
    [self.headView addSubview:self.collectBtn];
    [self.headView addSubview:self.shareBtn];
    [self.headView addSubview:self.priceLabel];
    [self.headView addSubview:self.nameLabel];
    [self.view addSubview:self.applyShadowView];
    
    [self.view addSubview:self.applyBtn];
    [self.view addSubview:self.talkBtn];
    
    [self fetchData];
    [self masLayout];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 80)];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.cycleView.imageClickBlock = ^(NSInteger index) {
        
    };
}

- (void)masLayout {
    [self.cycleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.headView);
    }];
    
    [self.collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headView).offset(70);
        make.right.equalTo(self.headView).offset(-17);
        make.width.height.mas_equalTo(54);
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headView).offset(-9.5);
        make.left.equalTo(self.headView).offset(21);
        make.width.height.mas_equalTo(54);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headView).offset(-16.5);
        make.bottom.equalTo(self.headView).offset(-18.5);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(105);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headView).offset(24);
        make.top.equalTo(self.headView).offset(70);
    }];
    
    [self.talkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(90);
        make.width.mas_equalTo(90);
        make.left.equalTo(self.view).offset(14);
        make.top.equalTo(self.applyBtn).offset(0);
    }];
    
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.talkBtn.mas_right).offset(24);
        make.height.equalTo(@(58));
        make.right.equalTo(self.view).offset(-18);
        make.bottom.equalTo(self.view).offset(-26);
    }];
    
    [self.applyShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.applyBtn);
    }];
}

- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh {
    [self fetchData];
}

#pragma mark - Private Method

- (void)fetchData {
    
    [QSNetworkManager inspectionDetial:[@(self.lineId) stringValue] successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            NSDictionary *dic = responseModel.data[@"bussData"];
            self.detailModel = [LineModel yy_modelWithDictionary:dic];
            [self configData];
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [self.tableView.mj_header endRefreshing];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [self.tableView.mj_header endRefreshing];
    }];
    
}

- (void)configData {
    self.priceLabel.text = [NSString stringWithFormat:@"￥%@/人",@(self.detailModel.totalpriceyuan)];
    self.cycleView.imageArray = self.detailModel.images;
    [self.introduceWebView loadHTMLString:self.detailModel.introduce baseURL:nil];
    [self.feeWebView loadHTMLString:self.detailModel.feeinclude baseURL:nil];
    [self.infoWebView loadHTMLString:self.detailModel.info baseURL:nil];

    self.collectBtn.selected = self.detailModel.isfavorites;
    
    if (self.detailModel.currentnum < self.detailModel.totalnum) {
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"立即考察（余位：%@人）",@(self.detailModel.totalnum - self.detailModel.currentnum) ] attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [self.applyBtn setAttributedTitle:btnString forState:UIControlStateNormal];
    } else {
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"我愿意候补" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [self.applyBtn setAttributedTitle:btnString forState:UIControlStateNormal];
    }
    self.nameLabel.text = self.detailModel.name;
}

- (void)collectTap {
    
    if (self.detailModel.isfavorites) {
        [QSNetworkManager favoriteDelete:self.detailModel.id outType:@"line" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {//成功
                self.detailModel.isfavorites = !self.detailModel.isfavorites;
                self.collectBtn.selected = self.detailModel.isfavorites;
            } else {//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];
    } else {
        [QSNetworkManager favoriteAdd:self.detailModel.id outType:@"line" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {//成功
                self.detailModel.isfavorites = !self.detailModel.isfavorites;
                self.collectBtn.selected = self.detailModel.isfavorites;
            } else {//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];
    }
    
    
}

- (void)talkTap {
    ZBConsultingView *consulting = [[[NSBundle mainBundle] loadNibNamed:@"ZBConsultingView" owner:self options:nil] firstObject];
    consulting.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:consulting];
    consulting.phoneBlock = ^{
        //        ZBLog(@"电话咨询");
        NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"4007888798"];UIWebView * callWebview = [[UIWebView alloc] init];[callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
    };
    consulting.onlineBlock = ^{
        //        ZBLog(@"在线咨询");
        ZBOnLineController *onLine = [ZBOnLineController new];
        [self.navigationController pushViewController:onLine animated:YES];
    };
    consulting.visitiBlock = ^{
        //        ZBLog(@"要求回访");
        ZBReturnVisitController *visit = [[ZBReturnVisitController alloc] init];
        [self.navigationController pushViewController:visit animated:YES];
        
    };
}

- (void)shareTap {
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_Sina)]];
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
        [self shareWebPageToPlatformType:platformType];
    }];
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:@"侨一侨" descr:@"" thumImage:[UIImage imageNamed:@"home_advisory"]];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"https://migrate.letsqyq.com/share/index?socialParentId=%@",@(1)];
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
            [MBProgressHUD showError:@"分享失败"];
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                [MBProgressHUD showError:resp.message];
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

#pragma mark - Event Response

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"]) {
        // 方法一
        if (object == self.introduceWebView.scrollView) {
            UIScrollView *scrollView = (UIScrollView *)object;
            CGFloat height = scrollView.contentSize.height;
            
            self.introduceWebView.frame = CGRectMake(0, 0, self.view.frame.size.width - 68, height);
            self.introduceScrollView.frame = CGRectMake(34, 60, self.view.frame.size.width - 68, height);
            self.introduceScrollView.contentSize =CGSizeMake(self.view.frame.size.width - 68, height);
            
            if (ABS(_webCellHeight - height) > 1) {
                _webCellHeight = height;
                [self.tableView reloadData];
            }
            
            
        } else if (object == self.feeWebView.scrollView) {
            UIScrollView *scrollView = (UIScrollView *)object;
            CGFloat height = scrollView.contentSize.height;
            self.feeWebView.frame = CGRectMake(0, 0, self.view.frame.size.width - 68, height);
            self.feeScrollView.frame = CGRectMake(34, 60, self.view.frame.size.width - 68, height);
            self.feeScrollView.contentSize =CGSizeMake(self.view.frame.size.width - 68, height);
            if (ABS(_feeWebCellHeight - height) > 1) {
                _feeWebCellHeight = height;
                [self.tableView reloadData];
            }
        } else if (object == self.infoWebView.scrollView) {
            UIScrollView *scrollView = (UIScrollView *)object;
            CGFloat height = scrollView.contentSize.height;
            self.infoWebView.frame = CGRectMake(0, 0, self.view.frame.size.width - 68, height);
            self.infoScrollView.frame = CGRectMake(34, 60, self.view.frame.size.width - 68, height);
            self.infoScrollView.contentSize =CGSizeMake(self.view.frame.size.width - 68, height);
            if (ABS(_infoWebCellHeight - height) > 1) {
                _infoWebCellHeight = height;
                [self.tableView reloadData];
            }
        }
        
    }
}

- (void)btnClick:(UIButton *)btn {
    if (btn == self.applyBtn) {
        if (self.detailModel) {
            LineSignupController *vc = [[LineSignupController alloc] init];
            vc.model = self.detailModel;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } else if (btn == self.talkBtn) {
        [self talkTap];
    } else if (btn == self.shareBtn) {
        [self shareTap];
    } else if (btn == self.collectBtn) {
        [self collectTap];
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ImgTitleContainCell *cell = [ImgTitleContainCell cellForTableView:tableView];
        cell.leftImgv.image = [UIImage imageNamed:@"shijian"];
        
        NSAttributedString *hintStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"考察时间  "] attributes:@{NSForegroundColorAttributeName : [UIColor getColor:@"4e4e4e"], NSFontAttributeName: [UIFont boldSystemFontOfSize:18]}];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithAttributedString:hintStr];
        
        if (self.detailModel.datestr) {
            NSAttributedString *timeStr = [[NSAttributedString alloc] initWithString:self.detailModel.datestr attributes:@{NSForegroundColorAttributeName : [UIColor getColor:@"4e4e4e"], NSFontAttributeName: [UIFont systemFontOfSize:15]}];
            [str appendAttributedString:timeStr];
        }
        
        cell.titleLabel.attributedText = str;
        
        return cell;
    } else if (indexPath.row == 1) {
        ImgTitleContainCell *cell = [ImgTitleContainCell cellForTableView:tableView];
        cell.leftImgv.image = [UIImage imageNamed:@"luxianxiangqing_manren"];
        
        
        NSAttributedString *numberStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@人",@(self.detailModel.totalnum)] attributes:@{NSForegroundColorAttributeName : [UIColor getColor:@"4e4e4e"], NSFontAttributeName: [UIFont systemFontOfSize:18]}];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithAttributedString:numberStr];
        
        if (self.detailModel.currentnum < self.detailModel.totalnum) {
            
            NSAttributedString *currentNumberStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"（已报名%@）",@(self.detailModel.currentnum)] attributes:@{NSForegroundColorAttributeName : [UIColor getColor:@"FF8535"], NSFontAttributeName: [UIFont systemFontOfSize:18]}];
            [str appendAttributedString:currentNumberStr];

        } else {
            NSAttributedString *currentNumberStr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"（已报满）"] attributes:@{NSForegroundColorAttributeName : [UIColor getColor:@"FF8535"], NSFontAttributeName: [UIFont systemFontOfSize:18]}];
            [str appendAttributedString:currentNumberStr];

        }
        
        [cell updateImgWidth:25];
        cell.titleLabel.attributedText = str;
        return cell;
    } else if (indexPath.row == 2) {
        PointWebContainCell *cell = [PointWebContainCell cellForTableView:tableView];
        cell.titleLabel.text = @"行程介绍";
        [cell.contentView addSubview:self.introduceScrollView];
        
        return cell;
    } else if (indexPath.row == 3) {
        PointWebContainCell *cell = [PointWebContainCell cellForTableView:tableView];
        cell.titleLabel.text = @"项目信息";
        [cell.contentView addSubview:self.infoScrollView];
        
        return cell;
    } else if (indexPath.row == 4) {
        PointWebContainCell *cell = [PointWebContainCell cellForTableView:tableView];
        cell.titleLabel.text = @"费用说明";
        [cell.contentView addSubview:self.feeScrollView];
        
        return cell;
    }
    
    return [UITableViewCell cellForTableView:tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 2:
            return _webCellHeight + 90;
        case 3:
            return _infoWebCellHeight + 90;
        case 4:
            return _feeWebCellHeight + 90;
        default:
            break;
    }
    return UITableViewAutomaticDimension;
}

#pragma mark - Getter

- (XRCarouselView *)cycleView {
    if (!_cycleView) {
        _cycleView = [[XRCarouselView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 356.0 / 374 * SystemScreenWidth)];
        _cycleView.pagePosition = PositionHide;
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:_cycleView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
        shapeLayer.frame = _cycleView.bounds;
        shapeLayer.path = path.CGPath;
        _cycleView.layer.mask = shapeLayer;
    }
    return _cycleView;
}

- (UIView *)headView {
    if (!_headView) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 356.0 / 374 * SystemScreenWidth)];
        _headView.backgroundColor = UIColor.whiteColor;
    }
    return _headView;
}

- (UIButton *)collectBtn {
    if (!_collectBtn) {
        _collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_collectBtn setImage:[UIImage imageNamed:@"collect2"] forState:UIControlStateNormal];
        [_collectBtn setImage:[UIImage imageNamed:@"anlixiangqing_yishoucang"] forState:UIControlStateSelected];
    }
    return _collectBtn;
}

- (UIButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareBtn setImage:[UIImage imageNamed:@"fenxiang-2"] forState:UIControlStateNormal];
    }
    return _shareBtn;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = [UIColor getColor:@"E7E7E7"];
        _priceLabel.backgroundColor = [UIColor getColor:@"303030" alpha:0.69];
        _priceLabel.frame = CGRectMake(0, 0, 105, 45);
        _priceLabel.layer.cornerRadius = 45.0 / 2;
        _priceLabel.layer.masksToBounds = true;
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.font = [UIFont systemFontOfSize:22 weight:UIFontWeightBlack];
    }
    return _priceLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:24];
    }
    return _nameLabel;
}

- (WKWebView *)introduceWebView {
    if (!_introduceWebView) {
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        wkWebConfig.userContentController = wkUController;
        // 自适应屏幕宽度js
        NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        // 添加js调用
        [wkUController addUserScript:wkUserScript];
        
        _introduceWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1) configuration:wkWebConfig];
        _introduceWebView.backgroundColor = [UIColor clearColor];
        _introduceWebView.opaque = NO;
        _introduceWebView.userInteractionEnabled = NO;
        _introduceWebView.scrollView.bounces = NO;
        [_introduceWebView sizeToFit];
        [_introduceWebView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        if (@available(iOS 11, *)) {
            _introduceWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _introduceWebView;
}

- (UIScrollView *)introduceScrollView {
    if (!_introduceScrollView) {
        _introduceScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        [self.introduceScrollView addSubview:self.introduceWebView];
        if (@available(iOS 11, *)) {
            _introduceScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _introduceScrollView;
}

- (WKWebView *)feeWebView {
    if (!_feeWebView) {
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        wkWebConfig.userContentController = wkUController;
        // 自适应屏幕宽度js
        NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        // 添加js调用
        [wkUController addUserScript:wkUserScript];
        
        _feeWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1) configuration:wkWebConfig];
        _feeWebView.backgroundColor = [UIColor clearColor];
        _feeWebView.opaque = NO;
        _feeWebView.userInteractionEnabled = NO;
        _feeWebView.scrollView.bounces = NO;
        [_feeWebView sizeToFit];
        [_feeWebView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        if (@available(iOS 11, *)) {
            _feeWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _feeWebView;
}

- (UIScrollView *)feeScrollView {
    if (!_feeScrollView) {
        _feeScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        [self.feeScrollView addSubview:self.feeWebView];
        if (@available(iOS 11, *)) {
            _feeScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _feeScrollView;
}

- (WKWebView *)infoWebView {
    if (!_infoWebView) {
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        wkWebConfig.userContentController = wkUController;
        // 自适应屏幕宽度js
        NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        // 添加js调用
        [wkUController addUserScript:wkUserScript];
        
        _infoWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1) configuration:wkWebConfig];
        _infoWebView.backgroundColor = [UIColor clearColor];
        _infoWebView.opaque = NO;
        _infoWebView.userInteractionEnabled = NO;
        _infoWebView.scrollView.bounces = NO;
        [_infoWebView sizeToFit];
        [_infoWebView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        if (@available(iOS 11, *)) {
            _infoWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _infoWebView;
}

- (UIScrollView *)infoScrollView {
    if (!_infoScrollView) {
        _infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        [self.infoScrollView addSubview:self.infoWebView];
        if (@available(iOS 11, *)) {
            _infoScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _infoScrollView;
}

- (UIButton *)applyBtn {
    if (!_applyBtn) {
        _applyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_applyBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"立即考察" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_applyBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _applyBtn.layer.cornerRadius = 25;
        _applyBtn.layer.masksToBounds = true;
        [_applyBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _applyBtn;
}

- (UIView *)applyShadowView {
    if (!_applyShadowView) {
        _applyShadowView = [[UIView alloc] init];
        _applyShadowView.backgroundColor = UIColor.whiteColor;
        _applyShadowView.layer.cornerRadius = 25;
        _applyShadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _applyShadowView.layer.shadowRadius = 17.5;
        _applyShadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _applyShadowView.layer.shadowOpacity = 0.26;
    }
    return _applyShadowView;
}

- (UIButton *)talkBtn {
    if (!_talkBtn) {
        _talkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_talkBtn setImage:[UIImage imageNamed:@"zixun"] forState:UIControlStateNormal];
        _talkBtn.layer.masksToBounds = true;
        [_talkBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _talkBtn;
}


@end
