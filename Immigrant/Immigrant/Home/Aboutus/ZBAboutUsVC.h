//
//  ZBAboutUsVC.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/14.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ZBAboutUsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIImageView *topBigImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomBigImageView;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;

//右边视图
@property (strong, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIImageView *rightTopImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *nameValueLab;
@property (weak, nonatomic) IBOutlet UILabel *lineOneLab;
@property (weak, nonatomic) IBOutlet UILabel *cardIDTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *cardIDValueLab;
@property (weak, nonatomic) IBOutlet UILabel *lineTwoLab;
@property (weak, nonatomic) IBOutlet UILabel *emailTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *emailValueLab;
@property (weak, nonatomic) IBOutlet UILabel *lineThreeLab;
@property (weak, nonatomic) IBOutlet UILabel *uploadResumeTitleLab;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadResume;
@property (weak, nonatomic) IBOutlet UIImageView *rightBottomImgView;
@property (weak, nonatomic) IBOutlet UIView *topContentView;

@end
