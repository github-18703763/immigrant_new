//
//  ZBAboutUsVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/14.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBAboutUsVC.h"
#import "FSSegmentTitleView.h"
@interface ZBAboutUsVC ()<FSSegmentTitleViewDelegate,UITextViewDelegate>

@property (nonatomic,strong) FSSegmentTitleView *titleView;
@property(nonatomic,strong) NSMutableArray *segmentTitlesArr;
@end

@implementation ZBAboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于我们";
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor grayColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    self.titleView.selectIndex = 0;
    [self.view addSubview:self.titleView];
    
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self createFrame];

}
//修改界面的frame
- (void)createFrame{
    //关于我们
    _leftView.backgroundColor = [UIColor redColor];
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.titleView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(0);
    }];
    [self.topBigImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.titleView.mas_bottom).offset(0);
        make.height.mas_equalTo(300);
    }];
//    、、399
    [self.bottomBigImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.topBigImageView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(0);
    }];
    self.detailLab.text = @"邮箱：lzg@icebartech.com\n© 2016 ICEBARTECH.COM 版权所有 深圳市冰棍科技有限公司 粤ICP备16035793号-13333\n电话：400-9690-981 0755-86719430\n地址：深圳市南山区科技园科慧路沛鸿 大厦A2栋506";
    [self.detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(20);
         make.right.mas_equalTo(-20);
        make.top.equalTo(self.bottomBigImageView.mas_bottom).offset(-270);
        make.bottom.mas_equalTo(-30);
    }];
    //精英推荐
    _rightView.backgroundColor = [UIColor whiteColor];
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(SystemScreenWidth);
        
        make.top.equalTo(self.titleView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(0);
    }];
    [self.rightTopImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.titleView.mas_bottom).offset(0);
        make.height.mas_equalTo(320.0);
    }];
    self.topContentView.backgroundColor = [UIColor whiteColor];
    self.topContentView.layer.cornerRadius = 15.0;
    [self.topContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(24);
        make.right.mas_equalTo(-24);
        make.top.equalTo(self.rightTopImgView).offset(30);
        make.bottom.equalTo(self.rightTopImgView.mas_bottom).offset(-10);
    }];
    [self.nameTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(21);
        make.top.equalTo(self.topContentView).offset(28);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(17);
    }];
    [self.nameValueLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.topContentView).offset(-21);
        make.top.equalTo(self.topContentView).offset(28);
        make.left.equalTo(self.nameTitleLab.mas_right).offset(10);
        make.height.mas_equalTo(17);
    }];
    [self.lineOneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(4);
        make.right.mas_equalTo(-4);
        make.top.equalTo(self.nameTitleLab.mas_bottom).offset(20);
        make.height.mas_equalTo(1);
    }];
    [self.cardIDTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(21);
        make.top.equalTo(self.lineOneLab).offset(20);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(17);
    }];
    [self.cardIDValueLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.topContentView).offset(-21);
        make.top.equalTo(self.lineOneLab).offset(20);
        make.left.equalTo(self.nameTitleLab.mas_right).offset(10);
        make.height.mas_equalTo(17);
    }];
    [self.lineTwoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(4);
        make.right.mas_equalTo(-4);
        make.top.equalTo(self.cardIDValueLab.mas_bottom).offset(20);
        make.height.mas_equalTo(1);
    }];
    
    [self.emailTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(21);
        make.top.equalTo(self.lineTwoLab).offset(20);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(17);
    }];
    [self.emailValueLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.topContentView).offset(-21);
        make.top.equalTo(self.lineTwoLab).offset(20);
        make.left.equalTo(self.emailTitleLab.mas_right).offset(10);
        make.height.mas_equalTo(17);
    }];
    [self.lineThreeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(4);
        make.right.mas_equalTo(-4);
        make.top.equalTo(self.emailTitleLab.mas_bottom).offset(20);
        make.height.mas_equalTo(1);
    }];
    
    
    [self.uploadResumeTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(21);
        make.top.equalTo(self.lineThreeLab).offset(20);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(17);
    }];
    [self.btnUploadResume mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.topContentView).offset(-21);
        make.top.equalTo(self.lineThreeLab).offset(20);
        make.width.height.mas_equalTo(30);
    }];
    
    [self.rightBottomImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.rightTopImgView.mas_bottom).offset(-20);
        make.bottom.mas_equalTo(10);
    }];
}
-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSMutableArray*)segmentTitlesArr{
    
    if (!_segmentTitlesArr) {
        _segmentTitlesArr = [[NSMutableArray alloc]initWithArray:@[@"关于我们",@"精英推荐"]];
    }
    
    return _segmentTitlesArr;
}

#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    if (endIndex==1) {

        
        [self.leftView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(-SystemScreenWidth);
        }];
        [self.rightView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(0);
        }];
    }
    else{
        [self.leftView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(0);
        }];
        [self.rightView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(SystemScreenWidth);
        }];
    }
}

@end
