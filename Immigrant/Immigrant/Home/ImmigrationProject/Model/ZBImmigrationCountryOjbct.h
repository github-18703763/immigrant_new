//
//  ZBImmigrationCountryOjbct.h
//  Immigrant
//
//  Created by 张波 on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationCountryOjbct : NSObject

/**
 "chinesename": "英国",
 */
@property (nonatomic,copy) NSString  *chinesename;
/**
 "creator": "string",
 */
@property (nonatomic,copy) NSString  *creator;
/**
 "englishname": "Britain",
 */
@property (nonatomic,copy) NSString  *englishname;
/**
 "gmtCreated": "2019-01-04T08:46:13.631Z",
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
 "gmtModified": "2019-01-04T08:46:13.631Z",
 */
@property (nonatomic,copy) NSString  *gmtModified;
/**
 "icon": "img.png",
 */
@property (nonatomic,copy) NSString  *icon;
/**
 "iconkey": "img.png",
 */
@property (nonatomic,copy) NSString  *iconkey;
/**
 "id": 0,
 */
@property (nonatomic,copy) NSString  *id;
/**
 "isDeleted": "string",
 */
@property (nonatomic,copy) NSString  *isDeleted;
/**
 "modifier": "string"
 */
@property (nonatomic,copy) NSString  *modifier;

@end

NS_ASSUME_NONNULL_END
