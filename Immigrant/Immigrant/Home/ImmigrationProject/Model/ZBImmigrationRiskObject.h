//
//  ZBImmigrationRiskObject.h
//  Immigrant
//
//  Created by 张波 on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationRiskObject : NSObject

/**
 "content": "贫贱不能移",
 */
@property (nonatomic,copy) NSString  *content;
/**
"cover": "img.jpg",
*/
@property (nonatomic,copy) NSString  *cover;
/**
"coverkey": "img.jpg",
*/
@property (nonatomic,copy) NSString  *coverkey;
/**
"creator": "string",
*/
@property (nonatomic,copy) NSString  *creator;
/**
"gmtCreated": "2019-01-04T08:46:14.023Z",
*/
@property (nonatomic,copy) NSString  *gmtCreated;
/**
"gmtModified": "2019-01-04T08:46:14.023Z",
*/
@property (nonatomic,copy) NSString  *gmtModified;
/**
"id": 0,
*/
@property (nonatomic,copy) NSString  *id;
/**
"isDeleted": "string",
*/
@property (nonatomic,copy) NSString  *isDeleted;
/**
"modifier": "string",
*/
@property (nonatomic,copy) NSString  *modifier;
/**
"rate": 3.5,
*/
@property (nonatomic,copy) NSString  *rate;
/**
"title": "移民项目风险评估的重要性"
*/
@property (nonatomic,copy) NSString  *title;

@end

NS_ASSUME_NONNULL_END
