//
//  ZBImmigrationExampleModel.h
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBImmigrationExampleModel : NSObject

/**
 "content": "所需材料。。。",
 */
@property (nonatomic,copy) NSString  *content;
/**
 "cover": "img1.png",
 */
@property (nonatomic,copy) NSString  *cover;

/**
 "coverkey": "img1.png",
 */
@property (nonatomic,copy) NSString  *coverkey;

/**
 "creator": "string",
 */
@property (nonatomic,copy) NSString  *creator;

/**
 "gmtCreated": "2018-12-27T02:29:45.886Z",
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 "gmtModified": "2018-12-27T02:29:45.886Z",
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 "id": 0,
 */
@property (nonatomic,copy) NSString  *id;

/**
 "isDeleted": "string",
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 "modifier": "string",
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 "title": "对购房要求严苛的刘先生在美国选到中意房产，成功获得永居"
 */
@property (nonatomic,copy) NSString  *title;

@end
