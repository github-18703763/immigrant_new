//
//  MigrateQuestionModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MigrateQuestionModel : NSObject

@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, copy) NSString *optiona;
@property (nonatomic, assign) NSInteger              scored;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, copy) NSString *optionc;
@property (nonatomic, assign) NSInteger              scorec;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, assign) NSInteger              scoreb;
@property (nonatomic, copy) NSString *optionb;
@property (nonatomic, copy) NSString *question;
@property (nonatomic, assign) NSInteger              scorea;
@property (nonatomic, copy) NSString *optiond;

@end


