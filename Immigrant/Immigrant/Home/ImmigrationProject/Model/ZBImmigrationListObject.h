//
//  ZBImmigrationListObject.h
//  Immigrant
//
//  Created by 张波 on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationListObject : NSObject
/**
 "cardtype": "护照",
 */
@property (nonatomic,copy) NSString  *cardtype;
/**
 "country": "英国",
 */
@property (nonatomic,copy) NSString  *country;
/**
 "countryId": 0,
 */
@property (nonatomic,copy) NSString  *countryId;
/**
 "cover": "png1.jpg",
 */
@property (nonatomic,copy) NSString  *cover;
/**
 "creator": "string",
 */
@property (nonatomic,copy) NSString  *creator;
/**
 "depositfee": 12888900,
 */
@property (nonatomic,copy) NSString  *depositfee;
/**
 "depositfeeyuan": 128889,
 */
@property (nonatomic,copy) NSString  *depositfeeyuan;
/**
 "feature": "项目优势",
 */
@property (nonatomic,copy) NSString  *feature;
/**
 "fee": "费用详情",
 */
@property (nonatomic,copy) NSString  *fee;
/**
 "gmtCreated": "2019-01-04T08:46:14.059Z",
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
 "gmtModified": "2019-01-04T08:46:14.059Z",
 */
@property (nonatomic,copy) NSString  *gmtModified;
/**
 "id": 0,
 */
@property (nonatomic,copy) NSString  *id;
/**
 "images": "png1.jpg,png2.jpg",
 */
@property (nonatomic,copy) NSArray  *images;
/**
 "imageskey": "img1.png,img2.png",
 */
@property (nonatomic,strong) NSArray  *imageskey;
/**
 "info": "项目信息",
 */
@property (nonatomic,strong) NSDictionary  *info;
/**
 "investment": 23463000,
 */
@property (nonatomic,copy) NSString  *investment;
/**
 "investmentyuan": 234630,
 */
@property (nonatomic,copy) NSString  *investmentyuan;
/**
 "isDeleted": "string",
 */
@property (nonatomic,copy) NSString  *isDeleted;
/**
 "isactivity": false,
 */
@property (nonatomic,assign) BOOL  isactivity;
/**
 "isfavorites": false,
 */
@property (nonatomic,assign) BOOL  isfavorites;
/**
 "modifier": "string",
 */
@property (nonatomic,copy) NSString  *modifier;
/**
 "name": "美国探亲访友签证",
 */
@property (nonatomic,copy) NSString  *name;
/**
 "process": "申请流程",
 */
@property (nonatomic,copy) NSString  *process;
/**
 "questions": [
 {
 "creator": "string",
 "gmtCreated": "2019-01-04T08:46:14.059Z",
 "gmtModified": "2019-01-04T08:46:14.059Z",
 "id": 0,
 "isDeleted": "string",
 "modifier": "string",
 "optiona": "您最美",
 "optionb": "我最美",
 "optionc": "她最美",
 "optiond": "大家美",
 "question": "世界上谁最美",
 "scorea": 20,
 "scoreb": 0,
 "scorec": 30,
 "scored": 50
 }
 ],
 */
@property (nonatomic,strong) NSArray  *questions;
/**
 "require0": 0,
 */
@property (nonatomic,copy) NSString  *require0;
/**
 "require0fit": false,
 */
@property (nonatomic,assign) BOOL  require0fit;
/**
 "require1": 3,
 */
@property (nonatomic,copy) NSString  *require1;
/**
 "require1fit": false,
 */
@property (nonatomic,assign) BOOL  require1fit;
/**
 "require2": 3,
 */
@property (nonatomic,copy) NSString  *require2;
/**
 "require2fit": false,
 */
@property (nonatomic,assign) BOOL  require2fit;
/**
 "require3": 3,
 */
@property (nonatomic,copy) NSString  *require3;
/**
 "require3fit": false,
 */
@property (nonatomic,assign) BOOL require3fit;
/**
 "require4": 3,
 */
@property (nonatomic,copy) NSString  *require4;
/**
 "require4fit": false,
 */
@property (nonatomic,assign) BOOL  require4fit;
/**
 "require5": 3,
 */
@property (nonatomic,copy) NSString  *require5;
/**
 "require5fit": false,
 */
@property (nonatomic,assign) BOOL  require5fit;
/**
 "requirefitscore": 88,
 */
@property (nonatomic,copy) NSString  *requirefitscore;
/**
 "requires": "申请条件",
 */
@property (nonatomic,copy) NSString  *requires;
/**
 "residencerequires": "无居住要求",
 */
@property (nonatomic,copy) NSString  *residencerequires;
/**
 "servicefee": 12888900,
 */
@property (nonatomic,copy) NSString  *servicefee;
/**
 "servicefeeyuan": 128889,
 */
@property (nonatomic,copy) NSString  *servicefeeyuan;
/**
 "servietime": "30天左右"
 */
@property (nonatomic,copy) NSString  *servietime;
@end

NS_ASSUME_NONNULL_END
