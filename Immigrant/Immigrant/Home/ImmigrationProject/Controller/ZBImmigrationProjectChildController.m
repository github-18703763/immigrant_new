//
//  ZBImmigrationProjectChildController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationProjectChildController.h"
#import "ZBImmigrationProjectSectionOneHeadView.h"
#import "ZBImmigrationProjectSectionTwoHeadView.h"
#import "ZBImmigrationProjectOneCell.h"
#import "ZBImmigrationProjectTwoCell.h"
#import "ZBImmigrationProjectFooterView.h"
#import "ZBImmigrationExampleController.h"
#import "ZBImmigrationProjectFooterOneView.h"
#import "ZBImmigrationListObject.h"//上部实体
#import "ZBImmigrationRiskObject.h"//下部实体
#import "ZBImmigrationExamplePagingController.h"
#import "ZBImmigrationRiskPagingController.h"
#import "ZBImmigrationAssessmentController.h"
#import "ZBImmigrationAppealController.h"
#import "ZBImmigrationAssessmentDetailController.h"
#import "ZBImmrgrationListDetialController.h"
#import "MigrateDetailController.h"
#import "MigrateEvaluateController.h"
#import "MigrateSuccessListController.h"
@interface ZBImmigrationProjectChildController ()

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,strong) NSMutableArray  *projectArray;

@property (nonatomic,strong) NSMutableArray  *riskArray;

@end

@implementation ZBImmigrationProjectChildController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.titleStr;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBImmigrationProjectOneCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBImmigrationProjectTwoCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
    
    self.pageSize = 3;
    
//    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
//    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
//    self.tableView.mj_header = header;
//    
//    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
//    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
    [self loadDataForRisk];
    
}

-(void)loadDataForRisk{
    WS(weakSELF);
    [QSNetworkManager migrateRiskWithPage:1 withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.riskArray = [ZBImmigrationRiskObject mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            [weakSELF.tableView reloadData];
        }else{
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager migrateNameLinek:self.country.id withPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.projectArray = [ZBImmigrationListObject mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.projectArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.projectArray addObjectsFromArray:[ZBImmigrationListObject mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [weakSELF.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.projectArray.count;
    }else{
        return self.riskArray.count;
    }

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ZBImmigrationProjectOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.projectArray[indexPath.row];
        return cell;
    }else{
        ZBImmigrationProjectTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
         cell.model = self.riskArray[indexPath.row];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MigrateDetailController *vc = [[MigrateDetailController alloc] init];
        
        ZBImmigrationListObject *obj = self.projectArray[indexPath.row];
        vc.migrateId = [obj.id integerValue];
//        ZBImmrgrationListDetialController *detial = [ZBImmrgrationListDetialController new];
//        detial.model = self.projectArray[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 1) {
        ZBImmigrationAssessmentDetailController *detial = [ZBImmigrationAssessmentDetailController new];
        detial.model = self.riskArray[indexPath.row];
        [self.navigationController pushViewController:detial animated:YES];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    WS(weakSELF);
    if (section == 0) {
        ZBImmigrationProjectSectionOneHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationProjectSectionOneHeadView" owner:self options:nil] firstObject];
        headView.assessmentBlock = ^{
//            ZBLog(@"移民评估");
            MigrateEvaluateController *vc = [[MigrateEvaluateController alloc] init];
            [weakSELF.navigationController pushViewController:vc animated:true];
//            ZBImmigrationAssessmentController *assessment = [ZBImmigrationAssessmentController new];
//            [weakSELF.navigationController pushViewController:assessment animated:YES];
        };
        headView.successBlock = ^{
//            ZBLog(@"成功案列");
            MigrateSuccessListController *vc = [[MigrateSuccessListController alloc] init];
            [weakSELF.navigationController pushViewController:vc animated:YES];

//            ZBImmigrationExampleController *example = [ZBImmigrationExampleController new];
//            [weakSELF.navigationController pushViewController:example animated:YES];
        };
        return headView;
    }else{
        ZBImmigrationProjectSectionTwoHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationProjectSectionTwoHeadView" owner:self options:nil] firstObject];
        return headView;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 348.5;
    }else{
        return 221;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    WS(weakSELF);
    if (section == 0) {
        ZBImmigrationProjectFooterOneView *footer = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationProjectFooterOneView" owner:self options:nil] firstObject];
        footer.lookMoreBlock = ^{
            //查看更多
            ZBImmigrationExamplePagingController *immgration = [ZBImmigrationExamplePagingController new];
            immgration.country = weakSELF.country;
            [weakSELF.navigationController pushViewController:immgration animated:YES];
        };
        return footer;
    }else{
        ZBImmigrationProjectFooterView *footer = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationProjectFooterView" owner:self options:nil] firstObject];
        footer.lookMoreBlock = ^{
            //查看更多
            ZBImmigrationRiskPagingController *risk = [ZBImmigrationRiskPagingController new];
            [weakSELF.navigationController pushViewController:risk animated:YES];
        };
        footer.assessmentBlock = ^{
            ZBImmigrationAppealController *assessment = [ZBImmigrationAppealController new];
            [weakSELF.navigationController pushViewController:assessment animated:YES];
        };
        return footer;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 83;
    }else{
        return 134;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 40;
    }else{
        return 232;
    }
    
}

@end
