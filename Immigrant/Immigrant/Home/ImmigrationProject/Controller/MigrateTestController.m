//
//  MigrateTestController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateTestController.h"
#import "QSNetworkManager+order.h"
#import <YYModel/YYModel.h>
#import "MigrateQuestionModel.h"
#import "QuestionCell.h"
#import "StudyModel.h"
#import "QSNetworkManager+order.h"
#import <YYModel.h>
#import "StudySignupController.h"
#import "XRCarouselView.h"
#import "SingleLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import <WebKit/WebKit.h>
#import "PointWebContainCell.h"
#import "SGPagingView.h"
#import "WebContainCell.h"
#import "QSNetworkManager+Order.h"
#import "ZBConsultingView.h"
#import "ZBReturnVisitController.h"
#import "MigrateModel.h"
#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>
#import <MBProgressHUD.h>
#import "MigrateSignupController.h"
#import "MigrateSaleCell.h"
#import "ZBImmigrationConditionTestController.h"
#import "ZBImmigrationListObject.h"
#import "MigrateQuestionModel.h"
#import "ZBOnLineController.h"
#import "ZBImmigrationConditionTestResultController.h"
#import "TestResultController.h"
@interface MigrateTestController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *applyBtn;

@property (nonatomic, strong) UIView *applyShadowView;

@property (nonatomic, strong) UIView *headView;
@property (nonatomic, strong) UILabel *testNameHintLabel;
@property (nonatomic, strong) UILabel *testNameLabel;

@property (nonatomic, strong) NSArray<NSNumber *> *answerArray;

@end

@implementation MigrateTestController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.navigationController.navigationBar setBackgroundImage: [UIImage createImageWithColor:UIColor.whiteColor withRect:CGRectMake(0, 0, 1, 1)] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"条件测试";
    [self.view addSubview:self.applyShadowView];
    [self.view addSubview:self.applyBtn];
    
    self.tableView.mj_header.hidden = true;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self masLayout];
    [self fetchQuestion];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)masLayout {
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-46);
        make.left.equalTo(self.view).offset(66);
        make.right.equalTo(self.view).offset(-66);
        make.height.equalTo(@(50));
    }];
    
    [self.applyShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.applyBtn);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.applyBtn.mas_top);
    }];
}

- (void)fetchQuestion {
//    MigrateQuestionModel
    
    [QSNetworkManager migratequestionList:self.model.questionIds successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.tableData = [NSArray yy_modelArrayWithClass:[MigrateQuestionModel class] json:responseModel.data[@"bussData"]].mutableCopy;
//            [self configData];
            NSMutableArray *array = [NSMutableArray arrayWithCapacity:self.tableData.count];
            for (NSObject *obj in self.tableData) {
                [array addObject:@(-1)];
            }
            self.answerArray = array.mutableCopy;
            [self.tableView reloadData];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [self.tableView.mj_header endRefreshing];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];

}

#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    if (btn == self.applyBtn) {
        if ([self.answerArray containsObject:@(-1)]) {
            [MBProgressHUD showError:@"请选择选项"];
            return;
        } else {
            __block NSInteger point = 0;
            
            WS(weakSELF);
            [self.tableData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                MigrateQuestionModel *model = obj;
                NSInteger selectOption = [weakSELF.answerArray[idx] integerValue];
                switch (selectOption) {
                    case 0:
                        point += model.scorea;
                        break;
                    case 1:
                        point += model.scoreb;
                        break;
                    case 2:
                        point += model.scorec;
                        break;
                    case 3:
                        point += model.scored;
                        break;
                    default:
                        break;
                }
                
            }];
            TestResultController *vc = [[TestResultController alloc] init];
            vc.score = point;
            vc.name = self.model.name;
            [self.navigationController pushViewController:vc animated:true];
        }
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QuestionCell *cell = [QuestionCell cellForTableView:tableView];
    
    MigrateQuestionModel *model = self.tableData[indexPath.row];
    cell.rowNumber = 2;
    cell.questions = @[model.optiona,model.optionb,model.optionc,model.optiond];
    cell.questionLabel.text = model.question;
    WS(weakSELF);
//    __block weakSELF.answerArray;
    
    __block NSMutableArray *array = self.answerArray;
    cell.itemSelected = ^(NSInteger row) {
        array[indexPath.row] = @(row);
        weakSELF.answerArray = array;
    };
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}

#pragma mark - Getter

- (UIButton *)applyBtn {
    if (!_applyBtn) {
        _applyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_applyBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"签约办理" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_applyBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _applyBtn.layer.cornerRadius = 25;
        _applyBtn.layer.masksToBounds = true;
        [_applyBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _applyBtn;
}

- (UIView *)applyShadowView {
    if (!_applyShadowView) {
        _applyShadowView = [[UIView alloc] init];
        _applyShadowView.backgroundColor = UIColor.whiteColor;
        _applyShadowView.layer.cornerRadius = 25;
        _applyShadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _applyShadowView.layer.shadowRadius = 17.5;
        _applyShadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _applyShadowView.layer.shadowOpacity = 0.26;
    }
    return _applyShadowView;
}


@end
