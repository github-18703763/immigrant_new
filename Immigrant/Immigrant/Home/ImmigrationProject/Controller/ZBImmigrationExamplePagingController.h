//
//  ZBImmigrationPagingController.h
//  Immigrant
//
//  Created by 张波 on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationCountryOjbct.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationExamplePagingController : UITableViewController

@property (nonatomic,strong) ZBImmigrationCountryOjbct  *country;

@end

NS_ASSUME_NONNULL_END
