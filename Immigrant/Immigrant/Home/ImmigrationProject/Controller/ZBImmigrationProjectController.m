//
//  ZBImmigrationProjectController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationProjectController.h"
#import "FSScrollContentView.h"
#import "ZBImmigrationProjectChildController.h"
#import "ZBImmigrationCountryOjbct.h"

@interface ZBImmigrationProjectController ()<FSPageContentViewDelegate,FSSegmentTitleViewDelegate>

@property (nonatomic, strong) FSPageContentView *pageContentView;
@property (nonatomic, strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) NSMutableArray  *countryArray;

@end

@implementation ZBImmigrationProjectController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"移民项目";
    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
//    
    WS(weakSELF);
    [QSNetworkManager migrateContrysuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            weakSELF.countryArray = [ZBImmigrationCountryOjbct mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
            for (ZBImmigrationCountryOjbct *country in weakSELF.countryArray) {
                [array addObject:country.chinesename];
            }
            weakSELF.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:array delegate:weakSELF indicatorType:FSIndicatorTypeNone];
            weakSELF.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
            weakSELF.titleView.backgroundColor = [UIColor whiteColor];
            weakSELF.titleView.titleFont = [UIFont systemFontOfSize:16];
            weakSELF.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
            weakSELF.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
            weakSELF.titleView.selectIndex = 2;
            [weakSELF.view addSubview:weakSELF.titleView];
            
            NSMutableArray *childVCs = [[NSMutableArray alloc]init];
            for (NSInteger i = 0;i<weakSELF.countryArray.count;i++) {
                ZBImmigrationCountryOjbct *country = weakSELF.countryArray[i];
                ZBImmigrationProjectChildController *vc = [[ZBImmigrationProjectChildController alloc]init];
                vc.titleStr = country.chinesename;
                vc.country = country;
                [childVCs addObject:vc];
            }
            weakSELF.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 50, SystemScreenWidth, SystemScreenHeight-50) childVCs:childVCs parentVC:weakSELF delegate:weakSELF];
            weakSELF.pageContentView.contentViewCurrentIndex = 2;
            [weakSELF.view addSubview:weakSELF.pageContentView];
            
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
//    self.title = @[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"][endIndex];
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
//    self.title = @[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"][endIndex];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc{
//    if (self.backBlock) {
//        self.backBlock();
//    }
}

@end
