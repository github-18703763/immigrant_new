//
//  ZBImmigrationHandleOrOrderController.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationHandleOrOrderController.h"
#import "ZBImmigrationHandleHeadView.h"
#import "ZBImmigrationHandleFooterView.h"

@interface ZBImmigrationHandleOrOrderController ()

@end

@implementation ZBImmigrationHandleOrOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.type==IMGORDER) {
        self.title = @"移民订单";
    }else{
         self.title = @"移民办理";
    }
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0,SystemScreenWidth, 0, 20);
    self.tableView.tableFooterView = [UIView new];
    ZBImmigrationHandleHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationHandleHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 268);
    self.tableView.tableHeaderView = headView;
    ZBImmigrationHandleFooterView *footerView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationHandleFooterView" owner:self options:nil] firstObject];
    footerView.frame = CGRectMake(0, 0, SystemScreenWidth, 427);
    self.tableView.tableFooterView = footerView;
    
}



@end
