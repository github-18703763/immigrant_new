
//
//  ZBImmigrationAssessmentDetailController.m
//  Immigrant
//
//  Created by 张波 on 2019/1/7.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationAssessmentDetailController.h"

@interface ZBImmigrationAssessmentDetailController ()

@end

@implementation ZBImmigrationAssessmentDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评估详情";
    [self loadData];
    
}

-(void)loadData{
    [QSNetworkManager ImmigrationAssessmentDetail:self.model.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            
        }else{
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

@end
