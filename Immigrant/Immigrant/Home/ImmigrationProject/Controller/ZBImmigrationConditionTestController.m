//
//  ZBImmigrationConditionTestController.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationConditionTestController.h"
#import "ZBImmigrationConditionTestCell.h"

@interface ZBImmigrationConditionTestController ()

@end

@implementation ZBImmigrationConditionTestController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"条件测试";
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0,SystemScreenWidth, 0, 20);
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBImmigrationConditionTestCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.scrollEnabled = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBImmigrationConditionTestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.model;
    WS(weakSELF);
    cell.commitBlock = ^(NSArray * _Nonnull array) {
        NSDictionary *dict = @{@"answers":array,@"migrateId":self.model.id};
        [QSNetworkManager ImmigrationAdd:dict successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {
              
            }else{
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];
    };
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SystemScreenHeight-self.navigationController.navigationBar.bounds.size.height-StatuBarHeight;
}

@end
