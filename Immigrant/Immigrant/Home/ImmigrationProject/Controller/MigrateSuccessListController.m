//
//  MigrateSuccessListController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateSuccessListController.h"
#import "StudyModel.h"
#import <YYModel.h>
#import "ZBImmigrationExampleCell.h"
#import "ZBImmigrationExampleDetailController.h"
#import "ZBImmigrationExampleModel.h"
@interface MigrateSuccessListController ()

@end

@implementation MigrateSuccessListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView.mj_header beginRefreshing];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyApplyListCell" bundle:nil] forCellReuseIdentifier:@"ZBOverseasStudyApplyListCell"];
    self.title = @"成功案例";
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBImmigrationExampleCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.view.backgroundColor = UIColor.whiteColor;
}


#pragma mark: - Private Methods

- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh {
    if (isRefresh) {
        self.pageIndex = 1;
    }
    [QSNetworkManager migratesuccess:self.pageIndex withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            //            NSDictionary *dic = responseModel.data[@"bussData"];
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZBImmigrationExampleModel class] json:responseModel.data[@"bussData"]];
            [self handleResponseData:array isRefresh:isRefresh];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
    
}

#pragma mark: - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBImmigrationExampleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.tableData[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.separatorInset = UIEdgeInsetsMake(0, 24, 0, 24);
    if (indexPath.row == self.tableData.count - 1) {
        cell.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 136;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    StudyModel *model = self.tableData[indexPath.row];
//
//    StudyApplyDetailController *vc = [[StudyApplyDetailController alloc] init];
//    vc.studyId = model.id;
//    [self.navigationController pushViewController:vc animated:true];
    
    ZBImmigrationExampleDetailController *detial = [ZBImmigrationExampleDetailController new];
    detial.model = self.tableData[indexPath.row];
    [self.navigationController pushViewController:detial animated:YES];
}

@end
