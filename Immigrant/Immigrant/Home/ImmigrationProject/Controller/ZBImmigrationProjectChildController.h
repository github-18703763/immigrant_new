//
//  ZBImmigrationProjectChildController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationCountryOjbct.h"

@interface ZBImmigrationProjectChildController : UITableViewController

@property (nonatomic,copy) NSString *titleStr;
@property (nonatomic,strong) ZBImmigrationCountryOjbct  *country;

@end
