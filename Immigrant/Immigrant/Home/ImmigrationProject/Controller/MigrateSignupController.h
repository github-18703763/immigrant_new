//
//  MigrateSignupController.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MigrateModel;
NS_ASSUME_NONNULL_BEGIN

@interface MigrateSignupController : UIViewController

@property (nonatomic, strong) MigrateModel *model;

@end

NS_ASSUME_NONNULL_END
