//
//  ZBImmigrationExampleDetailController.m
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationExampleDetailController.h"
#import <WebKit/WebKit.h>

@interface ZBImmigrationExampleDetailController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UIWebView *contentWebView;

@end

@implementation ZBImmigrationExampleDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"成功案列";
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    wkWebConfig.userContentController = wkUController;
    // 自适应屏幕宽度js
    NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    // 添加js调用
    [wkUController addUserScript:wkUserScript];
    
    [self loadData];
    self.contentWebView.backgroundColor = [UIColor whiteColor];
    self.titleLab.text = self.model.title;
    self.timeLab.text =self.model.gmtCreated;
}

-(void)loadData{
    [QSNetworkManager migrateDetailSuccess:self.model.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            [self.contentWebView loadHTMLString:responseModel.data[@"bussData"][@"content"] baseURL:nil];
        }else{
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}


@end
