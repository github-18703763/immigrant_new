//
//  TestResultController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "TestResultController.h"
#import "ZPCAshapelGradientView.h"
#import "MQGradientProgressView.h"
#import "ZBConsultingView.h"
#import "ZBOnLineController.h"
#import "ZBReturnVisitController.h"
@interface TestResultController ()

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) ZPCAshapelGradientView *cycleView;
@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UILabel *scoreHintLabel;
@property (nonatomic, strong) UILabel *resultHintLabel;
@property (nonatomic, strong) UIButton *contactBtn;
@property (nonatomic, strong) UIView *shadowView;
@property (nonatomic, strong) UIImageView *backImgv;
@end

@implementation TestResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"条件测试";
    
    [self masLayout];
    self.cycleView.percent = self.score;
    self.view.backgroundColor = UIColor.whiteColor;
    
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.view addSubview:self.cycleView];
    [self.view addSubview:self.nameLabel];
    [self.view addSubview:self.scoreLabel];
    [self.view addSubview:self.scoreHintLabel];
    [self.view addSubview:self.resultHintLabel];
    [self.view addSubview:self.backImgv];
    [self.view addSubview:self.shadowView];
    [self.view addSubview:self.contactBtn];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(21.5);
        make.centerX.equalTo(self.view);
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.height.equalTo(@(50));
    }];
    
    [self.cycleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(26);
        make.centerX.equalTo(self.view);
        make.width.height.mas_equalTo(240);
    }];
    
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(192));
        make.center.equalTo(self.cycleView);
    }];

    [self.scoreHintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.scoreLabel).offset(-41);
    }];
    
    [self.resultHintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.cycleView.mas_bottom).offset(41);
    }];
    
    [self.backImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.resultHintLabel.mas_bottom).offset(35);
    }];
    
    [self.contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-42);
        make.centerX.equalTo(self.view);
        make.height.equalTo(@(50));
        make.width.equalTo(@(186));
    }];
    
    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contactBtn);
    }];
    
}

#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    ZBConsultingView *consulting = [[[NSBundle mainBundle] loadNibNamed:@"ZBConsultingView" owner:self options:nil] firstObject];
    consulting.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:consulting];
    consulting.phoneBlock = ^{
        //        ZBLog(@"电话咨询");
        NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"4007888798"];UIWebView * callWebview = [[UIWebView alloc] init];[callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
    };
    consulting.onlineBlock = ^{
        //        ZBLog(@"在线咨询");
        ZBOnLineController *onLine = [ZBOnLineController new];
        [self.navigationController pushViewController:onLine animated:YES];
    };
    consulting.visitiBlock = ^{
        //        ZBLog(@"要求回访");
        ZBReturnVisitController *visit = [[ZBReturnVisitController alloc] init];
        [self.navigationController pushViewController:visit animated:YES];
        
    };
}

#pragma mark - Getter

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.layer.cornerRadius = 25;
        _nameLabel.layer.masksToBounds = true;
        _nameLabel.backgroundColor = [UIColor getColor:@"c0c0c0"];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.text = [NSString stringWithFormat:@"测试项目：%@",self.name];
        _nameLabel.textColor = [UIColor getColor:@"7E7E7E"];
        _nameLabel.font = [UIFont systemFontOfSize:15];
    }
    return _nameLabel;
}

- (ZPCAshapelGradientView *)cycleView {
    if (!_cycleView) {
        _cycleView = [[ZPCAshapelGradientView alloc] initWithFrame:CGRectMake(98, 100, 240, 240)];
        _cycleView.transform = CGAffineTransformMakeRotation(-M_PI_2);
        
    }
    return _cycleView;
}

- (UILabel *)scoreLabel {
    if (!_scoreLabel) {
        _scoreLabel = [[UILabel alloc] init];
        _scoreLabel.layer.cornerRadius = 190.0 / 2;
        _scoreLabel.layer.masksToBounds = true;
        _scoreLabel.backgroundColor = [UIColor getColor:@"f4f7f9"];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        _scoreLabel.text = [NSString stringWithFormat:@"%@",@(self.score)];
        _scoreLabel.textColor = [UIColor blackColor];
        _scoreLabel.font = [UIFont systemFontOfSize:62 weight:UIFontWeightThin];
    }
    return _scoreLabel;
}

- (UILabel *)scoreHintLabel {
    if (!_scoreHintLabel) {
        _scoreHintLabel = [[UILabel alloc] init];
        _scoreHintLabel.text = @"匹配度";
        _scoreHintLabel.textColor = [UIColor getColor:@"7E7E7E"];
        _scoreHintLabel.font = [UIFont systemFontOfSize:12.77];
    }
    return _scoreLabel;
}

- (UILabel *)resultHintLabel {
    if (!_resultHintLabel) {
        _resultHintLabel = [[UILabel alloc] init];
        if (self.score >= 100) {
            _resultHintLabel.text = @"您的条件非常适合本项目";

        } else {
            _resultHintLabel.text = @"分数还差一点 联系客服帮你涨分";
        }
        _resultHintLabel.textColor = [UIColor getColor:@"7a7a7a"];
        _resultHintLabel.font = [UIFont systemFontOfSize:16];
    }
    return _resultHintLabel;
}

- (UIButton *)contactBtn {
    if (!_contactBtn) {
        _contactBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_contactBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        
        NSString *str;
        if (self.score >= 100) {
            str = @"立即联系客服办理";
        } else {
            str = @"联系客服";
        }
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_contactBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _contactBtn.layer.cornerRadius = 25;
        _contactBtn.layer.masksToBounds = true;
        [_contactBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _contactBtn;
}

- (UIView *)shadowView {
    if (!_shadowView) {
        _shadowView = [[UIView alloc] init];
        _shadowView.backgroundColor = UIColor.whiteColor;
        _shadowView.layer.cornerRadius = 25;
        _shadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _shadowView.layer.shadowRadius = 17.5;
        _shadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _shadowView.layer.shadowOpacity = 0.26;
    }
    return _shadowView;
}

- (UIImageView *)backImgv {
    if (!_backImgv) {
        _backImgv = [[UIImageView alloc] init];
        _backImgv.image = [UIImage imageNamed:@"ceshijieguo_bg"];
    }
    return _backImgv;
}


@end
