//
//  ZBImmrgrationListDetialController.h
//  Immigrant
//
//  Created by 张波 on 2019/1/7.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationListObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmrgrationListDetialController : UIViewController

@property (nonatomic,strong) ZBImmigrationListObject  *model;

@end

NS_ASSUME_NONNULL_END
