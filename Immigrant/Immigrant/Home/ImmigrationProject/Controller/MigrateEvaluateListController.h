//
//  MigrateEvaluateListController.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "ListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MigrateEvaluateListController : ListViewController

@property (nonatomic, copy) NSArray *array;

@end

NS_ASSUME_NONNULL_END
