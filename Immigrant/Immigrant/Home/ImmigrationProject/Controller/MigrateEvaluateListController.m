//
//  MigrateEvaluateListController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateEvaluateListController.h"
#import "QSNetworkManager+order.h"
#import <YYModel/YYModel.h>
#import "MigrateQuestionModel.h"
#import "QuestionCell.h"
#import "StudyModel.h"
#import "QSNetworkManager+order.h"
#import <YYModel.h>
#import "StudySignupController.h"
#import "XRCarouselView.h"
#import "SingleLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import <WebKit/WebKit.h>
#import "PointWebContainCell.h"
#import "SGPagingView.h"
#import "WebContainCell.h"
#import "QSNetworkManager+Order.h"
#import "ZBConsultingView.h"
#import "ZBReturnVisitController.h"
#import "MigrateModel.h"
#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>
#import <MBProgressHUD.h>
#import "MigrateSignupController.h"
#import "MigrateSaleCell.h"
#import "ZBImmigrationConditionTestController.h"
#import "ZBImmigrationListObject.h"
#import "MigrateQuestionModel.h"
#import "ZBOnLineController.h"
#import "ZBImmigrationConditionTestResultController.h"
#import "TestResultController.h"
#import "MigratePlanCell.h"
#import "ZBImmigrationCountryOjbct.h"
@interface MigrateEvaluateListController ()<UITableViewDataSource>

@end

@implementation MigrateEvaluateListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView.mj_header beginRefreshing];
    self.title = @"移民方案";
    self.tableView.rowHeight = 383;
    
}





- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh {
    
    [QSNetworkManager migrateFindlistByrequire:self.array successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.tableData = [NSArray yy_modelArrayWithClass:[MigrateModel class] json:responseModel.data[@"bussData"]].mutableCopy;
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [self.tableView.mj_header endRefreshing];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [self.tableView.mj_header endRefreshing];

    }];
    
    
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    MigratePlanCell *cell = [MigratePlanCell cellForTableView:tableView];
    cell.model = self.tableData[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}



@end
