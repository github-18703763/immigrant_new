//
//  ZBImmigrationInConsultingController.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationInConsultingController.h"
#import "ZBImmigrationHandleHeadView.h"
#import "ZBReturnVisitHead.h"
#import "ZBImmigrationInConsultingMsgCell.h"
#import "ZBReturnVisitCell.h"

@interface ZBImmigrationInConsultingController ()

@end

@implementation ZBImmigrationInConsultingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预约咨询";
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0,SystemScreenWidth, 0, 20);
    self.tableView.tableFooterView = [UIView new];
    ZBImmigrationHandleHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationHandleHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 268);
    self.tableView.tableHeaderView = headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBImmigrationInConsultingMsgCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBReturnVisitCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else{
        return 2;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ZBImmigrationInConsultingMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        ZBReturnVisitCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
  
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 160;
    }else{
        return 120;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }else{
        ZBReturnVisitHead *head = [[[NSBundle mainBundle] loadNibNamed:@"ZBReturnVisitHead" owner:self options:nil] firstObject];
        head.frame = CGRectMake(0, 0, SystemScreenWidth, 102);
        return head;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.01;
    }else{
         return 157;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 0.01;
    }else{
        return 120;
    }
}

@end
