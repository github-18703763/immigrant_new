//
//  MigrateDetailController.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "ListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MigrateDetailController : ListViewController

@property (nonatomic, assign) NSInteger migrateId;

@end

NS_ASSUME_NONNULL_END
