//
//  MigrateEvaluateController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateEvaluateController.h"
#import "QSNetworkManager+order.h"
#import <YYModel/YYModel.h>
#import "MigrateQuestionModel.h"
#import "QuestionCell.h"
#import "StudyModel.h"
#import "QSNetworkManager+order.h"
#import <YYModel.h>
#import "StudySignupController.h"
#import "XRCarouselView.h"
#import "SingleLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import <WebKit/WebKit.h>
#import "PointWebContainCell.h"
#import "SGPagingView.h"
#import "WebContainCell.h"
#import "QSNetworkManager+Order.h"
#import "ZBConsultingView.h"
#import "ZBReturnVisitController.h"
#import "MigrateModel.h"
#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>
#import <MBProgressHUD.h>
#import "MigrateSignupController.h"
#import "MigrateSaleCell.h"
#import "ZBImmigrationConditionTestController.h"
#import "ZBImmigrationListObject.h"
#import "MigrateQuestionModel.h"
#import "ZBOnLineController.h"
#import "ZBImmigrationConditionTestResultController.h"
#import "TestResultController.h"
#import "ZBImmigrationCountryOjbct.h"
#import "MigrateEvaluateListController.h"
@interface MigrateEvaluateController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *applyBtn;

@property (nonatomic, strong) UIView *applyShadowView;

@property (nonatomic, strong) UIView *headView;
@property (nonatomic, strong) UILabel *testNameHintLabel;
@property (nonatomic, strong) UILabel *testNameLabel;

@property (nonatomic, strong) NSArray<NSNumber *> *answerArray;
@property (nonatomic, strong) NSArray<NSString *> *titleArray;

@property (nonatomic, copy) NSArray<NSString *> *require0OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require1OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require2OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require3OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require4OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require5OptionArray;

@property (nonatomic, copy) NSArray<ZBImmigrationCountryOjbct *> *countryArray;

@end

@implementation MigrateEvaluateController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.navigationController.navigationBar setBackgroundImage: [UIImage createImageWithColor:UIColor.whiteColor withRect:CGRectMake(0, 0, 1, 1)] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"移民评估";
    [self.view addSubview:self.applyShadowView];
    [self.view addSubview:self.applyBtn];
    
    self.tableView.mj_header.hidden = true;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self masLayout];
    [self fetchCountry];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self configData];
    self.view.backgroundColor = UIColor.whiteColor;
    self.tableView.backgroundColor = UIColor.clearColor;
}

- (void)configData {
    self.answerArray = @[@(-1),@(-1),@(-1),@(-1),@(-1),@(-1),].mutableCopy;
    self.titleArray = @[@"1.您想去的国家？",
                        @"2.您为什么想移民？",
                        @"3.您的移民资产？",
                        @"4.您可接受的居住时间？",
                        @"5.您的最高学历？",
                        @"6.您的英语能力？",
                        ];
    self.require0OptionArray = @[];
    
    self.require1OptionArray = @[@"子女教育",
                                 @"海外生育",
                                 @"养老储蓄",
                                 @"出行便利",
                                 @"海外置业",
                                 @"投资理财",
                                 @"旅游度假",
                                 @"税务筹划",
                                 ];
    
    self.require2OptionArray = @[@"100万以内",
                                 @"100万-300万",
                                 @"300万-500万",
                                 @"500万-1000万",
                                 @"1000万-3000万",
                                 @"3000万以上",
                                 ];
    
    self.require3OptionArray = @[@"不方便居住",
                                 @"每年入境1次",
                                 @"每年住7天",
                                 @"每年住30天",
                                 @"每年住半年",
                                 @"累计住满2年",
                                 ];
    
    self.require4OptionArray = @[@"高中以下",
                                 @"高中或中专",
                                 @"大专",
                                 @"本科或硕士",
                                 @"博士",
                                 ];
    
    self.require5OptionArray = @[@"完全不会",
                                 @"大学英语4级及以下",
                                 @"大学英语6级",
                                 @"大学英语6级优秀",
                                 @"专业英语8级",
                                 ];
    [self.tableView reloadData];
}

- (void)masLayout {
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-46);
        make.left.equalTo(self.view).offset(66);
        make.right.equalTo(self.view).offset(-66);
        make.height.equalTo(@(50));
    }];
    
    [self.applyShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.applyBtn);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.applyBtn.mas_top);
    }];
}

- (void)fetchCountry {
    //    MigrateQuestionModel
    [QSNetworkManager migrateContrysuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.countryArray = [NSArray yy_modelArrayWithClass:[ZBImmigrationCountryOjbct class] json:responseModel.data[@"bussData"]];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    if (btn == self.applyBtn) {
        if ([self.answerArray containsObject:@(-1)]) {
            [MBProgressHUD showError:@"请选择选项"];
            return;
        } else {
            
            NSMutableArray *array = self.answerArray;
            
            array[0] = @([self.countryArray[[self.answerArray[0] integerValue]].id integerValue]);
            
            MigrateEvaluateListController *vc = [[MigrateEvaluateListController alloc] init];
            vc.array = array;
            [self.navigationController pushViewController:vc animated:true];
        }
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QuestionCell *cell = [QuestionCell cellForTableView:tableView reuserIdentifier:[NSString stringWithFormat:@"QuestionCell%@",@(indexPath.row)]];
    
    cell.questionLabel.text = self.titleArray[indexPath.row];
    WS(weakSELF);
    //    __block weakSELF.answerArray;
    
    __block NSMutableArray *array = self.answerArray;
    cell.itemSelected = ^(NSInteger row) {
        array[indexPath.row] = @(row);
        weakSELF.answerArray = array;
        NSLog(@"%@",array);
    };
    
    switch (indexPath.row) {
        case 0:
            cell.rowNumber = 3;
            cell.questions = self.require0OptionArray;
            break;
        case 1:
            cell.rowNumber = 3;
            cell.questions = self.require1OptionArray;
            break;
        case 2:
            cell.rowNumber = 2;
            cell.questions = self.require2OptionArray;
            break;
        case 3:
            cell.rowNumber = 2;
            cell.questions = self.require3OptionArray;
            break;
        case 4:
            cell.rowNumber = 2;
            cell.questions = self.require4OptionArray;
            break;
        case 5:
            cell.rowNumber = 2;
            cell.questions = self.require5OptionArray;
            break;

        default:
            break;
    }
    
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

#pragma mark - Getter

- (UIButton *)applyBtn {
    if (!_applyBtn) {
        _applyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_applyBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"生产移民方案" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_applyBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _applyBtn.layer.cornerRadius = 25;
        _applyBtn.layer.masksToBounds = true;
        [_applyBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _applyBtn;
}

- (UIView *)applyShadowView {
    if (!_applyShadowView) {
        _applyShadowView = [[UIView alloc] init];
        _applyShadowView.backgroundColor = UIColor.whiteColor;
        _applyShadowView.layer.cornerRadius = 25;
        _applyShadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _applyShadowView.layer.shadowRadius = 17.5;
        _applyShadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _applyShadowView.layer.shadowOpacity = 0.26;
    }
    return _applyShadowView;
}

- (void)setCountryArray:(NSArray<ZBImmigrationCountryOjbct *> *)countryArray {
    _countryArray = countryArray;
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:countryArray.count];
    for (ZBImmigrationCountryOjbct *obj in countryArray) {
        [array addObject:obj.chinesename];
    }
    self.require0OptionArray = array.copy;
}

@end
