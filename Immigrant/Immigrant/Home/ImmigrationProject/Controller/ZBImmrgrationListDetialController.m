//
//  ZBImmrgrationListDetialController.m
//  Immigrant
//
//  Created by 张波 on 2019/1/7.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmrgrationListDetialController.h"
#import "ZBImmrgrationListDetialHeadView.h"
#import "ZBImmigrationHandleOrOrderController.h"
#import "ZBImmigrationConditionTestController.h"
#import "MigrateModel.h"
#import <YYModel.h>
#import "MigrateSignupController.h"

@interface ZBImmrgrationListDetialController ()

@property (weak, nonatomic) IBOutlet UIButton *handleBtn;
@property (weak, nonatomic) IBOutlet UITableView *handleTable;
@property (nonatomic, strong) MigrateModel *detailModel;
@end

@implementation ZBImmrgrationListDetialController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"移民详情";
    self.navigationController.navigationBar.hidden = NO;
    self.handleBtn.layer.cornerRadius = 25;
    self.handleBtn.layer.masksToBounds = YES;
    self.handleTable.showsHorizontalScrollIndicator = NO;
    self.handleTable.showsVerticalScrollIndicator = NO;
    self.handleTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0   );
    self.handleTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    ZBImmrgrationListDetialHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmrgrationListDetialHeadView" owner:self options:nil] firstObject];
    WS(weakSELF);
    headView.conditionTestBlock = ^{
        ZBImmigrationConditionTestController *test = [ZBImmigrationConditionTestController new];
        test.model = weakSELF.model;
        [weakSELF.navigationController pushViewController:test animated:YES];
    };
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 300);
    self.handleTable.tableHeaderView = headView;
    
    [self loadData];
    
}

-(void)loadData{
    [QSNetworkManager migrateDetial:self.model.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.detailModel = [MigrateModel yy_modelWithJSON:responseModel.data[@"bussData"]];
        } else {
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

- (IBAction)handleAction:(id)sender {
//    ZBImmigrationHandleOrOrderController *handle = [ZBImmigrationHandleOrOrderController new];
    if (self.detailModel) {
        MigrateSignupController *vc = [[MigrateSignupController alloc] init];
        vc.model = self.detailModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
