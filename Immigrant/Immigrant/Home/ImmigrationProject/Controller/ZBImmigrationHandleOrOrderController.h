//
//  ZBImmigrationHandleOrOrderController.h
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    IMGORDER=0,
    IMHANDLE=1,
} ImmigrationType;

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationHandleOrOrderController : UITableViewController

@property (nonatomic,assign) ImmigrationType  type;

@end

NS_ASSUME_NONNULL_END
