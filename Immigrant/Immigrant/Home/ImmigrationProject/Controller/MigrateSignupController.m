//
//  MigrateSignupController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateSignupController.h"
#import "NumberCountView.h"
#import "UITableViewCell+identifier.h"
#import "TextFieldCell.h"
#import "CampInfoCell.h"
#import "NumberCountCell.h"
#import "PointLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "TwoLabelCell.h"
#import "OrderCampAddModel.h"
#import "QSNetworkManager+order.h"
#import "HouseOrderController.h"
#import "HouseModel.h"
#import "OrderHouseAddModel.h"
#import "HouseInfoCell.h"
#import "OrderMigrateAddModel.h"
#import "LineOrderController.h"
#import "MigrateModel.h"
#import "ZBImmigrationHandleHeadView.h"
#import "MigrateOrderController.h"
@interface MigrateSignupController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *table;

@property (nonatomic, strong) UIButton *confirmBtn;

@property (nonatomic, strong) UIView *shadowView;

@property (nonatomic, strong) NSArray<NSString *> *titleArray;

@property (nonatomic, strong) OrderMigrateAddModel *addModel;

@property (nonatomic, strong) UILabel *remarkLabel;
@end

@implementation MigrateSignupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"移民办理";
    [self masLayout];
    self.view.backgroundColor = UIColor.whiteColor;
    
    ZBImmigrationHandleHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBImmigrationHandleHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 268);
    
    [headView configView:self.model];
    self.table.tableHeaderView = headView;
    
    self.titleArray = @[@"姓名",
                        @"电子邮箱",
                        @"手机号",
                        @"移民定金",
                        ];
    
    self.addModel = [[OrderMigrateAddModel alloc] init];
}

- (void)masLayout {
    
    [self.view addSubview:self.table];
    [self.view addSubview:self.shadowView];
    [self.view addSubview:self.confirmBtn];
    [self.view addSubview:self.remarkLabel];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(214);
        make.bottom.equalTo(self.view).offset(-51);
        make.centerX.equalTo(self.view);
    }];
    
    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.confirmBtn);
    }];
    
    [self.table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.confirmBtn.mas_top);
    }];
    
    [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.confirmBtn.mas_top).offset(-9);
    }];
}

#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    if (btn == self.confirmBtn) {
        
        if (self.addModel.userName.length <= 0) {
            [MBProgressHUD showError:@"请输入姓名"];
            return;
        }
        
        if (self.addModel.userMobile.length <= 0) {
            [MBProgressHUD showError:@"请输入手机号"];
            return;
        }
        
        if (self.addModel.userEmail.length <= 0) {
            [MBProgressHUD showError:@"请输入电子邮箱"];
            return;
        }
        
        self.addModel.migrateId = self.model.id;
        
        [QSNetworkManager addMigrateOrder:self.addModel successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {//成功
                MigrateOrderController *vc = [[MigrateOrderController alloc] init];
                vc.orderId = [responseModel.data[@"bussData"] stringValue];
                [self.navigationController pushViewController:vc animated:true];
            } else {//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];
    }
}

- (void)textChanged:(UITextField *)textField {
    
    TextFieldCell *nameCell = (TextFieldCell *)[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    TextFieldCell *mailCell = (TextFieldCell *)[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    TextFieldCell *mobileCell = (TextFieldCell *)[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
    if (textField == nameCell.textField) {
        self.addModel.userName = textField.text;
    } else if (textField == mobileCell.textField) {
        self.addModel.userMobile = textField.text;
    } else if (textField == mailCell.textField) {
        self.addModel.userEmail = textField.text;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return self.titleArray.count;
        default:
            return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        PointLabelCell *cell = [PointLabelCell cellForTableView:tableView];
        cell.leftLabel.text = @"移民信息";
        return cell;
    } else if (indexPath.section == 1) {
        
        if (indexPath.row == 3) {
            TwoLabelCell *cell = [TwoLabelCell cellForTableView:tableView];
            cell.leftLabel.text = self.titleArray[indexPath.row];
            cell.leftLabel.textColor = [UIColor getColor:@"848484"];
            cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
            
            cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", @(self.model.depositfeeyuan)];
            cell.rightLabel.font = [UIFont boldSystemFontOfSize:20];
            
            return cell;
        } else {
            TextFieldCell *cell = [TextFieldCell cellForTableView:tableView];
            cell.leftLabel.text = self.titleArray[indexPath.row];
            cell.textField.placeholder = [NSString stringWithFormat:@"请输入%@", self.titleArray[indexPath.row]];
            [cell setBottomSeparatorInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
            
            [cell.textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
            
            return cell;
        }
    }
    return [UITableViewCell cellForTableView:tableView];
}


#pragma mark - Getter

- (UITableView *)table {
    if (!_table) {
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _table.delegate = self;
        _table.dataSource = self;
        _table.tableFooterView = [[UIView alloc] init];
        _table.estimatedRowHeight = 60;
        _table.rowHeight = UITableViewAutomaticDimension;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}

- (UIButton *)confirmBtn {
    if (!_confirmBtn) {
        _confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"去支付定金" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_confirmBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _confirmBtn.layer.cornerRadius = 25;
        _confirmBtn.layer.masksToBounds = true;
        [_confirmBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmBtn;
}

- (UIView *)shadowView {
    if (!_shadowView) {
        _shadowView = [[UIView alloc] init];
        _shadowView.backgroundColor = UIColor.whiteColor;
        _shadowView.layer.cornerRadius = 25;
        _shadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _shadowView.layer.shadowRadius = M_PI_2;
        _shadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _shadowView.layer.shadowOpacity = 0.26;
    }
    return _shadowView;
}

- (UILabel *)remarkLabel {
    if (!_remarkLabel) {
        _remarkLabel = [[UILabel alloc] init];
        _remarkLabel.text = @"注：此定金不退，签单可抵扣";
        _remarkLabel.font = [UIFont systemFontOfSize:13.44];
        _remarkLabel.textColor = [UIColor getColor:@"D0D0D0"];
    }
    return _remarkLabel;
}

@end
