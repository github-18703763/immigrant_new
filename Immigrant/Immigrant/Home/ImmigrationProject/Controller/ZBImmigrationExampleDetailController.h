//
//  ZBImmigrationExampleDetailController.h
//  Immigrant
//
//  Created by 张波 on 2019/2/19.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationExampleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationExampleDetailController : UIViewController

@property (nonatomic,strong) ZBImmigrationExampleModel  *model;

@end

NS_ASSUME_NONNULL_END
