//
//  ZBImmigrationHandleHeadView.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationHandleHeadView.h"
#import "MigrateModel.h"

@interface ZBImmigrationHandleHeadView ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

/**
 国家地区
 */
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;

/**
 办理周期
 */
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeLabel;

/**
 身份类型
 */
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLabel;

/**
 投资金额
 */
@property (weak, nonatomic) IBOutlet UILabel *investmentLabel;

/**
 居住要求
 */
@property (weak, nonatomic) IBOutlet UILabel *livingRequireLabel;

@end

@implementation ZBImmigrationHandleHeadView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)configView: (id)model {
    if ([model isKindOfClass:[MigrateModel class]]) {
        
        MigrateModel *obj = model;
        self.areaLabel.text = obj.country;
        self.serviceTimeLabel.text = obj.servietime;
        self.cardTypeLabel.text = obj.cardtype;
        self.investmentLabel.text = [NSString stringWithFormat:@"%@万美元", @(obj.investmentyuan / 10000)];
        self.livingRequireLabel.text = obj.residencerequires;
    }
}

@end
