//
//  ZBImmigrationProjectTwoCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationRiskObject.h"

@interface ZBImmigrationProjectTwoCell : UITableViewCell

@property (nonatomic,strong) ZBImmigrationRiskObject  *model;

@end
