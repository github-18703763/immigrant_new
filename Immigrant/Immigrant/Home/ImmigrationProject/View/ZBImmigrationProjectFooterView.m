//
//  ZBImmigrationProjectFooterView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationProjectFooterView.h"

@interface ZBImmigrationProjectFooterView()
@property (weak, nonatomic) IBOutlet UIButton *assessmentBtn;

@end

@implementation ZBImmigrationProjectFooterView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.assessmentBtn.layer.cornerRadius = 25;
    self.assessmentBtn.layer.masksToBounds = YES;
    self.assessmentBtn.layer.borderColor = [UIColor getColor:@"2585F8"].CGColor;
    self.assessmentBtn.layer.borderWidth = 3;
}
- (IBAction)assessmentAction:(id)sender {
//    ZBLog(@"我有项目要评估");
    if (self.assessmentBlock) {
        self.assessmentBlock();
    }
}


- (IBAction)lookMoreAction:(id)sender {
    if (self.lookMoreBlock) {
        self.lookMoreBlock();
    }
}

@end
