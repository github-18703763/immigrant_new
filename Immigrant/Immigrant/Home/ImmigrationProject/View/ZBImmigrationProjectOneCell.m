//
//  ZBImmigrationProjectOneCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationProjectOneCell.h"

@interface ZBImmigrationProjectOneCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *weekView;
@property (weak, nonatomic) IBOutlet UIView *moneyView;

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *serviceLab;
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLab;
@property (weak, nonatomic) IBOutlet UILabel *weekLab;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;

@end

@implementation ZBImmigrationProjectOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerView.layer.cornerRadius = 14;
    self.containerView.layer.masksToBounds = YES;
    self.weekView.layer.cornerRadius = 15.5;
    self.weekView.layer.masksToBounds = YES;
    self.moneyView.layer.cornerRadius = 15.5;
    self.moneyView.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 13;
    self.photoImg.layer.masksToBounds = YES;
    
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
    effectView.frame = self.containerView.bounds;
    [self.containerView addSubview:effectView];
//    [self.containerView insertSubview:effectView belowSubview:self.weekView];
    [self.containerView sendSubviewToBack:effectView];
//    [self bringSubviewToFront:self.contentView];
//    [self.containerView addSubview:effectView];
    self.containerView.backgroundColor = [UIColor getColor:@"c7c7c7" alpha:0.38];
}

-(void)setModel:(ZBImmigrationListObject *)model{
    _model = model;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.titleLab.text = _model.name;
    CGFloat money = [_model.servicefeeyuan floatValue]/10000.0;
    self.serviceLab.text = [NSString stringWithFormat:@"¥%@万/服务费",[NSNumber numberWithFloat:money]];
    self.cardTypeLab.text = [NSString stringWithFormat:@"身份类型：%@",_model.cardtype];
    self.weekLab.text = [NSString stringWithFormat:@"办理周期：%@",_model.servietime];
    CGFloat invest = [_model.investmentyuan floatValue]/10000.0;
    self.moneyLab.text = [NSString stringWithFormat:@"投资金额：%@万美元",[NSNumber numberWithFloat:invest]];
}
- (void)setModelMigrate:(ZBMigrateDTOModel *)modelMigrate{
    _modelMigrate = modelMigrate;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelMigrate.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.titleLab.text = _modelMigrate.name;
    CGFloat money = [_modelMigrate.servicefeeyuan floatValue]/10000.0;
    self.serviceLab.text = [NSString stringWithFormat:@"¥%@万/服务费",[NSNumber numberWithFloat:money]];
    self.cardTypeLab.text = [NSString stringWithFormat:@"身份类型：%@",_modelMigrate.cardtype];
    self.weekLab.text = [NSString stringWithFormat:@"办理周期：%@",_modelMigrate.servietime];
    CGFloat invest = [_modelMigrate.investmentyuan floatValue]/10000.0;
    self.moneyLab.text = [NSString stringWithFormat:@"投资金额：%@万美元",[NSNumber numberWithFloat:invest]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
