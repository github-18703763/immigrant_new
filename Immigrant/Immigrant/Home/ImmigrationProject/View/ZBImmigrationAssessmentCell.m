//
//  ZBImmigrationAssessmentCell.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationAssessmentCell.h"

@interface ZBImmigrationAssessmentCell ()
    
    @property (weak, nonatomic) IBOutlet UIButton *ageItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *ageItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *ageItemThree;
    @property (weak, nonatomic) IBOutlet UIButton *ageItemFour;
    @property (nonatomic,strong) NSArray  *ageArray;
    @property (nonatomic,assign) NSInteger  ageIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *countryItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemThree;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemFour;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemFive;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemSix;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemSeven;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemEight;
    @property (weak, nonatomic) IBOutlet UIButton *countryItemNight;
    @property (nonatomic,strong) NSArray  *countryArray;
    @property (nonatomic,assign) NSInteger  countryIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *whyItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemThree;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemFour;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemFive;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemSix;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemSeven;
    @property (weak, nonatomic) IBOutlet UIButton *whyItemEight;
    @property (nonatomic,strong) NSArray  *whyArray;
    @property (nonatomic,assign) NSInteger  whyIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *assetsItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *assetsItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *assetsItemThree;
    @property (weak, nonatomic) IBOutlet UIButton *assetsItemFour;
    @property (weak, nonatomic) IBOutlet UIButton *assetsItemFive;
    @property (weak, nonatomic) IBOutlet UIButton *assetsItemFix;
     @property (nonatomic,strong) NSArray  *assetsArray;
    @property (nonatomic,assign) NSInteger  assetsIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *acceptItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *accepItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *accepItemThree;
    @property (weak, nonatomic) IBOutlet UIButton *accepItemFour;
    @property (weak, nonatomic) IBOutlet UIButton *accepItemFive;
    @property (weak, nonatomic) IBOutlet UIButton *accepItemSix;
    @property (nonatomic,strong) NSArray  *accepArray;
    @property (nonatomic,assign) NSInteger  accepIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *schoolItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *schoolItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *schoolItemThree;
    @property (weak, nonatomic) IBOutlet UIButton *schoolItemFour;
    @property (weak, nonatomic) IBOutlet UIButton *schoolItemFive;
    @property (nonatomic,strong) NSArray  *schoolArray;
    @property (nonatomic,assign) NSInteger  schoolIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *englishItemOne;
    @property (weak, nonatomic) IBOutlet UIButton *englishItemTwo;
    @property (weak, nonatomic) IBOutlet UIButton *englishItemThee;
    @property (weak, nonatomic) IBOutlet UIButton *englishItemFour;
    @property (weak, nonatomic) IBOutlet UIButton *englishItemFive;
    @property (nonatomic,strong) NSArray  *englishArray;
    @property (nonatomic,assign) NSInteger  englishIndex;
    
    @property (weak, nonatomic) IBOutlet UIButton *filishBtn;
    
@end

@implementation ZBImmigrationAssessmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //选中 267BFF背景色  字体白色
    //未选中 EDEDF0  4A4A4A
    self.ageArray = @[self.ageItemOne,self.ageItemTwo,self.ageItemThree,self.ageItemFour];
    for (UIButton *btn in self.ageArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.countryArray = @[self.countryItemOne,self.countryItemTwo,self.countryItemThree,self.countryItemFour,self.countryItemFive,self.countryItemSix,self.countryItemSeven,self.countryItemEight,self.countryItemNight];
    for (UIButton *btn in self.countryArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.whyArray = @[self.whyItemOne,self.whyItemTwo,self.whyItemThree,self.whyItemFour,self.whyItemFive,self.whyItemSix,self.whyItemSeven,self.whyItemEight];
    for (UIButton *btn in self.whyArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.assetsArray = @[self.assetsItemOne,self.assetsItemTwo,self.assetsItemThree,self.assetsItemFour,self.assetsItemFive,self.assetsItemFix];
    for (UIButton *btn in self.assetsArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.accepArray = @[self.acceptItemOne,self.accepItemTwo,self.accepItemThree,self.accepItemFour,self.accepItemFive,self.accepItemSix];
    for (UIButton *btn in self.accepArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.schoolArray = @[self.schoolItemOne,self.schoolItemTwo,self.schoolItemThree,self.schoolItemFour,self.schoolItemFive];
    for (UIButton *btn in self.schoolArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.englishArray = @[self.englishItemOne,self.englishItemTwo,self.englishItemThee,self.englishItemFour,self.englishItemFive];
    for (UIButton *btn in self.englishArray) {
        btn.layer.cornerRadius = 19;
        btn.layer.masksToBounds = YES;
    }
    self.filishBtn.layer.cornerRadius = 25;
    self.filishBtn.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
    
- (IBAction)ageAction:(UIButton *)sender {
    
    for (NSInteger i=0; i<self.ageArray.count; i++) {
        UIButton *btn = self.ageArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.ageIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
    
}
    
- (IBAction)countryAction:(UIButton *)sender {
    
    for (NSInteger i=0; i<self.countryArray.count; i++) {
        UIButton *btn = self.countryArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.countryIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
    
}
    
    
- (IBAction)whyAction:(UIButton *)sender {
    
    for (NSInteger i=0; i<self.whyArray.count; i++) {
        UIButton *btn = self.whyArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.whyIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
    
}
    
    
- (IBAction)assetsAction:(UIButton *)sender {
    
    for (NSInteger i=0; i<self.assetsArray.count; i++) {
        UIButton *btn = self.assetsArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.assetsIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
}
    
- (IBAction)accepyAction:(UIButton *)sender {
    
    for (NSInteger i=0; i<self.accepArray.count; i++) {
        UIButton *btn = self.accepArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.accepIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
}
    
- (IBAction)schoolAction:(UIButton *)sender {
    for (NSInteger i=0; i<self.schoolArray.count; i++) {
        UIButton *btn = self.schoolArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.schoolIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
}
    
- (IBAction)englishAction:(UIButton *)sender {
    for (NSInteger i=0; i<self.englishArray.count; i++) {
        UIButton *btn = self.englishArray[i];
        if (btn == sender) {
            sender.backgroundColor = [UIColor getColor:@"267BFF"];
            [sender setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            self.englishIndex = i;
        }else{
            btn.backgroundColor = [UIColor getColor:@"EDEDF0"];
            [btn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        }
    }
}
    
- (IBAction)filishAction:(UIButton *)sender {
    
}
    
    @end
