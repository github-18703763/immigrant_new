//
//  MigratePlanRequireCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigratePlanRequireCell.h"

@interface MigratePlanRequireCell ()



@end

@implementation MigratePlanRequireCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    return self;
}

- (void)masLayout {
    
    [self.contentView addSubview:self.leftLabel];
    [self.contentView addSubview:self.rightLabel];
    [self.contentView addSubview:self.chekcImgv];
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(29);
        make.centerY.equalTo(self.contentView);
        make.width.lessThanOrEqualTo(@(70));
    }];
    
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-31);
        make.centerY.equalTo(self.contentView);
        make.width.mas_lessThanOrEqualTo(SystemScreenWidth - 123 - 16 - 56);
    }];
    
    [self.chekcImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(98);
        make.width.height.mas_equalTo(16);
    }];
}

- (UIImageView *)chekcImgv {
    if (!_chekcImgv) {
        _chekcImgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dongxialingyin_dui"]];
        _chekcImgv.backgroundColor = [UIColor getColor:@"EDEDF0"];
        _chekcImgv.layer.cornerRadius = 8;
        _chekcImgv.layer.masksToBounds = true;
    }
    return _chekcImgv;
}

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.font = [UIFont systemFontOfSize:13];
        _leftLabel.textColor = [UIColor getColor:@"5E5E5E"];
    }
    return _leftLabel;
}

- (UILabel *)rightLabel {
    if (!_rightLabel) {
        _rightLabel = [[UILabel alloc] init];
        _rightLabel.textAlignment = NSTextAlignmentRight;
        _rightLabel.font = [UIFont systemFontOfSize:15];
        _rightLabel.textColor = [UIColor getColor:@"6A6A6A"];

    }
    return _rightLabel;
}

@end
