//
//  ZBImmigrationProjectFooterOneView.m
//  Immigrant
//
//  Created by 张波 on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationProjectFooterOneView.h"

@implementation ZBImmigrationProjectFooterOneView

-(void)awakeFromNib{
    [super awakeFromNib];
}
- (IBAction)lookMoreAction:(id)sender {
    if (self.lookMoreBlock) {
        self.lookMoreBlock();
    }
}

@end
