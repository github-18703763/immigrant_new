//
//  ZBImmigrationExampleCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationExampleCell.h"

@interface ZBImmigrationExampleCell()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation ZBImmigrationExampleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
}

-(void)setModel:(ZBImmigrationExampleModel *)model{
    _model = model;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.timeLab.text = _model.gmtCreated;
    self.titleLab.text = _model.title;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
