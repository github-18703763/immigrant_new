//
//  MigratePlanCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigratePlanCell.h"
#import "ZPCAshapelGradientView.h"
#import "UITableViewCell+identifier.h"
#import "MigratePlanRequireCell.h"
#import "ZBImmigrationCountryOjbct.h"
#import <YYModel/YYModel.h>
@interface MigratePlanCell ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ZPCAshapelGradientView *cycleView;
@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSArray *titleArray;

@property (nonatomic, strong) NSArray<NSString *> *require1OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require2OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require3OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require4OptionArray;
@property (nonatomic, strong) NSArray<NSString *> *require5OptionArray;

@property (nonatomic, copy) NSArray<ZBImmigrationCountryOjbct *> *countryArray;

@end

@implementation MigratePlanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = UIColor.clearColor;
    self.contentView.backgroundColor = UIColor.clearColor;
    self.titleArray = @[@"国家地区",
                        @"移民目的",
                        @"居住要求",
                        @"最高学历",
                        @"资金要求",
                        @"外语要求",
                        ];
    [self configData];
    [self fetchCountry];
    return self;
}

- (void)configData {
    
    self.require1OptionArray = @[@"子女教育",
                                 @"海外生育",
                                 @"养老储蓄",
                                 @"出行便利",
                                 @"海外置业",
                                 @"投资理财",
                                 @"旅游度假",
                                 @"税务筹划",
                                 ];
    
    self.require2OptionArray = @[@"100万以内",
                                 @"100万-300万",
                                 @"300万-500万",
                                 @"500万-1000万",
                                 @"1000万-3000万",
                                 @"3000万以上",
                                 ];
    
    self.require3OptionArray = @[@"不方便居住",
                                 @"每年入境1次",
                                 @"每年住7天",
                                 @"每年住30天",
                                 @"每年住半年",
                                 @"累计住满2年",
                                 ];
    
    self.require4OptionArray = @[@"高中以下",
                                 @"高中或中专",
                                 @"大专",
                                 @"本科或硕士",
                                 @"博士",
                                 ];
    
    self.require5OptionArray = @[@"完全不会",
                                 @"大学英语4级及以下",
                                 @"大学英语6级",
                                 @"大学英语6级优秀",
                                 @"专业英语8级",
                                 ];
}

- (void)fetchCountry {
    //    MigrateQuestionModel
    [QSNetworkManager migrateContrysuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.countryArray = [NSArray yy_modelArrayWithClass:[ZBImmigrationCountryOjbct class] json:responseModel.data[@"bussData"]];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.cycleView];
    [self.contentView addSubview:self.scoreLabel];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.tableView];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(47);
        make.left.equalTo(self.contentView).offset(24);
        make.right.equalTo(self.contentView).offset(-24);
        make.bottom.equalTo(self.contentView);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backView).offset(-14);
        make.left.right.equalTo(self.backView);
        make.height.mas_equalTo(7 * 32);
    }];
    
    [self.cycleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(76);
        make.top.equalTo(self.contentView).offset(13);
        make.centerX.equalTo(self.contentView);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.cycleView.mas_bottom).offset(14);
    }];
    
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(57);
        make.center.equalTo(self.cycleView);
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MigratePlanRequireCell *cell = [MigratePlanRequireCell cellForTableView:tableView];
    cell.leftLabel.text = self.titleArray[indexPath.row];
    switch (indexPath.row) {
        case 0:
        {
            cell.chekcImgv.hidden = !self.model.require0fit;
            NSString *tmp = @"";
            
            for (ZBImmigrationCountryOjbct *obj in self.countryArray) {
                if ([obj.id integerValue] == self.model.require0) {
                    tmp = obj.chinesename;
                }
            }
            
            NSTextAttachment *attch = [[NSTextAttachment alloc] init];
            attch.image = [UIImage imageNamed:@"yiminfangan_dingwei"];
            NSAttributedString *attchStr = [NSAttributedString attributedStringWithAttachment:attch];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:tmp];
            [str insertAttributedString:attchStr atIndex:0];
            cell.rightLabel.attributedText = str;
        }
            break;
        case 1:
        {
            cell.chekcImgv.hidden = !self.model.require2fit;
            
            NSString *tmp = self.require2OptionArray[self.model.require2];
            
            NSTextAttachment *attch = [[NSTextAttachment alloc] init];
            attch.image = [UIImage imageNamed:@"yiminfangan_feiji"];
            NSAttributedString *attchStr = [NSAttributedString attributedStringWithAttachment:attch];
            if (tmp) {
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: tmp];
                [str insertAttributedString:attchStr atIndex:0];
                cell.rightLabel.attributedText = str;
            }
            
        }
            break;
        case 2:
        {
            cell.chekcImgv.hidden = !self.model.require4fit;
            NSString *tmp = self.require4OptionArray[self.model.require4];

            NSTextAttachment *attch = [[NSTextAttachment alloc] init];
            attch.image = [UIImage imageNamed:@"yiminfangan_fangzi"];
            NSAttributedString *attchStr = [NSAttributedString attributedStringWithAttachment:attch];
            if (tmp) {
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: tmp];
                [str insertAttributedString:attchStr atIndex:0];
                cell.rightLabel.attributedText = str;
            }
        }
            break;
        case 3:
        {
            cell.chekcImgv.hidden = !self.model.require1fit;
            NSString *tmp = self.require1OptionArray[self.model.require1];

            NSTextAttachment *attch = [[NSTextAttachment alloc] init];
            attch.image = [UIImage imageNamed:@"yiminfangan_xuelihao"];
            NSAttributedString *attchStr = [NSAttributedString attributedStringWithAttachment:attch];
            if (tmp) {
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: tmp];
                [str insertAttributedString:attchStr atIndex:0];
                cell.rightLabel.attributedText = str;
            }
        }
            break;
        case 4:
        {
            cell.chekcImgv.hidden = !self.model.require4fit;
            NSString *tmp = self.require4OptionArray[self.model.require4];

            NSTextAttachment *attch = [[NSTextAttachment alloc] init];
            attch.image = [UIImage imageNamed:@"yiminfangan_zijin"];
            NSAttributedString *attchStr = [NSAttributedString attributedStringWithAttachment:attch];
            if (tmp) {
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: tmp];
                [str insertAttributedString:attchStr atIndex:0];
                cell.rightLabel.attributedText = str;
            }
        }
            break;
        case 5:
        {
            cell.chekcImgv.hidden = !self.model.require5fit;
            NSString *tmp = self.require5OptionArray[self.model.require5];

            NSTextAttachment *attch = [[NSTextAttachment alloc] init];
            attch.image = [UIImage imageNamed:@"yiminfangan_waiyu"];
            NSAttributedString *attchStr = [NSAttributedString attributedStringWithAttachment:attch];
            if (tmp) {
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: tmp];
                [str insertAttributedString:attchStr atIndex:0];
                cell.rightLabel.attributedText = str;
            }
        }
            break;
        default:
            break;
    }
    
    
    return cell;
}

#pragma mark - Getter

- (ZPCAshapelGradientView *)cycleView {
    if (!_cycleView) {
        _cycleView = [[ZPCAshapelGradientView alloc] initWithFrame:CGRectMake(0, 0, 76, 76)];
        _cycleView.transform = CGAffineTransformMakeRotation(-M_PI_2);
        _cycleView.progressLineWidth = 9.5;
    }
    return _cycleView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor getColor:@"4A4A4A"];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
    }
    return _nameLabel;
}

- (UILabel *)scoreLabel {
    if (!_scoreLabel) {
        _scoreLabel = [[UILabel alloc] init];
        _scoreLabel.layer.cornerRadius = 57.0 / 2;
        _scoreLabel.layer.masksToBounds = true;
        _scoreLabel.backgroundColor = [UIColor getColor:@"f4f7f9"];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        _scoreLabel.textColor = [UIColor blackColor];
        _scoreLabel.font = [UIFont boldSystemFontOfSize:19];
    }
    return _scoreLabel;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 2);
        _backView.layer.shadowRadius = 21.5;
        _backView.layer.shadowColor = [UIColor getColor:@"a9a9a9"].CGColor;
        _backView.layer.shadowOpacity = 0.12;
    }
    return _backView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 32;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.scrollEnabled = false;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIApplicationBackgroundFetchIntervalNever;
        }
    }
    return _tableView;
}

- (void)setModel:(MigrateModel *)model {
    _model = model;
    self.nameLabel.text = model.name;
    self.cycleView.percent = model.requirefitscore;
    self.scoreLabel.text = [NSString stringWithFormat:@"%@",@(model.requirefitscore)];
    [self.tableView reloadData];
}
@end
