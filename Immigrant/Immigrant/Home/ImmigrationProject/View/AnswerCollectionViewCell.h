//
//  AnswerCollectionViewCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^tapBlock)(UIButton *);

NS_ASSUME_NONNULL_BEGIN

@interface AnswerCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *answerBtn;
@property (nonatomic, copy) tapBlock block;

@end

NS_ASSUME_NONNULL_END
