//
//  ZBImmigrationConditionTestCell.h
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationListObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationConditionTestCell : UITableViewCell

@property (nonatomic,strong) ZBImmigrationListObject  *model;

@property (nonatomic,copy) void(^commitBlock)(NSArray *array);

@end

NS_ASSUME_NONNULL_END
