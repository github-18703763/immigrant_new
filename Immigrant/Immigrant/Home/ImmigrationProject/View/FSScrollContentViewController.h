//
//  FSScrollContentViewController.h
//  Immigrant
//
//  Created by a on 2018/12/28.
//  Copyright © 2018 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBCountryObject.h"
#import "ZBVisaObject.h"

@protocol FSScrollContentViewControllerDelegae <NSObject>

-(void)didSelect:(NSIndexPath *)path withModel:(ZBVisaObject *)model;

@end

NS_ASSUME_NONNULL_BEGIN

@interface FSScrollContentViewController : UIViewController

@property (nonatomic, assign) BOOL vcCanScroll;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic, strong) NSString *str;
@property (nonatomic,strong) ZBCountryObject  *contry;

@property (nonatomic,weak) id<FSScrollContentViewControllerDelegae>  delegage;

@end

NS_ASSUME_NONNULL_END
