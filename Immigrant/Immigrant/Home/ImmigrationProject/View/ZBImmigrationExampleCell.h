//
//  ZBImmigrationExampleCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationExampleModel.h"

@interface ZBImmigrationExampleCell : UITableViewCell

@property (nonatomic,strong) ZBImmigrationExampleModel  *model;

@end
