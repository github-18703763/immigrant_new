//
//  MigratePlanCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MigrateModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MigratePlanCell : UITableViewCell

@property (nonatomic, strong) MigrateModel *model;

@end

NS_ASSUME_NONNULL_END
