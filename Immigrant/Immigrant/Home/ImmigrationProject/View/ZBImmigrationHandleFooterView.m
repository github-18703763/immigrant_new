//
//  ZBImmigrationHandleFooterView.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationHandleFooterView.h"

@interface ZBImmigrationHandleFooterView ()

@property (weak, nonatomic) IBOutlet UIView *pointOne;


@end

@implementation ZBImmigrationHandleFooterView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.pointOne.layer.cornerRadius = 4;
    self.pointOne.layer.masksToBounds = YES;
}

@end
