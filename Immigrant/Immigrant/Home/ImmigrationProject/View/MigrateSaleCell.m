//
//  MigrateSaleCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateSaleCell.h"
#import "MigrateModel.h"
@interface MigrateSaleCell ()

@property (nonatomic, strong) UILabel *timeHintLabel;
@property (nonatomic, strong) UIImageView *timeImgv;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UILabel *cardTypeHintLabel;
@property (nonatomic, strong) UIImageView *cardTypeImgv;
@property (nonatomic, strong) UILabel *cardTypeLabel;

@property (nonatomic, strong) UILabel *investmentHintLabel;
@property (nonatomic, strong) UIImageView *investmentImgv;
@property (nonatomic, strong) UILabel *investmentLabel;

@property (nonatomic, strong) UILabel *requireHintLabel;
@property (nonatomic, strong) UIImageView *requireImgv;
@property (nonatomic, strong) UILabel *requireLabel;

@end

@implementation MigrateSaleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)masLayout {
    [self.contentView addSubview:self.timeHintLabel];
    [self.contentView addSubview:self.timeImgv];
    [self.contentView addSubview:self.timeLabel];
    
    [self.contentView addSubview:self.cardTypeHintLabel];
    [self.contentView addSubview:self.cardTypeImgv];
    [self.contentView addSubview:self.cardTypeLabel];
    
    [self.contentView addSubview:self.investmentHintLabel];
    [self.contentView addSubview:self.investmentImgv];
    [self.contentView addSubview:self.investmentLabel];
    
    [self.contentView addSubview:self.requireHintLabel];
    [self.contentView addSubview:self.requireImgv];
    [self.contentView addSubview:self.requireLabel];
    
    [self.timeHintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(47);
        make.top.equalTo(self.contentView).offset(27);
    }];
    
    [self.timeImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(47);
        make.centerY.equalTo(self.timeLabel);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeHintLabel.mas_bottom).offset(11);
        make.left.equalTo(self.timeImgv.mas_right).offset(9);
//        make.right.equalTo(self.contentView.mas_centerY);
        make.width.mas_lessThanOrEqualTo((SystemScreenWidth / 2.0) - 70);
        make.bottom.equalTo(self.contentView).offset(-20);
    }];
    
    [self.cardTypeHintLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset((SystemScreenWidth / 2.0) + 47);
        make.top.equalTo(self.contentView).offset(27);
    }];
    
    [self.cardTypeImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset((SystemScreenWidth / 2.0) + 47);
        make.centerY.equalTo(self.timeLabel);
    }];
    
    [self.cardTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeHintLabel.mas_bottom).offset(11);
        make.left.equalTo(self.cardTypeImgv.mas_right).offset(9);
        //        make.right.equalTo(self.contentView.mas_centerY);
        make.width.mas_lessThanOrEqualTo((SystemScreenWidth / 2.0) - 70);
        make.bottom.equalTo(self.contentView).offset(-20);
    }];
    
}


- (void)configCell: (id)object {
    if ([object isKindOfClass:[MigrateModel class]]) {
        MigrateModel *obj = object;
        
        self.timeLabel.text = obj.servietime;
        self.cardTypeLabel.text = obj.cardtype;
//        self.investmentLabel.text = [NSString stringWithFormat:@"%@元",@(obj.investmentyuan)];
//        self.requireLabel.text = obj.residencerequires;
    }
}

- (void)configRequireCell: (id)object {
    if ([object isKindOfClass:[MigrateModel class]]) {
        MigrateModel *obj = object;
        
        self.timeLabel.text = [NSString stringWithFormat:@"%@元",@(obj.investmentyuan)];
        self.cardTypeLabel.text = obj.residencerequires;
        self.timeHintLabel.text = @"投资金额";
        self.requireHintLabel.text = @"居住要求";
        self.cardTypeImgv.image = [UIImage imageNamed:@"fangzi"];
    }
}

- (UILabel *)timeHintLabel {
    if (!_timeHintLabel) {
        _timeHintLabel = [[UILabel alloc] init];
        _timeHintLabel.textColor = [UIColor getColor:@"5E5E5E"];
        _timeHintLabel.text = @"办理周期";
        _timeLabel.font = [UIFont systemFontOfSize:13];
    }
    return _timeHintLabel;
}

- (UIImageView *)timeImgv {
    if (!_timeImgv) {
        _timeImgv = [[UIImageView alloc] init];
        _timeImgv.image = [UIImage imageNamed:@"shijian"];
    }
    return _timeImgv;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textColor = [UIColor getColor:@"4E4E4E"];
        _timeLabel.text = @"办理周期";
        _timeLabel.font = [UIFont systemFontOfSize:17];
        _timeLabel.numberOfLines = 0;
    }
    return _timeLabel;
}

- (UILabel *)cardTypeHintLabel {
    if (!_cardTypeHintLabel) {
        _cardTypeHintLabel = [[UILabel alloc] init];
        _cardTypeHintLabel.textColor = [UIColor getColor:@"5E5E5E"];
        _cardTypeHintLabel.text = @"身份类型";
        _cardTypeLabel.font = [UIFont systemFontOfSize:13];
    }
    return _cardTypeHintLabel;
}

- (UIImageView *)cardTypeImgv {
    if (!_cardTypeImgv) {
        _cardTypeImgv = [[UIImageView alloc] init];
        _cardTypeImgv.image = [UIImage imageNamed:@"yiminbanli_shenfen"];
    }
    return _cardTypeImgv;
}

- (UILabel *)cardTypeLabel {
    if (!_cardTypeLabel) {
        _cardTypeLabel = [[UILabel alloc] init];
        _cardTypeLabel.textColor = [UIColor getColor:@"4E4E4E"];
        _cardTypeLabel.font = [UIFont systemFontOfSize:17];
        _cardTypeLabel.numberOfLines = 0;
    }
    return _cardTypeLabel;
}

- (UILabel *)investmentHintLabel {
    if (!_investmentHintLabel) {
        _investmentHintLabel = [[UILabel alloc] init];
        _investmentHintLabel.textColor = [UIColor getColor:@"5E5E5E"];
        _investmentHintLabel.text = @"投资金额";
        _investmentLabel.font = [UIFont systemFontOfSize:13];
    }
    return _investmentHintLabel;
}

- (UIImageView *)investmentImgv {
    if (!_investmentImgv) {
        _investmentImgv = [[UIImageView alloc] init];
        _investmentImgv.image = [UIImage imageNamed:@"shijian"];
    }
    return _investmentImgv;
}

- (UILabel *)investmentLabel {
    if (!_investmentLabel) {
        _investmentLabel = [[UILabel alloc] init];
        _investmentLabel.textColor = [UIColor getColor:@"4E4E4E"];
        _investmentLabel.font = [UIFont systemFontOfSize:17];
        _investmentLabel.numberOfLines = 0;
    }
    return _investmentLabel;
}

- (UILabel *)requireHintLabel {
    if (!_requireHintLabel) {
        _requireHintLabel = [[UILabel alloc] init];
        _requireHintLabel.textColor = [UIColor getColor:@"5E5E5E"];
        _requireHintLabel.text = @"居住要求";
        _requireLabel.font = [UIFont systemFontOfSize:13];
    }
    return _requireHintLabel;
}

- (UIImageView *)requireImgv {
    if (!_requireImgv) {
        _requireImgv = [[UIImageView alloc] init];
        _requireImgv.image = [UIImage imageNamed:@"shijian"];
    }
    return _requireImgv;
}

- (UILabel *)requireLabel {
    if (!_requireLabel) {
        _requireLabel = [[UILabel alloc] init];
        _requireLabel.textColor = [UIColor getColor:@"4E4E4E"];
        _requireLabel.font = [UIFont systemFontOfSize:17];
        _requireLabel.numberOfLines = 0;
    }
    return _requireLabel;
}

@end
