//
//  QuestionCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "QuestionCell.h"
#import "AnswerCollectionViewCell.h"

@interface QuestionCell ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource> {
    NSInteger _selectIndex;
}

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UIView *backView;
@end

@implementation QuestionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.rowNumber = 2;
    _selectIndex = -1;
    
    self.contentView.backgroundColor = UIColor.clearColor;
    self.backgroundColor = UIColor.clearColor;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.questionLabel];
    [self.backView addSubview:self.collectionView];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.contentView).offset(18);
    }];
    
    [self.questionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(19);
        make.right.equalTo(self.backView).offset(-19);
        make.top.equalTo(self.contentView).offset(38);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(30);
        make.right.equalTo(self.contentView).offset(-30);
        make.top.equalTo(self.questionLabel.mas_bottom).offset(17);
        make.bottom.equalTo(self.contentView).offset(-13);
        make.height.mas_equalTo(104);
    }];
}

- (void)heightChange {
    
    NSInteger row = (self.questions.count / self.rowNumber) + ((self.questions.count % self.rowNumber) == 0 ? 0 : 1);
    
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(30);
        make.right.equalTo(self.contentView).offset(-30);
        make.top.equalTo(self.questionLabel.mas_bottom).offset(17);
        make.bottom.equalTo(self.contentView).offset(-13);
        make.height.mas_equalTo(row * 52);
    }];
    
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.questions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AnswerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AnswerCollectionViewCell" forIndexPath:indexPath];
    
    [cell.answerBtn setTitle:self.questions[indexPath.row] forState:UIControlStateNormal];

    cell.answerBtn.selected = _selectIndex == indexPath.row;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(floor((SystemScreenWidth - 60 - (14 * (self.rowNumber - 1))) / self.rowNumber), 38);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemSelected) {
        _selectIndex = indexPath.row;
        self.itemSelected(indexPath.row);
        [collectionView reloadData];
    }
}

#pragma mark - Getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = self.bounds.size;
        flowLayout.minimumLineSpacing = 13;
        flowLayout.minimumInteritemSpacing = 13;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        //        CGFloat collectionViewX = 0;
        //        CGFloat collectionViewY = 0;
        //        CGFloat collectionViewW = self.SG_width;
        //        CGFloat collectionViewH = self.SG_height;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.pagingEnabled = YES;
        _collectionView.bounces = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = false;
        [_collectionView registerClass:[AnswerCollectionViewCell class] forCellWithReuseIdentifier:@"AnswerCollectionViewCell"];
    }
    return _collectionView;
}

- (UILabel *)questionLabel {
    if (!_questionLabel) {
        _questionLabel = [[UILabel alloc] init];
        _questionLabel.textColor = [UIColor getColor:@"4A4A4A"];
        _questionLabel.font = [UIFont systemFontOfSize:18];
        _questionLabel.numberOfLines = 0;
    }
    return _questionLabel;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 6);
        _backView.layer.shadowRadius = 20.5;
        _backView.layer.shadowColor = [UIColor getColor:@"b2b2b2"].CGColor;
        _backView.layer.shadowOpacity = 0.44;
    }
    return _backView;
}

- (void)setQuestions:(NSArray<NSString *> *)questions {
    _questions = questions;
    [self heightChange];
}

@end
