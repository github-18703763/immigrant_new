//
//  MigrateSaleCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MigrateSaleCell : UITableViewCell

- (void)configCell: (id)object;
- (void)configRequireCell: (id)object;

@end

NS_ASSUME_NONNULL_END
