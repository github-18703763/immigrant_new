//
//  MigratePlanRequireCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MigratePlanRequireCell : UITableViewCell
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UIImageView *chekcImgv;
@property (nonatomic, strong) UILabel *rightLabel;

@end

NS_ASSUME_NONNULL_END
