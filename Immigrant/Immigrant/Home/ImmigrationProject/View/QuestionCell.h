//
//  QuestionCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ItemSelected)(NSInteger);
NS_ASSUME_NONNULL_BEGIN

@interface QuestionCell : UITableViewCell

@property (nonatomic, assign) NSInteger rowNumber;

@property (nonatomic, copy) NSArray<NSString *> *questions;

@property (nonatomic, copy) ItemSelected itemSelected;

@property (nonatomic, strong) UILabel *questionLabel;

@end

NS_ASSUME_NONNULL_END
