//
//  ZBImmigrationInConsultingMsgCell.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationInConsultingMsgCell.h"

@interface ZBImmigrationInConsultingMsgCell()

@property (weak, nonatomic) IBOutlet UIView *pointVIew;


@end

@implementation ZBImmigrationInConsultingMsgCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.pointVIew.layer.cornerRadius= 4;
    self.pointVIew.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
