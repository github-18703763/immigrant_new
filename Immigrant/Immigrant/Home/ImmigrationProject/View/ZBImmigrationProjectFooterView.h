//
//  ZBImmigrationProjectFooterView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBImmigrationProjectFooterView : UIView

@property (nonatomic,copy) void(^lookMoreBlock)(void);

@property (nonatomic,copy) void(^assessmentBlock)(void);

@end
