//
//  ZBImmigrationProjectSectionOneHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBImmigrationProjectSectionOneHeadView : UIView
//移民评估
@property (nonatomic, copy) void(^assessmentBlock)(void);
//成功案列
@property (nonatomic, copy) void(^successBlock)(void);

@end
