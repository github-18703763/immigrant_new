//
//  ZBImmigrationConditionTestCell.m
//  Immigrant
//
//  Created by 张波 on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrationConditionTestCell.h"

@interface ZBImmigrationConditionTestCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnThree;
@property (weak, nonatomic) IBOutlet UIButton *btnFour;

@property (weak, nonatomic) IBOutlet UIButton *btnTwoOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoTwo;

@property (weak, nonatomic) IBOutlet UIButton *commitBtn;

@property (nonatomic,weak) UIButton  *preSelectPBtn;
@property (nonatomic,weak) UIButton  *preSelectNBtn;

@end

@implementation ZBImmigrationConditionTestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.btnOne.layer.cornerRadius = 19;
    self.btnOne.layer.masksToBounds = YES;
    self.btnTwo.layer.cornerRadius = 19;
    self.btnTwo.layer.masksToBounds = YES;
    self.btnThree.layer.cornerRadius = 19;
    self.btnThree.layer.masksToBounds = YES;
    self.btnFour.layer.cornerRadius = 19;
    self.btnFour.layer.masksToBounds = YES;
    
    self.btnTwoOne.layer.cornerRadius = 19;
    self.btnTwoOne.layer.masksToBounds = YES;
    self.btnTwoTwo.layer.cornerRadius = 19;
    self.btnTwoTwo.layer.masksToBounds = YES;
    
    //选中  267BFF  白色 
    self.commitBtn.layer.cornerRadius = 25;
    self.commitBtn.layer.masksToBounds = YES;
    
}

- (IBAction)ageAction:(UIButton *)sender {
    if (sender == self.preSelectPBtn) {
        [sender setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [sender setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        self.preSelectPBtn = nil;
        return;
    }
    if (sender == self.btnOne) {
        [self.btnOne setBackgroundColor:[UIColor getColor:@"267BFF"]];
        [self.btnOne setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnTwo setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnTwo setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnThree setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnThree setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnFour setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnFour setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
    }else if (sender == self.btnTwo){
        [self.btnTwo setBackgroundColor:[UIColor getColor:@"267BFF"]];
        [self.btnTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnOne setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnOne setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnThree setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnThree setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnFour setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnFour setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
    }else if (sender == self.btnThree){
        [self.btnThree setBackgroundColor:[UIColor getColor:@"267BFF"]];
        [self.btnThree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnTwo setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnTwo setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnOne setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnOne setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnFour setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnFour setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
    }
    else{
        [self.btnFour setBackgroundColor:[UIColor getColor:@"267BFF"]];
        [self.btnFour setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnTwo setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnTwo setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnThree setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnThree setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        [self.btnOne setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnOne setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
    }
    self.preSelectPBtn = sender;
    
}


- (IBAction)conditionAction:(UIButton *)sender {
    if (sender == self.preSelectNBtn) {
        [sender setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [sender setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        self.preSelectNBtn = nil;
        return;
    }
    if (sender == self.btnTwoOne) {
        [self.btnTwoOne setBackgroundColor:[UIColor getColor:@"267BFF"]];
        [self.btnTwoOne setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnTwoTwo setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnTwoTwo setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
    }else{
        [self.btnTwoOne setBackgroundColor:[UIColor getColor:@"EDEDF0"]];
        [self.btnTwoOne setTitleColor:[UIColor getColor:@"4a4a5a"] forState:UIControlStateNormal];
        
        [self.btnTwoTwo setBackgroundColor:[UIColor getColor:@"267BFF"]];
        [self.btnTwoTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    self.preSelectNBtn = sender;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)commitAction:(id)sender {
    if (!self.preSelectPBtn) {
         [[UIApplication sharedApplication].delegate.window makeToast:@"请选择年龄" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    if (!self.preSelectNBtn) {
         [[UIApplication sharedApplication].delegate.window makeToast:@"请选择是否满足条件" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    NSString *strA = @"";
    if (self.preSelectPBtn == self.btnOne) {
        strA = @"A";
    }else if (self.preSelectPBtn == self.btnTwo){
          strA = @"B";
    }else if (self.preSelectPBtn == self.btnThree){
         strA = @"C";
    }else if (self.preSelectPBtn == self.btnFour){
         strA = @"D";
    }
     NSString *strB = @"";
    if (self.preSelectNBtn == self.btnTwoOne) {
        strB = @"A";
    }else{
        strB = @"B";
    }
    if (self.commitBlock) {
        self.commitBlock(@[strA,strB]);
    }
}

-(void)setModel:(ZBImmigrationListObject *)model{
    _model = model;
    self.nameLab.text = _model.name;
}

@end
