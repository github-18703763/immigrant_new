//
//  ZBImmigrationProjectTwoCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationProjectTwoCell.h"

@interface ZBImmigrationProjectTwoCell()

@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgOne;
@property (weak, nonatomic) IBOutlet UIImageView *imgTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imgThree;
@property (weak, nonatomic) IBOutlet UIImageView *imgFour;
@property (weak, nonatomic) IBOutlet UIImageView *imgFive;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;

@end

@implementation ZBImmigrationProjectTwoCell

-(void)setModel:(ZBImmigrationRiskObject *)model{
    _model = model;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.titleLab.text = _model.title;
    self.contentLab.text = _model.content;
    NSInteger number = [_model.rate integerValue];
    self.numberLab.text = [NSString stringWithFormat:@"%li.0",number];
    switch (number) {
        case 0:
        {
            self.imgOne.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgTwo.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgThree.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFour.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFive.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];

        }
            break;
        case 1:
        {
            self.imgOne.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgTwo.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgThree.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFour.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFive.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            
        }
            break;
        case 2:
        {
            self.imgOne.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgTwo.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgThree.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFour.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFive.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            
        }
            break;
        case 3:
        {
            self.imgOne.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgTwo.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgThree.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgFour.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            self.imgFive.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            
        }
            break;
        case 4:
        {
            self.imgOne.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgTwo.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgThree.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgFour.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgFive.image = [UIImage imageNamed:@"yiminxiangmu_xingxing_b"];
            
        }
            break;
        case 5:
        {
            self.imgOne.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgTwo.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgThree.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgFour.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            self.imgFive.image = [UIImage imageNamed:@"yiminxiangmu_xingxing"];
            
        }
            break;
            
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.numberLab.layer.cornerRadius = 4;
    self.numberLab.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 13;
    self.photoImg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
