//
//  ZBImmigrationProjectSectionOneHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBImmigrationProjectSectionOneHeadView.h"

@interface ZBImmigrationProjectSectionOneHeadView()
@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwo;

@end

@implementation ZBImmigrationProjectSectionOneHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.btnOne.layer.cornerRadius = 23.5;
    self.btnOne.layer.masksToBounds = YES;
    self.btnTwo.layer.cornerRadius = 23.5;
    self.btnTwo.layer.masksToBounds = YES;
}

- (IBAction)assessmentAction:(id)sender {
    if (self.assessmentBlock) {
        self.assessmentBlock();
    }
}
- (IBAction)sucsessAction:(id)sender {
    if (self.successBlock) {
        self.successBlock();
    }
}


@end
