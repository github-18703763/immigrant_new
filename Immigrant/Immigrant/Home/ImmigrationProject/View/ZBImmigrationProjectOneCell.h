//
//  ZBImmigrationProjectOneCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBImmigrationListObject.h"
#import "ZBMyCollectionModel.h"
@interface ZBImmigrationProjectOneCell : UITableViewCell

@property (nonatomic,strong) ZBImmigrationListObject *model;
//我的收藏
@property (nonatomic,strong) ZBMigrateDTOModel *modelMigrate;//移民项目
@end
