//
//  MigrateInfoCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import "MigrateInfoCell.h"
#import "ZBCampObject.h"
#import "ZBMyOrderModel.h"
#import <UIImageView+WebCache.h>
#import "ZBInspectionObject.h"

@interface MigrateInfoCell()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *migrateImgv;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *priceHintLabel;
@property (nonatomic, strong) UILabel *priceLabel;


@end

@implementation MigrateInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.migrateImgv];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.priceHintLabel];
    [self.backView addSubview:self.priceLabel];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(16);
        make.bottom.equalTo(self.contentView).offset(-5);
        make.height.mas_equalTo(130);
    }];
    
    [self.migrateImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(22);
        make.top.equalTo(self.backView).offset(17);
        make.bottom.equalTo(self.backView).offset(-17);
        make.width.mas_equalTo(123);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.migrateImgv.mas_right).offset(20);
        make.top.equalTo(self.backView).offset(20.5);
        make.right.equalTo(self.backView).offset(-20);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backView).offset(-17);
        make.right.equalTo(self.backView).offset(-31);
    }];
    
    [self.priceHintLabel  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.migrateImgv.mas_right).offset(18);
        make.bottom.equalTo(self.backView).offset(-21.5);
    }];
    
}

- (void)configCell:(id)object {
    
    if ([object isKindOfClass:[ZBMyOrderModel class]]) {
        ZBMyOrderModel *obj = object;
        
        if (obj.orderMigrate != nil) {
            ZBMyOrderMigrateModel *tmp = obj.orderMigrate;
            [self.migrateImgv sd_setImageWithURL:[NSURL URLWithString:tmp.cover]];
            self.priceLabel.text = [NSString stringWithFormat:@"￥%@", tmp.depositfeeyuan];
            self.titleLabel.text = tmp.name;
        }
    }
    
    
}

#pragma mark - Getter

- (UIImageView *)migrateImgv {
    if (!_migrateImgv) {
        _migrateImgv = [[UIImageView alloc] init];
        _migrateImgv.contentMode = UIViewContentModeScaleAspectFill;
        _migrateImgv.layer.cornerRadius = 8;
        _migrateImgv.layer.masksToBounds = true;
        _migrateImgv.clipsToBounds = true;
    }
    return _migrateImgv;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 2);
        _backView.layer.shadowRadius = 21.5;
        _backView.layer.shadowColor = [UIColor getColor:@"a9a9a9"].CGColor;
        _backView.layer.shadowOpacity = 0.12;
    }
    return _backView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textColor = [UIColor getColor:@"4E4E4E"];
    }
    return _titleLabel;
}

- (UILabel *)priceHintLabel {
    if (!_priceHintLabel) {
        _priceHintLabel = [[UILabel alloc] init];
        _priceHintLabel.font = [UIFont systemFontOfSize:15];
        _priceHintLabel.textColor = [UIColor getColor:@"9A9A9A"];
        _priceHintLabel.text = @"移民定金";
    }
    return _priceHintLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont boldSystemFontOfSize:16];
        _priceLabel.textColor = [UIColor getColor:@"717171"];
    }
    return _priceLabel;
}


@end
