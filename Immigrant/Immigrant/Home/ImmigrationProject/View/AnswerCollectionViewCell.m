//
//  AnswerCollectionViewCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "AnswerCollectionViewCell.h"

@implementation AnswerCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self addSubview:self.answerBtn];
    [self.answerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    return self;
}

- (void)btnClick:(UIButton *)btn {
    if (self.block) {
        self.block(btn);
    }
}

- (UIButton *)answerBtn {
    if (!_answerBtn) {
        _answerBtn = [[UIButton alloc] init];
        _answerBtn.layer.cornerRadius = 19.25;
        _answerBtn.layer.masksToBounds = true;
        [_answerBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"EDEDF0"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        [_answerBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"267BFF"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateSelected];
        [_answerBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_answerBtn setTitleColor:[UIColor getColor:@"4A4A4A"] forState:UIControlStateNormal];
        [_answerBtn setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateSelected];
        _answerBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        _answerBtn.userInteractionEnabled = false;
    }
    return _answerBtn;
}

@end
