//
//  ZBImmigrationProjectFooterOneView.h
//  Immigrant
//
//  Created by 张波 on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBImmigrationProjectFooterOneView : UIView

@property (nonatomic,copy) void(^lookMoreBlock)(void);

@end

NS_ASSUME_NONNULL_END
