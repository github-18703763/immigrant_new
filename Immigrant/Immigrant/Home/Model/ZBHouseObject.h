//
//  ZBHouseObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBHouseObject : NSObject

/**
 address = "\U6e29\U6b4c\U534e";
 */
@property (nonatomic,copy) NSString  *address;

/**
  annuprofit = 58;
 */
@property (nonatomic,copy) NSString  *annuprofit;

/**
 country = "\U7f8e\U56fd";
 */
@property (nonatomic,copy) NSString  *country;

/**
 countryId = 10002;
 */
@property (nonatomic,copy) NSString  *countryId;

/**
 cover = "https://file.icebartech.com/icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *cover;

/**
 creator = "[SYS]";
 */
@property (nonatomic,copy) NSString  *creator;
/**
"deliverystandard": "精装修",
 */
@property (nonatomic,copy) NSString  *deliverystandard;
/**
 "deliverytime": "2018年底",
 */
@property (nonatomic,copy) NSString  *deliverytime;
/**
 depositfee = 888800;
 */
@property (nonatomic,copy) NSString  *depositfee;

/**
 depositfeeyuan = 8888;
 */
@property (nonatomic,copy) NSString  *depositfeeyuan;

/**
 edate = "2018-12-30 00:00:00";
 */
@property (nonatomic,copy) NSString  *edate;
/**
 "downpayment": 30,
 */
@property (nonatomic,copy) NSString  *downpayment;

/**
"floorage": 120,
 */
@property (nonatomic,copy) NSString  *floorage;
/**
 gmtCreated = "2018-12-26 10:13:10";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-26 10:18:31";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 1;
 */
@property (nonatomic,copy) NSString  *id;

/**
 imageskey = "icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *imageskey;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 name = "\U6e29\U6b4c\U534e\U63a2\U79d8\U6ed1\U96ea\U51ac\U4ee4\U8425";
 */
@property (nonatomic,copy) NSString  *name;

/**
 sdate = "2018-12-25 00:00:00";
 */
@property (nonatomic,copy) NSString  *sdate;

/**
 totalnum = 10;
 */
@property (nonatomic,copy) NSString  *totalnum;

/**
 totalprice = 2500;
 */
@property (nonatomic,copy) NSString  *totalprice;

/**
 totalpriceyuan = 25;
 */
@property (nonatomic,copy) NSString  *totalpriceyuan;

/**
 management
 */
@property (nonatomic,copy) NSString  *management;

/**
 ownerrights
 */
@property (nonatomic,copy) NSString  *ownerrights;

/**
 ownershipyear
 */
@property (nonatomic,copy) NSString  *ownershipyear;

/**
 servicefee
 */
@property (nonatomic,copy) NSString  *servicefee;

/**
 servicefeeyuan
 */
@property (nonatomic,copy) NSString  *servicefeeyuan;

@end
