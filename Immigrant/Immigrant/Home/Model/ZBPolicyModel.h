//
//  ZBPolicyModel.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBPolicyModel : NSObject

/**
 content = "\U671d\U9c9c\U4e3e\U884c\U9605\U5175\U4eea\U5f0f\U5e86\U795d\U5efa\U56fd70\U5468\U5e74";
 */
@property (nonatomic,copy) NSString  *content;

/**
 creator = 2;
 */
@property (nonatomic,copy) NSString  *creator;

/**
 gmtCreated = "2018-12-25 17:27:40";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-25 17:27:40";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 4;
 */
@property (nonatomic,copy) NSString  *id;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 title = "\U653f\U7b564";
 */
@property (nonatomic,copy) NSString  *title;

@end
