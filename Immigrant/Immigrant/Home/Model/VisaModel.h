//
//  VisaModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VisaModel : NSObject
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, copy) NSString *maxstaytime;
@property (nonatomic, assign) NSInteger              servicefee;
@property (nonatomic, copy) NSString *creator;

/**
 办理周期
 */
@property (nonatomic, copy) NSString *servietime;


/**
 国家
 */
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, copy) NSString *imageskey;

/**
 面试要求
 */
@property (nonatomic, copy) NSString *interviewrequires;

/**
 折扣（0-1 0.8-八折 0.9-九折 1-无折扣
 */
@property (nonatomic, assign) CGFloat              discount;
@property (nonatomic, copy) NSArray<NSString *> *images;

/**
  办理流程
 */
@property (nonatomic, copy) NSString *process;
@property (nonatomic, copy) NSString *cover;

/**
 servicefeeyuan
 */
@property (nonatomic, assign) CGFloat              servicefeeyuan;
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *vidoesKey;

/**
 签证办理类型
 */
@property (nonatomic, copy) NSString *visatype;
@property (nonatomic, assign) NSInteger              countryId;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, assign) BOOL              isfavorites;

/**
 办理须知
 */
@property (nonatomic, copy) NSString *notice;
@property (nonatomic, copy) NSString *modifier;

/**
 所需材料
 */
@property (nonatomic, copy) NSString *material;

/**
  入境次数
 */
@property (nonatomic, copy) NSString *entriesnum;

/**
 有效期
 */
@property (nonatomic, copy) NSString *expirydate;

/**
 办理说明
 */
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSArray<NSString *> *videos;
@property (nonatomic, assign) BOOL              isactivity;
@end

NS_ASSUME_NONNULL_END
