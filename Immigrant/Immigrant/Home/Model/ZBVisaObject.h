//
//  ZBVisaObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBVisaObject : NSObject

/**
 country = "\U82f1\U56fd"; 国家
 */
@property (nonatomic,copy) NSString *country;

/**
 countryId = 10001; 国家编号
 */
@property (nonatomic,copy) NSString  *countryId;

/**
 cover = "https://file.icebartech.com/img.png"; 封面
 */
@property (nonatomic,copy) NSString  *cover;

/**
 creator = "[SYS]";
 */
@property (nonatomic,copy) NSString  *creator;

/**
 entriesnum = 3; 多次
 */
@property (nonatomic,copy) NSString  *entriesnum;

/**
 expirydate = 3; 有效期
 */
@property (nonatomic,copy) NSString  *expirydate;

/**
 gmtCreated = "2018-12-25 10:00:59";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-25 11:42:41";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 10001;
 */
@property (nonatomic,copy) NSString  *id;

/**
 imageskey = "img.png";
 */
@property (nonatomic,copy) NSString  *imageskey;

/**
 interviewrequires = "\U6d4b\U8bd5\U8bf7\U6c42"; 需要
 */
@property (nonatomic,copy) NSString  *interviewrequires;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 maxstaytime = 3天;
 */
@property (nonatomic,copy) NSString  *maxstaytime;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 name = "\U7f8e\U56fd\U79fb\U6c11\U9879\U76ee";
 */
@property (nonatomic,copy) NSString  *name;

/**
 servicefee = 3000;
 */
@property (nonatomic,copy) NSString  *servicefee;

/**
 servicefeeyuan = 30;
 */
@property (nonatomic,copy) NSString  *servicefeeyuan;

/**
 servietime = 30天左右
 */
@property (nonatomic,copy) NSString  *servietime;

/**
"visatype": "短期签证办理"
 */
@property (nonatomic,copy) NSString  *visatype;

/**
 "images": [
 "string"
 ],
 */
@property (nonatomic,strong) NSArray  *images;

/**
 "material": "所需材料。。。",
 */
@property (nonatomic,copy) NSString  *material;

/**
 "notice": "办理须知。。。",
 */
@property (nonatomic,copy) NSString  *notice;

/**
 "process": "办理流程。。。",
 */
@property (nonatomic,copy) NSString  *process;

/**
 "remark": "办理说明。。。",
 */
@property (nonatomic,copy) NSString  *remark;

/**
 下单之后的出行时间
 */
@property (nonatomic,copy) NSString  *userTraveldate;
/**
 下单之后的用户地址
 */
@property (nonatomic,copy) NSString  *userAddress;


@end
