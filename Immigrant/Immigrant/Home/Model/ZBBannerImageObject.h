//
//  ZBBannerImageObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBBannerImageObject : NSObject

/**
 creator = "[SYS]";
 */
@property (nonatomic,copy) NSString  *creator;

/**
 gmtCreated = "2018-12-25 17:05:32";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-25 17:05:32";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 6;
 */
@property (nonatomic,copy) NSString *id;

/**
 image = "https://file.icebartech.com/icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *image;

/**
 imagekey = "icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *imagekey;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 slidertype = index;
 */
@property (nonatomic,copy) NSString  *slidertype;

/**
 slidertypetext = "\U9996\U9875\U8f6e\U64ad\U56fe";
 */
@property (nonatomic,copy) NSString  *slidertypetext;

/**
 content
 */
@property (nonatomic,copy) NSString  *content;

/**
 title
 */
@property (nonatomic,copy) NSString  *title;

@end
