//
//  MigrateModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MigrateModel : NSObject
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, copy) NSString *country;


@property (nonatomic, copy) NSString *imageskey;
@property (nonatomic, copy) NSString *residencerequires;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, assign) NSInteger              investment;

/**
 投资金额（元
 */
@property (nonatomic, assign) CGFloat              investmentyuan;


/**
 身份证类型
 */
@property (nonatomic, copy) NSString *cardtype;
@property (nonatomic, assign) NSInteger              servicefee;

/**
 服务费（元
 */
@property (nonatomic, assign) CGFloat              servicefeeyuan;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *servietime;
@property (nonatomic, assign) CGFloat              depositfeeyuan;
@property (nonatomic, assign) NSInteger              countryId;


@property (nonatomic, copy) NSArray<NSString *> *images;
@property (nonatomic, assign) BOOL              isactivity;
@property (nonatomic, copy) NSArray<NSString *> *videos;


@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, assign) BOOL              isfavorites;
@property (nonatomic, copy) NSString *questionIds;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, assign) NSInteger              depositfee;
@property (nonatomic, copy) NSString *vidoesKey;

/**
 项目优势
 */
@property (nonatomic, copy) NSString *feature;
/**
 项目信息
 */
@property (nonatomic, copy) NSString *info;
/**
 申请条件
 */
@property (nonatomic, copy) NSString *requires;

/**
 申请流程
 */
@property (nonatomic, copy) NSString *process;
/**
 费用详情
 */
@property (nonatomic, copy) NSString *fee;


/**
 移民方案——国家地区（从地址库获取ID）
 */
@property (nonatomic, assign) NSInteger              require0;

/**
 移民方案——最高学历（0-高中以下 1-高中或中专 2-大专 3-本科或硕士 4-博士）
 */
@property (nonatomic, assign) NSInteger              require1;

/**
 移民方案——移民目的（0-子女教育 1-海外生育 2-养老储备 3-出行便利 4-海外置业 5-投资理财 6-旅游度假 7-税务筹划）
 */
@property (nonatomic, assign) NSInteger              require2;

/**
 移民方案——资金要求（0-100万以内 1-100万-300万 2-300万-500万 3-500万-1000万 4-1000万-3000万 5-3000万以上）
 */
@property (nonatomic, assign) NSInteger              require3;

/**
 移民方案——居住要求（0-不方便居住 1-每年入境一次 2-每年住7天 3-每年住30天 4-每年住半年 5-累计住满2年 ）
 */
@property (nonatomic, assign) NSInteger              require4;

/**
 移民方案——外语要求（0-完全不会 1-大学英语4级以下 2-大学英语6级 3-大学英语6级优秀 4-专业英语8级）
 */
@property (nonatomic, assign) NSInteger              require5;

@property (nonatomic, assign) NSInteger requirefitscore;

@property (nonatomic, assign) BOOL require0fit;
@property (nonatomic, assign) BOOL require1fit;
@property (nonatomic, assign) BOOL require2fit;
@property (nonatomic, assign) BOOL require3fit;
@property (nonatomic, assign) BOOL require4fit;
@property (nonatomic, assign) BOOL require5fit;


@end

NS_ASSUME_NONNULL_END
