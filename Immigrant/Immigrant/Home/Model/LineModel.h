//
//  LineModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LineModel : NSObject

@property (nonatomic, copy) NSString *introduce;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, assign) NSInteger              totalprice;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, assign) NSInteger              depositfeeyuan;
@property (nonatomic, copy) NSString *imageskey;
@property (nonatomic, copy) NSArray<NSString *> *images;
@property (nonatomic, assign) NSInteger              currentnum;
@property (nonatomic, assign) NSInteger              totalpriceyuan;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *vidoesKey;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *info;
@property (nonatomic, assign) NSInteger              countryId;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, assign) BOOL              isfull;
@property (nonatomic, assign) NSInteger              depositfee;
@property (nonatomic, assign) NSInteger              totalnum;
@property (nonatomic, copy) NSString *feeinclude;
@property (nonatomic, copy) NSString *datestr;
@property (nonatomic, copy) NSString *edate;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, copy) NSString *sdate;
@property (nonatomic, copy) NSString *goal;
@property (nonatomic, copy) NSArray<NSString *> *videos;
@property (nonatomic, assign) BOOL              isactivity;
@property (nonatomic, assign) BOOL isfavorites;
@end




