//
//  StudyModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StudyModel :NSObject
@property (nonatomic, assign) BOOL              isactivity;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, assign) NSInteger              totalprice;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, assign) CGFloat              depositfeeyuan;
@property (nonatomic, copy) NSString *imageskey;
@property (nonatomic, copy) NSString *process;
@property (nonatomic, copy) NSArray<NSString *> *images;
@property (nonatomic, assign) CGFloat              totalpriceyuan;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, assign) NSInteger              depositfee;
@property (nonatomic, assign) BOOL              isfavorites;
@property (nonatomic, copy) NSString *notice;
@property (nonatomic, copy) NSString *service;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, copy) NSString *material;
@property (nonatomic, copy) NSString *introduce;

@end

NS_ASSUME_NONNULL_END
