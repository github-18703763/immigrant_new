//
//  ZBStudyObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBStudyObject : NSObject

@property (nonatomic,copy) NSString  *id;

/**
 name
 */
@property (nonatomic,copy) NSString  *name;

/**
 cover
 */
@property (nonatomic,copy) NSString  *cover;

@end
//------------------------------------------学院国家集合
@interface ZBSchoolCountrysObject : NSObject

/**
 "chinesename": "美国",
 */
@property (nonatomic,copy) NSString  *chinesename;

/**
 "creator": "2",
 */
@property (nonatomic,copy) NSString  *creator;
/**
  "englishname": "United States",
 */
@property (nonatomic,copy) NSString  *englishname;

/**
 "gmtCreated": "2018-12-26 16:23:49",
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
"gmtModified": "2018-12-26 16:29:46",
*/
@property (nonatomic,copy) NSString  *gmtModified;

/**
 "icon": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/7.gif?Expires=1546002733&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=m%2F2eC5uuWtGcU2Cz2%2BxcKOmDnkM%3D",
 */
@property (nonatomic,copy) NSString  *icon;
/**
 "iconkey": "icebartech-migrate/7.gif",
*/
@property (nonatomic,copy) NSString  *iconkey;

/**
 "id": 2,
 */
@property (nonatomic,copy) NSString  *id;
/**
 "isDeleted": "n",
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 "modifier": "[SYS]"
 */
@property (nonatomic,copy) NSString  *modifier;

//咨询详情
/**
 "cover": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/7.gif?Expires=1546002733&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=m%2F2eC5uuWtGcU2Cz2%2BxcKOmDnkM%3D",
 */
@property (nonatomic,copy) NSString  *cover;
/**
 "country": "美国",
 */
@property (nonatomic,copy) NSString  *country;

@end

//------------------------------------------轮播图集合
@interface ZBSlidersObject : NSObject
/**
 "creator": "2",
 */
@property (nonatomic,copy) NSString  *creator;
/**
 "gmtCreated": "2018-12-26 16:23:49",
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
 "gmtModified": "2018-12-26 16:29:46",
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 "image": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/7.gif?Expires=1546002733&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=m%2F2eC5uuWtGcU2Cz2%2BxcKOmDnkM%3D",
 */
@property (nonatomic,copy) NSString  *image;
/**
 "imagekey": "icebartech-migrate/7.gif",
 */
@property (nonatomic,copy) NSString  *imagekey;

/**
 "id": 2,
 */
@property (nonatomic,copy) NSString  *id;
/**
 "isDeleted": "n",
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 "modifier": "[SYS]"
 */
@property (nonatomic,copy) NSString  *modifier;
/**
"slidertype": "study",
 */
@property (nonatomic,copy) NSString  *slidertype;

/**
 "slidertypetext": "海外留学轮播图"
 */
@property (nonatomic,copy) NSString  *slidertypetext;
@end


//------------------------------------------留学咨询集合
@interface ZBStudyInfosObject : NSObject
/**
 "country": "美国",
 */
@property (nonatomic,copy) NSString  *country;
/**
 "countryId": 2,
 */
@property (nonatomic,copy) NSString  *countryId;
/**
 "cover": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1546002733&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=XiwXUsigweOyL9vzo4BzmeIYCIU%3D",
 */
@property (nonatomic,copy) NSString  *cover;

/**
  "creator": "2",
 */
@property (nonatomic,copy) NSString  *creator;
/**
 "gmtCreated": "2018-12-28 11:57:51",
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
  "gmtModified": "2018-12-28 11:57:51",
 */
@property (nonatomic,copy) NSString  *gmtModified;
/**
  "id": 3,
 */
@property (nonatomic,copy) NSString  *id;

/**
  "imageskey": "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg",
 */
@property (nonatomic,copy) NSString  *imageskey;
/**
isDeleted": "n",
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 "modifier": "[SYS]",
 */
@property (nonatomic,copy) NSString  *modifier;
/**
 "schoolIds": "2,3,5",
 */
@property (nonatomic,copy) NSString  *schoolIds;

/**
 "title": "大龄客户语言学校申请成功"
 */
@property (nonatomic,copy) NSString  *title;

//------经典案例需要的
/**
 "degree": "研究生",
 */
@property (nonatomic,copy) NSString  *degree;
/**
"school": "哈佛大学",
 */
@property (nonatomic,copy) NSString  *school;
/**
 "schoolId": 2,
 */
@property (nonatomic,copy) NSString  *schoolId;
@end

//------------------------_----------------------学院列表
@interface ZBSchoolListObject : NSObject
/**
 "chinesename": "哈佛大学",
 */
@property (nonatomic,copy) NSString  *chinesename;
/**
 "country": "英国",
 */
@property (nonatomic,copy) NSString  *country;
/**
"countryId": 0,
 */
@property (nonatomic,copy) NSString  *countryId;
/*
    "cover": "cover.png",
 */
@property (nonatomic,copy) NSString  *cover;
/**
 "creator": "2",
 */
@property (nonatomic,copy) NSString  *creator;
/*
    "englishname": "Britain"
 */
@property (nonatomic,copy) NSString  *englishname;
/**
 "gmtCreated": "2018-12-28 11:57:51",
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 "gmtModified": "2018-12-28 11:57:51",
 */
@property (nonatomic,copy) NSString  *gmtModified;
/**
 "id": 3,
 */
@property (nonatomic,copy) NSString  *id;
/*
    "images": "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg",
 */
@property (nonatomic,copy) NSString  *images;
/**
 "imageskey": "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg",
 */
@property (nonatomic,copy) NSString  *imageskey;
/**
 "introduce": "学院简介。。。",
 */
@property (nonatomic,copy) NSString  *introduce;

/**
"isDeleted": "string",
 */
@property (nonatomic,copy) NSString  *isDeleted;
/**
 "isrecommend": false,
 */
@property (nonatomic,copy) NSString  *isrecommend;

/**
 "modifier": "string",
 */
@property (nonatomic,copy) NSString  *modifier;
/**
  "profess": "经济金融,商业管理",
 */
@property (nonatomic,copy) NSString  *profess;
/**
  "qs": 43,
 */
@property (nonatomic,copy) NSString  *qs;
/**
"region": "牛津",
 */
@property (nonatomic,copy) NSString  *region;
/**
 "regionId": 0,
 */
@property (nonatomic,copy) NSString  *regionId;
/**
  "threshold": "申请门槛。。。",
 */
@property (nonatomic,copy) NSString  *threshold;
/**
 "tuimes": 24
 */
@property (nonatomic,copy) NSString  *tuimes;

@end
