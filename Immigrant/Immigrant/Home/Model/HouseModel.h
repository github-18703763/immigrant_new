//
//  HouseModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HouseModel : NSObject
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, copy) NSString *deliverystandard;
@property (nonatomic, copy) NSString *country;

/**
 总售价（元）
 */
@property (nonatomic, assign) CGFloat              totalpriceyuan;
@property (nonatomic, assign) NSInteger              downpayment;

/**
 地点
 */
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *deliverytime;

/**
 购房流程
 */
@property (nonatomic, copy) NSString *process;

/**
 在售户型
 */
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *ownershipyear;
@property (nonatomic, copy) NSString *imageskey;

/**
 办理费（元）
 */
@property (nonatomic, assign) CGFloat              servicefeeyuan;
@property (nonatomic, copy) NSString *cover;

/**
 投资评估
 */
@property (nonatomic, copy) NSString *investment;

/**
 总售价（元）（文字描述)
 */
@property (nonatomic, copy) NSString *totalpriceyuanstr;
@property (nonatomic, assign) NSInteger              servicefee;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger              annuprofit;

/**
 定金（元)
 */
@property (nonatomic, assign) CGFloat              depositfeeyuan;
@property (nonatomic, assign) NSInteger              countryId;

/**
 项目特色
 */
@property (nonatomic, copy) NSString *feature;

/**
 图集
 */
@property (nonatomic, copy) NSArray<NSString *> *images;
@property (nonatomic, assign) BOOL              isactivity;
@property (nonatomic, copy) NSString *management;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, copy) NSString *ownerrights;
@property (nonatomic, assign) NSInteger              totalprice;

/**
 周边设施
 */
@property (nonatomic, copy) NSString *facility;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, assign) NSInteger              depositfee;
@property (nonatomic, assign) NSInteger              floorage;
@property (nonatomic, assign) NSInteger              increaserate;

@property (nonatomic, assign) BOOL isfavorites;

@end

