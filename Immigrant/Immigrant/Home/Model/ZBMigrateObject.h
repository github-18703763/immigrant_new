//
//  ZBMigrateObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBMigrateObject : NSObject

/**
 cardtype = "\U62a4\U7167";
 */
@property (nonatomic,copy) NSString  *cardtype;
/**
country = "\U82f1\U56fd";
*/
@property (nonatomic,copy) NSString  *country;

/**
countryId = 10001;
*/
@property (nonatomic,copy) NSString  *countryId;

/**
cover = "https://file.icebartech.com/icebartech-gcsgame/241.jpg";
*/
@property (nonatomic,copy) NSString  *cover;

/**
creator = "[SYS]";
*/
@property (nonatomic,copy) NSString  *creator;

/**
depositfee = 500;
*/
@property (nonatomic,copy) NSString  *depositfee;

/**
depositfeeyuan = 5;
*/
@property (nonatomic,copy) NSString  *depositfeeyuan;

/**
gmtCreated = "2018-12-26 10:15:17";
*/
@property (nonatomic,copy) NSString  *gmtCreated;

/**
gmtModified = "2018-12-26 10:18:26";
*/
@property (nonatomic,copy) NSString  *gmtModified;

/**
id = 1;
*/
@property (nonatomic,copy) NSString  *id;

/**
imageskey = "icebartech-gcsgame/241.jpg";
*/
@property (nonatomic,copy) NSString  *imageskey;

/**
investment = 23463000;
*/
@property (nonatomic,copy) NSString  *investment;

/**
investmentyuan = 234630;
*/
@property (nonatomic,copy) NSString  *investmentyuan;

/**
isDeleted = n;
*/
@property (nonatomic,copy) NSString  *isDeleted;

/**
modifier = "[SYS]";
*/
@property (nonatomic,copy) NSString  *modifier;

/**
name = "\U7f8e\U56fdEB5\U6295\U8d44\U79fb\U6c11";
*/
@property (nonatomic,copy) NSString  *name;

/**
residencerequires = "\U65e0\U8981\U6c42";
*/
@property (nonatomic,copy) NSString  *residencerequires;

/**
servicefee = 12888900;
*/
@property (nonatomic,copy) NSString  *servicefee;

/**
servicefeeyuan = 128889;
*/
@property (nonatomic,copy) NSString  *servicefeeyuan;

/**
servietime = "30\U5929\U5de6\U53f3";
*/
@property (nonatomic,copy) NSString  *servietime;

@end
