//
//  ZBInspectionObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBInspectionObject : NSObject

/**
 country = "\U82f1\U56fd";
 */
@property (nonatomic,copy) NSString  *country;
/**
countryId = 10001;
*/
@property (nonatomic,copy) NSString  *countryId;
/**
cover = "https://file.icebartech.com/icebartech-gcsgame/241.jpg";
*/
@property (nonatomic,copy) NSString  *cover;
/**
creator = "[SYS]";
*/
@property (nonatomic,copy) NSString  *creator;
/**
depositfee = 2222;
*/
@property (nonatomic,copy) NSString  *depositfee;
/**
depositfeeyuan = "22.22";
*/
@property (nonatomic,copy) NSString  *depositfeeyuan;
/**
edate = "2018-12-30 00:00:00";
*/
@property (nonatomic,copy) NSString  *edate;
/**
gmtCreated = "2018-12-26 10:09:24";
*/
@property (nonatomic,copy) NSString  *gmtCreated;
/**
gmtModified = "2018-12-26 10:18:51";
*/
@property (nonatomic,copy) NSString  *gmtModified;
/**
goal = dfgdfhfh;
*/
@property (nonatomic,copy) NSString  *goal;
/**
id = 1;
*/
@property (nonatomic,copy) NSString  *id;
/**
imageskey = "icebartech-gcsgame/241.jpg";
*/
@property (nonatomic,copy) NSString  *imageskey;
/**
isDeleted = n;
*/
@property (nonatomic,copy) NSString  *isDeleted;
/**
modifier = "[SYS]";
*/
@property (nonatomic,copy) NSString  *modifier;
/**
name = "\U6d4b\U8bd5\U8def\U7ebf";
*/
@property (nonatomic,copy) NSString  *name;
/**
sdate = "2018-12-26 00:00:00";
*/
@property (nonatomic,copy) NSString  *sdate;
/**
totalnum = 5;
*/
@property (nonatomic,copy) NSString  *totalnum;

/**
 currentnum = 5; //当前人数
 */
@property (nonatomic,copy) NSString  *currentnum;
/**
totalprice = 2300;
*/
@property (nonatomic,copy) NSString  *totalprice;
/**
totalpriceyuan = 23; 每个人的价格
*/
@property (nonatomic,copy) NSString  *totalpriceyuan;

@end
