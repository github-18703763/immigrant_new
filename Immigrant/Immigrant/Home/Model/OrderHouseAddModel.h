//
//  OrderHouseAddModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderHouseAddModel : NSObject

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userIdcard;
@property (nonatomic, assign) NSInteger              houseId;
@property (nonatomic, copy) NSString *userMobile;
@property (nonatomic, copy) NSString *userEmail;

@end

NS_ASSUME_NONNULL_END
