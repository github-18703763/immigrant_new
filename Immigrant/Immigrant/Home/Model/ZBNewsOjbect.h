//
//  ZBNewsOjbect.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBNewsOjbect : NSObject

/**
 cover = "https://file.icebartech.com/icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *cover;

/**
 coverkey = "icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *coverkey;

/**
 creator = "[SYS]";
 */
@property (nonatomic,copy) NSString  *creator;

/**
 gmtCreated = "2018-12-25 17:14:13";
 */
@property (nonatomic,copy) NSString *gmtCreated;

/**
 gmtModified = "2018-12-25 17:14:13";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 9;
 */
@property (nonatomic,copy) NSString  *id;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 title = "\U6295\U8d44\U79fb\U6c1125\U56fd\U5bf9\U6bd4\Uff0c\U975e\U6d32\U7adf\U4e0d\U8f93\U6b27\U7f8e\Uff01";
 */
@property (nonatomic,copy) NSString  *title;

/**
 content
 */
@property (nonatomic,copy) NSString  *content;

@end
