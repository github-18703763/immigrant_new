//
//  LineModel.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "LineModel.h"

@implementation LineModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"images" : [NSString class],
             @"videos" : [NSString class],
             };
}

@end
