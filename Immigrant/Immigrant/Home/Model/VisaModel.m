//
//  VisaModel.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "VisaModel.h"

@implementation VisaModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"images" : [NSString class],
             @"videos" : [NSString class],
             };
}

@end
