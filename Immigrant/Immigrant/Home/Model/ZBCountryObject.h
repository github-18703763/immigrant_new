//
//  ZBCountryObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/28.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBCountryObject : NSObject

/**
 chinesename = "\U7f8e\U56fd";
 */
@property (nonatomic,copy) NSString  *chinesename;
/**
creator = 2;
*/
@property (nonatomic,copy) NSString  *creator;
/**
englishname = "United States";
*/
@property (nonatomic,copy) NSString  *englishname;
/**
gmtCreated = "2018-12-26 16:23:49";
*/
@property (nonatomic,copy) NSString  *gmtCreated;
/**
gmtModified = "2018-12-26 16:29:46";
*/
@property (nonatomic,copy) NSString  *gmtModified;
/**
iconkey = "icebartech-migrate/7.gif";
*/
@property (nonatomic,copy) NSString  *iconkey;
/**
id = 2;
*/
@property (nonatomic,copy) NSString  *id;
/**
isDeleted = n;
*/
@property (nonatomic,copy) NSString  *isDeleted;
/**
modifier = "[SYS]";
*/
@property (nonatomic,copy) NSString  *modifier;

@end

NS_ASSUME_NONNULL_END
