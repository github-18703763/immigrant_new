//
//  OrderCampAddModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderCampAddModel : NSObject

@property (nonatomic, assign) NSInteger              booknum;
@property (nonatomic, assign) NSInteger              campId;
@property (nonatomic, copy) NSString *userMobile;
@property (nonatomic, copy) NSString *userName;


@end

NS_ASSUME_NONNULL_END
