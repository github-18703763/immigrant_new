//
//  OrderLineAddModel.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderLineAddModel : NSObject

@property (nonatomic, assign) NSInteger              lineId;
@property (nonatomic, assign) NSInteger              booknum;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userIdcard;
@property (nonatomic, copy) NSString *userGoal;
@property (nonatomic, copy) NSString *userMobile;
@property (nonatomic, copy) NSString *userEmail;

@end

