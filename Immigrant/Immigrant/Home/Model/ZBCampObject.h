//
//  ZBCampObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//
//冬夏令营
#import <Foundation/Foundation.h>

@interface ZBCampObject : NSObject

/**
 address = "\U6e29\U6b4c\U534e";
 */
@property (nonatomic,copy) NSString  *address;

/**
 country = "\U7f8e\U56fd";
 */
@property (nonatomic,copy) NSString  *country;

/**
 countryId = 10002;
 */
@property (nonatomic,copy) NSString  *countryId;

/**
 cover = "https://file.icebartech.com/icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *cover;

/**
 creator = "[SYS]";
 */
@property (nonatomic,copy) NSString  *creator;

/**
 depositfee = 888800;
 */
@property (nonatomic,copy) NSString  *depositfee;

/**
 depositfeeyuan = 8888;
 */
@property (nonatomic,copy) NSString  *depositfeeyuan;

/**
 edate = "2018-12-30 00:00:00";
 */
@property (nonatomic,copy) NSString  *edate;

/**
 gmtCreated = "2018-12-26 10:13:10";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-26 10:18:31";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 1;
 */
@property (nonatomic,copy) NSString  *id;

/**
 imageskey = "icebartech-gcsgame/241.jpg";
 */
@property (nonatomic,copy) NSString  *imageskey;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 name = "\U6e29\U6b4c\U534e\U63a2\U79d8\U6ed1\U96ea\U51ac\U4ee4\U8425";
 */
@property (nonatomic,copy) NSString  *name;

/**
 sdate = "2018-12-25 00:00:00";
 */
@property (nonatomic,copy) NSString  *sdate;

/**
 totalnum = 10;
 */
@property (nonatomic,copy) NSString  *totalnum;

/**
 totalprice = 2500;
 */
@property (nonatomic,copy) NSString  *totalprice;

/**
 totalpriceyuan = 25;
 */
@property (nonatomic,copy) NSString  *totalpriceyuan;
/**
 currentnum = 25; 当前人数
 */
@property (nonatomic,copy) NSString  *currentnum;

@end

@interface CampModel :NSObject
@property (nonatomic, copy) NSString *introduce;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, assign) NSInteger              totalprice;
@property (nonatomic, copy) NSString *plan;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, assign) CGFloat              depositfeeyuan;
@property (nonatomic, copy) NSString *imageskey;
@property (nonatomic, copy) NSArray<NSString *> *images;
@property (nonatomic, assign) NSInteger              currentnum;
@property (nonatomic, assign) CGFloat              totalpriceyuan;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger              countryId;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, assign) BOOL              isfull;
@property (nonatomic, assign) NSInteger              depositfee;
@property (nonatomic, assign) NSInteger              totalnum;
@property (nonatomic, copy) NSString *notice;
@property (nonatomic, copy) NSString *datestr;
@property (nonatomic, copy) NSString *edate;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, copy) NSString *sdate;
@property (nonatomic, assign) BOOL              isactivity;
@property (nonatomic, assign) BOOL isfavorites;
@end
