//
//  ZBOverseasStudySchoolChildController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySchoolChildController.h"
#import "ZBOverseasStudySchoolChildOneCell.h"
#import "ZBOverseasStudySchoolDetailController.h"
#import "ZBOverseasStudySchoolChildTwoCell.h"
#import "ZBStudyObject.h"

@interface ZBOverseasStudySchoolChildController ()
@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;
@end

@implementation ZBOverseasStudySchoolChildController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self.title isEqualToString:@"推荐"]) {
        UIImageView *img = [[UIImageView alloc] init];
        [img sd_setImageWithURL:[NSURL URLWithString:@"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1183298141,4129650466&fm=27&gp=0.jpg"]];
        img.frame = CGRectMake(0, 0, SystemScreenWidth, 320);
        self.tableView.tableHeaderView = img;
        UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 320) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = path.bounds;
        maskLayer.path=path.CGPath;
        img.layer.mask=maskLayer;
    
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudySchoolChildOneCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudySchoolChildTwoCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
    
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
/*
 ascOrderBy (string, optional): TUIMES升序则填：tuimes，QS升序则填：qs，。两者都升序则填：tuimes,qs ,
 chinesenameLike: 中文名 ,
 countryId (integer, optional): 国家主键 ,
 descOrderBy (string, optional): TUIMES降序则填：tuimes，QS升序则填：qs，。两者都降降序则填：tuimes,qs ,
 isrecommend (boolean, optional): 是否推荐 ,
 pageIndex (integer, optional): 第几页，默认第一页 ,
 pageSize (integer, optional): 每页大小，默认10 ,
 professLike (string, optional): 专业 ,
 regionIdIn (Array[integer], optional): 地区主键集合
 */
    [QSNetworkManager schoolFindpageChinesenameLike:@"" withAscOrderBy:@"" withDescOrderBy:@"" withIsrecommend:false withCountryId:@"" withProfessLike:@"" withRegionIdIn:@[] withPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBSchoolListObject mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBSchoolListObject mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if ([self.title isEqualToString:@"推荐"]) {
        return self.newsArray.count;
//    }else{
//        return 20;
//    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.title isEqualToString:@"推荐"]) {
        ZBOverseasStudySchoolChildOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.isCornor = NO;
        if (indexPath.row == 9) {
             cell.isCornor = YES;
        }
        cell.modelSchoolList = self.newsArray[indexPath.row];
        return cell;
    }else{
        ZBOverseasStudySchoolChildTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.modelSchoolList = self.newsArray[indexPath.row];
        return cell;
    }
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (![self.title isEqualToString:@"推荐"]) {
        //头部预留位置
    }
    return nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (![self.title isEqualToString:@"推荐"]) {
        return 50;
    }
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.title isEqualToString:@"推荐"]) {
        return 71;
    }else{
        return 115;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBOverseasStudySchoolDetailController *detail = [ZBOverseasStudySchoolDetailController new];
    detail.modelSchoolList = self.newsArray[indexPath.row];
    [self.navigationController pushViewController:detail animated:YES];
}

@end
