//
//  ZBOverseasStudyApplyDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyApplyDetailController.h"
#import "ZBOverseaStudySchoolApplyController.h"
#import "StudyModel.h"
#import "StudySignupController.h"
#import "QSNetworkManager+order.h"
#import <YYModel.h>
@interface ZBOverseasStudyApplyDetailController ()

@property (weak, nonatomic) IBOutlet UIButton *handleBtn;

@property (nonatomic, strong) StudyModel *detailModel;

@end

@implementation ZBOverseasStudyApplyDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.handleBtn.layer.cornerRadius = 25;
    self.handleBtn.layer.masksToBounds = YES;
    
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchData {
    [QSNetworkManager studyFindDetail:self.studyId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            NSDictionary *dic = responseModel.data[@"bussData"];
            self.detailModel = [StudyModel yy_modelWithDictionary:dic];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

- (IBAction)messageAction:(id)sender {
    
//    ZBLog(@"咨询");
    
}

- (IBAction)handleAction:(id)sender {
    
//    ZBOverseaStudySchoolApplyController *apply = [ZBOverseaStudySchoolApplyController new];
//    [self.navigationController pushViewController:apply animated:YES];
    if (self.detailModel) {
        StudySignupController *vc = [[StudySignupController alloc] init];
        vc.model = self.detailModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


@end
