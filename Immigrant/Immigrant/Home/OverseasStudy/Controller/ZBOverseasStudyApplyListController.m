//
//  ZBOverseasStudyApplyListController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyApplyListController.h"
#import "ZBOverseasStudyApplyListCell.h"
#import "ZBOverseasStudyApplyDetailController.h"

@interface ZBOverseasStudyApplyListController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;
@property (weak, nonatomic) IBOutlet UITableView *applyTable;

@end

@implementation ZBOverseasStudyApplyListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchTxt.layer.cornerRadius = 6;
    self.searchTxt.layer.masksToBounds = YES;
    self.title = @"留学产品申请";
    self.applyTable.showsVerticalScrollIndicator = NO;
    self.applyTable.showsHorizontalScrollIndicator = NO;
    self.applyTable.separatorColor = [UIColor clearColor];
    self.applyTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    [self.applyTable registerNib:[UINib nibWithNibName:@"ZBOverseasStudyApplyListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.applyTable.tableFooterView = [UIView new];
    self.applyTable.delegate = self;
    self.applyTable.dataSource = self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 30;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBOverseasStudyApplyListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBOverseasStudyApplyDetailController *detail = [ZBOverseasStudyApplyDetailController new];
    detail.studyId = 1;
    [self.navigationController pushViewController:detail animated:YES];
}


@end
