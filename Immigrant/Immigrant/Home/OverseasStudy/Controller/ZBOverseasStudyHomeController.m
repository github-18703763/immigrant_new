//
//  ZBOverseasStudyHomeController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyHomeController.h"
#import "ZBOverseasStudyContryCell.h"
#import "ZBOverseasStudyConsultingCell.h"
#import "ZBOverseasStudyHeadView.h"
#import "ZBOverseasStudyOneSectionHeadView.h"
#import "ZBOverseasStudyTwoSectionHeadView.h"
#import "ZBOverseasStudySchoolListController.h"
#import "ZBOverseasStudyConsultingDetailController.h"
#import "ZBOverseasStudyConsultingListController.h"
#import "ZBOverseasStudySuccessListController.h"
#import "ZBOverseasStudyApplyListController.h"
#import "ZBOverseasStudySchoolDetailController.h"
#import "ZBStudyObject.h"
#import "StudyApplyListController.h"
@interface ZBOverseasStudyHomeController ()
@property (nonatomic,strong)NSArray *schoolCountrys;//学院国家集合
@property (nonatomic,strong)NSArray *sliders;//轮播图集合
@property (nonatomic,strong)NSArray *studyInfos;//留学咨询集合
@end

@implementation ZBOverseasStudyHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"海外留学";
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyContryCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyConsultingCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
    UIView *v = [UIView new];
    v.backgroundColor = COLOR(252, 252, 252, 1);
    v.frame = CGRectMake(0, 0, SystemScreenWidth, 1);
    self.tableView.tableFooterView = v;
    ZBOverseasStudyHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBOverseasStudyHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 190);
    self.tableView.tableHeaderView = headView;
    //数组请求
    [QSNetworkManager findStudyIndexSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            //
            NSDictionary *dic = responseModel.data[@"bussData"];
            headView.dataArray = dic[@"sliders"];
            self.schoolCountrys = [ZBSchoolCountrysObject mj_objectArrayWithKeyValuesArray:dic[@"schoolCountrys"]];
            self.studyInfos = [ZBStudyInfosObject mj_objectArrayWithKeyValuesArray:dic[@"studyInfos"]];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
           [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.schoolCountrys.count;
    }else{
        return self.studyInfos.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ZBOverseasStudyContryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.modelSchoolCountry = self.schoolCountrys[indexPath.row];
        return cell;
    }else{
        ZBOverseasStudyConsultingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.modelStudyInfos = self.studyInfos[indexPath.row];
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 70;
    }else{
        return 145;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *v = [UIView new];
    v.backgroundColor = COLOR(252, 252, 252, 1);
    return v;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        ZBOverseasStudyOneSectionHeadView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"ZBOverseasStudyOneSectionHeadView" owner:self options:nil] firstObject];
        sectionView.introduceBlock = ^{
            //学院介绍
            ZBOverseasStudySchoolDetailController *study = [ZBOverseasStudySchoolDetailController new];
            [self.navigationController pushViewController:study animated:YES];
        };
        return sectionView;
    }else{
        WS(weakSELF);
        ZBOverseasStudyTwoSectionHeadView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"ZBOverseasStudyTwoSectionHeadView" owner:self options:nil] firstObject];
        sectionView.applyBlock = ^{
          //申请列表
            StudyApplyListController *vc = [[StudyApplyListController alloc] init];
//            ZBOverseasStudyApplyListController *list = [ZBOverseasStudyApplyListController new];
            [weakSELF.navigationController pushViewController:vc animated:YES];
        };
        sectionView.listBlock = ^{
          //成功案列
            ZBOverseasStudySuccessListController *list = [ZBOverseasStudySuccessListController new];
            [weakSELF.navigationController pushViewController:list animated:YES];
        };
        sectionView.contingBlock = ^{
          //咨询列表
            ZBOverseasStudyConsultingListController *list = [ZBOverseasStudyConsultingListController new];
            [weakSELF.navigationController pushViewController:list animated:YES];
        };
        return sectionView;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 50;
    }else{
        return 175;
    }
  
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        ZBOverseasStudySchoolListController *school = [ZBOverseasStudySchoolListController new];
        [self.navigationController pushViewController:school animated:YES];
    }else{
        ZBOverseasStudyConsultingDetailController *detail = [ZBOverseasStudyConsultingDetailController new];
        detail.modelStudyInfo = self.studyInfos[indexPath.row];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

@end
