//
//  ApplyDetailController.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/18.
//  Copyright © 2019 张波. All rights reserved.
//

#import "ListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface StudyApplyDetailController : ListViewController

@property (nonatomic, assign) NSInteger studyId;

@end

NS_ASSUME_NONNULL_END
