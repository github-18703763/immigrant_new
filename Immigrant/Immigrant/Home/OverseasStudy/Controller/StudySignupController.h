//
//  StudySignupController.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface StudySignupController : UIViewController

@property (nonatomic, strong) StudyModel *model;

@end

NS_ASSUME_NONNULL_END
