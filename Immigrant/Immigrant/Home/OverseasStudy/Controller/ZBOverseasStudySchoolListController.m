//
//  ZBOverseasStudySchoolListController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySchoolListController.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "ZBOverseasStudySchoolChildController.h"

@interface ZBOverseasStudySchoolListController ()<FSPageContentViewDelegate,FSSegmentTitleViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;

@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) NSArray *segmentTitlesArr;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@end

@implementation ZBOverseasStudySchoolListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"学院列表";
    self.searchTxt.layer.cornerRadius = 22.5;
    self.searchTxt.layer.masksToBounds = YES;
    
    self.segmentTitlesArr = @[@"推荐",@"专业",@"地区",@"TUIMES排名",@"QS排名"];
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 82,SystemScreenWidth, 50) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeEqualTitle];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:18];
    self.titleView.indicatorColor = [UIColor whiteColor];
    self.titleView.backgroundColor = [UIColor clearColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:15];
    self.titleView.titleNormalColor = [UIColor whiteColor];
    self.titleView.titleSelectColor = [UIColor whiteColor];
    [self.view addSubview:self.titleView];
    self.titleView.selectIndex = 0;
    [self.titleView layoutSubviews];
    [self.view bringSubviewToFront:self.titleView];
    
    NSMutableArray *childVCs = [[NSMutableArray alloc]init];
    for (NSString *title in self.segmentTitlesArr) {
        ZBOverseasStudySchoolChildController *vc = [[ZBOverseasStudySchoolChildController alloc]init];
        vc.title = title;
        [childVCs addObject:vc];
    }
    self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 138, SystemScreenWidth, SystemScreenHeight-self.navigationController.navigationBar.frame.size.height-138-StatusbarHeight) childVCs:childVCs parentVC:self delegate:self];
    self.pageContentView.contentViewCurrentIndex = 0;
    [self.view addSubview:self.pageContentView];
}

#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
