//
//  ZBOverSeasStudyChooseContryController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverSeasStudyChooseContryController.h"
#import "ZBOverseasStudyContryCell.h"

@interface ZBOverSeasStudyChooseContryController ()

@end

@implementation ZBOverSeasStudyChooseContryController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyContryCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    
    UIView *v = [UIView new];
    v.backgroundColor = COLOR(252, 252, 252, 1);
    v.frame = CGRectMake(0, 0, SystemScreenWidth, 22);
    self.tableView.tableHeaderView = v;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZBOverseasStudyContryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

@end
