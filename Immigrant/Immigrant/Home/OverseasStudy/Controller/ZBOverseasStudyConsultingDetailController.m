//
//  ZBOverseasStudyConsultingDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyConsultingDetailController.h"
#import "ZBOverseasStudyConsultingDetailCell.h"
#import "ZBOverseasStudyConsultingDetailHeaderView.h"
#import "ZBStudyObject.h"
@interface ZBOverseasStudyConsultingDetailController ()
@property (nonatomic,strong)ZBOverseasStudyConsultingDetailHeaderView *headView;
@property (nonatomic,strong)NSDictionary *dicAll;

@end

@implementation ZBOverseasStudyConsultingDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"咨询详情";
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.tableFooterView = [UIView new];
    self.headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBOverseasStudyConsultingDetailHeaderView" owner:self options:nil] firstObject];
//    97
//    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 120);
    self.tableView.tableHeaderView = self.headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyConsultingDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.rowHeight = 130;
    
    [QSNetworkManager studyinfoDetial:self.modelStudyInfo.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.dicAll = responseModel.data[@"bussData"];
            NSString *webViewContent = self.dicAll[@"content"];
            self.headView.frame = CGRectMake(0, 0, SystemScreenWidth, [self getWebViewHeight: webViewContent]);
            self.headView.dicAll = self.dicAll;
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *ary = self.dicAll[@"schools"];
    return ary.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBOverseasStudyConsultingDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//     weakSELF.newsArray = [ZBInspectionObject mj_objectArrayWithKeyValuesArray:array];
     NSArray *ary = self.dicAll[@"schools"];
    NSArray *newArray = [ZBSchoolCountrysObject mj_objectArrayWithKeyValuesArray:ary];
    cell.model = newArray[indexPath.row];
    return cell;
}

- (CGFloat )getWebViewHeight:(NSString *)content{
  
    content = [NSString stringWithFormat:content,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.width-15];
    
    [self.headView.webView stringByEvaluatingJavaScriptFromString:content];
    [self.headView.webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];
    
    NSInteger width = [[self.headView.webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollWidth"] integerValue];
    CGFloat bili = width/SystemScreenWidth;
    NSInteger height = [[self.headView.webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] integerValue];
    return height/bili;
}
@end
