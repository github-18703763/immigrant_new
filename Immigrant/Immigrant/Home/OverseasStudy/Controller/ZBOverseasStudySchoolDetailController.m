//
//  ZBOverseasStudySchoolDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySchoolDetailController.h"
#import "ZBOverseasStudySchoolDetailCell.h"
#import "ZBRealEstateDetailHeaderView.h"
#import "ZBStudyObject.h"
@interface ZBOverseasStudySchoolDetailController ()

@end

@implementation ZBOverseasStudySchoolDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"学院介绍";
//    ZBRealEstateDetailHeaderView
    ZBRealEstateDetailHeaderView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBRealEstateDetailHeaderView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 355);    self.tableView.tableHeaderView = headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudySchoolDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 200;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    schoolDetial
    [QSNetworkManager schoolDetial:self.modelSchoolList.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            headView.dicSchoolList = responseModel.data[@"bussData"];
            
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBOverseasStudySchoolDetailCell*cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
