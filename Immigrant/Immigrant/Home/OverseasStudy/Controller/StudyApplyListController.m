//
//  StudyApplyListController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/18.
//  Copyright © 2019 张波. All rights reserved.
//

#import "StudyApplyListController.h"
#import "ZBOverseasStudyApplyListCell.h"
#import "QSNetworkManager+Order.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import <YYModel.h>
#import "StudyModel.h"
#import "StudyApplyDetailController.h"
@interface StudyApplyListController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, copy) NSString *searchString;

@end

@implementation StudyApplyListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView.mj_header beginRefreshing];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyApplyListCell" bundle:nil] forCellReuseIdentifier:@"ZBOverseasStudyApplyListCell"];
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.title = @"留学产品申请";
    
    [self.view addSubview:self.searchBar];
    [self masLayout];
    self.view.backgroundColor = UIColor.whiteColor;
}

- (void)masLayout {
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(21);
        make.right.equalTo(self.view).offset(-21);
        make.height.mas_equalTo(40);
        make.top.equalTo(self.view).offset(5);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.searchBar.mas_bottom).offset(20);
    }];
    
}

#pragma mark: - Private Methods

- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh {
    if (isRefresh) {
        self.pageIndex = 1;
    }
    
    [QSNetworkManager studyFindPage:self.pageIndex nameLike:self.searchString successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
//            NSDictionary *dic = responseModel.data[@"bussData"];
            NSArray *array = [NSArray yy_modelArrayWithClass:[StudyModel class] json:responseModel.data[@"bussData"]];
            [self handleResponseData:array isRefresh:isRefresh];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

#pragma mark: - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBOverseasStudyApplyListCell *cell = [ZBOverseasStudyApplyListCell cellForTableView:tableView];
    [cell configCell:self.tableData[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    StudyModel *model = self.tableData[indexPath.row];
    
    StudyApplyDetailController *vc = [[StudyApplyDetailController alloc] init];
    vc.studyId = model.id;
    [self.navigationController pushViewController:vc animated:true];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchString = searchText;
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - Getter

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        
        UIView *searchBarBg = [_searchBar valueForKeyPath:@"_background"];
        [searchBarBg removeFromSuperview];
        
        UITextField *textField = [_searchBar valueForKeyPath:@"searchField"];
        textField.backgroundColor = [UIColor getColor:@"EDEDF0"];
        //移除灰色背景之后，没有边框了，通过layer设置
        _searchBar.layer.cornerRadius = 6.25;
        _searchBar.layer.masksToBounds = YES;
        _searchBar.placeholder = @"搜索留学产品名称";
        _searchBar.backgroundColor = [UIColor getColor:@"EDEDF0"];
    }
    return _searchBar;
}



@end
