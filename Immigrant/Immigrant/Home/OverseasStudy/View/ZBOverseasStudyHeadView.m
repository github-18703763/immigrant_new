//
//  ZBOverseasStudyHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyHeadView.h"
#import "XRCarouselView.h"

@interface ZBOverseasStudyHeadView()<XRCarouselViewDelegate>

@property (nonatomic,strong)XRCarouselView *photoView;

@end

@implementation ZBOverseasStudyHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.photoView.layer.cornerRadius = 15;
    self.photoView.layer.masksToBounds = YES;
    
//    XRCarouselView *xr=[[XRCarouselView alloc]initWithFrame:CGRectMake(25,0,SystemScreenWidth-50,180) imageArray:@[@"https://goss.veer.com/creative/vcg/veer/612/veer-134298137.jpg",@"https://goss1.veer.com/creative/vcg/veer/612/veer-302764281.jpg",@"https://goss2.veer.com/creative/vcg/veer/612/veer-130129793.jpg",@"https://goss3.veer.com/creative/vcg/veer/612/veer-134579925.jpg"]];
//    xr.delegate=self;
//    xr.pagePosition = PositionHide;
//    xr.layer.cornerRadius = 15;
//    xr.layer.masksToBounds = YES;
//    [self addSubview:xr];
    
}
- (void)setDataArray:(NSArray *)dataArray{
    _dataArray = dataArray;
    if (self.photoView) {
        [self.photoView removeFromSuperview];
    }
//    ZBLog(@"-----%li",self.dataArray.count);
    NSMutableArray *photoArray = [[NSMutableArray alloc]initWithCapacity:0];
    for (NSDictionary *dic in self.dataArray) {
        [photoArray addObject:dic[@"image"]];
    }
//    ZBLog(@"-----%li",photoArray.count);
//    XRCarouselView *xr=[[XRCarouselView alloc]initWithFrame:CGRectMake(0,0,SystemScreenWidth,350) imageArray:imageArray];
//    xr.desLabelBgColor = [UIColor clearColor];
//    xr.desLabelColor = [UIColor whiteColor];
//    xr.desLabelFont = [UIFont systemFontOfSize:15];
//    //    xr.describeArray = desArray;
//    //    xr.describeArray1 = desArray1;
//    //    xr.describeArray2 = desArray2;
//    xr.delegate=self;
//    xr.pagePosition = PositionBottomRight;
//    [self addSubview:xr];
    
    self.photoView = [[XRCarouselView alloc]initWithFrame:CGRectMake(0,0,SystemScreenWidth,180) imageArray:photoArray];
//    self.photoView.imageArray = photoArray;
    self.photoView.delegate=self;
    self.photoView.pagePosition = PositionHide;
    [self addSubview:self.photoView];
}
- (void)carouselView:(XRCarouselView *)carouselView didClickImage:(NSInteger)index{

}

@end
