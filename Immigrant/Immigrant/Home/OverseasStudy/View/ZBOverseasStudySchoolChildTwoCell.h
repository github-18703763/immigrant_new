//
//  ZBOverseasStudySchoolChildTwoCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBStudyObject.h"
@interface ZBOverseasStudySchoolChildTwoCell : UITableViewCell

@property (nonatomic,strong)ZBSchoolListObject *modelSchoolList;//学院列表

@end
