//
//  ZBOverseasStudySuccessDetailCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySuccessListCell.h"
@interface ZBOverseasStudySuccessListCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;

@end
@implementation ZBOverseasStudySuccessListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
//经典案例
-(void)setModelStudyInfos:(ZBStudyInfosObject *)modelStudyInfos{
    _modelStudyInfos = modelStudyInfos;
    self.titleLab.text = _modelStudyInfos.title;
    self.addressLab.text = _modelStudyInfos.country;
    self.schoolNameLab.text = _modelStudyInfos.school;
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelStudyInfos.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
//我的收藏
-(void)setModelStudySuccessDto:(ZBStudySuccessDTOModel *)modelStudySuccessDto{
    _modelStudySuccessDto = modelStudySuccessDto;
    self.titleLab.text = _modelStudySuccessDto.title;
    self.addressLab.text = _modelStudySuccessDto.country;
    self.schoolNameLab.text = _modelStudySuccessDto.school;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelStudySuccessDto.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
