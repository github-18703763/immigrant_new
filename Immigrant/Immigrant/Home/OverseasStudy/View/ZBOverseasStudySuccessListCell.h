//
//  ZBOverseasStudySuccessDetailCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBStudyObject.h"
#import "ZBMyCollectionModel.h"
@interface ZBOverseasStudySuccessListCell : UITableViewCell

@property(nonatomic,strong)ZBStudyInfosObject *modelStudyInfos;

//我的收藏
@property(nonatomic,strong)ZBStudySuccessDTOModel *modelStudySuccessDto;
@end
