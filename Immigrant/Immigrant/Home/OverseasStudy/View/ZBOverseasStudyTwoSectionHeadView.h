//
//  ZBOverseasStudyTwoSectionHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBOverseasStudyTwoSectionHeadView : UIView

@property (nonatomic,copy) void(^listBlock)(void);

@property (nonatomic,copy) void(^contingBlock)(void);

@property (nonatomic,copy) void(^applyBlock)(void);

@end
