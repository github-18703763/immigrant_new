//
//  ZBOverseasStudyConsultingHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyConsultingHeadView.h"

@interface ZBOverseasStudyConsultingHeadView()
@property (weak, nonatomic) IBOutlet UIButton *contryBtn;

@end

@implementation ZBOverseasStudyConsultingHeadView
- (IBAction)contryAction:(id)sender {
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.contryBtn.layer.cornerRadius = 12;
    self.contryBtn.layer.masksToBounds = YES;
}

@end
