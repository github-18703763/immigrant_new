//
//  ZBOverseasStudyContryCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBStudyObject.h"

@interface ZBOverseasStudyContryCell : UITableViewCell

@property (nonatomic,strong)ZBSchoolCountrysObject *modelSchoolCountry;

@end
