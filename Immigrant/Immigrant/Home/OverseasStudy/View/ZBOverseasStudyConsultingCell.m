//
//  ZBOverseasStudyConsultingCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyConsultingCell.h"

@interface ZBOverseasStudyConsultingCell()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;

@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@end

@implementation ZBOverseasStudyConsultingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
}
//留学咨询
-(void)setModelStudyInfos:(ZBStudyInfosObject *)modelStudyInfos{
    _modelStudyInfos = modelStudyInfos;
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelStudyInfos.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.detailLab.text = _modelStudyInfos.title;
    self.timeLab.text = _modelStudyInfos.gmtCreated;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
