//
//  ZBOverseasStudyTwoSectionHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyTwoSectionHeadView.h"

@interface ZBOverseasStudyTwoSectionHeadView()
@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwo;


@end

@implementation ZBOverseasStudyTwoSectionHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.btnOne.layer.cornerRadius = 23.5;
    self.btnOne.layer.masksToBounds = YES;
    self.btnTwo.layer.cornerRadius = 23.5;
    self.btnTwo.layer.masksToBounds = YES;
    
//    [self.btnOne addTarget:self action:@selector(applyTap:) forControlEvents:UIControlEventTouchUpOutside];
}

//- (void)applyTap: (UIButton *)btn {
//    if (self.applyBlock) {
//        self.applyBlock();
//    }
//}

- (IBAction)listAction:(id)sender {
    if (self.applyBlock) {
        self.applyBlock();
    }
}
- (IBAction)caseAction:(id)sender {
    if (self.listBlock) {
        self.listBlock();
    }
}
- (IBAction)applyAction:(id)sender {
    if (self.applyBlock) {
        self.applyBlock();
    }
}
- (IBAction)contingAction:(id)sender {
    if (self.contingBlock) {
        self.contingBlock();
    }
}



@end
