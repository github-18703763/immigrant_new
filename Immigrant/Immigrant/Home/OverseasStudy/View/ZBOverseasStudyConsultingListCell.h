//
//  ZBOverseasStudyConsultingListCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBStudyObject.h"

@interface ZBOverseasStudyConsultingListCell : UITableViewCell

@property(nonatomic,strong)ZBStudyInfosObject *modelStudyInfo;

@end
