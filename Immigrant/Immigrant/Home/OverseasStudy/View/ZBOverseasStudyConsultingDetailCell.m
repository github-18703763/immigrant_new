//
//  ZBOverseasStudyConsultingDetailCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyConsultingDetailCell.h"

@interface ZBOverseasStudyConsultingDetailCell()
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolAddressLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolEnameLab;

@property (weak, nonatomic) IBOutlet UIImageView *schoolLogoImg;
@property (weak, nonatomic) IBOutlet UIView *contianerView;

@end

@implementation ZBOverseasStudyConsultingDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.contianerView.layer.cornerRadius = 15;
    self.contianerView.layer.masksToBounds = YES;
}
//咨询详情

- (void)setModel:(ZBSchoolCountrysObject *)model{
    _model = model;
    self.schoolNameLab.text = _model.chinesename;
    self.schoolEnameLab.text = _model.englishname;
    self.schoolAddressLab.text = _model.country;
    [self.schoolLogoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
