//
//  ZBOverseasStudySchoolDetailCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySchoolDetailCell.h"

@interface ZBOverseasStudySchoolDetailCell()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *pointOne;
@property (weak, nonatomic) IBOutlet UIView *pointTwo;

@end

@implementation ZBOverseasStudySchoolDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 355) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = path.bounds;
    maskLayer.path=path.CGPath;
    self.photoImg.layer.mask=maskLayer;
    [self.containerView addShadow];
    
    self.pointOne.layer.cornerRadius = 6;
    self.pointOne.layer.masksToBounds = YES;
    self.pointTwo.layer.cornerRadius = 6;
    self.pointTwo.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
