//
//  ZBOverseasStudySchoolChildOneCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySchoolChildOneCell.h"

@interface ZBOverseasStudySchoolChildOneCell()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *schoolCnameLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolEnameLab;
@property (weak, nonatomic) IBOutlet UIImageView *schoolIconImg;

@end

@implementation ZBOverseasStudySchoolChildOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)setIsCornor:(BOOL)isCornor{
    _isCornor = isCornor;
    if (_isCornor) {
        UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 320) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = path.bounds;
        maskLayer.path=path.CGPath;
        self.containerView.layer.mask=maskLayer;
        self.lineView.hidden = YES;
    }else{
        UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 320) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(0, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = path.bounds;
        maskLayer.path=path.CGPath;
        self.containerView.layer.mask=maskLayer;
        self.lineView.hidden = NO;
    }
}
//学院列表
-(void)setModelSchoolList:(ZBSchoolListObject *)modelSchoolList{
    _modelSchoolList = modelSchoolList;
    self.schoolCnameLab.text = _modelSchoolList.chinesename;
    self.schoolEnameLab.text = _modelSchoolList.englishname;
    [self.schoolIconImg sd_setImageWithURL:[NSURL URLWithString:_modelSchoolList.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
