//
//  ZBOverseasStudyOneSectionHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyOneSectionHeadView.h"

@implementation ZBOverseasStudyOneSectionHeadView

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
}
- (IBAction)introduceAction:(id)sender {
    if (self.introduceBlock) {
        self.introduceBlock();
    }
}

@end
