//
//  ZBOverseasStudySchoolChildTwoCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySchoolChildTwoCell.h"

@interface ZBOverseasStudySchoolChildTwoCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *schoolCnameLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolEnameLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UIImageView *schoolIconImg;

@end

@implementation ZBOverseasStudySchoolChildTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.containerView addShadow];
}
//学院列表
-(void)setModelSchoolList:(ZBSchoolListObject *)modelSchoolList{
    _modelSchoolList = modelSchoolList;
    self.schoolCnameLab.text = _modelSchoolList.chinesename;
    self.schoolEnameLab.text = _modelSchoolList.englishname;
    self.addressLab.text = _modelSchoolList.country;
    [self.schoolIconImg sd_setImageWithURL:[NSURL URLWithString:_modelSchoolList.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
