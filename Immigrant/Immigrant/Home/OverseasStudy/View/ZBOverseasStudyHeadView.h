//
//  ZBOverseasStudyHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBOverseasStudyHeadView : UIView

@property (nonatomic,strong)NSArray *dataArray;//集合
//{
//    "creator": "2",
//    "gmtCreated": "2018-12-26 17:46:39",
//    "gmtModified": "2018-12-26 17:46:39",
//    "id": 22,
//    "image": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1546002733&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=XiwXUsigweOyL9vzo4BzmeIYCIU%3D",
//    "imagekey": "icebartech-migrate/9.jpg",
//    "isDeleted": "n",
//    "modifier": "[SYS]",
//    "slidertype": "study",
//    "slidertypetext": "海外留学轮播图"
//},
@end
