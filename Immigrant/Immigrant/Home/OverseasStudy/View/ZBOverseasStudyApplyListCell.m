//
//  ZBOverseasStudyApplyListCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyApplyListCell.h"
#import "StudyModel.h"
@interface ZBOverseasStudyApplyListCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIButton *moneyBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLaebl;

@end


@implementation ZBOverseasStudyApplyListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerView.backgroundColor = [UIColor whiteColor];
    [self.containerView addShadow];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    self.moneyBtn.layer.cornerRadius = 19;
    self.moneyBtn.layer.masksToBounds = YES;
}
- (void)setModelStudyDto:(ZBStudyDTOModel *)modelStudyDto{
    _modelStudyDto = modelStudyDto;
    [self.moneyBtn setTitle:[NSString stringWithFormat:@"￥%@",_modelStudyDto.totalpriceyuan] forState:UIControlStateNormal];
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelStudyDto.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCell:(id)object {
    if ([object isKindOfClass:[StudyModel class]]) {
        
        StudyModel *obj = object;
        [self.moneyBtn setTitle:[NSString stringWithFormat:@"￥%@",@(obj.totalpriceyuan)] forState:UIControlStateNormal];
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:obj.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.nameLaebl.text = obj.name;
    }
    
    
}

@end
