//
//  ZBOverseasStudyApplyListCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyCollectionModel.h"
@interface ZBOverseasStudyApplyListCell : UITableViewCell
//我的收藏--留学产品
@property (nonatomic,strong)ZBStudyDTOModel *modelStudyDto;

- (void)configCell:(id)object;

@end
