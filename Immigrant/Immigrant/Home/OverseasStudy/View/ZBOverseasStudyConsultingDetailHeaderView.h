//
//  ZBOverseasStudyConsultingDetailHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>


//海外留学----咨询详情的头部视图
@interface ZBOverseasStudyConsultingDetailHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic,strong)NSDictionary *dicAll;

@end


