//
//  ZBOverseasStudyConsultingListCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyConsultingListCell.h"

@interface ZBOverseasStudyConsultingListCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation ZBOverseasStudyConsultingListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
}
//留学资讯
- (void)setModelStudyInfo:(ZBStudyInfosObject *)modelStudyInfo{
    _modelStudyInfo = modelStudyInfo;
    
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelStudyInfo.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelStudyInfo.title;
    self.timeLab.text = _modelStudyInfo.gmtCreated;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
