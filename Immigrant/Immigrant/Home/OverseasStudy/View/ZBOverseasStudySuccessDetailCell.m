//
//  ZBOverseasStudySuccessDetailCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudySuccessDetailCell.h"

@interface ZBOverseasStudySuccessDetailCell()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *line;

@end

@implementation ZBOverseasStudySuccessDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.photoImg.layer.cornerRadius = 15;
    self.photoImg.layer.masksToBounds = YES;
    self.line.layer.cornerRadius = 3.5/2;
    self.line.layer.masksToBounds = YES;
    
   
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
