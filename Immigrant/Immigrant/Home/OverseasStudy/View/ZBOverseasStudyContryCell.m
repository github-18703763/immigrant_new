//
//  ZBOverseasStudyContryCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasStudyContryCell.h"

@interface ZBOverseasStudyContryCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *contryImg;
@property (weak, nonatomic) IBOutlet UILabel *schooolCnameLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolEnameLab;

@end

@implementation ZBOverseasStudyContryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 57/2;
    self.containerView.layer.masksToBounds = YES;
    self.contryImg.layer.cornerRadius = 17;
    self.contryImg.layer.masksToBounds = YES;
}
//学院介绍
-(void)setModelSchoolCountry:(ZBSchoolCountrysObject *)modelSchoolCountry{
    _modelSchoolCountry = modelSchoolCountry;
    self.schooolCnameLab.text = _modelSchoolCountry.chinesename;
    self.schoolEnameLab.text = _modelSchoolCountry.englishname;
     [self.contryImg sd_setImageWithURL:[NSURL URLWithString:_modelSchoolCountry.icon] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
