//
//  ZBRealEstateController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateController.h"
#import "ZBVisacenterHeadView.h"
#import "ZBVisacenterHeadViewCell.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "ZBRealEstateChildController.h"
#import "ZBRealEstateDetailController.h"
#import "ZBHouseObject.h"
#import "HouseDetailController.h"
@interface ZBRealEstateController()<FSPageContentViewDelegate,FSSegmentTitleViewDelegate,ZBRealEstateChildControllerDelegate>

@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@property (nonatomic,weak) NSMutableArray *childVCs;

@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;
@end

@implementation ZBRealEstateController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"海外房产";
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    ZBVisacenterHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 245);
    self.tableView.tableHeaderView = headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBVisacenterHeadViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    headView.searchStyle =  EstateSearch;
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([scrollView isKindOfClass:[self.tableView class]]) {
        if (offsetY>=200) {
            for (ZBRealEstateChildController *child in self.childVCs) {
                child.tableView.scrollEnabled = YES;
            }
        }else{
            for (ZBRealEstateChildController *child in self.childVCs) {
                child.tableView.scrollEnabled = NO;
            }
        }

    }
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didSelect:(ZBRealEstateChildController *)child withIndexPath:(NSIndexPath *)path withModel:(id)model{
    
    HouseDetailController *vc = [[HouseDetailController alloc] init];
    ZBHouseObject *obj = (ZBHouseObject *)model;
    
    vc.houseId = [obj.id integerValue];
    [self.navigationController pushViewController:vc animated:YES];

//    ZBRealEstateDetailController *estate = [ZBRealEstateDetailController new];
//    estate.houseModel = model;
//    [self.navigationController pushViewController:estate animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBVisacenterHeadViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSMutableArray *childVCs = [[NSMutableArray alloc]init];
    for (NSString *title in @[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"]) {
        ZBRealEstateChildController *vc = [[ZBRealEstateChildController alloc]init];
        vc.delgate = self;
        vc.titleStr = title;
        vc.dataArrays = self.newsArray;
        vc.tableView.scrollEnabled = NO;
        [childVCs addObject:vc];
    }
    self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-50) childVCs:childVCs parentVC:self delegate:self];
    self.pageContentView.contentViewCurrentIndex = 2;
    [cell addSubview:self.pageContentView];
    self.childVCs = childVCs;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:@[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"] delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    self.titleView.selectIndex = 2;
    
    return self.titleView;
}

#pragma mark --
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
    //    self.title = @[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"][endIndex];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SystemScreenHeight-50;
}
///**
// 同时识别多个手势
//
// @param gestureRecognizer gestureRecognizer description
// @param otherGestureRecognizer otherGestureRecognizer description
// @return return value description
// */
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return YES;
//}


-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager houseFindpageNameLinek:@"" withPage:self.pageIndex withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
//            ZBLog(@"%@",array);
            if (isRefresh) {
                weakSELF.newsArray = [ZBHouseObject mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBHouseObject mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}
@end
