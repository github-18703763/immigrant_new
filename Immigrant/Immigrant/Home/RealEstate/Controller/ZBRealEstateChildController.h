//
//  ZBRealEstateChildController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZBRealEstateChildController;

@protocol ZBRealEstateChildControllerDelegate

-(void)didSelect:(ZBRealEstateChildController *)child withIndexPath:(NSIndexPath *)path withModel:(id)model;

@end


@interface ZBRealEstateChildController : UITableViewController

@property (nonatomic,copy) NSString *titleStr;
@property (nonatomic,weak) id<ZBRealEstateChildControllerDelegate> delgate;
@property (nonatomic,strong)NSArray *dataArrays; //总的数据
@end
