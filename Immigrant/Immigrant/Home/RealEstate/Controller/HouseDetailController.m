//
//  HouseDetailController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "HouseDetailController.h"
#import "StudyModel.h"
#import "QSNetworkManager+order.h"
#import <YYModel.h>
#import "RealEstateSignupController.h"
#import "XRCarouselView.h"
#import "SingleLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import <WebKit/WebKit.h>
#import "PointWebContainCell.h"
#import "SGPagingView.h"
#import "WebContainCell.h"
#import "QSNetworkManager+Order.h"
#import "ZBConsultingView.h"
#import "ZBReturnVisitController.h"
#import "ZBRealEstateDetailSectionOneHeadView.h"
#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>
#import <MBProgressHUD.h>
#import "HouseModel.h"
#import "ZBOnLineController.h"

@interface HouseDetailController ()<UITableViewDataSource, UITableViewDelegate, SGPageTitleViewDelegate>{
    CGFloat _bottomWebCellHeight;
}

@property (nonatomic, strong) HouseModel *detailModel;

@property (nonatomic, strong) XRCarouselView *cycleView;

@property (nonatomic, strong) UIView *headView;

@property (nonatomic, strong) UIButton *collectBtn;

@property (nonatomic, strong) UIButton *shareBtn;

@property (nonatomic, strong) UILabel *priceLabel;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) WKWebView *bottomWebView;

@property (nonatomic, strong) UIScrollView *bottomScrollView;

@property (nonatomic, strong) SGPageTitleView *pageTitleView;

@property (nonatomic, strong) UIButton *talkBtn;

@property (nonatomic, strong) UIButton *applyBtn;

@property (nonatomic, strong) UIView *applyShadowView;

@end

@implementation HouseDetailController

- (void)dealloc {
    [self.bottomWebView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    [self.navigationController.navigationBar setBackgroundImage: [UIImage createImageWithColor:UIColor.clearColor withRect:CGRectMake(0, 0, 1, 1)] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = UIColor.clearColor;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    self.navigationItem.leftBarButtonItem.tintColor = UIColor.whiteColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableHeaderView = self.headView;
    [self.headView addSubview:self.cycleView];
    
    [self.headView addSubview:self.collectBtn];
    [self.headView addSubview:self.shareBtn];
    [self.headView addSubview:self.priceLabel];
    [self.headView addSubview:self.nameLabel];
    
    [self.view addSubview:self.applyShadowView];
    
    [self.view addSubview:self.applyBtn];
    [self.view addSubview:self.talkBtn];
    
    [self fetchData];
    [self masLayout];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 80)];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.cycleView.imageClickBlock = ^(NSInteger index) {
        
    };
}

- (void)masLayout {
    [self.cycleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.headView);
    }];
    
    [self.collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headView).offset(70);
        make.right.equalTo(self.headView).offset(-17);
        make.width.height.mas_equalTo(54);
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headView).offset(-9.5);
        make.left.equalTo(self.headView).offset(21);
        make.width.height.mas_equalTo(54);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headView).offset(-16.5);
        make.bottom.equalTo(self.headView).offset(-18.5);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(105);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headView).offset(24);
        make.top.equalTo(self.headView).offset(70);
    }];
    
    [self.talkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(90);
        make.width.mas_equalTo(90);
        make.left.equalTo(self.view).offset(14);
        make.top.equalTo(self.applyBtn).offset(0);
    }];
    
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.talkBtn.mas_right).offset(24);
        make.height.equalTo(@(58));
        make.right.equalTo(self.view).offset(-18);
        make.bottom.equalTo(self.view).offset(-26);
    }];
    
    [self.applyShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.applyBtn);
    }];
}

- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh {
    [self fetchData];
}

#pragma mark - Private Method

- (void)fetchData {
    [QSNetworkManager houseDetial:[@(self.houseId) stringValue] successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            NSDictionary *dic = responseModel.data[@"bussData"];
            self.detailModel = [HouseModel yy_modelWithDictionary:dic];
            [self configData];
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [self.tableView.mj_header endRefreshing];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [self.tableView.mj_header endRefreshing];
    }];
    
}

- (void)configData {
    self.priceLabel.text = self.detailModel.totalpriceyuanstr;
    self.cycleView.imageArray = self.detailModel.images;
    self.pageTitleView.resetSelectedIndex = 0;
    [self.bottomWebView loadHTMLString:self.detailModel.type baseURL:nil];
    self.collectBtn.selected = self.detailModel.isfavorites;
    self.nameLabel.text = self.detailModel.name;
}

- (void)collectTap {
    
    if (self.detailModel.isfavorites) {
        [QSNetworkManager favoriteDelete:self.detailModel.id outType:@"house" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {//成功
                self.detailModel.isfavorites = !self.detailModel.isfavorites;
                self.collectBtn.selected = self.detailModel.isfavorites;
            } else {//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];
    } else {
        [QSNetworkManager favoriteAdd:self.detailModel.id outType:@"house" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {//成功
                self.detailModel.isfavorites = !self.detailModel.isfavorites;
                self.collectBtn.selected = self.detailModel.isfavorites;
            } else {//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];
    }
}

- (void)talkTap {
    ZBConsultingView *consulting = [[[NSBundle mainBundle] loadNibNamed:@"ZBConsultingView" owner:self options:nil] firstObject];
    consulting.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:consulting];
    consulting.phoneBlock = ^{
        //        ZBLog(@"电话咨询");
        NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"4007888798"];UIWebView * callWebview = [[UIWebView alloc] init];[callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
    };
    consulting.onlineBlock = ^{
        //        ZBLog(@"在线咨询");
        ZBOnLineController *onLine = [ZBOnLineController new];
        [self.navigationController pushViewController:onLine animated:YES];
    };
    consulting.visitiBlock = ^{
        //        ZBLog(@"要求回访");
        ZBReturnVisitController *visit = [[ZBReturnVisitController alloc] init];
        [self.navigationController pushViewController:visit animated:YES];
        
    };
}

- (void)shareTap {
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_Sina)]];
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
        [self shareWebPageToPlatformType:platformType];
    }];
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:@"侨一侨" descr:@"" thumImage:[UIImage imageNamed:@"home_advisory"]];
    //设置网页地址
    shareObject.webpageUrl = [NSString stringWithFormat:@"https://migrate.letsqyq.com/share/index?socialParentId=%@",@(1)];
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
            [MBProgressHUD showError:@"分享失败"];
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                [MBProgressHUD showError:resp.message];
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
    }];
}

#pragma mark - Event Response

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"]) {
        // 方法一
        if (object == self.bottomWebView.scrollView) {
            UIScrollView *scrollView = (UIScrollView *)object;
            CGFloat height = scrollView.contentSize.height;
            self.bottomWebView.frame = CGRectMake(0, 0, self.view.frame.size.width - 68, height);
            self.bottomScrollView.frame = CGRectMake(34, 30, self.view.frame.size.width - 68, height);
            self.bottomScrollView.contentSize =CGSizeMake(self.view.frame.size.width - 68, height);
            if (ABS(_bottomWebCellHeight - height) > 1) {
                _bottomWebCellHeight = height;
                [self.tableView reloadData];
            }
        }
        
    }
}



- (void)btnClick:(UIButton *)btn {
    if (btn == self.applyBtn) {
        if (self.detailModel) {
            RealEstateSignupController *vc = [[RealEstateSignupController alloc] init];
            vc.houseModel = self.detailModel;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } else if (btn == self.talkBtn) {
        [self talkTap];
    } else if (btn == self.shareBtn) {
        [self shareTap];
    } else if (btn == self.collectBtn) {
        [self collectTap];
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        UITableViewCell *cell = [UITableViewCell cellForTableView:tableView];
        [cell.contentView addSubview:self.pageTitleView];
        return cell;
    } else if (indexPath.row == 1) {
        WebContainCell *cell = [WebContainCell cellForTableView:tableView];
        [cell.contentView addSubview:self.bottomScrollView];
        return cell;
    }
    
    return [UITableViewCell cellForTableView:tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            return 38;
            break;
        case 1:
            return _bottomWebCellHeight + 50;
        default:
            break;
    }
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 420;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        ZBRealEstateDetailSectionOneHeadView *view = [[[NSBundle mainBundle] loadNibNamed:@"ZBRealEstateDetailSectionOneHeadView" owner:self options:nil] firstObject];
        view.dicAll = [self.detailModel yy_modelToJSONObject];
        return view;
    }
    return [[UIView alloc] init];
}

#pragma mark - SGPageTitleViewDelegate

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    
    if (self.detailModel) {
        switch (selectedIndex) {
            case 0:
                [self.bottomWebView loadHTMLString:self.detailModel.type baseURL:nil];
                break;
            case 1:
                [self.bottomWebView loadHTMLString:self.detailModel.facility baseURL:nil];
                break;
            case 2:
                [self.bottomWebView loadHTMLString:self.detailModel.investment baseURL:nil];
                break;
            case 3:
                [self.bottomWebView loadHTMLString:self.detailModel.process baseURL:nil];
                break;
            case 4:
                [self.bottomWebView loadHTMLString:self.detailModel.feature baseURL:nil];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Getter

- (XRCarouselView *)cycleView {
    if (!_cycleView) {
        _cycleView = [[XRCarouselView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 356.0 / 374 * SystemScreenWidth)];
        _cycleView.pagePosition = PositionHide;
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:_cycleView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(15, 15)];
        CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
        shapeLayer.frame = _cycleView.bounds;
        shapeLayer.path = path.CGPath;
        _cycleView.layer.mask = shapeLayer;
    }
    return _cycleView;
}

- (UIView *)headView {
    if (!_headView) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 356.0 / 374 * SystemScreenWidth)];
        _headView.backgroundColor = UIColor.whiteColor;
    }
    return _headView;
}

- (UIButton *)collectBtn {
    if (!_collectBtn) {
        _collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_collectBtn setImage:[UIImage imageNamed:@"collect2"] forState:UIControlStateNormal];
        [_collectBtn setImage:[UIImage imageNamed:@"anlixiangqing_yishoucang"] forState:UIControlStateSelected];
    }
    return _collectBtn;
}

- (UIButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_shareBtn setImage:[UIImage imageNamed:@"fenxiang-2"] forState:UIControlStateNormal];
    }
    return _shareBtn;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = [UIColor getColor:@"E7E7E7"];
        _priceLabel.backgroundColor = [UIColor getColor:@"303030" alpha:0.69];
        _priceLabel.frame = CGRectMake(0, 0, 105, 45);
        _priceLabel.layer.cornerRadius = 45.0 / 2;
        _priceLabel.layer.masksToBounds = true;
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.font = [UIFont systemFontOfSize:22 weight:UIFontWeightBlack];
    }
    return _priceLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:24];
    }
    return _nameLabel;
}

- (SGPageTitleView *)pageTitleView {
    if (!_pageTitleView) {
        SGPageTitleViewConfigure *config = [[SGPageTitleViewConfigure alloc] init];
        config.indicatorStyle = SGIndicatorStyleCover;
        config.indicatorCornerRadius = 19;
        config.indicatorHeight = 38;
        config.titleAdditionalWidth = 28;
        config.indicatorAdditionalWidth = 28;
        config.titleFont = [UIFont systemFontOfSize:15];
        config.titleSelectedFont = [UIFont systemFontOfSize:15];
        config.showBottomSeparator = false;
        config.titleColor = [UIColor getColor:@"4E4E4E"];
        config.titleSelectedColor = UIColor.whiteColor;
        config.indicatorColor = [UIColor getColor:@"267BFF"];
        config.needBounces = false;
        
        _pageTitleView = [[SGPageTitleView alloc] initWithFrame:CGRectMake(5, 0, SystemScreenWidth - 10, 38) delegate:self titleNames:@[@"在售户型",@"周边设施",@"投资评估",@"购房流程",@"项目特色"] configure:config];
        _pageTitleView.backgroundColor = [UIColor getColor:@"EDEDF0"];
        _pageTitleView.layer.cornerRadius = 19;
        //        if (@available(iOS 11, *)) {
        //            _pageTitleView..contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        //        }
    }
    return _pageTitleView;
}

- (WKWebView *)bottomWebView {
    if (!_bottomWebView) {
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        wkWebConfig.userContentController = wkUController;
        // 自适应屏幕宽度js
        NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        // 添加js调用
        [wkUController addUserScript:wkUserScript];
        
        _bottomWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1) configuration:wkWebConfig];
        _bottomWebView.backgroundColor = [UIColor clearColor];
        _bottomWebView.opaque = NO;
        _bottomWebView.userInteractionEnabled = NO;
        _bottomWebView.scrollView.bounces = NO;
        [_bottomWebView sizeToFit];
        [_bottomWebView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
        if (@available(iOS 11, *)) {
            _bottomWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _bottomWebView;
}

- (UIScrollView *)bottomScrollView {
    if (!_bottomScrollView) {
        _bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        [self.bottomScrollView addSubview:self.bottomWebView];
        if (@available(iOS 11, *)) {
            _bottomScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _bottomScrollView;
}

- (UIButton *)applyBtn {
    if (!_applyBtn) {
        _applyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_applyBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"立刻办理" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_applyBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _applyBtn.layer.cornerRadius = 25;
        _applyBtn.layer.masksToBounds = true;
        [_applyBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _applyBtn;
}

- (UIView *)applyShadowView {
    if (!_applyShadowView) {
        _applyShadowView = [[UIView alloc] init];
        _applyShadowView.backgroundColor = UIColor.whiteColor;
        _applyShadowView.layer.cornerRadius = 25;
        _applyShadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _applyShadowView.layer.shadowRadius = 17.5;
        _applyShadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _applyShadowView.layer.shadowOpacity = 0.26;
    }
    return _applyShadowView;
}

- (UIButton *)talkBtn {
    if (!_talkBtn) {
        _talkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_talkBtn setImage:[UIImage imageNamed:@"zixun"] forState:UIControlStateNormal];
        _talkBtn.layer.masksToBounds = true;
        [_talkBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _talkBtn;
}


@end
