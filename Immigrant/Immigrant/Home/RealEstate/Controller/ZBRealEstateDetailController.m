//
//  ZBVisacenterDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateDetailController.h"
#import "ZBRealEstateDetailHeaderView.h"
#import "ZBRealEstateDetailSectionOneHeadView.h"
#import "ZBVisacenterDetailSectionTwoHeadView.h"
#import "ZBVisacenterDetailCell.h"
#import "ZBVisacenterHandleController.h"
#import "ZBRealEstateSignupController.h"
#import "ZBRealEstateDetailSectionTwoHeadView.h"
#import "RealEstateSignupController.h"
#import "HouseModel.h"
#import <YYModel.h>
@interface ZBRealEstateDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *handleBtn;
@property (weak, nonatomic) IBOutlet UITableView *handleTable;
@property (strong,nonatomic)NSDictionary *dicAll;
@property (strong,nonatomic)ZBRealEstateDetailSectionOneHeadView *hvOne;
@property (nonatomic, strong) HouseModel *houseDetailModel;


@end

@implementation ZBRealEstateDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"房源信息";
    self.handleBtn.layer.cornerRadius = 25;
    self.handleBtn.layer.masksToBounds = YES;
    self.handleTable.showsHorizontalScrollIndicator = NO;
    self.handleTable.showsVerticalScrollIndicator = NO;
    self.handleTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0   );
    self.handleTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    ZBRealEstateDetailHeaderView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBRealEstateDetailHeaderView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 355);
    self.handleTable.tableHeaderView = headView;
    self.handleTable.delegate = self;
    self.handleTable.dataSource = self;
    ZBRealEstateDetailSectionTwoHeadView  *hvTwo = [[[NSBundle mainBundle] loadNibNamed:@"ZBRealEstateDetailSectionTwoHeadView" owner:self options:nil] firstObject];
//    view.backgroundColor = COLOR(252, 252, 252, 1);
    hvTwo.frame = CGRectMake(0, 0, SystemScreenWidth,SystemScreenHeight);
    self.handleTable.tableFooterView = hvTwo;
    [self.handleTable registerNib:[UINib nibWithNibName:@"ZBVisacenterDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [QSNetworkManager houseDetial:self.houseModel.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.dicAll = responseModel.data[@"bussData"];
            self.houseDetailModel = [HouseModel yy_modelWithJSON:self.dicAll];
            headView.dicAll = self.dicAll;
            self.hvOne.dicAll = self.dicAll;
            hvTwo.dicAll = self.dicAll;
            [self.handleTable reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];

    //详情页UI待调整
    
}
- (IBAction)handleAction:(id)sender {
    
//    ZBRealEstateSignupController *signup = [ZBRealEstateSignupController new];
//    [self.navigationController pushViewController:signup animated:YES];
    RealEstateSignupController *vc = [[RealEstateSignupController alloc] init];
    vc.houseModel = self.houseDetailModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBVisacenterDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        self.hvOne = [[[NSBundle mainBundle] loadNibNamed:@"ZBRealEstateDetailSectionOneHeadView" owner:self options:nil] firstObject];
        self.hvOne.dicAll = self.dicAll;
        return self.hvOne;
    }else{
        ZBVisacenterDetailSectionTwoHeadView *hv = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterDetailSectionTwoHeadView" owner:self options:nil] firstObject];
        return hv;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 267;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 420;
    }else{
        return 50;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
