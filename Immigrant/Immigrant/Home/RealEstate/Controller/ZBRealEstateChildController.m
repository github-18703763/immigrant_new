//
//  ZBRealEstateChildController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateChildController.h"
#import "ZBVisacenterChildCell.h"
#import "ZBHouseObject.h"

@interface ZBRealEstateChildController ()
//@property (nonatomic,assign) NSInteger  pageIndex;
//
//@property (nonatomic,assign) NSInteger  pageSize;
//
//@property (nonatomic,strong) NSMutableArray  *newsArray;
@end

@implementation ZBRealEstateChildController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBVisacenterChildCell" bundle:nil] forCellReuseIdentifier:@"cell"];
//    //刷新
//    self.pageSize = 10;
//    
//    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
//    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
//    self.tableView.mj_header = header;
//    
//    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
//    self.tableView.mj_footer = footer;
//    
//    [self loadData:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.newsArray.count;
    return self.dataArrays.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBVisacenterChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.houseModel = self.dataArrays[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 170;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.delgate didSelect:self withIndexPath:indexPath withModel:self.dataArrays[indexPath.row]];
}
//
//
//-(void)loadHeadData{
//    [self loadData:YES];
//}
//
//-(void)loadMoreData{
//    self.pageIndex++;
//    [self loadData:NO];
//}
//
//-(void)loadData:(BOOL)isRefresh{
//    if (isRefresh) {
//        self.pageIndex = 1;
//    }
//    WS(weakSELF);
//    [QSNetworkManager inspectionNameLinek:@"" withPage:1 withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
//        if (responseModel.status == 200) {
//            NSInteger count = [responseModel.data[@"count"] integerValue];
//            [weakSELF.tableView.mj_footer endRefreshing];
//            [weakSELF.tableView.mj_header endRefreshing];
//            NSArray *array = responseModel.data[@"bussData"];
//            if (isRefresh) {
//                weakSELF.newsArray = [ZBHouseObject mj_objectArrayWithKeyValuesArray:array];
//                [weakSELF.tableView reloadData];
//                if (weakSELF.newsArray.count>=count) {
//                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
//                }
//            }else{
//                if (array.count>0) {
//                    [weakSELF.newsArray addObjectsFromArray:[ZBHouseObject mj_objectArrayWithKeyValuesArray:array]];
//                }else{
//                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
//                }
//            }
//        }else{//失败
//            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
//            [weakSELF.tableView.mj_footer endRefreshing];
//            [weakSELF.tableView.mj_header endRefreshing];
//        }
//        
//    } failBlock:^(NSError * _Nullable error) {
//        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
//        [weakSELF.tableView.mj_footer endRefreshing];
//        [weakSELF.tableView.mj_header endRefreshing];
//    }];
//}
@end
