//
//  ZBRealEstateDetailHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/28.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateDetailHeaderView.h"
#import "XRCarouselView.h"
#import "ZBBannerImageObject.h"

@interface ZBRealEstateDetailHeaderView()<XRCarouselViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UIButton *isLikeBtn;
@property (weak, nonatomic) IBOutlet UIButton *isShareBtn;
@property (weak, nonatomic) IBOutlet UIButton *moneyBtn;
@property (weak, nonatomic) IBOutlet XRCarouselView *bgXRView;
@property (nonatomic,strong)XRCarouselView *photoView;
@end

@implementation ZBRealEstateDetailHeaderView
- (void)awakeFromNib {
    [super awakeFromNib];
    self.moneyBtn.layer.cornerRadius = 22.5;
    self.moneyBtn.layer.masksToBounds = YES;
    self.bgXRView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"jifenshangchang_bg"]];
}

//房源信息
- (void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
    self.titleLab.text = _dicAll[@"name"];
    //设置富文本
    NSMutableAttributedString *attributeStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_dicAll[@"country"]]];
    NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14],NSFontAttributeName,
                                   [UIColor colorWithRed:159/255.0 green:159/255.0 blue:159/255.0 alpha:1.0],NSForegroundColorAttributeName,nil];
    [attributeStr1 addAttributes:attributeDict range:NSMakeRange(0, attributeStr1.length)];
    //添加图片
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"yiminfangan_dingwei"];
    attach.bounds = CGRectMake(0, -2, 12, 12);
    NSAttributedString *attributeStr2 = [NSAttributedString attributedStringWithAttachment:attach];
    [attributeStr1 insertAttributedString:attributeStr2 atIndex:0];
    self.addressLab.attributedText = attributeStr1;
    [self.moneyBtn setTitle:_dicAll[@"totalpriceyuanstr"] forState:UIControlStateNormal];
    
    
    self.photoView = [[XRCarouselView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 355) imageArray:_dicAll[@"images"]];
    self.photoView.delegate=self;
    self.photoView.pagePosition = PositionHide;
    [self.bgXRView addSubview:self.photoView];
}
//学院介绍
- (void)setDicSchoolList:(NSDictionary *)dicSchoolList{
    _dicSchoolList = dicSchoolList;
    self.titleLab.text = _dicSchoolList[@"chinesename"];
    //设置富文本
    NSMutableAttributedString *attributeStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_dicSchoolList[@"country"]]];
    NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14],NSFontAttributeName,
                                   [UIColor colorWithRed:159/255.0 green:159/255.0 blue:159/255.0 alpha:1.0],NSForegroundColorAttributeName,nil];
    [attributeStr1 addAttributes:attributeDict range:NSMakeRange(0, attributeStr1.length)];
    //添加图片
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"yiminfangan_dingwei"];
    attach.bounds = CGRectMake(0, -2, 12, 12);
    NSAttributedString *attributeStr2 = [NSAttributedString attributedStringWithAttachment:attach];
    [attributeStr1 insertAttributedString:attributeStr2 atIndex:0];
    self.addressLab.attributedText = attributeStr1;
    [self.moneyBtn setTitle:@"缺少字段" forState:UIControlStateNormal];
    
    
    self.photoView = [[XRCarouselView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 355) imageArray:_dicAll[@"images"]];
    self.photoView.delegate=self;
    self.photoView.pagePosition = PositionHide;
    [self.bgXRView addSubview:self.photoView];
}
- (void)carouselView:(XRCarouselView *)carouselView didClickImage:(NSInteger)index{

}
@end
