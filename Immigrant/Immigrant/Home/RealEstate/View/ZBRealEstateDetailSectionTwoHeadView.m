//
//  ZBRealEstateDetailSectionTwoHeadView.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/28.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateDetailSectionTwoHeadView.h"
@interface ZBRealEstateDetailSectionTwoHeadView()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ZBRealEstateDetailSectionTwoHeadView

//NSString *strHTML = @"<p>你好</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;这是一个例子，请显示</p><p>外加一个table</p><table><tbody><tr class=\"firstRow\"><td valign=\"top\" width=\"261\">aaaa</td><td valign=\"top\" width=\"261\">bbbb</td><td valign=\"top\" width=\"261\">cccc</td></tr></tbody></table><p></p>";
//
//UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bonus];
//[self.view addSubview:webView];
//
//[webView loadHTMLString:strHTML baseURL:nil];
-(void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
//     "process": "购房流程
//    "type": "在售户型
//     "investment": "投资评估
//    "feature": "项目特色
//     "facility": "周边设施
    NSString *strHtml = _dicAll[@"investment"];
    [self.webView loadHTMLString:strHtml baseURL:nil];
}
@end
