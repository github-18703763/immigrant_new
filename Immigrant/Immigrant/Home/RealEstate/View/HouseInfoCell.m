//
//  HouseInfoCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import "HouseInfoCell.h"
#import "HouseModel.h"
#import "ZBMyOrderModel.h"
@interface HouseInfoCell ()
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *houseImgv;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *locationLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIImageView *locationImgv;

@end

@implementation HouseInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.houseImgv];
    [self.backView addSubview:self.locationImgv];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.locationLabel];
    [self.backView addSubview:self.priceLabel];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(16);
        make.bottom.equalTo(self.contentView).offset(-5);
        make.height.mas_equalTo(130);
    }];
    
    [self.houseImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(22);
        make.top.equalTo(self.backView).offset(17);
        make.bottom.equalTo(self.backView).offset(-17);
        make.width.mas_equalTo(123);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.houseImgv.mas_right).offset(20);
        make.top.equalTo(self.backView).offset(20.5);
        make.right.equalTo(self.backView).offset(-20);
    }];
    
    [self.locationImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.houseImgv.mas_right).offset(19);
        make.width.mas_equalTo(12);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8.5);
    }];
    
    [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.locationImgv);
        make.right.equalTo(self.backView).offset(-20);
        make.left.equalTo(self.locationImgv.mas_right).offset(9);
    }];
    

    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backView).offset(-17);
        make.right.equalTo(self.backView).offset(-31);
    }];
    
}

- (void)configCell:(id)object {
    
    if ([object isKindOfClass:[HouseModel class]]) {
        HouseModel *obj = object;
        
        [self.houseImgv sd_setImageWithURL:[NSURL URLWithString:obj.cover]];
        self.titleLabel.text = obj.name;
        self.locationLabel.text = obj.address;
        
        self.priceLabel.text = obj.totalpriceyuanstr;
    } else if ([object isKindOfClass:[ZBMyOrderModel class]]) {
        ZBMyOrderModel *obj = object;
        
        if (obj.orderHouse) {
            ZBMyOrderHouseModel *tmp = obj.orderHouse;
            
            
            [self.houseImgv sd_setImageWithURL:[NSURL URLWithString:tmp.cover]];
            self.titleLabel.text = tmp.name;
            self.locationLabel.text = tmp.address;
            
            self.priceLabel.text = tmp.totalpriceyuanstr;
            
        }
    }
}

#pragma mark - Getter

- (UIImageView *)houseImgv {
    if (!_houseImgv) {
        _houseImgv = [[UIImageView alloc] init];
        _houseImgv.contentMode = UIViewContentModeScaleAspectFill;
        _houseImgv.layer.cornerRadius = 8;
        _houseImgv.layer.masksToBounds = true;
        _houseImgv.clipsToBounds = true;
    }
    return _houseImgv;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 2);
        _backView.layer.shadowRadius = 21.5;
        _backView.layer.shadowColor = [UIColor getColor:@"a9a9a9"].CGColor;
        _backView.layer.shadowOpacity = 0.12;
    }
    return _backView;
}

- (UIImageView *)locationImgv {
    if (!_locationImgv) {
        _locationImgv = [[UIImageView alloc] init];
        _locationImgv.image = [UIImage imageNamed:@"yiminfangan_dingwei"];
    }
    return _locationImgv;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textColor = [UIColor getColor:@"4E4E4E"];
    }
    return _titleLabel;
}

- (UILabel *)locationLabel {
    if (!_locationLabel) {
        _locationLabel = [[UILabel alloc] init];
        _locationLabel.font = [UIFont systemFontOfSize:14];
        _locationLabel.textColor = [UIColor getColor:@"4E4E4E"];
    }
    return _locationLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont boldSystemFontOfSize:16];
        _priceLabel.textColor = [UIColor getColor:@"437DFF"];
    }
    return _priceLabel;
}


@end
