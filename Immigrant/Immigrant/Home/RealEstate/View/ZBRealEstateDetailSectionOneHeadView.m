//
//  ZBRealEstateDetailSectionOneHeadView.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/28.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateDetailSectionOneHeadView.h"
@interface ZBRealEstateDetailSectionOneHeadView()
@property (weak, nonatomic) IBOutlet UILabel *annualIncomeLab; //年收益
@property (weak, nonatomic) IBOutlet UILabel *gainLab;//涨幅
@property (weak, nonatomic) IBOutlet UILabel *downPayRatioLab;//首付比例
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *propertyTypeLab;//物业类型
@property (weak, nonatomic) IBOutlet UILabel *propertyRightYearsLab;//产权年限
@property (weak, nonatomic) IBOutlet UILabel *areaOfUseLab;//使用面积
@property (weak, nonatomic) IBOutlet UILabel *deliverCriterionLab;//交房标准
@property (weak, nonatomic) IBOutlet UILabel *ownerLab;//业主权益
@property (weak, nonatomic) IBOutlet UILabel *timeOfDeliveryLab; //交房时间
@property (weak, nonatomic) IBOutlet UIView *oneView;
@property (weak, nonatomic) IBOutlet UIView *twoView;


@end
@implementation ZBRealEstateDetailSectionOneHeadView
- (void)awakeFromNib {
    [super awakeFromNib];
    self.oneView.layer.borderColor =  [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.12].CGColor;
    self.oneView.layer.shadowOffset = CGSizeMake(0,0);
    self.oneView.layer.shadowOpacity = 0.5;
    self.oneView.layer.shadowRadius = 0.5;
    self.oneView.layer.cornerRadius = 15;
    
    self.twoView.layer.borderColor =  [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.12].CGColor;
    self.twoView.layer.shadowOffset = CGSizeMake(0,0);
    self.twoView.layer.shadowOpacity = 0.5;
    self.twoView.layer.shadowRadius = 0.5;
    self.twoView.layer.cornerRadius = 15;
    
}
//房源信息
-(void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
//    annuprofit年收益  downpayment首付比例 increaserate涨幅
    self.annualIncomeLab.text = [NSString stringWithFormat:@"%@%%",_dicAll[@"annuprofit"]];
    self.gainLab.text = [NSString stringWithFormat:@"%@%%",_dicAll[@"increaserate"]];
    self.downPayRatioLab.text = [NSString stringWithFormat:@"%@%%",_dicAll[@"downpayment"]];
    self.titleLab.text = [NSString stringWithFormat:@"%@",_dicAll[@"name"]];
    self.propertyTypeLab.text = [NSString stringWithFormat:@"%@",_dicAll[@"management"]];
    self.propertyRightYearsLab.text = [NSString stringWithFormat:@"%@",_dicAll[@"ownershipyear"]];
    self.areaOfUseLab.text = [NSString stringWithFormat:@"%@m²",_dicAll[@"floorage"]];
    self.deliverCriterionLab.text = [NSString stringWithFormat:@"%@",_dicAll[@"deliverystandard"]];
    self.ownerLab.text = [NSString stringWithFormat:@"%@",_dicAll[@"ownerrights"]];
    self.timeOfDeliveryLab.text = [NSString stringWithFormat:@"%@",_dicAll[@"deliverytime"]];
}

@end
