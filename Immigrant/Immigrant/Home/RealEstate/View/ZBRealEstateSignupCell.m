//
//  ZBInspectionRouteSureCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBRealEstateSignupCell.h"
#import "ZBRealEstateOrderController.h"

@interface ZBRealEstateSignupCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIView *numberView;
@property (weak, nonatomic) IBOutlet UIView *pointView;


@end

@implementation ZBRealEstateSignupCell

- (IBAction)sureAction:(id)sender {
    ZBRealEstateOrderController *pay = [ZBRealEstateOrderController new];
    [[self currentViewController].navigationController pushViewController:pay animated:YES];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.backgroundColor = [UIColor whiteColor];
    [self.containerView addShadow];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
    
    self.numberView.layer.cornerRadius = 22.5;
    self.numberView.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
