//
//  ZBVisacenterHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBBannerImageObject.h"

typedef NS_ENUM(NSInteger, SeacrchStyle) {
    VisacenterSearch=0,
    EstateSearch=1
};

@interface ZBVisacenterHeadView : UIView

@property (nonatomic,assign) SeacrchStyle searchStyle;

@property (nonatomic,strong) NSArray  *photoArray;

@end
