//
//  ZBVisacenterPaySuccessHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterPaySuccessHeadView.h"
#import "ZBVisacenterOrderControler.h"

@interface ZBVisacenterPaySuccessHeadView()
@property (weak, nonatomic) IBOutlet UIButton *orderBtn;
@property (weak, nonatomic) IBOutlet UIButton *shradBtn;


@end

@implementation ZBVisacenterPaySuccessHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.orderBtn.layer.cornerRadius = 25;
    self.orderBtn.layer.masksToBounds = YES;
    self.shradBtn.layer.cornerRadius = 25;
    self.shradBtn.layer.masksToBounds = YES;
}

- (IBAction)orderAction:(id)sender {
    ZBVisacenterOrderControler *order = [ZBVisacenterOrderControler new];
    [[self parentController].navigationController pushViewController:order animated:YES];
}

- (IBAction)shradeAction:(id)sender {
}

@end
