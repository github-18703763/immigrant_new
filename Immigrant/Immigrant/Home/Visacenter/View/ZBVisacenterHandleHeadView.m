//
//  ZBVisacenterHandleHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterHandleHeadView.h"
#import "ZBPayMethodView.h"
#import "ZBCalendarController.h"
#import "GYAddressVC.h"
#import "ZBMineAddressObject.h"

@interface ZBVisacenterHandleHeadView()

@property (weak, nonatomic) IBOutlet UIView *headContainer;
@property (weak, nonatomic) IBOutlet UIView *numberView;
@property (weak, nonatomic) IBOutlet UIButton *submmitBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *cycleLab;
@property (weak, nonatomic) IBOutlet UILabel *interViewLab;
@property (weak, nonatomic) IBOutlet UILabel *vaidLab;
@property (weak, nonatomic) IBOutlet UILabel *maxLab;
@property (weak, nonatomic) IBOutlet UILabel *manyTimesLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *freeLab;

@property (weak, nonatomic) IBOutlet UILabel *peopleNumberLab;

@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property (nonatomic,strong) ZBMineAddressObject  *mineAddress;

@end

@implementation ZBVisacenterHandleHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.headContainer.layer.cornerRadius = 15;
    self.headContainer.layer.masksToBounds = YES;
    self.numberView.layer.cornerRadius = 22.5;
    self.numberView.layer.masksToBounds = YES;
    self.submmitBtn.layer.cornerRadius = 25;
    self.submmitBtn.layer.masksToBounds = YES;
    
}

-(void)setModel:(ZBVisaObject *)model{
    _model = model;
    self.cycleLab.text = _model.servietime;
    self.interViewLab.text = _model.interviewrequires;
    self.vaidLab.text = _model.expirydate;
    self.maxLab.text = _model.maxstaytime;
    self.manyTimesLab.text = _model.entriesnum;
    self.typeLab.text = _model.visatype;
    self.titleLab.text = _model.name;
    self.freeLab.text = [NSString stringWithFormat:@"¥%@",_model.servicefeeyuan];
//    self.nameLab.text = [QSUserManager shareInstance].userModel.username;
//    self.phoneLab.text = [QSUserManager shareInstance].userModel.mobile;
    self.nameLab.text = @" ";
    self.phoneLab.text = @" ";
    self.addressLab.text = @"";
}

- (IBAction)submmitAction:(id)sender {
    /**
     "booknum": 3,
     "userAddressId": 0,
     "userEmail": "1106988868@qq.com",
     "userIdcard": "15814520421",
     "userTraveldate": "2019-01-03T18:08:11.096Z",
     "visaId": 0
     */
    if (!self.mineAddress) {
           [[UIApplication sharedApplication].delegate.window makeToast:@"请选择签证地址" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    if (self.timeLab.text.length <= 0) {
               [[UIApplication sharedApplication].delegate.window makeToast:@"请选择签证时间" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    
    if (self.commitOrderBlock) {
//        NSDictionary *dict =@{@"booknum":self.peopleNumberLab.text,@"userAddressId":self.mineAddress.id,@"userEmail":[QSUserManager shareInstance].userModel.email?[QSUserManager shareInstance].userModel.email:@" ",@"userIdcard":@" ",@"userTraveldate":self.timeLab.text,@"visaId":self.model.id};
         NSDictionary *dict =@{@"booknum":self.peopleNumberLab.text,@"userAddressId":self.mineAddress.id,@"userTraveldate":self.timeLab.text,@"visaId":self.model.id};
        self.commitOrderBlock(dict);
    }
}

- (IBAction)addTimeAction:(id)sender {
    WS(weakSELF);
    ZBCalendarController *calender = [ZBCalendarController new];
    calender.chooseTimeBlock = ^(NSString * _Nonnull time, NSInteger selectDay, NSIndexPath * _Nonnull path) {
        weakSELF.timeLab.text = time;
    };
    [[self parentController].navigationController pushViewController:calender animated:YES];
}

- (IBAction)addnumberAction:(id)sender {
    NSInteger number = [self.peopleNumberLab.text integerValue];
    number = number + 1;
    self.peopleNumberLab.text = [NSString stringWithFormat:@"%li",number];
}

- (IBAction)releseNumberAction:(id)sender {
    NSInteger number = [self.peopleNumberLab.text integerValue];
    number = number - 1;
    if (number <=1 ) {
        number = 1;
    }
    self.peopleNumberLab.text = [NSString stringWithFormat:@"%li",number];
}

- (IBAction)chooseAddressAction:(id)sender {
    GYAddressVC *address = [GYAddressVC new];
    address.isChoose = YES;
    WS(weakSELF);
    address.chooseBlock = ^(ZBMineAddressObject *object) {
        weakSELF.mineAddress = object;
        weakSELF.addressLab.text = object.str;
        weakSELF.nameLab.text = object.consignee;
        weakSELF.phoneLab.text = object.mobile;
    };
    [[self parentController].navigationController pushViewController:address animated:YES];
    
}

@end
