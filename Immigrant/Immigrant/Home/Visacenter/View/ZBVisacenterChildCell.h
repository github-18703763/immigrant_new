//
//  ZBVisacenterChildCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBHouseObject.h"
#import "ZBVisaObject.h"
#import "ZBMyCollectionModel.h"
@interface ZBVisacenterChildCell : UITableViewCell
@property (nonatomic,strong)ZBHouseObject *houseModel;
@property (nonatomic,strong) ZBVisaObject *visaModel;
//我的收藏
@property (nonatomic,strong) ZBVisaDTOModel *modelVisaDTO;//签证中心
@property (nonatomic,strong) ZBHouseDTOModel *modelHouseDTO;//海外房产
@end
