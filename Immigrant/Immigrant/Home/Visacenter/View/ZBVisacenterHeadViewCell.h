//
//  ZBVisacenterHeadViewCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPageContentView.h"

@interface ZBVisacenterHeadViewCell : UITableViewCell

@property (nonatomic, copy) void(^changeBlock)(NSInteger endIndex);

@end
