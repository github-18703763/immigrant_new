//
//  ZBPayMethodView.m
//  ImmigrantView
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBPayMethodView.h"

@interface ZBPayMethodView()
//微信支付按钮
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
//支付宝支付按钮
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;

@end

@implementation ZBPayMethodView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.wechatBtn.layer.cornerRadius = 25;
    self.wechatBtn.layer.masksToBounds = YES;
    self.alipayBtn.layer.cornerRadius = 25;
    self.alipayBtn.layer.masksToBounds = YES;
}
/**
 微信支付
 */
- (IBAction)wechatPayAction:(id)sender {
    if (self.wechatPayBlock) {
        self.wechatPayBlock();
        [self removeFromSuperview];
    }
}
/**
 支付宝支付
 */
- (IBAction)aliPayAction:(id)sender {
    if (self.alipayBlock) {
        self.alipayBlock();
        [self removeFromSuperview];
    }
    
}
/**
 关闭窗口
 */
- (IBAction)closePayAction:(id)sender {
    [self removeFromSuperview];
    if (self.payMethodCloseBlock) {
        self.payMethodCloseBlock();
    }
}

@end
