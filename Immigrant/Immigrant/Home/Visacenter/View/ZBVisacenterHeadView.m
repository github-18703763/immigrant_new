//
//  ZBVisacenterHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterHeadView.h"
#import "XRCarouselView.h"
#import "ZBRealEstateSearchController.h"
#import "ZBVisacenterSearchController.h"

@interface ZBVisacenterHeadView()<XRCarouselViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *seachTxt;

@property (nonatomic,strong) XRCarouselView  *xr;


@end

@implementation ZBVisacenterHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.seachTxt.layer.cornerRadius = 6;
    self.seachTxt.layer.masksToBounds = YES;
    self.seachTxt.delegate = self;
}

-(void)setPhotoArray:(NSArray *)photoArray{
    _photoArray = photoArray;
    if (self.xr) {
        [self.xr removeFromSuperview];
    }
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    for (ZBBannerImageObject *object in _photoArray) {
        [array addObject:object.image];
    }
    XRCarouselView *xr=[[XRCarouselView alloc]initWithFrame:CGRectMake(0,0,SystemScreenWidth,180) imageArray:array];
    xr.delegate=self;
    xr.pagePosition = PositionHide;
    self.xr = xr;
    [self addSubview:xr];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (self.searchStyle == EstateSearch) {
        ZBRealEstateSearchController *satate = [ZBRealEstateSearchController new];
        [[self parentController].navigationController pushViewController:satate animated:YES];
    }
    if (self.searchStyle == VisacenterSearch) {
        ZBVisacenterSearchController *visacenter = [ZBVisacenterSearchController new];
         [[self parentController].navigationController pushViewController:visacenter animated:YES];
    }
    return NO;
}

- (void)carouselView:(XRCarouselView *)carouselView didClickImage:(NSInteger)index{
    
}

@end
