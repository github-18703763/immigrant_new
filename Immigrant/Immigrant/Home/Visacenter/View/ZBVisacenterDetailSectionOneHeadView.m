//
//  ZBVisacenterDetailSectionOneHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterDetailSectionOneHeadView.h"
@interface ZBVisacenterDetailSectionOneHeadView()
@property (weak, nonatomic) IBOutlet UILabel *handlingCycleLab; //办理周期
@property (weak, nonatomic) IBOutlet UILabel *interviewRequireLab; //面试需求
@property (weak, nonatomic) IBOutlet UILabel *visaValidityPeriodLab;//签证有效期
@property (weak, nonatomic) IBOutlet UILabel *MaxLengthOfStayLab;//最长时间停留
@property (weak, nonatomic) IBOutlet UILabel *manyTimesLab; //多次
@property (weak, nonatomic) IBOutlet UILabel *visaTypeLab; //签证类型

@end
@implementation ZBVisacenterDetailSectionOneHeadView

-(void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
}

-(void)setModel:(ZBVisaObject *)model{
    _model = model;
    self.handlingCycleLab.text = _model.servietime;
    self.interviewRequireLab.text = _model.interviewrequires;
    self.visaValidityPeriodLab.text = _model.expirydate;
    self.MaxLengthOfStayLab.text = _model.maxstaytime;
    self.manyTimesLab.text = _model.entriesnum;
    self.visaTypeLab.text = _model.visatype;
}

//{
//    "address": "美国洛杉矶",
//    "annuprofit": 58,
//    "country": "美国",
//    "countryId": 2,
//    "cover": "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545970402&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=EFBcfYvybTM3i7bUFUq8nVmFwdY%3D",
//    "creator": "[SYS]",
//    "deliverystandard": "精装修",
//    "deliverytime": "2018年底",
//    "depositfee": 12888900,
//    "depositfeeyuan": 128889,
//    "downpayment": 30,
//    "facility": "周边设施",
//    "feature": "项目特色",
//    "floorage": 120,
//    "gmtCreated": "2018-12-27 12:14:33",
//    "gmtModified": "2018-12-28 11:08:09",
//    "id": 3,
//    "images": [
//               "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545970402&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=EFBcfYvybTM3i7bUFUq8nVmFwdY%3D",
//               "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545970402&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=EFBcfYvybTM3i7bUFUq8nVmFwdY%3D",
//               "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545970402&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=EFBcfYvybTM3i7bUFUq8nVmFwdY%3D",
//               "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545970402&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=EFBcfYvybTM3i7bUFUq8nVmFwdY%3D"
//               ],
//    "imageskey": "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg,icebartech-migrate/9.jpg,icebartech-migrate/9.jpg",
//    "investment": "投资评估",
//    "isDeleted": "n",
//    "management": "别墅",
//    "modifier": "[SYS]",
//    "name": "旧金山戴利山庄",
//    "ownerrights": "居住",
//    "ownershipyear": "永久产权",
//    "process": "购房流程",
//    "servicefee": 12888900,
//    "servicefeeyuan": 128889,
//    "totalprice": 23463000,
//    "totalpriceyuan": 234630,
//    "type": "在售户型"
//}
@end
