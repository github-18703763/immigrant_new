//
//  ZBPayMethodView.h
//  ImmigrantView
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ZBWechatPayBlock)(void);
typedef void(^ZBAliPayBlocK)(void);

@interface ZBPayMethodView : UIView
//关闭
@property (nonatomic, copy) void(^payMethodCloseBlock)(void);
//微信支付
@property (nonatomic, copy) ZBWechatPayBlock wechatPayBlock;
//支付宝支付
@property (nonatomic, copy) ZBAliPayBlocK alipayBlock;

@end
