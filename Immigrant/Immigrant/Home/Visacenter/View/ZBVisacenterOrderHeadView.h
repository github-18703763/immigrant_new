//
//  ZBVisacenterOrderHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBVisaOderDetailObject.h"

@interface ZBVisacenterOrderHeadView : UIView

@property (nonatomic,copy) void(^payBlock)(void);

@property (nonatomic,strong) ZBVisaOderDetailObject  *orderObject;

@end
