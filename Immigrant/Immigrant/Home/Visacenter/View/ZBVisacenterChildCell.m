//
//  ZBVisacenterChildCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterChildCell.h"

@interface ZBVisacenterChildCell()

@property (weak, nonatomic) IBOutlet UIView *containView;
@property (weak, nonatomic) IBOutlet UIButton *moneyBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;

@end

@implementation ZBVisacenterChildCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containView.layer.cornerRadius = 15;
    self.containView.layer.masksToBounds = YES;
    self.moneyBtn.layer.cornerRadius = 19;
    self.moneyBtn.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    
}
-(void)setHouseModel:(ZBHouseObject *)houseModel{
    _houseModel = houseModel;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_houseModel.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _houseModel.name;
    [self.moneyBtn setTitle:[NSString stringWithFormat:@"   ￥%@   ",_houseModel.totalpriceyuan] forState:UIControlStateNormal];
    
    //设置富文本
    NSMutableAttributedString *attributeStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_houseModel.country]];
    NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14],NSFontAttributeName,
                                  [UIColor colorWithRed:159/255.0 green:159/255.0 blue:159/255.0 alpha:1.0],NSForegroundColorAttributeName,nil];
    [attributeStr1 addAttributes:attributeDict range:NSMakeRange(0, attributeStr1.length)];
    
    //添加图片
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"yiminfangan_dingwei"];
    attach.bounds = CGRectMake(0, -2, 12, 12);
    NSAttributedString *attributeStr2 = [NSAttributedString attributedStringWithAttachment:attach];
    [attributeStr1 insertAttributedString:attributeStr2 atIndex:0];
    
    self.detailLab.attributedText = attributeStr1;
}

-(void)setVisaModel:(ZBVisaObject *)visaModel{
    _visaModel = visaModel;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_visaModel.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _visaModel.name;
    [self.moneyBtn setTitle:[NSString stringWithFormat:@"   ￥%@   ",_visaModel.servicefeeyuan] forState:UIControlStateNormal];
    self.detailLab.text = _visaModel.visatype;
}
//我的收藏--下面2个都是
-(void)setModelVisaDTO:(ZBVisaDTOModel *)modelVisaDTO{
    _modelVisaDTO = modelVisaDTO;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelVisaDTO.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelVisaDTO.name;
    [self.moneyBtn setTitle:[NSString stringWithFormat:@"   ￥%@   ",_modelVisaDTO.servicefeeyuan] forState:UIControlStateNormal];
    self.detailLab.text = _modelVisaDTO.visatype;
}
- (void)setModelHouseDTO:(ZBHouseDTOModel *)modelHouseDTO{
    _modelHouseDTO = modelHouseDTO;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelHouseDTO.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelHouseDTO.name;
    [self.moneyBtn setTitle:[NSString stringWithFormat:@"   ￥%@   ",_modelHouseDTO.totalpriceyuan] forState:UIControlStateNormal];
    
    //设置富文本
    NSMutableAttributedString *attributeStr1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_modelHouseDTO.country]];
    NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14],NSFontAttributeName,
                                   [UIColor colorWithRed:159/255.0 green:159/255.0 blue:159/255.0 alpha:1.0],NSForegroundColorAttributeName,nil];
    [attributeStr1 addAttributes:attributeDict range:NSMakeRange(0, attributeStr1.length)];
    
    //添加图片
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"yiminfangan_dingwei"];
    attach.bounds = CGRectMake(0, -2, 12, 12);
    NSAttributedString *attributeStr2 = [NSAttributedString attributedStringWithAttachment:attach];
    [attributeStr1 insertAttributedString:attributeStr2 atIndex:0];
    
    self.detailLab.attributedText = attributeStr1;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
