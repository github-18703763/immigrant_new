//
//  ZBVisacenterDetailCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterDetailCell.h"

@interface ZBVisacenterDetailCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;

@end

@implementation ZBVisacenterDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
}
//海外房产详情
-(void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_dicAll[@"cover"]] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
