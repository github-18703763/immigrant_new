//
//  ZBVisacenterDetailHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterDetailHeadView.h"
#import "XRCarouselView.h"

@interface ZBVisacenterDetailHeadView()<XRCarouselViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIButton *moneyBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab; //标题
@property (weak, nonatomic) IBOutlet UIButton *isLikeBtn;

@property (weak, nonatomic) IBOutlet UIButton *isShareBtn;

@property (nonatomic,weak) XRCarouselView  *xr;

@end

@implementation ZBVisacenterDetailHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 355) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = path.bounds;
    maskLayer.path=path.CGPath;
    self.photoImg.layer.mask=maskLayer;
    self.moneyBtn.layer.cornerRadius = 22.5;
    self.moneyBtn.layer.masksToBounds = YES;
    
}

-(void)setModel:(ZBVisaObject *)model{
    _model = model;
    if (self.xr) {
        [self.xr removeFromSuperview];
    }
    XRCarouselView *xr=[[XRCarouselView alloc]initWithFrame:CGRectMake(0,0,SystemScreenWidth,355) imageArray:_model.images];
    xr.delegate=self;
    xr.pagePosition = PositionHide;
    self.xr = xr;
    [self addSubview:xr];
    
}

- (void)carouselView:(XRCarouselView *)carouselView didClickImage:(NSInteger)index{
    
}

@end
