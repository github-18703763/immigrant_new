//
//  ZBVisacenterHandleHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBVisaObject.h"

typedef void(^ZBCommitOrderBlock)(NSDictionary *dict);
typedef void(^ZBAliPayBlocK)(void);

@interface ZBVisacenterHandleHeadView : UIView

@property (nonatomic,strong) ZBVisaObject  *model;

/**
 提交订单
 */
@property (nonatomic, copy) ZBCommitOrderBlock commitOrderBlock;

@end
