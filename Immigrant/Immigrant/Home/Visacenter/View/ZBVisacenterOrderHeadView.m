//
//  ZBVisacenterOrderHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterOrderHeadView.h"

@interface ZBVisacenterOrderHeadView()
@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UIView *prodectView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *pointOneView;

@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UILabel *userNameLab;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLab;
@property (weak, nonatomic) IBOutlet UILabel *userAddressLab;

@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLab;
@property (weak, nonatomic) IBOutlet UILabel *quanLab;

@end

@implementation ZBVisacenterOrderHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.orderView.layer.cornerRadius = 25;
    self.orderView.layer.masksToBounds = YES;
    self.prodectView.backgroundColor = [UIColor whiteColor];
    [self.prodectView addShadow];

    [self.orderLab setAttributedText:[NSMutableAttributedString createColorString:@"订单编号：2018101610048575" withExcision:@"订单编号：" dainmaicColor:[UIColor getColor:@"4A4A4A"] excisionColor:[UIColor getColor:@"267BFF"]]];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    self.pointOneView.layer.cornerRadius = 4;
    self.pointOneView.layer.masksToBounds = YES;
    self.pointOneView.layer.cornerRadius = 4;
    self.pointOneView.layer.masksToBounds = YES;
    
    self.payBtn.layer.cornerRadius = 25;
    self.payBtn.layer.masksToBounds = YES;
    
}
- (IBAction)payAction:(id)sender {
    if (self.payBlock) {
        self.payBlock();
    }
}

-(void)setOrderObject:(ZBVisaOderDetailObject *)orderObject{
    _orderObject = orderObject;
    [self.orderLab setAttributedText:[NSMutableAttributedString createColorString:[NSString stringWithFormat:@"订单编号：%@",_orderObject.no] withExcision:@"订单编号：" dainmaicColor:[UIColor getColor:@"4A4A4A"] excisionColor:[UIColor getColor:@"267BFF"]]];
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_orderObject.visaObject.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = _orderObject.visaObject.name;
    self.timeLab.text = _orderObject.visaObject.userTraveldate;
    self.numberLab.text = _orderObject.booknum;
    self.userNameLab.text = _orderObject.userName;
    self.userPhoneLab.text = _orderObject.userMobile;
    self.userAddressLab.text = _orderObject.visaObject.userAddress;
    
}

@end
