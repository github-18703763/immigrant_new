//
//  ZBVisacenterDetailSectionTwoHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterDetailSectionTwoHeadView.h"

@interface ZBVisacenterDetailSectionTwoHeadView()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnThree;
@property (weak, nonatomic) IBOutlet UIButton *btnFour;

@end

@implementation ZBVisacenterDetailSectionTwoHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 19;
    self.containerView.layer.masksToBounds = YES;
    self.btnOne.layer.cornerRadius = 19;
    self.btnOne.layer.masksToBounds = YES;
    self.btnTwo.layer.cornerRadius = 19;
    self.btnTwo.layer.masksToBounds = YES;
    self.btnThree.layer.cornerRadius = 19;
    self.btnThree.layer.masksToBounds = YES;
    self.btnFour.layer.cornerRadius = 19;
    self.btnFour.layer.masksToBounds = YES;
}

@end
