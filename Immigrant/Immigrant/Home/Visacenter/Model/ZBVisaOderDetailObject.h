//
//  ZBVisaOderDetailObject.h
//  Immigrant
//
//  Created by 张波 on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBVisaObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBVisaOderDetailObject : NSObject

/**
 booknum = 1;
 */
@property (nonatomic,copy) NSString  *booknum;
/**
 creator = 2;
 */
@property (nonatomic,copy) NSString  *creator;
/**
 gmtCreated = "2019-01-04 17:04:38";
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
 gmtModified = "2019-01-04 17:04:38";
 */
@property (nonatomic,copy) NSString *gmtModified;
/**
 id = 20;
 */
@property (nonatomic,copy) NSString  *id;
/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;
/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;
/**
 name = "\U7f8e\U56fd\U63a2\U4eb2\U8bbf\U53cb\U7b7e\U8bc1002";
 */
@property (nonatomic,copy) NSString *name;
/**
 no = 14999372009058; 订单编号
 */
@property (nonatomic,copy) NSString  *no;

/**
 orderVisa
 orderVisa =         {
 countryId = 2;
 cover = "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1546594478&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=Nd8hQDJGEHkvR2%2FTKffz4%2FFA%2Fh8%3D";
 creator = 2;
 entriesnum = "\U591a\U6b21";
 expirydate = "10\U5e74";
 gmtCreated = "2019-01-04 17:04:38";
 gmtModified = "2019-01-04 17:04:38";
 id = 5;
 imageskey = "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg,icebartech-migrate/9.jpg,icebartech-migrate/9.jpg";
 interviewrequires = "\U9700\U8981";
 isDeleted = n;
 isactivity = 1;
 maxstaytime = "180\U5929";
 modifier = "[SYS]";
 name = "\U7f8e\U56fd\U63a2\U4eb2\U8bbf\U53cb\U7b7e\U8bc1002";
 orderId = 20;
 servicefee = 150000;
 servicefeeyuan = 1500;
 servietime = "30\U5929\U5de6\U53f3";
 userAddress = "\U5e7f\U4e1c\U6df1\U5733\U5357\U5c71\U533a 15814520421 \U4e00\U4e8c\U4e09\U56db\U6536";
 userTraveldate = "2019-01-25 00:00:00";
 visaId = 5;
 visatype = "\U77ed\U671f\U7b7e\U8bc1";
 };
 */
@property (nonatomic,strong) NSDictionary  *orderVisa;
@property (nonatomic,strong) ZBVisaObject  *visaObject;
/**
 orderstatus = unpaid;
 */
@property (nonatomic,copy) NSString  *orderstatus;
/**
 orderstatustext = "\U5f85\U4ed8\U6b3e";
 */
@property (nonatomic,copy) NSString  *orderstatustext;
/**
 ordertype = visa;
 */
@property (nonatomic,copy) NSString  *ordertype;
/**
 ordertypetext = "\U7b7e\U8bc1\U529e\U7406";
 */
@property (nonatomic,copy) NSString  *ordertypetext;
/**
 payamout = 150000;
 */
@property (nonatomic,copy) NSString  *payamout;
/**
 payamoutyuan = 1500;
 */
@property (nonatomic,copy) NSString  *payamoutyuan;
/**
 userId = 2;
 */
@property (nonatomic,copy) NSString  *userId;
/**
 userMobile = 15814520421;
 */
@property (nonatomic,copy) NSString  *userMobile;
/**
 userName = "\U4e00\U4e8c\U4e09\U56db";
 */
@property (nonatomic,copy) NSString  *userName;

@end

NS_ASSUME_NONNULL_END
