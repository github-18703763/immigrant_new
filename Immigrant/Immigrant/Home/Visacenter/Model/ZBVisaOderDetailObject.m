//
//  ZBVisaOderDetailObject.m
//  Immigrant
//
//  Created by 张波 on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBVisaOderDetailObject.h"

@implementation ZBVisaOderDetailObject

-(void)setOrderVisa:(NSDictionary *)orderVisa{
    _orderVisa = orderVisa;
    self.visaObject = [ZBVisaObject mj_objectWithKeyValues:orderVisa];
}

@end
