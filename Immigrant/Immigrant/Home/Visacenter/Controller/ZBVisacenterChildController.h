//
//  ZBVisacenterChildController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZBVisacenterChildController;

@protocol ZBVisacenterChildControllerDelegate

-(void)didSelect:(ZBVisacenterChildController *)child withIndexPath:(NSIndexPath *)path withModel:(id)model;

@end

@interface ZBVisacenterChildController : UITableViewController

@property (nonatomic,copy) NSString *titleStr;
@property (nonatomic,weak) id<ZBVisacenterChildControllerDelegate> delgate;

@end
