//
//  ZBVisacenterDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterDetailController.h"
#import "ZBVisacenterDetailHeadView.h"
#import "ZBVisacenterDetailSectionOneHeadView.h"
#import "ZBVisacenterDetailSectionTwoHeadView.h"
#import "ZBVisacenterDetailCell.h"
#import "ZBVisacenterHandleController.h"
#import "GYLoginRegisterVC.h"

@interface ZBVisacenterDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *handleBtn;
@property (weak, nonatomic) IBOutlet UITableView *handleTable;

@property (nonatomic,strong) ZBVisaObject  *currentModel;

@property (nonatomic,weak) ZBVisacenterDetailHeadView  *headView;

@end

@implementation ZBVisacenterDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.handleBtn.layer.cornerRadius = 25;
    self.handleBtn.layer.masksToBounds = YES;
    self.handleTable.showsHorizontalScrollIndicator = NO;
    self.handleTable.showsVerticalScrollIndicator = NO;
    self.handleTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0   );
    self.handleTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    ZBVisacenterDetailHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterDetailHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 355);
    self.handleTable.tableHeaderView = headView;
    self.headView = headView;
    self.handleTable.delegate = self;
    self.handleTable.dataSource = self;
    UIView *view = [UIView new];
    view.backgroundColor = COLOR(252, 252, 252, 1);
    view.frame = CGRectMake(0, 0, SystemScreenWidth, 100);
    self.handleTable.tableFooterView = view;
    [self.handleTable registerNib:[UINib nibWithNibName:@"ZBVisacenterDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [self loadData];
    
}

-(void)loadData{
    WS(weakSELF);
    [QSNetworkManager visaDetial:self.model.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.currentModel = [ZBVisaObject mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            weakSELF.headView.model = weakSELF.currentModel;
            [weakSELF.handleTable reloadData];
        }else{
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.localizedDescription duration:0.8 position:CSToastPositionBottom];
    }];
}

- (IBAction)handleAction:(id)sender {
    if (![QSUserManager shareInstance].isLogin) {
        XKNavigationController *nav = [[XKNavigationController alloc]  initWithRootViewController:[GYLoginRegisterVC new]];
        //                    GYLoginRegisterVC *login = [GYLoginRegisterVC new];
        [self presentViewController:nav animated:YES completion:nil];
        return;
    }
    ZBVisacenterHandleController *handle = [ZBVisacenterHandleController new];
    handle.model = self.currentModel;
    [self.navigationController pushViewController:handle animated:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }else{
        if (self.currentModel) {
            return 1;
        }else{
            return 0;
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBVisacenterDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        ZBVisacenterDetailSectionOneHeadView *hv = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterDetailSectionOneHeadView" owner:self options:nil] firstObject];
        hv.model = self.currentModel;
        return hv;
    }else{
        ZBVisacenterDetailSectionTwoHeadView *hv = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterDetailSectionTwoHeadView" owner:self options:nil] firstObject];
        return hv;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 267;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 207;
    }else{
        return 50;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
