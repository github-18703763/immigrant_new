//
//  ZBSummerCampController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterSearchController.h"
#import "ZBVisacenterChildCell.h"
#import "ZBVisacenterDetailController.h"

@interface ZBVisacenterSearchController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;
@property (weak, nonatomic) IBOutlet UITableView *campTable;

@end

@implementation ZBVisacenterSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"签证中心";
    self.searchTxt.layer.cornerRadius = 6;
    self.searchTxt.layer.masksToBounds = YES;
    if (!self.navigationItem.leftBarButtonItem) {
         self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.campTable.delegate = self;
    self.campTable.dataSource  = self;
    self.campTable.showsHorizontalScrollIndicator = NO;
    self.campTable.showsVerticalScrollIndicator = NO;
    self.campTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0   );
    self.campTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.campTable.backgroundColor = COLOR(252, 252, 252, 1);
    
    [self.campTable registerNib:[UINib nibWithNibName:@"ZBVisacenterChildCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

- (void)backAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else{
         return 10;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return nil;
    }
    ZBVisacenterChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 145;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *v = [UIView new];
        v.backgroundColor = [UIColor redColor];
        return v;
    }else{
        return [UIView new];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 300;
    }else{
        return 0.01;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBVisacenterDetailController *detial = [ZBVisacenterDetailController new];
    [self.navigationController pushViewController:detial animated:YES];
    
}


@end
