//
//  GYVisacenterVC.m
//  Immigrant
//
//  Created by a on 2018/12/28.
//  Copyright © 2018 张波. All rights reserved.
//

#import "GYVisacenterVC.h"
#import "FSBaseTableView.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "FSBottomTableViewCell.h"
#import "ZBVisacenterDetailController.h"

#import "ZBVisacenterHeadView.h"
#import "FSScrollContentViewController.h"
#import "ZBCountryObject.h"
#import "ZBBannerImageObject.h"
#import "VisaDetailController.h"
@interface GYVisacenterVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,FSPageContentViewDelegate,FSSegmentTitleViewDelegate,FSScrollContentViewControllerDelegae>

@property(nonatomic,strong)FSBaseTableView *tableView;

@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@property (nonatomic, strong) FSBottomTableViewCell *contentCell;

@property (nonatomic, assign) BOOL canScroll;

@property (nonatomic,strong) NSMutableArray  *countryArray;

@property (nonatomic,strong) NSMutableArray  *photoArray;

@property (nonatomic,strong) ZBVisacenterHeadView  *headView;

@end

@implementation GYVisacenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"签证中心";
    
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self loadCountry];
    
    self.canScroll = YES;
    self.tableView = [[FSBaseTableView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, self.view.frame.size.height) style:UITableViewStylePlain];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    ZBVisacenterHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 245);
    self.tableView.tableHeaderView = headView;
    self.headView = headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBVisacenterHeadViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [self.tableView  registerClass:[FSBottomTableViewCell class] forCellReuseIdentifier:@"cell"];
    
    headView.searchStyle =  VisacenterSearch;
    [self loadPhoto];
    
}

-(void)loadPhoto{
    WS(weakSELF);
    [QSNetworkManager takeBannerPhoto:@"visa" successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.photoArray = [ZBBannerImageObject mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            weakSELF.headView.photoArray = weakSELF.photoArray;
        }else{
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.localizedDescription duration:0.8 position:CSToastPositionBottom];
    }];
}

-(void)loadCountry{
    WS(weakSELF);
    [QSNetworkManager visaCoutryListsuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.countryArray = [ZBCountryObject mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            [weakSELF.tableView reloadData];
        }else{
               [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.localizedDescription duration:0.8 position:CSToastPositionBottom];
    }];
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeScrollStatus) name:@"leaveTop" object:nil];

}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    //    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"leaveTop" object:nil];

}

#pragma mark notify(修改主tableview滑动状态)

- (void)changeScrollStatus//改变主视图的状态
{
    self.canScroll = YES;
    if (self.tableView.scrollEnabled==NO) {
        self.tableView.scrollEnabled = YES;
        
    }
  
    self.contentCell.cellCanScroll = NO;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.countryArray.count == 0) {
        return 0;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _contentCell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
//    if (!_contentCell) {
//        _contentCell = [[FSBottomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//
        NSMutableArray *childVCs = [[NSMutableArray alloc]init];
    
    for (NSInteger i=0;i<self.countryArray.count;i++) {
        ZBCountryObject *contry = self.countryArray[i];
        FSScrollContentViewController *vc = [[FSScrollContentViewController alloc]init];
        vc.str = contry.chinesename;
        vc.delegage = self;
        vc.contry = self.countryArray[indexPath.row];
        vc.tableView.scrollEnabled = NO;
        
        [childVCs addObject:vc];
    }
        _contentCell.viewControllers = childVCs;

         _contentCell.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-50) childVCs:childVCs parentVC:self delegate:self];
        
        _contentCell.pageContentView.contentViewCurrentIndex = 0;
        [_contentCell addSubview: _contentCell.pageContentView];
        
  //  }
    
 
    return _contentCell;
}

-(void)didSelect:(NSIndexPath *)path withModel:(ZBVisaObject *)model{
    VisaDetailController *vc = [[VisaDetailController alloc] init];
    vc.visaId = [model.id integerValue];
    [self.navigationController pushViewController:vc animated:YES];

//    ZBVisacenterDetailController *visa = [ZBVisacenterDetailController new];
//    visa.model = model;
//    [self.navigationController pushViewController:visa animated:YES];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    for (ZBCountryObject *object in self.countryArray) {
        [array addObject:object.chinesename];
    }
    
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:array delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    self.titleView.selectIndex = 0;
    
    return self.titleView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGFloat offsetY = scrollView.contentOffset.y;
    if ([scrollView isKindOfClass:[self.tableView class]]) {
        if (offsetY>=245) {
            scrollView.contentOffset = CGPointMake(0, 245);
            self.canScroll = NO;
            self.contentCell.cellCanScroll = YES;
        }
        else{
            if (!self.canScroll) {
                //子视图没到顶部
                scrollView.contentOffset = CGPointMake(0, 245);
            }
        }
    }
}

#pragma mark --
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SystemScreenHeight-50;
}
@end
