//
//  ZBVisacenterHandleController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBVisaObject.h"

@interface ZBVisacenterHandleController : UITableViewController

@property (nonatomic,strong) ZBVisaObject  *model;

@end
