//
//  ZBVisacenterOrderControler.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterOrderControler.h"
#import "ZBVisacenterOrderHeadView.h"
#import "ZBPayMethodView.h"
#import "ZBVisacenterPaySuccessController.h"
#import "ZBVisaOderDetailObject.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>

@interface ZBVisacenterOrderControler ()

@property (nonatomic,strong) ZBVisaOderDetailObject  *orderObject;

@property (nonatomic,strong) NSDictionary  *payDict;

@end

@implementation ZBVisacenterOrderControler

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"签订订单";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self loadOderDetail];
    
    
}

-(void)loadOderDetail{
    WS(weakSELF);
    [QSNetworkManager orderDetail:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            
            weakSELF.orderObject = [ZBVisaOderDetailObject mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            ZBVisacenterOrderHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterOrderHeadView" owner:self options:nil] firstObject];
            headView.orderObject = weakSELF.orderObject;
            headView.frame = CGRectMake(0, 0, SystemScreenWidth, 810);
            weakSELF.tableView.tableHeaderView = headView;
            headView.payBlock = ^{
                ZBPayMethodView *pay = [[[NSBundle mainBundle] loadNibNamed:@"ZBPayMethodView" owner:self options:nil] firstObject];
                pay.frame = [UIScreen mainScreen].bounds;
                [[UIApplication sharedApplication].delegate.window addSubview:pay];
                pay.alipayBlock = ^{
                    
                    [QSNetworkManager appAliPay:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                        if (responseModel.status == 200) {
                            [[AlipaySDK defaultService] payUrlOrder:responseModel.data[@"bussData"][@"payInfo"] fromScheme:@"Immigrant" callback:^(NSDictionary *resultDic) {
//                                ZBLog(@"%@",resultDic);
                                if ([resultDic[@"resultCode"] integerValue] == 9000) {//支付成功

                                }else{//支付失败

                                }
                            }];
                        }else{
                            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                        }
                        
                    } failBlock:^(NSError * _Nullable error) {
                        [[UIApplication sharedApplication].delegate.window makeToast:@"提交支付失败" duration:0.8 position:CSToastPositionBottom];
                    }];
                    
                
//                    ZBVisacenterPaySuccessController *success = [ZBVisacenterPaySuccessController new];
//                    [weakSELF.navigationController pushViewController:success animated:YES];
                };
                pay.wechatPayBlock = ^{
                    
                    [QSNetworkManager appWeChatPay:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                        if (responseModel.status == 200) {
                            self.payDict = responseModel.data[@"bussData"];
                            [self jumpToBizPay];
                        }else{
                            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                        }
                    
                    } failBlock:^(NSError * _Nullable error) {
                        [[UIApplication sharedApplication].delegate.window makeToast:@"提交支付失败" duration:0.8 position:CSToastPositionBottom];
                    }];
                    
                };
            };
            
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}


-(void)jumpToBizPay {
    
    if (![WXApi isWXAppInstalled]) {
        
        UIAlertController *alertCon=[UIAlertController alertControllerWithTitle:nil message:@"您尚未安装微信" preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *action1=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            return ;
        }];
        
        UIAlertAction *action2=[UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            return ;
        }];
        
        [alertCon addAction:action1];
        [alertCon addAction:action2];
        
        [self presentViewController:alertCon animated:YES completion:nil];
        
        return;
        
    }
    
    //    NSMutableString *timeStamp=self.payDict[@"timestamp"];
    //调起微信支付
    PayReq* req  = [[PayReq alloc] init];
    req.openID= self.payDict[@"appid"];
    req.partnerId =self.payDict[@"partnerid"];
    //    self.payModel.partnerId;
    req.prepayId =self.payDict[@"prepayid"];
    //    self.payModel.prepayId;
    req.nonceStr  = self.payDict[@"noncestr"];
    //    self.payModel.nonceStr;
    req.timeStamp  = [self.payDict[@"timestamp"] integerValue];
    //    self.payModel.timeStamp;
    req.package =self.payDict[@"packageDesc"];
    //    @"Sign=WXPay";
    //    [NSString stringWithFormat:@"%li",[self.payDict[@"packageValue"] integerValue]];
    //    @"Sign=WXPay";
    req.sign=self.payDict[@"sign"];
    
//    WPLog(@"%@--%@--%@--%@--%li--%@",self.payDict[@"appid"],self.payDict[@"partnerid"],self.payDict[@"prepayid"],self.payDict[@"noncestr"],[self.payDict[@"timestamp"] integerValue],self.payDict[@"package"]);
    
    //    DataMD5 *md=[[DataMD5 alloc]init];
    //    NSString *signStr=[md createMD5SingForPay:@"wxb45ba06b1c5b2292" partnerid:req.partnerId prepayid:req.prepayId package:req.package noncestr:req.nonceStr timestamp:req.timeStamp];
    //    req.sign=[md createMD5SingForPay:@"wxb45ba06b1c5b2292" partnerid:req.partnerId prepayid:req.prepayId package:req.package noncestr:req.nonceStr timestamp:req.timeStamp];
    [WXApi sendReq:req];
    
}

@end
