//
//  ZBVisacenterController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterController.h"
#import "ZBVisacenterHeadView.h"
#import "ZBVisacenterHeadViewCell.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "ZBVisacenterChildController.h"
#import "ZBVisacenterDetailController.h"

@interface ZBVisacenterController ()<FSPageContentViewDelegate,FSSegmentTitleViewDelegate,ZBVisacenterChildControllerDelegate>

@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@property (nonatomic,weak) NSMutableArray *childVCs;

@end

@implementation ZBVisacenterController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"签证中心";
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    ZBVisacenterHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 245);
    self.tableView.tableHeaderView = headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBVisacenterHeadViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    headView.searchStyle =  VisacenterSearch;

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([scrollView isKindOfClass:[self.tableView class]]) {
        if (offsetY>=200) {
            for (ZBVisacenterChildController *child in self.childVCs) {
                child.tableView.scrollEnabled = YES;
            }
        }else{
            for (ZBVisacenterChildController *child in self.childVCs) {
                child.tableView.scrollEnabled = NO;
            }
        }
        
    }
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didSelect:(ZBVisacenterChildController *)child withIndexPath:(NSIndexPath *)path withModel:(id)model{
    ZBVisacenterDetailController *detial = [ZBVisacenterDetailController new];
    [self.navigationController pushViewController:detial animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBVisacenterHeadViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSMutableArray *childVCs = [[NSMutableArray alloc]init];
    for (NSString *title in @[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"]) {
        ZBVisacenterChildController *vc = [[ZBVisacenterChildController alloc]init];
        vc.delgate = self;
        vc.titleStr = title;
        vc.tableView.scrollEnabled = NO;
        [childVCs addObject:vc];
    }
    self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-50) childVCs:childVCs parentVC:self delegate:self];
    self.pageContentView.contentViewCurrentIndex = 2;
    [cell addSubview:self.pageContentView];
    self.childVCs = childVCs;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:@[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"] delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    self.titleView.selectIndex = 2;
    
    return self.titleView;
}

#pragma mark --
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
    //    self.title = @[@"法国",@"加拿大",@"美国",@"中国",@"澳大利亚"][endIndex];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 170;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SystemScreenHeight-50;
}

@end
