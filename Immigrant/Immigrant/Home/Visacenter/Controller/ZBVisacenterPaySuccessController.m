//
//  ZBVisacenterPaySuccessController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterPaySuccessController.h"
#import "ZBVisacenterPaySuccessHeadView.h"

@interface ZBVisacenterPaySuccessController ()

@end

@implementation ZBVisacenterPaySuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"支付成功";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ZBVisacenterPaySuccessHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterPaySuccessHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 611);
    self.tableView.tableHeaderView = headView;
    
}

@end
