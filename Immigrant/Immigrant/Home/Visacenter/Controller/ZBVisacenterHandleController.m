//
//  ZBVisacenterHandleController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBVisacenterHandleController.h"
#import "ZBVisacenterPaySuccessController.h"
#import "ZBVisacenterHandleHeadView.h"
#import "ZBVisacenterOrderControler.h"

@interface ZBVisacenterHandleController ()

@end

@implementation ZBVisacenterHandleController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.title = @"签证办理";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ZBVisacenterHandleHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBVisacenterHandleHeadView" owner:self options:nil] firstObject];
    headView.model = self.model;
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 650);
    self.tableView.tableHeaderView = headView;
    WS(weakSELF);
    headView.commitOrderBlock = ^(NSDictionary *dict) {
        [QSNetworkManager visaCommitOrder:dict successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {//成功
                NSString *oderId = [responseModel.data[@"bussData"] stringValue];
                ZBVisacenterOrderControler *order = [ZBVisacenterOrderControler new];
                order.orderId = oderId;
                [weakSELF.navigationController pushViewController:order animated:YES];
            }else{//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];

    };
}

@end
