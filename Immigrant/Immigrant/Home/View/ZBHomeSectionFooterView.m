//
//  ZBHomeSectionFooterView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBHomeSectionFooterView.h"
#import "ZBPolicyListController.h"

@interface ZBHomeSectionFooterView()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UILabel *markLab;
@property (nonatomic,assign) NSInteger  currentIndex;

//定时器
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ZBHomeSectionFooterView
- (IBAction)listAction:(id)sender {
    ZBPolicyListController *list = [ZBPolicyListController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:list];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:list animated:YES];
}

-(void)setDataArray:(NSArray *)dataArray{
    _dataArray = dataArray;
    if (_dataArray) {
        ZBPolicyModel *model = _dataArray[0];
        self.markLab.text = model.title;
        self.currentIndex = 0;
        if (self.timer) {
            [self.timer invalidate];
            self.timer = nil;
            self.timer = [NSTimer timerWithTimeInterval:2 target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        }else{
            self.timer = [NSTimer timerWithTimeInterval:2 target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        }
    }else{
        self.markLab.text = @"";
    }
}

-(void)nextPage{
    self.currentIndex ++;
    if (self.currentIndex == self.dataArray.count) {
        self.currentIndex = 0;
    }
    ZBPolicyModel *model = _dataArray[self.currentIndex];
    self.markLab.text = model.title;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 25;
    self.containerView.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
    
}

@end
