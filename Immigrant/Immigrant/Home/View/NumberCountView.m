//
//  NumberCountView.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "NumberCountView.h"

@interface NumberCountView()

@property(nonatomic, strong) UIButton *minusBtn;

@property(nonatomic, strong) UIButton *addBtn;

@property(nonatomic, strong) UITextField *numberTextField;

@end

@implementation NumberCountView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _count = 0;
        _maxCount = 99;
        [self masLayout];
    }
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    self.backgroundColor = [UIColor getColor:@"EDEDF0"];
    self.layer.cornerRadius = 22.25;
    self.layer.masksToBounds = true;
    [self addSubview:self.minusBtn];
    [self addSubview:self.addBtn];
    [self addSubview:self.numberTextField];
    
    [self.minusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.right.equalTo(self.numberTextField.mas_left);
    }];
    
    [self.numberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.top.bottom.equalTo(self);
        make.width.mas_equalTo(44);
    }];
    
    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self.numberTextField.mas_right);
    }];
}


#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    if (btn == self.minusBtn) {
        if (_count >= 1) {
            self.count -= 1;
        }
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    } else if (btn == self.addBtn) {
        if (_count < _maxCount) {
            self.count += 1;
        }
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

#pragma mark - lazyload

- (UIButton *)minusBtn {
    if (!_minusBtn) {
        ;
        _minusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_minusBtn setImage:[UIImage imageNamed:@"jian"] forState:UIControlStateNormal];
        [_minusBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _minusBtn;
}

- (UIButton *)addBtn {
    if (!_addBtn) {
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn setImage:[UIImage imageNamed:@"jia"] forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addBtn;
}

- (UITextField *)numberTextField {
    if (!_numberTextField) {
        _numberTextField = [[UITextField alloc] init];
        _numberTextField.keyboardType = UIKeyboardTypeNumberPad;
        _numberTextField.userInteractionEnabled = false;
        _numberTextField.font = [UIFont systemFontOfSize:17];
        _numberTextField.textColor = [UIColor getColor:@"4E4E4E"];
        _numberTextField.textAlignment = NSTextAlignmentCenter;
        _numberTextField.text = @"0";
    }
    return _numberTextField;
}

- (void)setCount:(NSInteger)count {
    _count = count;
    _numberTextField.text = @(count).stringValue;
}

@end
