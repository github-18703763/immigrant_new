//
//  ZBHomeSectionHeaderView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBHomeSectionHeaderView.h"
#import "ZBAnecdoteListController.h"

@implementation ZBHomeSectionHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
}
- (IBAction)listAction:(id)sender {
    ZBAnecdoteListController *list = [ZBAnecdoteListController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:list];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:list animated:YES];
}

@end
