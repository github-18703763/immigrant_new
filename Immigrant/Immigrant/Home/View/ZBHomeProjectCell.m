//
//  ZBHomeProjectCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBHomeProjectCell.h"

@interface ZBHomeProjectCell()

@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *markLab;

@end

@implementation ZBHomeProjectCell

- (void)setMrgrate:(ZBMigrateObject *)mrgrate{
    _mrgrate = mrgrate;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_mrgrate.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = @"移民项目";
    self.markLab.text = _mrgrate.name;
}
-(void)setStudy:(ZBStudyObject *)study{
    _study = study;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:study.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = @"海外留学";
    self.markLab.text = study.name;
}

- (void)setHouse:(ZBHouseObject *)house{
    _house = house;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:house.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = @"海外房产";
    self.markLab.text = house.name;
}

- (void)setCamp:(ZBCampObject *)camp{
    _camp = camp;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:camp.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = @"冬夏令营";
    self.markLab.text = camp.name;
}

-(void)setInspection:(ZBInspectionObject *)inspection{
    _inspection = inspection;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:inspection.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = @"考察定制";
    self.markLab.text = inspection.name;
}

-(void)setVisa:(ZBVisaObject *)visa{
    _visa = visa;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:visa.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.nameLab.text = @"签证中心";
    self.markLab.text = visa.name;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.photoImg.layer.cornerRadius = 15;
    self.photoImg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
