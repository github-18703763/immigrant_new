//
//  ZBHomeHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBHomeHeadView.h"
#import "XRCarouselView.h"
#import "ZBImmigrationProjectController.h"
#import "ZBInspectionHomeController.h"
#import "ZBVisacenterController.h"
#import "ZBSummerCampController.h"
#import "ZBRealEstateController.h"
#import "ZBOverseasStudyHomeController.h"
#import "ZBOverseasMedicalHomeController.h"
#import "ZBBannerDetialController.h"
#import "GYVisacenterVC.h"
#import "ZBAboutUsVC.h"
@interface ZBHomeHeadView()<XRCarouselViewDelegate>

@end

@implementation ZBHomeHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    
}

-(void)setImgArray:(NSArray *)imgArray{
    
    _imgArray = imgArray;
    NSMutableArray *imageArray = [NSMutableArray new];
    NSMutableArray *desArray = [NSMutableArray new];
    NSMutableArray *desArray1 = [NSMutableArray new];
    NSMutableArray *desArray2 = [NSMutableArray new];
    for (ZBBannerImageObject *object in _imgArray) {
        [imageArray addObject:object.image];
        [desArray addObject:object.slidertypetext];
        [desArray1 addObject:object.modifier];
        [desArray2 addObject:object.creator];
    }
    desArray[1] = @"一就你卡了号上课了弄啥嘞舒服而后悔佛是一usabjhsaobgib";
    XRCarouselView *xr=[[XRCarouselView alloc]initWithFrame:CGRectMake(0,0,SystemScreenWidth,350) imageArray:imageArray];
    xr.desLabelBgColor = [UIColor clearColor];
    xr.desLabelColor = [UIColor whiteColor];
    xr.desLabelFont = [UIFont systemFontOfSize:15];
//    xr.describeArray = desArray;
//    xr.describeArray1 = desArray1;
//    xr.describeArray2 = desArray2;
    xr.delegate=self;
    xr.pagePosition = PositionBottomRight;
    [self addSubview:xr];
    
}

- (void)carouselView:(XRCarouselView *)carouselView didClickImage:(NSInteger)index{
    ZBBannerDetialController *banner = [ZBBannerDetialController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:banner];
    banner.model = self.imgArray[index];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:banner animated:YES];
}

- (IBAction)actionOne:(UIButton *)sender {
//    ZBLog(@"移民项目");
//    WS(weakSELF);
    
    ZBImmigrationProjectController *project = [ZBImmigrationProjectController new];
//    project.backBlock = ^{
//        [weakSELF currentViewController].navigationController.navigationBar.hidden = YES;
//    };
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:project];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:project animated:YES];
}

- (IBAction)actionTwo:(UIButton *)sender {
//    ZBLog(@"签证中心");
//    ZBVisacenterController *visa = [ZBVisacenterController new];

    GYVisacenterVC *visa = [GYVisacenterVC new];
    
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:visa];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:visa animated:YES];

}

- (IBAction)actionThree:(UIButton *)sender {
//   ZBLog(@"海外房产");
    ZBRealEstateController *realEstate = [ZBRealEstateController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:realEstate];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:realEstate animated:YES];

}

- (IBAction)actionFour:(UIButton *)sender {
//    ZBLog(@"冬夏令营");
    ZBSummerCampController *summerCamp = [ZBSummerCampController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:summerCamp];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:summerCamp animated:YES];

}

- (IBAction)actionFive:(UIButton *)sender {
//    ZBLog(@"定制考察");
    
    ZBInspectionHomeController *inspection = [ZBInspectionHomeController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:inspection];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:inspection animated:YES];

}

- (IBAction)actionSix:(UIButton *)sender {
//    ZBLog(@"海外留学");
    ZBOverseasStudyHomeController *study = [ZBOverseasStudyHomeController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:study];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:study animated:YES];

}

- (IBAction)actionSeven:(UIButton *)sender {
//    ZBLog(@"海外医疗");
    ZBOverseasMedicalHomeController *medical = [ZBOverseasMedicalHomeController new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:medical];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:medical animated:YES];

}

- (IBAction)actionEight:(UIButton *)sender {
//    ZBLog(@"关于我们");
    ZBAboutUsVC *vc = [ZBAboutUsVC new];
//    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:vc];
//    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    [[self currentViewController].navigationController pushViewController:vc animated:YES];

}


@end
