//
//  ImgTitleContainCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImgTitleContainCell : UITableViewCell

@property(nonatomic, strong) UIImageView *leftImgv;
@property(nonatomic, strong) UILabel *titleLabel;

- (void)updateImgWidth:(CGFloat) width;
@end

NS_ASSUME_NONNULL_END
