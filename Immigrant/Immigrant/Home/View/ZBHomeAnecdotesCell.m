//
//  ZBHomeAnecdotesCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBHomeAnecdotesCell.h"

@interface ZBHomeAnecdotesCell()
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation ZBHomeAnecdotesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
}

-(void)setModel:(ZBNewsOjbect *)model{
    
    _model = model;
   [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.titleLab.text = _model.title;
    self.timeLab.text = _model.gmtCreated;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
