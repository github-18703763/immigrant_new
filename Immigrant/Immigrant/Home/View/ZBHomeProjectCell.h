//
//  ZBHomeProjectCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBNewsOjbect.h"
#import "ZBPolicyModel.h"
#import "ZBVisaObject.h"
#import "ZBHouseObject.h"
#import "ZBCampObject.h"
#import "ZBInspectionObject.h"
#import "ZBMigrateObject.h"
#import "ZBStudyObject.h"

@interface ZBHomeProjectCell : UITableViewCell

@property (nonatomic,strong) ZBMigrateObject  *mrgrate;

@property (nonatomic,strong) ZBStudyObject  *study;

@property (nonatomic,strong) ZBHouseObject  *house;

@property (nonatomic,strong) ZBCampObject  *camp;

@property (nonatomic,strong) ZBInspectionObject *inspection;

@property (nonatomic,strong) ZBVisaObject  *visa;

@end
