//
//  WebContainCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/18.
//  Copyright © 2019 张波. All rights reserved.
//

#import "WebContainCell.h"

@interface WebContainCell ()

@property(nonatomic, strong) UIView *backView;

@end

@implementation WebContainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(24);
        make.right.equalTo(self.contentView).offset(-24);
        make.bottom.equalTo(self.contentView).offset(-20);
    }];
    
}

#pragma mark - Getter

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 6);
        _backView.layer.shadowRadius = 20.5;
        _backView.layer.shadowColor = [UIColor getColor:@"b2b2b2"].CGColor;
        _backView.layer.shadowOpacity = 0.44;
    }
    return _backView;
}

@end
