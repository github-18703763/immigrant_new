//
//  NumberCountView.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NumberCountView : UIControl

@property(nonatomic, assign) NSInteger count;

@property(nonatomic, assign) NSInteger maxCount;

@end

NS_ASSUME_NONNULL_END
