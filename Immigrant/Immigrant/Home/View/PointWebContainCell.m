//
//  PointWebContainCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/18.
//  Copyright © 2019 张波. All rights reserved.
//

#import "PointWebContainCell.h"

@interface PointWebContainCell ()

@property(nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIView *pointView;

@end

@implementation PointWebContainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.pointView];
    [self.backView addSubview:self.titleLabel];

    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(24);
        make.right.equalTo(self.contentView).offset(-24);
        make.bottom.equalTo(self.contentView).offset(-20);
    }];
    
    [self.pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(12));
        make.top.equalTo(self.backView).offset(25);
        make.left.equalTo(self.backView).offset(17.5);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.pointView);
        make.left.equalTo(self.backView).offset(40.5);
    }];
    
}

#pragma mark - Getter

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 6);
        _backView.layer.shadowRadius = 20;
        _backView.layer.shadowColor = [UIColor getColor:@"b2b2b2"].CGColor;
        _backView.layer.shadowOpacity = 0.44;
    }
    return _backView;
}

- (UIView *)pointView {
    if (!_pointView) {
        _pointView = [[UIView alloc] init];
        _pointView.backgroundColor = [UIColor getColor:@"267BFF"];
        _pointView.layer.cornerRadius = 6;
        _pointView.layer.shadowOffset = CGSizeMake(0, 2);
        _pointView.layer.shadowRadius = 6.5;
        _pointView.layer.shadowColor = [UIColor getColor:@"267bff"].CGColor;
        _pointView.layer.shadowOpacity = 0.26;
    }
    return _pointView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor getColor:@"2F2F2F"];
        _titleLabel.font = [UIFont systemFontOfSize:18];
    }
    return _titleLabel;
}

@end
