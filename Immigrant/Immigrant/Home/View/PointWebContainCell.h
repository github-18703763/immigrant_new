//
//  PointWebContainCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/18.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PointWebContainCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;


@end

NS_ASSUME_NONNULL_END
