//
//  ZBHomeAnecdotesCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/14.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBNewsOjbect.h"

@interface ZBHomeAnecdotesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (nonatomic,strong) ZBNewsOjbect  *model;

@end
