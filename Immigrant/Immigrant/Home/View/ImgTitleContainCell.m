//
//  ImgTitleContainCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import "ImgTitleContainCell.h"

@interface ImgTitleContainCell ()

@property(nonatomic, strong) UIView *backView;

@end

@implementation ImgTitleContainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.leftImgv];
    [self.backView addSubview:self.titleLabel];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(5);
        make.left.equalTo(self.contentView).offset(24);
        make.right.equalTo(self.contentView).offset(-24);
        make.bottom.equalTo(self.contentView).offset(-5);
        make.height.equalTo(@(50));
    }];
    
    [self.leftImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(22.5);
        make.width.height.mas_equalTo(12);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(44);
        make.right.equalTo(self.backView);
    }];
    
}

- (void)updateImgWidth:(CGFloat) width {
    [self.leftImgv mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(width);
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(22.5);
    }];
}

#pragma mark - Getter

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 1);
        _backView.layer.shadowRadius = 3;
        _backView.layer.shadowColor = [UIColor getColor:@"a9a9a9"].CGColor;
        _backView.layer.shadowOpacity = 0.12;
    }
    return _backView;
}

- (UIImageView *)leftImgv {
    if (!_leftImgv) {
        _leftImgv = [[UIImageView alloc] init];
    }
    return _leftImgv;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
    }
    return _titleLabel;
}


@end
