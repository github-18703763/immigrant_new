//
//  NumberCountCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "NumberCountCell.h"

@implementation NumberCountCell
@synthesize countView = _countView;
@synthesize leftLabel = _leftLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}


#pragma mark - Private Method
- (void)masLayout {
    [self.contentView addSubview:self.leftLabel];
    [self.contentView addSubview:self.countView];
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.lessThanOrEqualTo(@(170));
        make.centerY.equalTo(self.countView);
        make.left.equalTo(self.contentView).offset(25);
    }];
    
    [self.countView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-23);
        make.height.equalTo(@(44));
        make.width.mas_equalTo(102);
        make.bottom.equalTo(self.contentView).offset(-13);
        make.top.equalTo(self.contentView).offset(19.5);
    }];
}

#pragma mark - Getter

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.font = [UIFont systemFontOfSize:17];
        _leftLabel.textColor = [UIColor getColor:@"848484"];
    }
    return _leftLabel;
}

- (NumberCountView *)countView {
    if (!_countView) {
        _countView = [[NumberCountView alloc] init];
    }
    return _countView;
}

@end
