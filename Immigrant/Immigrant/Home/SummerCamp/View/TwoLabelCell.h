//
//  TwoLabelCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TwoLabelCell : UITableViewCell

@property (nonatomic, strong, readonly) UILabel *leftLabel;
@property (nonatomic, strong, readonly) UILabel *rightLabel;

@end

NS_ASSUME_NONNULL_END
