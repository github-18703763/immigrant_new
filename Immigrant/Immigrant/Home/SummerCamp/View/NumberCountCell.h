//
//  NumberCountCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumberCountView.h"
NS_ASSUME_NONNULL_BEGIN

@interface NumberCountCell : UITableViewCell

@property (nonatomic, strong, readonly) NumberCountView *countView;
@property (nonatomic, strong, readonly) UILabel *leftLabel;

@end

NS_ASSUME_NONNULL_END
