//
//  ZBInspectionImmediatelyPayCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSummerCampOrderCell.h"
#import "ZBPayMethodView.h"
#import "ZBMyOrderModel.h"
@interface ZBSummerCampOrderCell()
@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;

@end

@implementation ZBSummerCampOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.orderView.layer.cornerRadius = 25;
    self.orderView.layer.masksToBounds = YES;
//    [self.orderLab setAttributedText:[NSMutableAttributedString createColorString:@"订单编号：2018101610048575" withExcision:@"订单编号：" dainmaicColor:[UIColor getColor:@"4A4A4A"] excisionColor:[UIColor getColor:@"267BFF"]]];
    self.containerView.backgroundColor = [UIColor whiteColor];
    [self.containerView addShadow];
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
    self.payBtn.layer.cornerRadius = 25;
    self.payBtn.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
//     [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:@"￥8888元/人" withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
}
- (IBAction)payAction:(id)sender {
    
    ZBPayMethodView *pay = [[[NSBundle mainBundle] loadNibNamed:@"ZBPayMethodView" owner:self options:nil] firstObject];
    
    pay.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:pay];
    
    pay.alipayBlock = ^{
        
    };
    
    pay.wechatPayBlock = ^{
        
    };
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCell:(ZBMyOrderModel *)model {
    
    [self.orderLab setAttributedText:[NSMutableAttributedString createColorString:[NSString stringWithFormat:@"订单编号：%@", model.no] withExcision:@"订单编号：" dainmaicColor:[UIColor getColor:@"4A4A4A"] excisionColor:[UIColor getColor:@"267BFF"]]];

    if (model.orderCamp != nil) {
        ZBMyOrderCampModel *tmp = model.orderCamp;
        [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:[NSString stringWithFormat:@"￥%@元/人", tmp.totalpriceyuan] withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
        
        self.titleLabel.text = tmp.name;
        self.timeLabel.text = tmp.datestr;
        
    } else if (model.orderCampStandby != nil) {
        ZBMyOrderCampStandbyModel *tmp = model.orderCampStandby;
        [self.moneyLab setAttributedText:[NSMutableAttributedString changeLabelWithText:[NSString stringWithFormat:@"￥%@元/人", @(tmp.totalpriceyuan)] withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
        
        self.titleLabel.text = tmp.name;
        self.timeLabel.text = tmp.datestr;
        
        
        
    }
}

@end
