//
//  ZBInspectionRouteDetailHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSummerCampDetialHeadView.h"

@interface ZBSummerCampDetialHeadView()
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *containerOne;
@property (weak, nonatomic) IBOutlet UIView *containerTwo;
@property (weak, nonatomic) IBOutlet UIView *containerThree;
@property (weak, nonatomic) IBOutlet UIView *containerFour;
@property (weak, nonatomic) IBOutlet UIView *containerFive;
@property (weak, nonatomic) IBOutlet UIView *pointOne;
@property (weak, nonatomic) IBOutlet UIView *pointTwo;
@property (weak, nonatomic) IBOutlet UIView *pointThree;

@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *manCountLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoTwoImg;
@property (weak, nonatomic) IBOutlet UILabel *activityIntroductionLab;//活动介绍

@property (weak, nonatomic) IBOutlet UILabel *applicationNotesLab;//报名须知
@end

@implementation ZBSummerCampDetialHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0,SystemScreenWidth, 255) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = path.bounds;
    maskLayer.path=path.CGPath;
    self.photoImg.layer.mask=maskLayer;
    self.containerOne.backgroundColor = [UIColor whiteColor];
    [self.containerOne addShadow];
    self.containerTwo.backgroundColor = [UIColor whiteColor];
    [self.containerTwo addShadow];
    self.containerThree.backgroundColor = [UIColor whiteColor];
    [self.containerThree addShadow];
    
    self.containerFour.backgroundColor = [UIColor whiteColor];
    [self.containerFour addShadow];
    self.containerFive.backgroundColor = [UIColor whiteColor];
    [self.containerFive addShadow];

    self.pointOne.layer.cornerRadius = 4;
    self.pointOne.layer.masksToBounds = YES;
    self.pointTwo.layer.cornerRadius = 4;
    self.pointTwo.layer.masksToBounds = YES;
    self.pointThree.layer.cornerRadius = 4;
    self.pointThree.layer.masksToBounds = YES;
}
-(void)setDicAll:(NSDictionary *)dicAll{
    _dicAll = dicAll;
    //这里可能是轮播图
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_dicAll[@"cover"]] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    NSArray *aryStart = [_dicAll[@"gmtCreated"] componentsSeparatedByString:@" "];
    NSArray *aryEnd = [_dicAll[@"gmtModified"] componentsSeparatedByString:@" "];
    self.timeLab.text = [NSString stringWithFormat:@"%@到%@",aryStart[0],aryEnd[0]];
    //当前人数
    NSInteger allCount = [_dicAll[@"totalnum"] integerValue];
    NSInteger currentCount = [_dicAll[@"currentnum"] integerValue];
    if (currentCount == allCount) {
       self.manCountLab.text = [NSString stringWithFormat:@"%li人 (已报满)",allCount];
    }
    self.manCountLab.text = [NSString stringWithFormat:@"%li人",allCount];
    self.activityIntroductionLab.text = _dicAll[@"introduce"];
    self.applicationNotesLab.text = _dicAll[@"notice"];
    
//    "country": "英国",
//    "countryId": 0,
//    "cover": "cover.png",
//    "creator": "string",
//    "currentnum": 0,
//    "depositfee": 12888900,
//    "depositfeeyuan": 128889,
//    "edate": "2018-10-20",
//    "gmtCreated": "2018-12-27T07:19:25.424Z",
//    "gmtModified": "2018-12-27T07:19:25.424Z",
//    "id": 0,
//    "images": [
//               "string"
//               ],
//    "imageskey": "img1.png,img2.png",
//    "introduce": "活动介绍。。。",
//    "isDeleted": "string",
//    "modifier": "string",
//    "name": "温哥华滑雪探秘冬令营",
//    "notice": "报名须知。。。",
//    "plan": "日程安排。。。",
//    "sdate": "2018-10-10",
//    "totalnum": 30,
//    "totalprice": 23463000,
//    "totalpriceyuan": 234630

}
@end
