//
//  PointLabelCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "PointLabelCell.h"

@implementation PointLabelCell
@synthesize pointView = _pointView;
@synthesize leftLabel = _leftLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Method
- (void)masLayout {
    [self.contentView addSubview:self.leftLabel];
    [self.contentView addSubview:self.pointView];
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(46.5);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-12.5);
        make.top.equalTo(self.contentView).offset(12.5);
        make.width.lessThanOrEqualTo(@(170));
    }];
    
    [self.pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(8);
        make.left.equalTo(self.contentView).offset(24);
        make.centerY.equalTo(self.contentView);
    }];
}

- (void)hidepoint {
    self.pointView.hidden = true;
    
    [self.leftLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    }];
}

#pragma mark - Getter

- (UIView *)pointView {
    if (!_pointView) {
        _pointView = [[UIView alloc] init];
        _pointView.backgroundColor = [UIColor getColor:@"267BFF"];
        _pointView.layer.cornerRadius = 4;
        _pointView.layer.shadowOffset = CGSizeMake(0, 2);
        _pointView.layer.shadowRadius = 6.5;
        _pointView.layer.shadowColor = [UIColor getColor:@"267bff"].CGColor;
        _pointView.layer.shadowOpacity = 0.26;
    }
    return _pointView;
}

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.font = [UIFont boldSystemFontOfSize:20];
        _leftLabel.textColor = [UIColor getColor:@"4a4a4a"];
    }
    return _leftLabel;
}


@end
