//
//  CampInfoCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CampModel;
NS_ASSUME_NONNULL_BEGIN

@interface CampInfoCell : UITableViewCell

- (void)configCell:(id)object;

@end

NS_ASSUME_NONNULL_END
