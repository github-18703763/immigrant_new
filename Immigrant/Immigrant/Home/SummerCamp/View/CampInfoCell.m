//
//  CampInfoCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "CampInfoCell.h"
#import "ZBCampObject.h"
#import "ZBMyOrderModel.h"
#import <UIImageView+WebCache.h>
#import "ZBInspectionObject.h"
#import "LineModel.h"
@interface CampInfoCell()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *campImgv;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIImageView *timeImgv;
@property (nonatomic, strong) UIImageView *leftImgv;


@end

@implementation CampInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Methods

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.campImgv];
    [self.backView addSubview:self.timeImgv];
    [self.backView addSubview:self.leftImgv];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.timeLabel];
    [self.backView addSubview:self.numberLabel];
    [self.backView addSubview:self.priceLabel];

    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(16);
        make.bottom.equalTo(self.contentView).offset(-5);
        make.height.mas_equalTo(130);
    }];
    
    [self.campImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(22);
        make.top.equalTo(self.backView).offset(17);
        make.bottom.equalTo(self.backView).offset(-17);
        make.width.mas_equalTo(123);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.campImgv.mas_right).offset(20);
        make.top.equalTo(self.backView).offset(20.5);
        make.right.equalTo(self.backView).offset(-20);
    }];
    
    [self.timeImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.campImgv.mas_right).offset(19);
        make.width.mas_equalTo(12);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8.5);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.timeImgv);
        make.right.equalTo(self.backView).offset(-20);
        make.left.equalTo(self.timeImgv.mas_right).offset(9);
    }];
    
    [self.leftImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.campImgv.mas_right).offset(19);
        make.width.mas_equalTo(12);
        make.height.mas_equalTo(12);
        make.top.equalTo(self.timeImgv.mas_bottom).offset(11.5);
    }];
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImgv);
        make.right.equalTo(self.backView).offset(-20);
        make.left.equalTo(self.leftImgv.mas_right).offset(9);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backView).offset(-17);
        make.right.equalTo(self.backView).offset(-31);
    }];
    
}

- (void)configCell:(id)object {
    
    if ([object isKindOfClass:[CampModel class]]) {
        CampModel *obj = object;
        
        [self.campImgv sd_setImageWithURL:[NSURL URLWithString:obj.cover]];
        self.titleLabel.text = obj.name;
        
        if (obj.sdate.length >= 10 && obj.edate.length >= 10) {
            self.timeLabel.text = [NSString stringWithFormat:@"%@-%@",[obj.sdate substringToIndex:10], [obj.edate substringToIndex:10]];
        }
        
        NSInteger number = obj.totalnum - obj.currentnum;
        if (number < 0) {
            number = 0;
        }
        
        self.numberLabel.text = [NSString stringWithFormat:@"剩余%ld人", number];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        self.priceLabel.text = [NSString stringWithFormat:@"￥%@元/人", [numberFormatter stringFromNumber:@(obj.totalpriceyuan)]];
    } else if ([object isKindOfClass:[ZBMyOrderModel class]]) {
        ZBMyOrderModel *obj = object;
        
        if (obj.orderCamp != nil) {
            ZBMyOrderCampModel *tmp = obj.orderCamp;
            [self.campImgv sd_setImageWithURL:[NSURL URLWithString:tmp.cover]];

            [self.priceLabel setAttributedText:[NSMutableAttributedString changeLabelWithText:[NSString stringWithFormat:@"￥%@元/人", tmp.totalpriceyuan] withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
            
            self.titleLabel.text = tmp.name;
            self.timeLabel.text = tmp.datestr;
            self.numberLabel.text = [NSString stringWithFormat:@"剩余%@人", tmp.totalnum];
        } else if (obj.orderCampStandby != nil) {
            ZBMyOrderCampStandbyModel *tmp = obj.orderCampStandby;
            [self.campImgv sd_setImageWithURL:[NSURL URLWithString:tmp.cover]];

            [self.priceLabel setAttributedText:[NSMutableAttributedString changeLabelWithText:[NSString stringWithFormat:@"￥%@元/人", @(tmp.totalpriceyuan)] withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
            
            self.titleLabel.text = tmp.name;
            self.timeLabel.text = tmp.datestr;
            self.numberLabel.text = [NSString stringWithFormat:@"剩余%ld人", tmp.totalnum];
        } else if (obj.orderLine != nil) {
            ZBMyOrderLineModel *tmp = obj.orderLine;
            [self.campImgv sd_setImageWithURL:[NSURL URLWithString:tmp.cover]];
            
            [self.priceLabel setAttributedText:[NSMutableAttributedString changeLabelWithText:[NSString stringWithFormat:@"￥%@元/人", tmp.totalpriceyuan] withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
            
            self.titleLabel.text = tmp.name;
            self.timeLabel.text = tmp.datestr;
            self.numberLabel.text = [NSString stringWithFormat:@"剩余%@人", tmp.totalnum];
        } else if (obj.orderLineStandby != nil) {
            ZBMyOrderLineStandbyModel *tmp = obj.orderLineStandby;
            [self.campImgv sd_setImageWithURL:[NSURL URLWithString:tmp.cover]];
            
            [self.priceLabel setAttributedText:[NSMutableAttributedString changeLabelWithText:[NSString stringWithFormat:@"￥%@元/人", tmp.totalpriceyuan] withBigFont:[UIFont systemFontOfSize:16 weight:0.4] withNeedchangeText:@"/人" withSmallFont:[UIFont systemFontOfSize:13] dainmaicColor:[UIColor getColor:@"437DFF"] excisionColor:[UIColor getColor:@"4E4E4E"]]];
            
            self.titleLabel.text = tmp.name;
            self.timeLabel.text = tmp.datestr;
            self.numberLabel.text = [NSString stringWithFormat:@"剩余%@人", tmp.totalnum];
        }
        
    } else if ([object isKindOfClass:[ZBInspectionObject class]]) {
        ZBInspectionObject *obj = object;
        
        [self.campImgv sd_setImageWithURL:[NSURL URLWithString:obj.cover]];
        self.titleLabel.text = obj.name;
        
        if (obj.sdate.length >= 10 && obj.edate.length >= 10) {
            self.timeLabel.text = [NSString stringWithFormat:@"%@-%@",[obj.sdate substringToIndex:10], [obj.edate substringToIndex:10]];
        }
        
        NSInteger number = [obj.totalnum integerValue] - [obj.currentnum integerValue];
        if (number < 0) {
            number = 0;
        }
        
        self.numberLabel.text = [NSString stringWithFormat:@"剩余%ld人", number];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        self.priceLabel.text = [NSString stringWithFormat:@"￥%@元/人", obj.totalpriceyuan];
    }  else if ([object isKindOfClass:[LineModel class]]) {
        LineModel *obj = object;
        
        [self.campImgv sd_setImageWithURL:[NSURL URLWithString:obj.cover]];
        self.titleLabel.text = obj.name;
        
        if (obj.sdate.length >= 10 && obj.edate.length >= 10) {
            self.timeLabel.text = [NSString stringWithFormat:@"%@-%@",[obj.sdate substringToIndex:10], [obj.edate substringToIndex:10]];
        }
        
        NSInteger number = obj.totalnum - obj.currentnum;
        if (number < 0) {
            number = 0;
        }
        
        self.numberLabel.text = [NSString stringWithFormat:@"剩余%ld人", number];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        self.priceLabel.text = [NSString stringWithFormat:@"￥%@元/人", @(obj.totalpriceyuan)];
    }
    
    
}

#pragma mark - Getter

- (UIImageView *)campImgv {
    if (!_campImgv) {
        _campImgv = [[UIImageView alloc] init];
        _campImgv.contentMode = UIViewContentModeScaleAspectFill;
        _campImgv.layer.cornerRadius = 8;
        _campImgv.layer.masksToBounds = true;
        _campImgv.clipsToBounds = true;
    }
    return _campImgv;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 15;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0, 2);
        _backView.layer.shadowRadius = 21.5;
        _backView.layer.shadowColor = [UIColor getColor:@"a9a9a9"].CGColor;
        _backView.layer.shadowOpacity = 0.12;
    }
    return _backView;
}

- (UIImageView *)timeImgv {
    if (!_timeImgv) {
        _timeImgv = [[UIImageView alloc] init];
        _timeImgv.image = [UIImage imageNamed:@"shijian"];
    }
    return _timeImgv;
}

- (UIImageView *)leftImgv {
    if (!_leftImgv) {
        _leftImgv = [[UIImageView alloc] init];
        _leftImgv.image = [UIImage imageNamed:@"yiminbanli_shenfen"];
    }
    return _leftImgv;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textColor = [UIColor getColor:@"4E4E4E"];
    }
    return _titleLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textColor = [UIColor getColor:@"4E4E4E"];
    }
    return _timeLabel;
}

- (UILabel *)numberLabel {
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc] init];
        _numberLabel.font = [UIFont systemFontOfSize:14];
        _numberLabel.textColor = [UIColor getColor:@"4E4E4E"];
    }
    return _numberLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont boldSystemFontOfSize:16];
        _priceLabel.textColor = [UIColor getColor:@"437DFF"];
    }
    return _priceLabel;
}


@end
