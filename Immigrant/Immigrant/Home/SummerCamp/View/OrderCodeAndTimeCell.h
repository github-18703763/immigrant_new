//
//  OrderCodeAndTimeCell.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderCodeAndTimeCell : UITableViewCell

@property (nonatomic, strong, readonly) UIView *backView;
@property (nonatomic, strong, readonly) UILabel *codeLabel;
@property (nonatomic, strong, readonly) UILabel *timeLabel;

@end

NS_ASSUME_NONNULL_END
