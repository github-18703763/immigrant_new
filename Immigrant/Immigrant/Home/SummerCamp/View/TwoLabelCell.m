//
//  TwoLabelCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "TwoLabelCell.h"

@implementation TwoLabelCell
@synthesize leftLabel = _leftLabel;
@synthesize rightLabel = _rightLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Method
- (void)masLayout {
    [self.contentView addSubview:self.leftLabel];
    [self.contentView addSubview:self.rightLabel];
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(24);
        make.width.lessThanOrEqualTo(@(170));
        make.centerY.equalTo(self.contentView);
        make.height.mas_equalTo(60);
        make.bottom.top.equalTo(self.contentView);
    }];
    
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-24);
        make.width.lessThanOrEqualTo(@(170));
        make.centerY.equalTo(self.contentView);
        make.height.mas_equalTo(60);
    }];
}

#pragma mark - Getter

- (UILabel *)rightLabel {
    if (!_rightLabel) {
        _rightLabel = [[UILabel alloc] init];
        _rightLabel.font = [UIFont systemFontOfSize:17];
        _rightLabel.textColor = [UIColor getColor:@"848484"];
        _rightLabel.textAlignment = NSTextAlignmentRight;
    }
    return _rightLabel;
}

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.font = [UIFont systemFontOfSize:17];
        _leftLabel.textColor = [UIColor getColor:@"848484"];
    }
    return _leftLabel;
}

@end
