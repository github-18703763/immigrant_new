//
//  TextFieldCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "TextFieldCell.h"

@interface TextFieldCell()

@end

@implementation TextFieldCell
@synthesize leftLabel = _leftLabel;
@synthesize textField = _textField;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

#pragma mark - Private Method
- (void)masLayout {
    [self.contentView addSubview:self.leftLabel];
    [self.contentView addSubview:self.textField];
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(24);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-9.5);
        make.width.lessThanOrEqualTo(@(170));
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-23);
        make.left.equalTo(self.contentView).offset(190);
        make.height.equalTo(@(44));
        make.bottom.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(16);
    }];
}

#pragma mark - Getter

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.font = [UIFont systemFontOfSize:17];
        _leftLabel.textColor = [UIColor getColor:@"848484"];
    }
    return _leftLabel;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.font = [UIFont systemFontOfSize:17];
        _textField.textColor = [UIColor getColor:@"4A4A4A"];
        _textField.textAlignment = NSTextAlignmentRight;
    }
    return _textField;
}


@end
