//
//  OrderCodeCell.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import "OrderCodeCell.h"

@implementation OrderCodeCell
@synthesize backView = _backView;
@synthesize codeLabel = _codeLabel;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self masLayout];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)masLayout {
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.codeLabel];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(23);
        make.right.equalTo(self.contentView).offset(-23);
        make.top.equalTo(self.contentView).offset(5);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(50);
    }];
    
    [self.codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.backView);
        make.width.lessThanOrEqualTo(self.backView);
    }];
}

#pragma mark - Getter

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        
        _backView.backgroundColor = [UIColor getColor:@"c0c0c0"];
        _backView.layer.cornerRadius = 25;
        _backView.layer.shadowOffset = CGSizeMake(0, 10);
        _backView.layer.shadowRadius = 9;
        _backView.layer.shadowColor = [UIColor getColor:@"a9a9a9"].CGColor;
        _backView.layer.shadowOpacity = 0.22;
    }
    return _backView;
}

- (UILabel *)codeLabel {
    if (!_codeLabel) {
        _codeLabel = [[UILabel alloc] init];
    }
    return _codeLabel;
}

@end
