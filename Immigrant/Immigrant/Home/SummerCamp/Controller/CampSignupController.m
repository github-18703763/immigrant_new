//
//  CampSignupController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "CampSignupController.h"
#import "NumberCountView.h"
#import "UITableViewCell+identifier.h"
#import "TextFieldCell.h"
#import "CampInfoCell.h"
#import "NumberCountCell.h"
#import "PointLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "TwoLabelCell.h"
#import "OrderCampAddModel.h"
#import "QSNetworkManager+order.h"
#import "ZBSummerCampOrderController.h"
#import "CampOrderController.h"

@interface CampSignupController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NumberCountView *countView;

@property (nonatomic, strong) UITableView *table;

@property (nonatomic, strong) UIButton *confirmBtn;

@property (nonatomic, strong) UIView *shadowView;

@property (nonatomic, strong) NSArray<NSString *> *titleArray;

@property (nonatomic, strong) OrderCampAddModel *addModel;

@end

@implementation CampSignupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.countView];
    self.title = @"立即报名";
    [self masLayout];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.titleArray = @[@"姓名",@"手机号",@"出行人数",@"活动定金"];
    self.addModel = [[OrderCampAddModel alloc] init];
}

- (void)masLayout {
    
    [self.view addSubview:self.table];
    [self.view addSubview:self.shadowView];
    [self.view addSubview:self.confirmBtn];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(214);
        make.bottom.equalTo(self.view).offset(-51);
        make.centerX.equalTo(self.view);
    }];
    
    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.confirmBtn);
    }];
    
    [self.table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.confirmBtn.mas_top);
    }];
}

#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    if (btn == self.confirmBtn) {
        
        if (self.addModel.userName.length <= 0) {
            [MBProgressHUD showError:@"请输入姓名"];
            return;
        }
        
        if (self.addModel.userMobile.length <= 0) {
            [MBProgressHUD showError:@"请输入手机号"];
            return;
        }
        
        if (self.addModel.booknum <= 0) {
            [MBProgressHUD showError:@"请增加人数"];
            return;
        }
        
        self.addModel.campId = self.campModel.id;
        
        if ((self.campModel.totalnum - self.campModel.currentnum) > 0) {
            [QSNetworkManager addCampOrder:self.addModel successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {//成功
                    CampOrderController *vc = [[CampOrderController alloc] init];
                    vc.orderId = [responseModel.data[@"bussData"] stringValue];
                    [self.navigationController pushViewController:vc animated:true];
                    
                } else {//失败
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
        } else {
            [QSNetworkManager addCampOrderStandby:self.addModel successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {//成功
                    CampOrderController *vc = [[CampOrderController alloc] init];
                    vc.orderId = responseModel.data[@"bussData"];
                    [self.navigationController pushViewController:vc animated:true];
                } else {//失败
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
        }
    }
}

- (void)textChanged:(UITextField *)textField {
    
    TextFieldCell *nameCell = (TextFieldCell *)[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    TextFieldCell *mobileCell = (TextFieldCell *)[self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];

    if (textField == nameCell.textField) {
        self.addModel.userName = textField.text;
    } else if (textField == mobileCell.textField) {
        self.addModel.userMobile = textField.text;
    }
}

- (void)numberCountChange:(NumberCountView *)countView {
    self.addModel.booknum = countView.count;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 1;
        case 2:
            return 4;
        default:
            return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        
        CampInfoCell *cell = [CampInfoCell cellForTableView:tableView];
        
        [cell configCell:self.campModel];
        
        return cell;
        
    } else if (indexPath.section == 1) {
        PointLabelCell *cell = [PointLabelCell cellForTableView:tableView];
        cell.leftLabel.text = @"报名信息";
        return cell;
    } else if (indexPath.section == 2) {
        
        if (indexPath.row == 2) {
            NumberCountCell *cell = [NumberCountCell cellForTableView:tableView];
            cell.leftLabel.text = @"出行人数";
            cell.countView.maxCount = self.campModel.totalnum;
            [cell setBottomSeparatorInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
            
            [cell.countView addTarget:self action:@selector(numberCountChange:) forControlEvents:UIControlEventValueChanged];
            
            return cell;
        } else if (indexPath.row == 3) {
            TwoLabelCell *cell = [TwoLabelCell cellForTableView:tableView];
            cell.leftLabel.text = self.titleArray[indexPath.row];
            cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", @(self.campModel.depositfeeyuan)];
            cell.leftLabel.textColor = [UIColor getColor:@"B7B7B7"];
            cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
            return cell;
        } else {
            TextFieldCell *cell = [TextFieldCell cellForTableView:tableView];
            cell.leftLabel.text = self.titleArray[indexPath.row];
            cell.textField.placeholder = [NSString stringWithFormat:@"请输入%@", self.titleArray[indexPath.row]];
            [cell setBottomSeparatorInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
            
            [cell.textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
            
            return cell;
        }
    }
    return [UITableViewCell cellForTableView:tableView];
}


#pragma mark - Getter

- (NumberCountView *)countView {
    if (!_countView) {
        _countView = [[NumberCountView alloc] initWithFrame: CGRectMake(0, 100, 102, 44)];
    }
    return _countView;
}

- (UITableView *)table {
    if (!_table) {
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _table.delegate = self;
        _table.dataSource = self;
        _table.tableFooterView = [[UIView alloc] init];
        _table.estimatedRowHeight = 60;
        _table.rowHeight = UITableViewAutomaticDimension;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}

- (UIButton *)confirmBtn {
    if (!_confirmBtn) {
        _confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"确认报名" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_confirmBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _confirmBtn.layer.cornerRadius = 25;
        _confirmBtn.layer.masksToBounds = true;
        [_confirmBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmBtn;
}

- (UIView *)shadowView {
    if (!_shadowView) {
        _shadowView = [[UIView alloc] init];
        _shadowView.backgroundColor = UIColor.whiteColor;
        _shadowView.layer.cornerRadius = 25;
        _shadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _shadowView.layer.shadowRadius = M_PI_2;
        _shadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _shadowView.layer.shadowOpacity = 0.26;
    }
    return _shadowView;
}

@end
