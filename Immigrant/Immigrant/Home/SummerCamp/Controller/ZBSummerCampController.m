//
//  ZBSummerCampController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSummerCampController.h"
#import "ZBInspectionRouteCell.h"
#import "ZBSummerCampDetialController.h"
#import "ZBCampObject.h"
#import "CampDetailController.h"
@interface ZBSummerCampController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;
@property (weak, nonatomic) IBOutlet UITableView *campTable;
@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;

@end

@implementation ZBSummerCampController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"冬夏令营";
    self.searchTxt.layer.cornerRadius = 6;
    self.searchTxt.layer.masksToBounds = YES;
    if (!self.navigationItem.leftBarButtonItem) {
         self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.campTable.delegate = self;
    self.campTable.dataSource  = self;
    self.campTable.showsHorizontalScrollIndicator = NO;
    self.campTable.showsVerticalScrollIndicator = NO;
    self.campTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0   );
    self.campTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.campTable.backgroundColor = COLOR(252, 252, 252, 1);
    
    [self.campTable registerNib:[UINib nibWithNibName:@"ZBInspectionRouteCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.campTable.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.campTable.mj_footer = footer;
    
    [self loadData:YES];
    
}
-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager campFindpageNameLinek:@"" withPage:1 withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.campTable.mj_footer endRefreshing];
            [weakSELF.campTable.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBCampObject mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.campTable reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.campTable.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBCampObject mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.campTable.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.campTable.mj_footer endRefreshing];
            [weakSELF.campTable.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.campTable.mj_footer endRefreshing];
        [weakSELF.campTable.mj_header endRefreshing];
    }];
}
- (void)backAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else{
         return self.newsArray.count;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return nil;
    }
    ZBInspectionRouteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.modelCamp = self.newsArray[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 145;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *v = [UIView new];
        v.backgroundColor = [UIColor redColor];
        return v;
    }else{
        return [UIView new];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 300;
    }else{
        return 0.01;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    ZBSummerCampDetialController *detail = [ZBSummerCampDetialController new];
//    detail.modelCamp = self.newsArray[indexPath.row];
//    [self.navigationController pushViewController:detail animated:YES];
    CampDetailController *vc = [[CampDetailController alloc] init];
    ZBCampObject *obj = self.newsArray[indexPath.row];
    vc.campId = [obj.id integerValue];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
