//
//  ZBSummerCampOrderController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSummerCampOrderController.h"
#import "ZBSummerCampOrderCell.h"
#import "ZBMyOrderModel.h"
#import <YYModel.h>

@interface ZBSummerCampOrderController ()

@property (nonatomic, strong) ZBMyOrderModel *orderModel;

@end

@implementation ZBSummerCampOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"报名订单";
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBSummerCampOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 200;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchData {
    
    [QSNetworkManager orderDetail:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            NSDictionary *dic = responseModel.data[@"bussData"];
            self.orderModel = [ZBMyOrderModel yy_modelWithDictionary:dic];
            [self.tableView reloadData];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBSummerCampOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell configCell:self.orderModel];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
@end
