//
//  CampOrderController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import "CampOrderController.h"
#import "CampInfoCell.h"
#import "PointLabelCell.h"
#import "TwoLabelCell.h"
#import "UITableViewCell+Extension.h"
#import "UITableViewCell+identifier.h"
#import "ZBMyOrderModel.h"
#import <YYModel.h>
#import "OrderCodeCell.h"
#import "OrderCodeAndTimeCell.h"
#import "OrderPayController.h"
#import "OrderPayTool.h"
#import "GYOrderListVC.h"
@interface CampOrderController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray<NSString *> *titleArray;

@property (nonatomic, strong) UITableView *table;

@property (nonatomic, strong) UIButton *confirmBtn;

@property (nonatomic, strong) UIView *shadowView;

@property (nonatomic, strong) ZBMyOrderModel *orderModel;

@property (nonatomic, strong) UILabel *remarkLabel;

@end

@implementation CampOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"报名订单";
    [self masLayout];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.titleArray = @[@"姓名",@"手机号",@"出行人数",@"费用合计",@"活动定金"];
    [self fetchData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccess) name:NotifactionAlipayPaySuccess object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccess) name:NotifactionWeixinPaySuccess object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFail) name:NotifactionAlipayPayFail object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFail) name:NotifactionWeixinPayFail object:nil];
}

#pragma mark - Private Methods

- (void)masLayout {
    
    [self.view addSubview:self.table];
    [self.view addSubview:self.shadowView];
    [self.view addSubview:self.confirmBtn];
    [self.view addSubview:self.remarkLabel];
    
    [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.confirmBtn.mas_top).offset(-9);
    }];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(214);
        make.bottom.equalTo(self.view).offset(-51);
        make.centerX.equalTo(self.view);
    }];
    
    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.confirmBtn);
    }];
    
    [self.table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.remarkLabel.mas_top);
    }];
}

- (void)fetchData {
    
    [QSNetworkManager orderDetail:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            NSDictionary *dic = responseModel.data[@"bussData"];
            self.orderModel = [ZBMyOrderModel yy_modelWithDictionary:dic];
            [self changeUI];
            [self.table reloadData];
        } else {//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}


/**
 根据订单状态变更UI
 */
- (void)changeUI {
    if ([self.orderModel.orderstatus isEqualToString:@"unpaid"]) {
        self.confirmBtn.hidden = false;
        self.shadowView.hidden = false;
        self.remarkLabel.hidden = false;
    } else {
        self.confirmBtn.hidden = true;
        self.shadowView.hidden = true;
        self.remarkLabel.hidden = true;
        self.titleArray = @[@"姓名",@"手机号",@"出行人数",@"费用合计",@"活动定金",@"支付金额"];
    }
}

#pragma mark - Event Response

- (void)btnClick:(UIButton *)btn {
    OrderPayController *vc = [[OrderPayController alloc] init];
    vc.orderId = self.orderId;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:vc animated:true completion:nil];
}

- (void)paySuccess {
    GYOrderListVC *vc = [[GYOrderListVC alloc] init];
    vc.orderType = @"";
    vc.hidesBottomBarWhenPushed = true;
    [self.navigationController setViewControllers:@[vc] animated:true];
}

- (void)payFail {
    [[UIApplication sharedApplication].delegate.window makeToast:@"支付失败" duration:0.8 position:CSToastPositionBottom];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return self.titleArray.count;
            break;
        default:
            return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if ([self.orderModel.orderstatus isEqualToString:@"unpaid"]) {
            OrderCodeCell *cell = [OrderCodeCell cellForTableView:tableView];
            [cell.codeLabel setAttributedText:[NSMutableAttributedString createColorString:[NSString stringWithFormat:@"订单编号：%@", self.orderModel.no] withExcision:@"订单编号：" dainmaicColor:[UIColor getColor:@"4A4A4A"] excisionColor:[UIColor getColor:@"267BFF"]]];
            return cell;
        } else {
            OrderCodeAndTimeCell *cell = [OrderCodeAndTimeCell cellForTableView:tableView];
            cell.codeLabel.text = [NSString stringWithFormat:@"订单编号：%@", self.orderModel.no];
            cell.timeLabel.text = [NSString stringWithFormat:@"下单时间：%@", self.orderModel.gmtCreated];
            return cell;
        }
    } else if (indexPath.section == 1) {
        
        CampInfoCell *cell = [CampInfoCell cellForTableView:tableView];
        
        [cell configCell:self.orderModel];
        
        return cell;
        
    } else if (indexPath.section == 2) {
        PointLabelCell *cell = [PointLabelCell cellForTableView:tableView];
        cell.leftLabel.text = @"报名信息";
        return cell;
    } else if (indexPath.section == 3) {
        
        TwoLabelCell *cell = [TwoLabelCell cellForTableView:tableView];
        cell.leftLabel.text = self.titleArray[indexPath.row];
        cell.leftLabel.textColor = [UIColor getColor:@"848484"];
        
        switch (indexPath.row) {
            case 0:
                cell.rightLabel.text = self.orderModel.userName;
                cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
                cell.rightLabel.font = [UIFont systemFontOfSize:17];
                break;
            case 1:
                cell.rightLabel.text = self.orderModel.userMobile;
                cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
                cell.rightLabel.font = [UIFont systemFontOfSize:17];
                break;
            case 2:
                cell.rightLabel.text = @(self.orderModel.booknum).stringValue;
                cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
                cell.rightLabel.font = [UIFont systemFontOfSize:17];
                break;
            case 3:
                cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
                cell.rightLabel.font = [UIFont boldSystemFontOfSize:20];
                
                if (self.orderModel.orderCamp) {
                    CGFloat fee = [self.orderModel.orderCamp.totalpriceyuan floatValue];
                    CGFloat totalFee = fee * self.orderModel.booknum;
                    
                    cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", @(totalFee)];
                } else if (self.orderModel.orderCampStandby) {
                    CGFloat fee = self.orderModel.orderCampStandby.totalpriceyuan;
                    CGFloat totalFee = fee * self.orderModel.booknum;
                    
                    cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", @(totalFee)];
                }
                break;
            case 4:
                cell.rightLabel.textColor = [UIColor getColor:@"4E4E4E"];
                cell.rightLabel.font = [UIFont boldSystemFontOfSize:20];

                if (self.orderModel.orderCamp) {
                    cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", self.orderModel.orderCamp.depositfeeyuan];
                } else {
                    cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", @(self.orderModel.orderCampStandby.depositfeeyuan)];
                }
                break;
            case 5:
                cell.rightLabel.textColor = [UIColor getColor:@"267bff"];
                cell.rightLabel.font = [UIFont boldSystemFontOfSize:20];
                cell.rightLabel.text = [NSString stringWithFormat:@"￥%@", self.orderModel.payamoutyuan];
                break;
            default:
                break;
        }
        [cell setBottomSeparatorInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
        
        return cell;
    }
    return [UITableViewCell cellForTableView:tableView];
}


#pragma mark - Getter

- (UITableView *)table {
    if (!_table) {
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _table.delegate = self;
        _table.dataSource = self;
        _table.tableFooterView = [[UIView alloc] init];
        _table.estimatedRowHeight = 60;
        _table.rowHeight = UITableViewAutomaticDimension;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}

- (UIButton *)confirmBtn {
    if (!_confirmBtn) {
        _confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor getColor:@"2585F8"] withRect:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        NSAttributedString *btnString = [[NSAttributedString alloc] initWithString:@"立即支付" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor, NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        [_confirmBtn setAttributedTitle:btnString forState:UIControlStateNormal];
        _confirmBtn.layer.cornerRadius = 25;
        _confirmBtn.layer.masksToBounds = true;
        [_confirmBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmBtn;
}

- (UIView *)shadowView {
    if (!_shadowView) {
        _shadowView = [[UIView alloc] init];
        _shadowView.backgroundColor = UIColor.whiteColor;
        _shadowView.layer.cornerRadius = 25;
        _shadowView.layer.shadowOffset = CGSizeMake(0, 4.5);
        _shadowView.layer.shadowRadius = M_PI_2;
        _shadowView.layer.shadowColor = [UIColor getColor:@"267BFF"].CGColor;
        _shadowView.layer.shadowOpacity = 0.26;
    }
    return _shadowView;
}

- (UILabel *)remarkLabel {
    if (!_remarkLabel) {
        _remarkLabel = [[UILabel alloc] init];
        _remarkLabel.text = @"注：此定金不退，签单可抵扣";
        _remarkLabel.font = [UIFont systemFontOfSize:13.44];
        _remarkLabel.textColor = [UIColor getColor:@"D0D0D0"];
    }
    return _remarkLabel;
}


@end
