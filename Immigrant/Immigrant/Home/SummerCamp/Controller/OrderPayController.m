//
//  OrderPayController.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/17.
//  Copyright © 2019 张波. All rights reserved.
//

#import "OrderPayController.h"
#import "ZBPayMethodView.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>
#import "OrderPayTool.h"
@interface OrderPayController ()

@end

@implementation OrderPayController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ZBPayMethodView *pay = [[[NSBundle mainBundle] loadNibNamed:@"ZBPayMethodView" owner:self options:nil] firstObject];
    
    pay.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview: pay];
    
    pay.alipayBlock = ^{
        
        [QSNetworkManager appAliPay:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {
                [[AlipaySDK defaultService] payUrlOrder:responseModel.data[@"bussData"][@"payInfo"] fromScheme:@"Immigrant" callback:^(NSDictionary *resultDic) {
                    
                    [[OrderPayTool shared] alipayResult:resultDic];
                    //                                ZBLog(@"%@",resultDic);
//                    if ([resultDic[@"resultCode"] integerValue] == 9000) {//支付成功
//
//                    }else{//支付失败
//
//                    }
                }];
                [self dismissViewControllerAnimated:true completion:nil];
            }else{
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                [self dismissViewControllerAnimated:true completion:nil];
            }
            
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:@"提交支付失败" duration:0.8 position:CSToastPositionBottom];
            [self dismissViewControllerAnimated:true completion:nil];
        }];
    };
    
    pay.wechatPayBlock = ^{
        
        [QSNetworkManager appWeChatPay:self.orderId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {
                [[OrderPayTool shared] sendWeixinPayReqWith:responseModel.data[@"bussData"]];
                [self dismissViewControllerAnimated:true completion:nil];
//                self.payDict = responseModel.data[@"bussData"];
//                [self jumpToBizPay];
            }else{
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                [self dismissViewControllerAnimated:true completion:nil];
            }
            
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:@"提交支付失败" duration:0.8 position:CSToastPositionBottom];
            [self dismissViewControllerAnimated:true completion:nil];
        }];
        
    };
    
    pay.payMethodCloseBlock = ^ {
        [self dismissViewControllerAnimated:true completion:nil];
    };
}





@end
