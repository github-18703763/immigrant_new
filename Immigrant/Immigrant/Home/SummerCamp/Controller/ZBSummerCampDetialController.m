//
//  ZBInspectionRouteDetailController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSummerCampDetialController.h"
#import "ZBSummerCampDetialHeadView.h"
#import "ZBSummerCampSignupController.h"
#import "CampSignupController.h"
#import <YYModel.h>

@interface ZBSummerCampDetialController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *detailTable;
@property (weak, nonatomic) IBOutlet UIButton *willingBtn;
@property (strong,nonatomic)ZBSummerCampDetialHeadView *headView;
@end

@implementation ZBSummerCampDetialController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.detailTable.showsVerticalScrollIndicator = NO;
    self.detailTable.showsHorizontalScrollIndicator = NO;
    self.headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBSummerCampDetialHeadView" owner:self options:nil] firstObject];
    self.detailTable.tableHeaderView = self.headView;
    self.detailTable.tableFooterView = [UIView new];
    self.detailTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.detailTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.detailTable.estimatedSectionHeaderHeight = 250;
    self.detailTable.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.detailTable.rowHeight = UITableViewAutomaticDimension;
    self.detailTable.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    self.detailTable.delegate = self;
    self.detailTable.dataSource = self;
    [self.detailTable registerNib:[UINib nibWithNibName:@"ZBInspectionRouteDetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    self.willingBtn.layer.cornerRadius = 25;
    self.willingBtn.layer.masksToBounds = YES;
    [QSNetworkManager campDetial:self.modelCamp.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
    if (responseModel.status == 200) {//成功
        self.headView.dicAll = responseModel.data[@"bussData"];
        [self.detailTable reloadData];
    }else{//失败
        [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
    }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

- (IBAction)willingAction:(id)sender {
    CampSignupController *vc = [[CampSignupController alloc] init];
    
    CampModel *model = [CampModel yy_modelWithJSON:[self.modelCamp yy_modelToJSONObject]];
    vc.campModel = model;
    
//    ZBSummerCampSignupController *sure = [ZBSummerCampSignupController new];
    [self.navigationController pushViewController:vc animated:YES];
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    ZBSummerCampDetialHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBSummerCampDetialHeadView" owner:self options:nil] firstObject];
//    return headView;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

@end
