//
//  ZBOverseasMedicalHomeController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBOverseasMedicalHomeController.h"

@interface ZBOverseasMedicalHomeController ()

@end

@implementation ZBOverseasMedicalHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"海外医疗";
}
- (IBAction)backAction:(id)sender {
    if (self.navigationItem.leftBarButtonItem) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
