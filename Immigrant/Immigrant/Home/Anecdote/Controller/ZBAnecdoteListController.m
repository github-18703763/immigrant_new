//
//  ZBAnecdoteListController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/20.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBAnecdoteListController.h"
#import "ZBAnecdoteListCell.h"
#import "ZBAnecdoteDetialController.h"
#import "ZBNewsOjbect.h"

@interface ZBAnecdoteListController ()

@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;

@end

@implementation ZBAnecdoteListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"各国趣闻";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.estimatedRowHeight = 70;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBAnecdoteListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
    
    
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager newsPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBNewsOjbect mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBNewsOjbect mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
//    [QSNetworkManager plicyPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
//        if (responseModel.status == 200) {
//            NSInteger count = [responseModel.data[@"count"] integerValue];
//            [weakSELF.tableView.mj_footer endRefreshing];
//            [weakSELF.tableView.mj_header endRefreshing];
//            NSArray *array = responseModel.data[@"bussData"];
//            if (isRefresh) {
//                weakSELF.newsArray = [ZBNewsOjbect mj_objectArrayWithKeyValuesArray:array];
//                [weakSELF.tableView reloadData];
//                if (weakSELF.newsArray.count>=count) {
//                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
//                }
//            }else{
//                if (array.count>0) {
//                    [weakSELF.newsArray addObjectsFromArray:[ZBNewsOjbect mj_objectArrayWithKeyValuesArray:array]];
//                }else{
//                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
//                }
//            }
//        }else{//失败
//            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
//            [weakSELF.tableView.mj_footer endRefreshing];
//            [weakSELF.tableView.mj_header endRefreshing];
//        }
//        
//    } failBlock:^(NSError * _Nullable error) {
//        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
//        [weakSELF.tableView.mj_footer endRefreshing];
//        [weakSELF.tableView.mj_header endRefreshing];
//    }];
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBAnecdoteListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.newsArray.count>0) {
            cell.model = self.newsArray[indexPath.row];
    }

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBAnecdoteDetialController *detail = [ZBAnecdoteDetialController new];
    detail.model = self.newsArray[indexPath.row];
    [self.navigationController pushViewController:detail animated:YES];
}
@end
