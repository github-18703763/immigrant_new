//
//  ZBBannerDetialController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBBannerImageObject.h"

@interface ZBBannerDetialController : UITableViewController

@property (nonatomic,strong) ZBBannerImageObject  *model;

@end
