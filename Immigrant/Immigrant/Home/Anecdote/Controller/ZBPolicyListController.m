//
//  ZBPolicyListController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/20.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBPolicyListController.h"
#import "ZBPolicyListCell.h"
#import "ZBPolicyListDetialController.h"
#import "ZBPolicyModel.h"

@interface ZBPolicyListController ()

@property (nonatomic,strong) NSMutableArray  *policyArray;

@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@end

@implementation ZBPolicyListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"政策快讯";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.estimatedRowHeight = 70;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBPolicyListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
    
    
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager plicyPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.policyArray = [ZBPolicyModel mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.policyArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.policyArray addObjectsFromArray:[ZBPolicyModel mj_objectArrayWithKeyValuesArray:array]];
                }else{
                     [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
      
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.policyArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBPolicyListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.policyArray[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBPolicyListDetialController *detail = [ZBPolicyListDetialController new];
    detail.model = self.policyArray[indexPath.row];
    [self.navigationController pushViewController:detail animated:YES];
}

@end
