//
//  ZBPolicyListDetialCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/20.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBPolicyListDetialCell.h"
#import <WebKit/WebKit.h>

@interface ZBPolicyListDetialCell()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ZBPolicyListDetialCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://blog.csdn.net/meegomeego/article/details/40153199"] encoding:NSUTF8StringEncoding error:nil];
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    self.contentLab.attributedText = attrStr;
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    wkWebConfig.userContentController = wkUController;
    // 自适应屏幕宽度js
    NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    // 添加js调用
    [wkUController addUserScript:wkUserScript];
    
    
}

-(void)setModel:(ZBPolicyModel *)model{
    _model = model;
//    self.contentLab.text = _model.content;
    [self.webView loadHTMLString:_model.content baseURL:nil];
}

-(void)setNewsModel:(ZBNewsOjbect *)newsModel{
    _newsModel = newsModel;
//    self.contentLab.text = _newsModel.content;
    [self.webView loadHTMLString:_newsModel.content baseURL:nil];
}

-(void)setBannerModel:(ZBBannerImageObject *)bannerModel{
    _bannerModel = bannerModel;
//    self.contentLab.text = _bannerModel.content;
    [self.webView loadHTMLString:_bannerModel.content baseURL:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
