//
//  ZBPolicyListDetialCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/20.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBPolicyModel.h"
#import "ZBNewsOjbect.h"
#import "ZBBannerImageObject.h"

@interface ZBPolicyListDetialCell : UITableViewCell

@property (nonatomic,strong) ZBPolicyModel  *model;

@property (nonatomic,strong) ZBNewsOjbect  *newsModel;

@property (nonatomic,strong) ZBBannerImageObject  *bannerModel;

@end
