//
//  ZBAnecdoteListCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/26.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBAnecdoteListCell.h"

@interface ZBAnecdoteListCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation ZBAnecdoteListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 6;
    self.photoImg.layer.masksToBounds = YES;
}

-(void)setModel:(ZBNewsOjbect *)model{
    _model = model;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@""]];
    self.contentLab.text = _model.title;
    self.timeLab.text = _model.gmtCreated;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
