//
//  ZBPolicyListCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/20.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBPolicyListCell.h"

@interface ZBPolicyListCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation ZBPolicyListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
}

-(void)setModel:(ZBPolicyModel *)model{
    _model = model;
    self.contentLab.text = _model.title;
    self.timeLab.text = _model.gmtModified;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
