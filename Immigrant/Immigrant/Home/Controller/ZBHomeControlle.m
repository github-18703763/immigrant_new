//
//  ZBHomeControlle.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBHomeControlle.h"
#import "ZBHomeHeadView.h"
#import "ZBHomeProjectCell.h"
#import "ZBHomeSectionFooterView.h"
#import "ZBHomeSectionHeaderView.h"
#import "ZBHomeAnecdotesCell.h"
#import "ZBBannerImageObject.h"
#import "ZBNewsOjbect.h"
#import "ZBPolicyModel.h"
#import "ZBVisaObject.h"
#import "ZBHouseObject.h"
#import "ZBCampObject.h"
#import "ZBInspectionObject.h"
#import "ZBMigrateObject.h"
#import "ZBStudyObject.h"
#import "ZBAnecdoteDetialController.h"
#import "ZBImmrgrationListDetialController.h"
#import "ZBImmigrationListObject.h"
#import "ZBAboutUsVC.h"
#import "ZBVisacenterDetailController.h"
#import "ZBInspectionRouteDetailController.h"
#import "ZBSummerCampDetialController.h"
#import "ZBRealEstateDetailController.h"
#import "ZBOverseasStudyApplyDetailController.h"
#import "MigrateDetailController.h"
#import "CampDetailController.h"
#import "StudyApplyDetailController.h"
#import "LineDetailController.h"
#import "VisaDetailController.h"
#import "HouseDetailController.h"

@interface ZBHomeControlle ()

@property (nonatomic,strong) NSMutableArray  *imageArray;

@property (nonatomic,strong) NSMutableArray  *newsArray;

@property (nonatomic,strong) NSMutableArray  *policyArray;

@property (nonatomic,weak) ZBHomeHeadView  *headView;

@property (nonatomic,strong) ZBMigrateObject  *mrgrate;

@property (nonatomic,strong) ZBStudyObject  *study;

@property (nonatomic,strong) ZBHouseObject  *house;

@property (nonatomic,strong) ZBCampObject  *camp;

@property (nonatomic,strong) ZBInspectionObject *inspection;

@property (nonatomic,strong) ZBVisaObject  *visa;

@end

@implementation ZBHomeControlle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);

    self.tableView.bounces = NO;
    ZBHomeHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBHomeHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 495);
    self.tableView.tableHeaderView = headView;
    self.headView = headView;
    self.tableView.contentInset = UIEdgeInsetsMake(-StatuBarHeight, 0, 0, 0);
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBHomeProjectCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBHomeAnecdotesCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
    WS(weakSELF);
    [QSNetworkManager homeDatasuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            weakSELF.imageArray = [ZBBannerImageObject mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"][@"indexSliders"]];
            weakSELF.headView.imgArray = weakSELF.imageArray;
            weakSELF.newsArray = [ZBNewsOjbect mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"][@"news"]];
            weakSELF.policyArray =[ZBPolicyModel mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"][@"policys"]];
            
            weakSELF.mrgrate = [ZBMigrateObject mj_objectWithKeyValues:responseModel.data[@"bussData"][@"migrate"]];
            
           weakSELF.study = [ZBStudyObject mj_objectWithKeyValues:responseModel.data[@"bussData"][@"study"]];
            
            weakSELF.house = [ZBHouseObject mj_objectWithKeyValues:responseModel.data[@"bussData"][@"house"]];
            
            weakSELF.camp = [ZBCampObject mj_objectWithKeyValues:responseModel.data[@"bussData"][@"camp"]];
            
            weakSELF.inspection = [ZBInspectionObject mj_objectWithKeyValues:responseModel.data[@"bussData"][@"line"]];
            
            weakSELF.visa = [ZBVisaObject mj_objectWithKeyValues:responseModel.data[@"bussData"][@"visa"]];
            [weakSELF.tableView reloadData];
        }else{//失败
             [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
     [self.navigationController setNavigationBarHidden:YES animated:YES];
//    [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:@"sessionId"];
    NSLog(@"sessionId====%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"sessionId"]);
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return self.newsArray.count;
    }
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ZBHomeProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            cell.mrgrate = self.mrgrate;
        }
        if (indexPath.row == 1) {
            cell.study = self.study;
        }
        if (indexPath.row == 2) {
            cell.house = self.house;
        }
        if (indexPath.row == 3) {
            cell.camp = self.camp;
        }
        if (indexPath.row == 4) {
            cell.inspection = self.inspection;
        }
        if (indexPath.row == 5) {
            cell.visa = self.visa;
        }

        return cell;
    }else{
        ZBHomeAnecdotesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.newsArray[indexPath.row];
        cell.lineView.hidden = NO;
        if (indexPath.row == self.newsArray.count-1) {
            cell.lineView.hidden = YES;
        }
        return cell;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return 135;
    }
    return 288;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }
    ZBHomeSectionHeaderView *header = [[[NSBundle mainBundle] loadNibNamed:@"ZBHomeSectionHeaderView" owner:self options:nil] firstObject];
    return header;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return nil;
    }
    ZBHomeSectionFooterView *footer = [[[NSBundle mainBundle] loadNibNamed:@"ZBHomeSectionFooterView" owner:self options:nil] firstObject];
    footer.dataArray = self.policyArray;
    return footer;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return 0.01;
    }
    return 85;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.01;
    }
    return 66;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            MigrateDetailController *vc = [[MigrateDetailController alloc] init];
            vc.migrateId = [self.mrgrate.id integerValue];
            [self.navigationController pushViewController:vc animated:true];
            
//            ZBImmrgrationListDetialController *detial = [ZBImmrgrationListDetialController new];
//            ZBImmigrationListObject *model = [ZBImmigrationListObject new];
//            model.id = self.mrgrate.id;
//            detial.model = model;
//            [self.navigationController pushViewController:detial animated:YES];
        }
        if (indexPath.row == 1) {
            StudyApplyDetailController *vc = [[StudyApplyDetailController alloc] init];
            vc.studyId = [self.study.id integerValue];
            [self.navigationController pushViewController:vc animated:YES];
//
//            ZBOverseasStudyApplyDetailController *detial = [ZBOverseasStudyApplyDetailController new];
//            detial.studyId = [self.study.id integerValue];
//            [self.navigationController pushViewController:detial animated:YES];
            
        }
        if (indexPath.row == 2) {
            HouseDetailController *vc = [[HouseDetailController alloc] init];
            vc.houseId = [self.house.id integerValue];
            [self.navigationController pushViewController:vc animated:true];
//            ZBRealEstateDetailController *detial = [ZBRealEstateDetailController new];
//            detial.houseModel = self.house;
//            [self.navigationController pushViewController:detial animated:YES];
            
        }
        if (indexPath.row == 3) {
            CampDetailController *vc = [[CampDetailController alloc] init];
            vc.campId = [self.camp.id integerValue];
            [self.navigationController pushViewController:vc animated:true];
            
//            ZBSummerCampDetialController *detial = [ZBSummerCampDetialController new];
//            detial.modelCamp = self.camp;
//            [self.navigationController pushViewController:detial animated:YES];
            
        }
        if (indexPath.row == 4) {
            LineDetailController *vc = [[LineDetailController alloc] init];
            vc.lineId = [self.inspection.id integerValue];
            [self.navigationController pushViewController:vc animated:YES];

//            ZBInspectionRouteDetailController *detial = [ZBInspectionRouteDetailController new];
//            detial.model = self.inspection;
//            [self.navigationController pushViewController:detial animated:YES];
            
        }
        if (indexPath.row == 5) {
            VisaDetailController *vc = [[VisaDetailController alloc] init];
            vc.visaId = [self.visa.id integerValue];
            [self.navigationController pushViewController:vc animated:YES];

            
//            ZBVisacenterDetailController *detial = [ZBVisacenterDetailController new];
//            detial.model = self.visa;
//            [self.navigationController pushViewController:detial animated:YES];
            
        }
    }else{
        ZBAnecdoteDetialController *detail = [ZBAnecdoteDetialController new];
//        XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:detail];
        detail.model = self.newsArray[indexPath.row];
//        [self presentViewController:nav animated:YES completion:nil];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

@end
