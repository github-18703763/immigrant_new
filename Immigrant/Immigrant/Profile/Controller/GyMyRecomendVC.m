//
//  GyMyRecomendVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GyMyRecomendVC.h"
#import "GYMyRecmendCell.h"
#import "ZBMyNetworkingVC.h"
#import "ZBMyMailListVC.h"
#import "ZBMyGroupBuyReturnVC.h"

@interface GyMyRecomendVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong) NSMutableArray *modelArr;

@end

static NSString *recID = @"GYMyRecmendCell";

@implementation GyMyRecomendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的推荐";
    
    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GYMyRecmendCell" bundle:nil] forCellReuseIdentifier:recID];

    [self addHeadRefreshView];
    [self addLoadMoreView];
}

-(void)addHeadRefreshView{
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshHead)];
    
    self.tableView.mj_header = header;
    
}

-(void)addLoadMoreView{
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    [footer setTitle:@"" forState:MJRefreshStateIdle];
    footer.refreshingTitleHidden = YES;

    self.tableView.mj_footer = footer;

}

-(void)refreshHead{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.tableView.mj_header endRefreshing];
    });
}

-(void)loadMoreData{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.tableView.mj_footer endRefreshing];

    });
}

#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    }
    
    return _tableView;
}

-(NSMutableArray*)modelArr{
    
    if (!_modelArr) {
        _modelArr = [NSMutableArray new];
    }
    return _modelArr;
}

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 180;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GYMyRecmendCell *cell = [tableView dequeueReusableCellWithIdentifier:recID];
  
    if (!cell) {
    
        cell =[[NSBundle mainBundle]loadNibNamed:@"GYMyRecmendCell" owner:self options:nil][0];
    }
    if (indexPath.row == 0) {
        cell.backImgView.image = [UIImage imageNamed:@"tuijian_renmai"];
    }else if (indexPath.row == 1){
         cell.backImgView.image = [UIImage imageNamed:@"tuijian_tongxunlu"];
    }else{
        cell.backImgView.image = [UIImage imageNamed:@"tuijian_tuangou"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        ZBMyNetworkingVC *vc = [ZBMyNetworkingVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        ZBMyMailListVC *vc = [ZBMyMailListVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        ZBMyGroupBuyReturnVC *vc = [ZBMyGroupBuyReturnVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
