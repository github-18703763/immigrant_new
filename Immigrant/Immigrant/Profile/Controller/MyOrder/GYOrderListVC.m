//
//  GYOrderListVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYOrderListVC.h"
#import "GYMyImmigrantOrderCell.h"
#import "GYMyVisaOrderCell.h"
#import "GYMyHouseOrderCell.h"
#import "GYMyTravelOrderCell.h"
#import "ZBMyOrderModel.h"
#import "ZBVisacenterChildCell.h"///
#import "ZBImmigrantOrderDetailTBVC.h" //移民订单详情
#import "ZBVisaOrderDetailsTBVC.h"//签证的i订单详情
#import "ZBSummerCampOrderDetailTBVC.h" //冬夏令营的订单详情
#import "ZBClassicRouteOrderDetailTBVC.h" //经典路线
#import "ZBBuyHouseOrderDetailTBVC.h" //房产
#import "ZBCallBackOrderDetailVC.h" //回访
#import "ZBGoodsOrderDetailTBVC.h" //商品
#import "ZBMyCallOnSbOrderCell.h" //上门回访
#import "ZBMyOrderGoodsTBCell.h" //商品
#import "ZBLookLogisticsTBVC.h" //查看物流

#import "StudyOrderController.h"//新留学
#import "HouseOrderController.h"//新买房
#import "CampOrderController.h"//新夏令营
#import "LineOrderController.h"//新路线
#import "MigrateOrderController.h"//新移民项目

@protocol GYOrderListVCDelegate<NSObject>

-(void)didSelectOrderModel:(id)model;

@end

@interface GYOrderListVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong) NSMutableArray *modelArr;
@property(nonatomic,weak)id<GYOrderListVCDelegate>delegate;

@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;
@end

static NSString *immigrantID = @"GYMyImmigrantOrderCell";
static NSString *visaID = @"GYMyVisaOrderCell";
static NSString *houseID = @"GYMyHouseOrderCell";
static NSString *travelID = @"GYMyTravelOrderCell";


@implementation GYOrderListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    [self.tableView setTableFooterView:[UIView new]];
    
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GYMyImmigrantOrderCell" bundle:nil] forCellReuseIdentifier:immigrantID];
     [self.tableView registerNib:[UINib nibWithNibName:@"GYMyVisaOrderCell" bundle:nil] forCellReuseIdentifier:visaID];
     [self.tableView registerNib:[UINib nibWithNibName:@"GYMyHouseOrderCell" bundle:nil] forCellReuseIdentifier:houseID];
     [self.tableView registerNib:[UINib nibWithNibName:@"GYMyTravelOrderCell" bundle:nil] forCellReuseIdentifier:travelID];
     [self.tableView registerNib:[UINib nibWithNibName:@"ZBMyOrderGoodsTBCell" bundle:nil] forCellReuseIdentifier:@"ZBMyOrderGoodsTBCell"];
      [self.tableView registerNib:[UINib nibWithNibName:@"ZBMyCallOnSbOrderCell" bundle:nil] forCellReuseIdentifier:@"ZBMyCallOnSbOrderCell"];
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
}


#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - TABBARHEIGHT) style:UITableViewStylePlain];
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 70, 0);
    }
    
    return _tableView;
}

-(NSMutableArray*)modelArr{
    
    if (!_modelArr) {
        _modelArr = [NSMutableArray new];
    }
    return _modelArr;
}

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBMyOrderModel *model = self.newsArray[indexPath.row];
    if (model.orderCamp != nil || model.orderLine != nil || model.orderLineStandby != nil || model.orderCampStandby != nil) {
        return 252;
    }else if(model.orderCallback != nil){
        return 285;
    }else{
        return 205; //
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMyOrderModel *model = self.newsArray[indexPath.row];
    if (model.orderVisa != nil) {
        //签证
        GYMyVisaOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:visaID];
        if (!cell) {
            cell =[[NSBundle mainBundle]loadNibNamed:@"GYMyVisaOrderCell" owner:self options:nil][0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.centerBtn addTarget:self action:@selector(btnLookWuliu) forControlEvents:UIControlEventTouchUpInside];
        cell.myOrderModel = self.newsArray[indexPath.row];
        return cell;

    } else if (model.orderHouse != nil) {
//        海外房产
        GYMyHouseOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:houseID];
        if (!cell) {
            cell =[[NSBundle mainBundle]loadNibNamed:@"GYMyHouseOrderCell" owner:self options:nil][0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.myOrderModel = self.newsArray[indexPath.row];
        return cell;
    }else if (model.orderCamp != nil || model.orderLine != nil || model.orderLineStandby != nil || model.orderCampStandby != nil ){
        //冬夏令营。定制考察
        GYMyTravelOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:travelID];
        if (!cell) {
            cell =[[NSBundle mainBundle]loadNibNamed:@"GYMyTravelOrderCell" owner:self options:nil][0];
        }
        //cell里面又做区分
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.myOrderModel = self.newsArray[indexPath.row];
        return cell;
    }else if (model.orderMigrate != nil || model.orderStudy != nil){
        //移民orderMigrate、留学也用这个
        GYMyImmigrantOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:immigrantID];
        if (!cell) {
            cell =[[NSBundle mainBundle]loadNibNamed:@"GYMyImmigrantOrderCell" owner:self options:nil][0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.orderMigrateModel = self.newsArray[indexPath.row];
        return cell;
    }else if(model.orderCallback != nil){
        ZBMyCallOnSbOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBMyCallOnSbOrderCell" forIndexPath:indexPath];
         cell.myOrderModel = self.newsArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        ZBMyOrderGoodsTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBMyOrderGoodsTBCell" forIndexPath:indexPath];
         cell.myOrderModel = self.newsArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.centerBtn addTarget:self action:@selector(btnLookWuliu) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}
//查看物流
- (void)btnLookWuliu{
//    #import "ZBLookLogisticsTBVC.h"
    ZBLookLogisticsTBVC *vc = [ZBLookLogisticsTBVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(didSelectOrderModel:)]) {
        [self.delegate didSelectOrderModel:nil];
    }
    ZBMyOrderModel *model = self.newsArray[indexPath.row];
    if (model.orderVisa != nil) {
        //签证
        ZBVisaOrderDetailsTBVC *vc = [ZBVisaOrderDetailsTBVC new];
        vc.modelOrderDetail = model;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if (model.orderHouse != nil) {
        //        海外房产
        HouseOrderController *vc = [[HouseOrderController alloc] init];
        vc.orderId = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (model.orderCamp != nil || model.orderCampStandby != nil ){
        //冬夏令营
        CampOrderController *vc = [[CampOrderController alloc] init];
        vc.orderId = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (model.orderMigrate != nil) {
        //移民orderMigrate
        MigrateOrderController *vc = [MigrateOrderController new];
        vc.orderId = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (model.orderStudy != nil) {
        //留学
        StudyOrderController *vc = [[StudyOrderController alloc] init];
        vc.orderId = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (model.orderLine || model.orderLineStandby != nil) {
        //考察
        LineOrderController *vc = [[LineOrderController alloc] init];
        vc.orderId = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    } else if(model.orderCallback != nil){
        //回访
        ZBCallBackOrderDetailVC *vc = [ZBCallBackOrderDetailVC new];
        vc.modelOrderDetail = model;
        [self.navigationController pushViewController:vc animated:YES];
    } else{
        //商品
        ZBGoodsOrderDetailTBVC *vc = [ZBGoodsOrderDetailTBVC new];
        vc.modelOrderDetail = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

//数据的请求
-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager orderFindpageOrderstatus:self.orderType  withPage:self.pageIndex withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBMyOrderModel mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBMyOrderModel mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}
@end
