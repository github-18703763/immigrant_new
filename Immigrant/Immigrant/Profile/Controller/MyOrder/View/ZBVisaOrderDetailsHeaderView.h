//
//  ZBVisaOrderDetailsHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"

NS_ASSUME_NONNULL_BEGIN
//签证订单详情的头部
@interface ZBVisaOrderDetailsHeaderView : UIView
@property (nonatomic,strong)ZBMyOrderModel *modelOrderDetail;
@end

NS_ASSUME_NONNULL_END
