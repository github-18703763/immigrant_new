//
//  ZBMyCallOnSbOrderCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"
//上门拜访
@interface ZBMyCallOnSbOrderCell : UITableViewCell

@property (nonatomic,strong)ZBMyOrderModel *myOrderModel;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

@end

