//
//  ZBImmigrantOrderDetailHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"

//移民订单详情的头部、留学申请也用这个
@interface ZBImmigrantOrderDetailHeaderView : UIView
@property (nonatomic,strong)ZBMyOrderModel *modelOrderDetail;
@end
