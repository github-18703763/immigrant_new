//
//  ZBLookLogisticsTVCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/21.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBLogisticFrameModel.h"
//查看物流
NS_ASSUME_NONNULL_BEGIN

@interface ZBLookLogisticsTVCell : UITableViewCell
+ (instancetype)czh_cellWithTableView:(UITableView *)tableView;
///<#注释#>
@property (nonatomic, strong) ZBLogisticFrameModel *frameModel;
@end

NS_ASSUME_NONNULL_END
