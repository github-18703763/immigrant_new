//
//  ZBSummerCampOrderDetailFooterView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBSummerCampOrderDetailFooterView.h"
@interface ZBSummerCampOrderDetailFooterView()
@property (weak, nonatomic) IBOutlet UILabel *userNameValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneValueLab;
@property (weak, nonatomic) IBOutlet UILabel *goOutNumLab;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyValueLab;
@property (weak, nonatomic) IBOutlet UILabel *despoitValueLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyValueLab;

@end
@implementation ZBSummerCampOrderDetailFooterView

-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    if (_modelOrderDetail.orderCamp != nil) {
        //冬夏令营
        ZBMyOrderCampModel *campModel = _modelOrderDetail.orderCamp;
        self.userNameValueLab.text = _modelOrderDetail.userName;
        self.userPhoneValueLab.text = _modelOrderDetail.userMobile;
        self.goOutNumLab.text = [NSString stringWithFormat:@"x%@",campModel.currentnum];
        self.totalMoneyValueLab.text = [NSString stringWithFormat:@"￥%@",campModel.totalprice];
        self.despoitValueLab.text = [NSString stringWithFormat:@"￥%@",campModel.depositfeeyuan];
        self.payMoneyValueLab.text = [NSString stringWithFormat:@"￥%@",campModel.totalpriceyuan];
    }else if (_modelOrderDetail.orderLine != nil){
        //定制考察
        ZBMyOrderLineModel *lineModel = _modelOrderDetail.orderLine;
        self.userNameValueLab.text = _modelOrderDetail.userName;
        self.userPhoneValueLab.text = _modelOrderDetail.userMobile;
        self.goOutNumLab.text = [NSString stringWithFormat:@"x%@",lineModel.currentnum];
        self.totalMoneyValueLab.text = [NSString stringWithFormat:@"￥%@",lineModel.totalprice];
        self.despoitValueLab.text = [NSString stringWithFormat:@"￥%@",lineModel.depositfeeyuan];
        self.payMoneyValueLab.text = [NSString stringWithFormat:@"￥%@",lineModel.totalpriceyuan];
    }
    
}

@end
