//
//  ZBSummerCampOrderDetailHeadrView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBSummerCampOrderDetailHeadrView.h"
@interface ZBSummerCampOrderDetailHeadrView()
@property (weak, nonatomic) IBOutlet UIView *topViewBg;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLab;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *dateLab;

@property (weak, nonatomic) IBOutlet UILabel *moneyLab;

@end

@implementation ZBSummerCampOrderDetailHeadrView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.topViewBg.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.22].CGColor;
    self.topViewBg.layer.shadowOffset = CGSizeMake(0,10);
    self.topViewBg.layer.shadowOpacity = 1;
    self.topViewBg.layer.shadowRadius = 9;
    self.topViewBg.layer.cornerRadius = 15;
}
-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    if (_modelOrderDetail.orderHouse != nil) {
        //海外房产
        ZBMyOrderHouseModel *houseModle = _modelOrderDetail.orderHouse;
        self.orderNoLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
        self.orderTimeLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.gmtCreated];
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:houseModle.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = houseModle.name;
        self.dateLab.text = [NSString stringWithFormat:@"%@-%@",houseModle.gmtCreated,houseModle.gmtModified];
        self.shengyuNumLab.text = [NSString stringWithFormat:@"剩余%li人",_modelOrderDetail.booknum];
        self.moneyLab.text = [NSString stringWithFormat:@"￥%@元/人",houseModle.totalpriceyuan];
    }else if (_modelOrderDetail.orderCamp != nil){
        //冬夏令营
        ZBMyOrderCampModel *campModel = _modelOrderDetail.orderCamp;
        self.orderNoLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
        self.orderTimeLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.gmtCreated];
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:campModel.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = campModel.name;
        self.dateLab.text = [NSString stringWithFormat:@"%@-%@",campModel.gmtCreated,campModel.gmtModified];
        self.shengyuNumLab.text = [NSString stringWithFormat:@"剩余%li人",_modelOrderDetail.booknum];
        self.moneyLab.text = [NSString stringWithFormat:@"￥%@元/人",campModel.totalpriceyuan];
    }else if (_modelOrderDetail.orderLine != nil){
        //经典线路
        ZBMyOrderLineModel *lineModel = _modelOrderDetail.orderLine;
        self.orderNoLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
        self.orderTimeLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.gmtCreated];
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:lineModel.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = lineModel.name;
        self.dateLab.text = [NSString stringWithFormat:@"%@-%@",lineModel.gmtCreated,lineModel.gmtModified];
        self.shengyuNumLab.text = [NSString stringWithFormat:@"剩余%li人",_modelOrderDetail.booknum];
        self.moneyLab.text = [NSString stringWithFormat:@"￥%@元/人",lineModel.totalpriceyuan];
    }
}
@end
