//
//  ZBBuyHouseOrderDetailFooterView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBBuyHouseOrderDetailFooterView.h"
@interface ZBBuyHouseOrderDetailFooterView()
@property (weak, nonatomic) IBOutlet UILabel *userNameTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userNameValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userIDTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userIDValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userEmailTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userEmailValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneValueLab;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userHouseOrderTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userHouseOrderValueLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyValueLab;

@end
@implementation ZBBuyHouseOrderDetailFooterView
//海外房产
-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    ZBMyOrderHouseModel *houseModel = _modelOrderDetail.orderHouse;
    self.userNameValueLab.text = _modelOrderDetail.userName;
    self.userEmailValueLab.text = _modelOrderDetail.userEmail;
     self.userIDValueLab.text = _modelOrderDetail.userIdcard;
    self.userPhoneValueLab.text = _modelOrderDetail.userMobile;
    self.totalMoneyValueLab.text = [NSString stringWithFormat:@"￥%@",@"我不知道"];
}

@end
