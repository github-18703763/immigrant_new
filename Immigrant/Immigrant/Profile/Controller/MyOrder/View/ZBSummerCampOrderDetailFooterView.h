//
//  ZBSummerCampOrderDetailFooterView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"
//夏令营订单详情的尾部视图
@interface ZBSummerCampOrderDetailFooterView : UIView
@property (nonatomic,strong)ZBMyOrderModel *modelOrderDetail;
@end

