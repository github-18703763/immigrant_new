//
//  GYMyTravelOrderCell.h
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"
//定制考察---冬夏令营订单cell
@interface GYMyTravelOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

//定制考察、冬夏令营
@property (nonatomic,strong)ZBMyOrderModel *myOrderModel;
@end
