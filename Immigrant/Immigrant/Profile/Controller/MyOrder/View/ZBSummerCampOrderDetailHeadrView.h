//
//  ZBSummerCampOrderDetailHeadrView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"
//冬夏令营订单详情的头部视图
@interface ZBSummerCampOrderDetailHeadrView : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnTitleBottom;//报名信息-----经典线路、房产【办理详情】

//房产下面2个空间隐藏 ---
@property (weak, nonatomic) IBOutlet UIImageView *iconNum;
@property (weak, nonatomic) IBOutlet UILabel *shengyuNumLab;

@property (nonatomic,strong)ZBMyOrderModel *modelOrderDetail;
@end

