//
//  ZBMyOrderGoodsHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/9.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyOrderGoodsHeaderView.h"
@interface ZBMyOrderGoodsHeaderView()
@property (weak, nonatomic) IBOutlet UIView *topViewBg;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLab;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *countLab;
@property (weak, nonatomic) IBOutlet UILabel *payTotalMoneyLab;
@property (weak, nonatomic) IBOutlet UIButton *returnNameLab;
@property (weak, nonatomic) IBOutlet UIButton *returnPhoneLab;
@property (weak, nonatomic) IBOutlet UILabel *returnAddressLab;
@property (weak, nonatomic) IBOutlet UIButton *payRMBLab;
@property (weak, nonatomic) IBOutlet UIButton *payPointLab;

@end

@implementation ZBMyOrderGoodsHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.topViewBg.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.22].CGColor;
    self.topViewBg.layer.shadowOffset = CGSizeMake(0,10);
    self.topViewBg.layer.shadowOpacity = 1;
    self.topViewBg.layer.shadowRadius = 9;
    self.topViewBg.layer.cornerRadius = 15;
}
-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    ZBMyOrderGoodsModel *goodsModel = _modelOrderDetail.orderGoods;
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:goodsModel.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.orderNoLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
    self.orderTimeLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.gmtCreated];
    self.titleLab.text = goodsModel.name;
    self.countLab.text = [NSString stringWithFormat:@"x%li",_modelOrderDetail.booknum];
    self.payTotalMoneyLab.text = [NSString stringWithFormat:@"￥%@+%@积分",goodsModel.price,goodsModel.point];
    [self.returnNameLab setTitle:[NSString stringWithFormat:@"收货人:%@",_modelOrderDetail.userName] forState:UIControlStateNormal];
    [self.returnPhoneLab setTitle:[NSString stringWithFormat:@"%@",_modelOrderDetail.userMobile] forState:UIControlStateNormal];
    self.returnAddressLab.text = goodsModel.userAddress;
      [self.payRMBLab setTitle:[NSString stringWithFormat:@"￥%@",goodsModel.priceyuan] forState:UIControlStateNormal];
      [self.payPointLab setTitle:[NSString stringWithFormat:@"%@积分",goodsModel.paypoint] forState:UIControlStateNormal];
}
@end
