//
//  ZBImmigrantOrderDetailFooterView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrantOrderDetailFooterView.h"
@interface ZBImmigrantOrderDetailFooterView()
@property (weak, nonatomic) IBOutlet UILabel *userNameTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userNameValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userEmailTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userEmailValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneValueLab;
@property (weak, nonatomic) IBOutlet UILabel *userDespoitTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *userDespoitValueLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyValueLab;

@end
@implementation ZBImmigrantOrderDetailFooterView

-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    if (_modelOrderDetail.orderMigrate != nil) {
        ZBMyOrderMigrateModel *migrateModel = _modelOrderDetail.orderMigrate;
        //移民
        self.userNameTitleLab.text = @"姓名";
        self.userNameValueLab.text = _modelOrderDetail.userName;
        self.userEmailTitleLab.text = @"电子邮箱";
        self.userEmailValueLab.text = _modelOrderDetail.userEmail;
        self.userPhoneTitleLab.text = @"手机号";
        self.userPhoneValueLab.text = _modelOrderDetail.userMobile;
        self.userDespoitTitleLab.text = @"移民定金";
        self.userDespoitValueLab.text = [NSString stringWithFormat:@"￥%@",migrateModel.depositfeeyuan];
        self.userEmailTitleLab.text = @"支付金额";
        self.userEmailValueLab.text = [NSString stringWithFormat:@"￥%@",migrateModel.servicefeeyuan];
    }else  if (_modelOrderDetail.orderStudy != nil) {
        ZBMyOderStudyModel *studyModel = _modelOrderDetail.orderStudy;

        //留学#
        self.userNameTitleLab.text = @"姓名";
        self.userNameValueLab.text = _modelOrderDetail.userName;
        self.userEmailTitleLab.text = @"电子邮箱";
        self.userEmailValueLab.text = _modelOrderDetail.userEmail;
        self.userPhoneTitleLab.text = @"手机号";
        self.userPhoneValueLab.text = _modelOrderDetail.userMobile;
        self.userDespoitTitleLab.text = @"留学定金";
        self.userDespoitValueLab.text = [NSString stringWithFormat:@"￥%@",studyModel.totalprice];
        self.userEmailTitleLab.text = @"支付金额";
        self.userEmailValueLab.text = [NSString stringWithFormat:@"￥%@",studyModel.totalpriceyuan];
    }
}

@end
