//
//  GYMyImmigrantOrderCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyImmigrantOrderCell.h"
@interface GYMyImmigrantOrderCell()
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *signDepositLab; //签约定金

@property (weak, nonatomic) IBOutlet UILabel *payAmouuntLab;//支付金额

@end

@implementation GYMyImmigrantOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftBtn.layer.cornerRadius = 20.0;
    self.leftBtn.layer.masksToBounds = YES;
    self.centerBtn.layer.cornerRadius = 20.0;
    self.centerBtn.layer.masksToBounds = YES;
    self.rightBtn.layer.cornerRadius = 20.0;
    self.rightBtn.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)setOrderMigrateModel:(ZBMyOrderModel *)orderMigrateModel
{
    _orderMigrateModel = orderMigrateModel;
    if (_orderMigrateModel.orderMigrate != nil) {
        ZBMyOrderMigrateModel *orderMigrate = _orderMigrateModel.orderMigrate;
          [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderMigrate.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = orderMigrate.name;
        self.signDepositLab.text = orderMigrate.servicefee;
        self.payAmouuntLab.text = orderMigrate.servicefeeyuan;
    }else{
        ZBMyOderStudyModel *orderStudy = _orderMigrateModel.orderStudy;
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderStudy.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = orderStudy.name;
        self.signDepositLab.text = orderStudy.totalprice;
        self.payAmouuntLab.text = orderStudy.totalpriceyuan;
    }
    if ([_orderMigrateModel.orderstatus isEqualToString:@"unpaid"]) {
        //待付款
        self.centerBtn.hidden = YES;
        [self.leftBtn setTitle:@"取消订单" forState:UIControlStateNormal];
        [self.leftBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.leftBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        [self.rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"FFFBFB"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"2585F8"];
        
    }else if ([_orderMigrateModel.orderstatus isEqualToString:@"undelivered"]) {
        //待发货
        self.leftBtn.hidden = YES;
        self.centerBtn.backgroundColor = [UIColor clearColor];
        [self.centerBtn setTitle:@"待发货" forState:UIControlStateNormal];
        [self.centerBtn setTitleColor:[UIColor getColor:@"A0A0A0"] forState:UIControlStateNormal];
        self.centerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.rightBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        
    }else if ([_orderMigrateModel.orderstatus isEqualToString:@"unreceived"]) {
        //待收货
        [self.leftBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.leftBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.leftBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
//        [self.centerBtn setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.centerBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
//        self.centerBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        self.centerBtn.hidden =YES;
        [self.rightBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"FFFBFB"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"2585F8"];
    }else if ([_orderMigrateModel.orderstatus isEqualToString:@"finished"]) {
        //已完成
        self.leftBtn.hidden = YES;
        self.centerBtn.backgroundColor = [UIColor clearColor];
        [self.centerBtn setTitle:@"交易完成" forState:UIControlStateNormal];
        [self.centerBtn setTitleColor:[UIColor getColor:@"A0A0A0"] forState:UIControlStateNormal];
        self.centerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.rightBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
    }else{
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = YES;
        self.centerBtn.hidden =YES;
    }
}

@end
