//
//  ZBMyCallOnSbOrderCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyCallOnSbOrderCell.h"
@interface ZBMyCallOnSbOrderCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *firstStartTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *firstEndTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *secondStartTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *secondEndTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *contryLab;
@property (weak, nonatomic) IBOutlet UILabel *payTotalMoneyLab;

@end

@implementation ZBMyCallOnSbOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftBtn.layer.cornerRadius = 20.0;
    self.leftBtn.layer.masksToBounds = YES;
    self.centerBtn.layer.cornerRadius = 20.0;
    self.centerBtn.layer.masksToBounds = YES;
    self.rightBtn.layer.cornerRadius = 20.0;
    self.rightBtn.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//上门拜访
-(void)setMyOrderModel:(ZBMyOrderModel *)myOrderModel{
    _myOrderModel = myOrderModel;
    if (_myOrderModel.orderCallback != nil) {
        ZBMyOrderCallbackModel *orderCallBack = _myOrderModel.orderCallback;
        self.titleLab.text = orderCallBack.outName;
        self.firstStartTimeLab.text = orderCallBack.firstsdate;
        self.firstEndTimeLab.text = orderCallBack.firstedate;
        self.secondStartTimeLab.text = orderCallBack.secondsdate;
        self.secondEndTimeLab.text = orderCallBack.secondedate;
        self.contryLab.text = orderCallBack.visitcity;
        self.payTotalMoneyLab.text = orderCallBack.priceyuan;
    }
    if ([_myOrderModel.orderstatus isEqualToString:@"unpaid"]) {
        //待付款
        self.centerBtn.hidden = YES;
        [self.leftBtn setTitle:@"取消订单" forState:UIControlStateNormal];
        [self.leftBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.leftBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        [self.rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"FFFBFB"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"2585F8"];
        
    }else if ([_myOrderModel.orderstatus isEqualToString:@"undelivered"]) {
        //待发货
        self.leftBtn.hidden = YES;
        self.centerBtn.backgroundColor = [UIColor clearColor];
        [self.centerBtn setTitle:@"待发货" forState:UIControlStateNormal];
        [self.centerBtn setTitleColor:[UIColor getColor:@"A0A0A0"] forState:UIControlStateNormal];
        self.centerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.rightBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        
    }else if ([_myOrderModel.orderstatus isEqualToString:@"unreceived"]) {
        //待收货
        [self.leftBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.leftBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.leftBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
//        [self.centerBtn setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.centerBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
//        self.centerBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        self.centerBtn.hidden = YES;
        [self.rightBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"FFFBFB"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"2585F8"];
    }else if ([_myOrderModel.orderstatus isEqualToString:@"finished"]) {
        //已完成
        self.leftBtn.hidden = YES;
        self.centerBtn.backgroundColor = [UIColor clearColor];
        [self.centerBtn setTitle:@"交易完成" forState:UIControlStateNormal];
        [self.centerBtn setTitleColor:[UIColor getColor:@"A0A0A0"] forState:UIControlStateNormal];
        self.centerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.rightBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
    }else{
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = YES;
        self.centerBtn.hidden =YES;
    }
}
@end
