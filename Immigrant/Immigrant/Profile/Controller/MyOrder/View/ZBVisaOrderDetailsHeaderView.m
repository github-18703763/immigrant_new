//
//  ZBVisaOrderDetailsHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBVisaOrderDetailsHeaderView.h"
@interface ZBVisaOrderDetailsHeaderView()
@property (weak, nonatomic) IBOutlet UIView *topViewBg;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLab;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *subheadLab;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UIButton *secondTitleLab;

@end

@implementation ZBVisaOrderDetailsHeaderView
-(void)awakeFromNib{
    [super awakeFromNib];
    self.topViewBg.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.22].CGColor;
    self.topViewBg.layer.shadowOffset = CGSizeMake(0,10);
    self.topViewBg.layer.shadowOpacity = 1;
    self.topViewBg.layer.shadowRadius = 9;
    self.topViewBg.layer.cornerRadius = 15;
}
//更新头部视图
-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    self.orderNumLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
    self.orderTimeLab.text = [NSString stringWithFormat:@"下单时间:%@",_modelOrderDetail.gmtCreated];
//    orderVisa
    ZBMyOrderVisaModel *orderVisa = _modelOrderDetail.orderVisa;
      [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderVisa.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = orderVisa.name;
    self.subheadLab.text = @"移民定金";
     self.moneyLab.text = orderVisa.servicefeeyuan;
    [self.secondTitleLab setTitle:@" 办理信息" forState:UIControlStateNormal];
}
@end
