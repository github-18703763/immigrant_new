//
//  GYMyTravelOrderCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyTravelOrderCell.h"
@interface GYMyTravelOrderCell()
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *goOutNumLab;//出行人数
@property (weak, nonatomic) IBOutlet UILabel *surplusNumLab;//剩余人数
@property (weak, nonatomic) IBOutlet UILabel *examiningDeposiLab;//考察定金
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLab;//支付金额
@property (weak, nonatomic) IBOutlet UILabel *howMuchLab;//每人多少钱

@end

@implementation GYMyTravelOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftBtn.layer.cornerRadius = 20.0;
    self.leftBtn.layer.masksToBounds = YES;
    self.centerBtn.layer.cornerRadius = 20.0;
    self.centerBtn.layer.masksToBounds = YES;
    self.rightBtn.layer.cornerRadius = 20.0;
    self.rightBtn.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//数据处理
-(void)setMyOrderModel:(ZBMyOrderModel *)myOrderModel{
    _myOrderModel = myOrderModel;
    if (_myOrderModel.orderCamp != nil) {
        ZBMyOrderCampModel *orderCamp = _myOrderModel.orderCamp;
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderCamp.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = orderCamp.name;
        self.goOutNumLab.text =  [NSString stringWithFormat:@"x%li",_myOrderModel.booknum];
        NSInteger allNum = [orderCamp.totalnum integerValue];
        NSInteger currentNum = [orderCamp.currentnum integerValue];
        self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%li人",allNum-currentNum];
        self.howMuchLab.text = [NSString stringWithFormat:@"￥%@元/人",orderCamp.totalpriceyuan];
        self.examiningDeposiLab.text = orderCamp.depositfeeyuan;
        self.payMoneyLab.text = _myOrderModel.payamoutyuan;
    } else if (_myOrderModel.orderCampStandby != nil) {
        ZBMyOrderCampStandbyModel *orderCamp = _myOrderModel.orderCampStandby;
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderCamp.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = orderCamp.name;
        self.goOutNumLab.text =  [NSString stringWithFormat:@"x%li",_myOrderModel.booknum];
        NSInteger allNum = orderCamp.totalnum;
        //TODO: - currentnum 缺失
        NSInteger currentNum = 0;
        self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%li人",allNum-currentNum];
        self.howMuchLab.text = [NSString stringWithFormat:@"￥%@元/人",@(orderCamp.totalpriceyuan)];
        self.examiningDeposiLab.text = [@(orderCamp.depositfeeyuan) stringValue];
        self.payMoneyLab.text = _myOrderModel.payamoutyuan;
    } else if (_myOrderModel.orderLineStandby != nil) {
        //定制考察
        ZBMyOrderLineStandbyModel *orderLine = _myOrderModel.orderLineStandby;
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderLine.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = orderLine.name;
        self.goOutNumLab.text =  [NSString stringWithFormat:@"x%li",_myOrderModel.booknum];
        NSInteger allNum = [orderLine.totalnum integerValue];
        //TODO: - currentnum 缺失
        NSInteger currentNum = 0;
        self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%li人",allNum-currentNum];
        self.howMuchLab.text = [NSString stringWithFormat:@"￥%@元/人",orderLine.totalpriceyuan];
        self.examiningDeposiLab.text = orderLine.depositfeeyuan;
        self.payMoneyLab.text = _myOrderModel.payamoutyuan;
    } else {
        //定制考察
        ZBMyOrderLineModel *orderLine = _myOrderModel.orderLine;
         [self.photoImg sd_setImageWithURL:[NSURL URLWithString:orderLine.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text = orderLine.name;
        self.goOutNumLab.text =  [NSString stringWithFormat:@"x%li",_myOrderModel.booknum];
        NSInteger allNum = [orderLine.totalnum integerValue];
        NSInteger currentNum = [orderLine.currentnum integerValue];
        self.surplusNumLab.text = [NSString stringWithFormat:@"剩余%li人",allNum-currentNum];
        self.howMuchLab.text = [NSString stringWithFormat:@"￥%@元/人",orderLine.totalpriceyuan];
        self.examiningDeposiLab.text = orderLine.depositfeeyuan;
        self.payMoneyLab.text = _myOrderModel.payamoutyuan;
    }
    if ([_myOrderModel.orderstatus isEqualToString:@"unpaid"]) {
        //待付款
        self.centerBtn.hidden = YES;
        [self.leftBtn setTitle:@"取消订单" forState:UIControlStateNormal];
        [self.leftBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.leftBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        [self.rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"FFFBFB"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"2585F8"];
        
    }else if ([_myOrderModel.orderstatus isEqualToString:@"undelivered"]) {
        //待发货
        self.leftBtn.hidden = YES;
        self.centerBtn.backgroundColor = [UIColor clearColor];
        [self.centerBtn setTitle:@"待发货" forState:UIControlStateNormal];
        [self.centerBtn setTitleColor:[UIColor getColor:@"A0A0A0"] forState:UIControlStateNormal];
        self.centerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.rightBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        
    }else if ([_myOrderModel.orderstatus isEqualToString:@"unreceived"]) {
        //待收货
        [self.leftBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.leftBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.leftBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
//        [self.centerBtn setTitle:@"查看物流" forState:UIControlStateNormal];
//        [self.centerBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
//        self.centerBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
        self.centerBtn.hidden =YES;
        [self.rightBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"FFFBFB"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"2585F8"];
    }else if ([_myOrderModel.orderstatus isEqualToString:@"finished"]) {
        //已完成
        self.leftBtn.hidden = YES;
        self.centerBtn.backgroundColor = [UIColor clearColor];
        [self.centerBtn setTitle:@"交易完成" forState:UIControlStateNormal];
        [self.centerBtn setTitleColor:[UIColor getColor:@"A0A0A0"] forState:UIControlStateNormal];
        self.centerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self.rightBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor getColor:@"4E4E4E"] forState:UIControlStateNormal];
        self.rightBtn.backgroundColor = [UIColor getColor:@"EDEDF0"];
    }else{
        self.leftBtn.hidden = YES;
        self.rightBtn.hidden = YES;
        self.centerBtn.hidden =YES;
    }
}
@end
