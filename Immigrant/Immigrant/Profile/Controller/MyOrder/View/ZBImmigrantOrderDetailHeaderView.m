//
//  ZBImmigrantOrderDetailHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBImmigrantOrderDetailHeaderView.h"
@interface ZBImmigrantOrderDetailHeaderView()
@property (weak, nonatomic) IBOutlet UIView *topViewBg;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLab;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLab;
@property (weak, nonatomic) IBOutlet UIButton *secondTitleLab;

@end
@implementation ZBImmigrantOrderDetailHeaderView
-(void)awakeFromNib{
    [super awakeFromNib];
    self.topViewBg.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.22].CGColor;
    self.topViewBg.layer.shadowOffset = CGSizeMake(0,10);
    self.topViewBg.layer.shadowOpacity = 1;
    self.topViewBg.layer.shadowRadius = 9;
    self.topViewBg.layer.cornerRadius = 15;
}
- (void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    if (_modelOrderDetail.orderMigrate != nil) {
        ZBMyOrderMigrateModel *migrateModel = _modelOrderDetail.orderMigrate;
        //移民
        [self.secondTitleLab setTitle:@"移民信息" forState:UIControlStateNormal];
        [self.photoImg sd_setImageWithURL:[NSURL URLWithString:migrateModel.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
        self.titleLab.text  = migrateModel.name;
         self.detailLab.hidden = NO;
        self.detailLab.text = @"移民定金";
        self.payMoneyLab.text = [NSString stringWithFormat:@"￥%@",migrateModel.depositfeeyuan];
    }else  if (_modelOrderDetail.orderStudy != nil) {
        ZBMyOderStudyModel *studyModel = _modelOrderDetail.orderStudy;
        self.detailLab.hidden = YES;
         self.titleLab.text  = studyModel.name;
        self.payMoneyLab.text = [NSString stringWithFormat:@"￥%@",studyModel.totalpriceyuan];
        self.payMoneyLab.textColor = [UIColor getColor:@"267BFF"];
        //留学#
        [self.secondTitleLab setTitle:@"联系信息" forState:UIControlStateNormal];
    }
    self.orderNoLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
    self.orderTimeLab.text = [NSString stringWithFormat:@"下单时间:%@",_modelOrderDetail.gmtCreated];
}

@end
