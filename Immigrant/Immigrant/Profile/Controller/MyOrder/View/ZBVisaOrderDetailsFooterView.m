//
//  ZBVisaOrderDetailsFooterView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBVisaOrderDetailsFooterView.h"
@interface ZBVisaOrderDetailsFooterView()
@property (weak, nonatomic) IBOutlet UILabel *goOutTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *goOutValueLab;
@property (weak, nonatomic) IBOutlet UILabel *transactorTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *transactorValueLab;
@property (weak, nonatomic) IBOutlet UILabel *returnNameLab;
@property (weak, nonatomic) IBOutlet UILabel *returnPhoneNumLab;
@property (weak, nonatomic) IBOutlet UILabel *returnAddressLab;
@property (weak, nonatomic) IBOutlet UILabel *totalCostTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *totalCostValueLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyValueLab;

@end
@implementation ZBVisaOrderDetailsFooterView

-(void)setModelOrderDetail:(ZBMyOrderModel *)modelOrderDetail{
    _modelOrderDetail = modelOrderDetail;
    ZBMyOrderVisaModel *orderVisa = _modelOrderDetail.orderVisa;
    self.goOutTitleLab.text = @"出行时间";
    self.goOutValueLab.text = orderVisa.userTraveldate;
    self.transactorTitleLab.text = @"办理人数";
    self.transactorValueLab.text = [NSString stringWithFormat:@"x%li",_modelOrderDetail.booknum];
    self.returnNameLab.text = [NSString stringWithFormat:@"收货人:%@",_modelOrderDetail.userName];
     self.returnPhoneNumLab.text = [NSString stringWithFormat:@"%@",_modelOrderDetail.userMobile];
    self.returnAddressLab.text = orderVisa.userAddress;
    self.totalCostTitleLab.text = @"费用合计";
    self.totalCostValueLab.text = orderVisa.servicefee;
    self.payMoneyTitleLab.text = @"支付金额";
    self.payMoneyValueLab.text = orderVisa.servicefeeyuan;
}

@end
