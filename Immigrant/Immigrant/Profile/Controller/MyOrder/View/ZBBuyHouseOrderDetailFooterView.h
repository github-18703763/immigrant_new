//
//  ZBBuyHouseOrderDetailFooterView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"
//购房订单详情
NS_ASSUME_NONNULL_BEGIN

@interface ZBBuyHouseOrderDetailFooterView : UIView
@property (nonatomic,strong)ZBMyOrderModel *modelOrderDetail;
@end

NS_ASSUME_NONNULL_END
