//
//  ZBClassicRouteOrderDetailFooterView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
//经典线路的尾部视图
NS_ASSUME_NONNULL_BEGIN

@interface ZBClassicRouteOrderDetailFooterView : UIView

@end

NS_ASSUME_NONNULL_END
