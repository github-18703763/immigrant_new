//
//  GYMyImmigrantOrderCell.h
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
//订单列表一-----移民
#import "ZBMyOrderModel.h"
@interface GYMyImmigrantOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

@property (nonatomic,strong)ZBMyOrderModel *orderMigrateModel;

@end
