//
//  ZBImmigrantOrderDetailTBVC.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyOrderModel.h"
//移民订单详情
@interface ZBImmigrantOrderDetailTBVC : UITableViewController
@property (nonatomic,strong)ZBMyOrderModel *modelOrderDetail;
@end

