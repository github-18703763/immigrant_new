//
//  GYMyOrderVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyOrderVC.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "GYOrderListVC.h"

@interface GYMyOrderVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,FSPageContentViewDelegate,FSSegmentTitleViewDelegate>

@property(nonatomic,strong)UITableView* tableView;

@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@property(nonatomic,strong) NSMutableArray *segmentTitlesArr;
@property(nonatomic,strong) NSArray *childVCs;
@end

@implementation GYMyOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的订单";
    
    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - lazyload
-(UITableView*)tableView{
        
        if (!_tableView) {
            _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        }
        
        return _tableView;
    }
    
-(NSMutableArray*)segmentTitlesArr{
        if (!_segmentTitlesArr) {
            _segmentTitlesArr = [[NSMutableArray alloc]initWithArray:@[@"全部",@"待付款",@"待发货",@"待收货",@"已完成"]];
        }
        return _segmentTitlesArr;
    }

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
        NSMutableArray *childVCs = [[NSMutableArray alloc]init];
//        ['unpaid', 'undelivered', 'unreceived', 'finished', 'user_cancelled', 'timeout_cancelled']
//        [“未付款”、“未送达”、“未接收”、“已完成”、“用户已取消”、“超时已取消”]
        for (int i=0; i<self.segmentTitlesArr.count; i++) {
            GYOrderListVC *vc = [[GYOrderListVC alloc]init];
            if (i==0) {
                //全部
                vc.orderType =  @"";
            }else if (i==1) {
                //待付款
                vc.orderType = @"unpaid";
            }else if (i==2) {
                //待发货
                vc.orderType = @"undelivered";
            }else if (i==3) {
                //待收货
                vc.orderType = @"unreceived";
            }else{
                //已完成
                vc.orderType = @"finished";
            }
            [childVCs addObject:vc];
        }
        self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-50-NavgationBarHeight) childVCs:childVCs parentVC:self delegate:self];
        //self.pageContentView.contentViewCurrentIndex = 2;
        [cell addSubview:self.pageContentView];
        
        self.childVCs = childVCs;
        
    }
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    //self.titleView.selectIndex = 2;
    
    return self.titleView;
}

#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SystemScreenHeight-50-NavgationBarHeight;
}

@end
