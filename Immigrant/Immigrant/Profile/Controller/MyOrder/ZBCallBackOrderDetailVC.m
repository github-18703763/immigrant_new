//
//  ZBCallBackOrderDetailVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/9.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBCallBackOrderDetailVC.h"
@interface ZBCallBackOrderDetailVC ()
@property (weak, nonatomic) IBOutlet UIView *topViewBg;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLab;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *firstStartTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *firstEndTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *secondStartTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *secondEndTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *contryLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLab;

@end

@implementation ZBCallBackOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"回访订单详情";
    self.topViewBg.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.22].CGColor;
    self.topViewBg.layer.shadowOffset = CGSizeMake(0,10);
    self.topViewBg.layer.shadowOpacity = 1;
    self.topViewBg.layer.shadowRadius = 9;
    self.topViewBg.layer.cornerRadius = 15;
    
    //回访
    ZBMyOrderCallbackModel *callModel = _modelOrderDetail.orderCallback;
    self.orderNoLab.text = [NSString stringWithFormat:@"订单编号:%@",_modelOrderDetail.no];
    self.orderTimeLab.text = [NSString stringWithFormat:@"下单时间:%@",_modelOrderDetail.gmtCreated];
    self.titleLab.text = callModel.outName;
    self.firstStartTimeLab.text= callModel.firstsdate;
    self.firstEndTimeLab.text = callModel.firstedate;
    self.secondStartTimeLab.text = callModel.secondsdate;
    self.secondEndTimeLab.text = callModel.secondedate;
    self.contryLab.text = callModel.outName;
    self.payMoneyLab.text = callModel.priceyuan;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
