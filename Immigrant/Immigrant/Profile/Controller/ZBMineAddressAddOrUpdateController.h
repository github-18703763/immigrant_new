//
//  ZBMineAddressAddOrUpdateController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMineAddressObject.h"

typedef enum : NSUInteger {
    AddAddressType,
    UpdateAddressType
} UpdateTypeEnum;

NS_ASSUME_NONNULL_BEGIN

@interface ZBMineAddressAddOrUpdateController : UITableViewController

@property (nonatomic,assign) UpdateTypeEnum  typeEnum;
@property (nonatomic,strong) ZBMineAddressObject  *model;

@property (nonatomic,copy) void(^commitBlock)(void);

@end

NS_ASSUME_NONNULL_END
