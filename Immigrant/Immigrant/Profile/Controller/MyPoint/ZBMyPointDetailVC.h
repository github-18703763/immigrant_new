//
//  ZBMyPointDetailVC.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyPointModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZBMyPointDetailVC : UIViewController
@property (nonatomic,strong)ZBMyPointModel *myPointModel;
@end

NS_ASSUME_NONNULL_END
