//
//  ZBMyPointDetailVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyPointDetailVC.h"

@interface ZBMyPointDetailVC ()
@property (weak, nonatomic) IBOutlet UIView *headerBgView;
@property (weak, nonatomic) IBOutlet UIButton *pointBtn;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation ZBMyPointDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"积分详情";
    
    self.headerBgView.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.11].CGColor;
    self.headerBgView.layer.shadowOffset = CGSizeMake(0,0);
    self.headerBgView.layer.shadowOpacity = 1;
    self.headerBgView.layer.shadowRadius = 26;
    self.headerBgView.layer.cornerRadius = 7;

    [self.pointBtn setTitle:[NSString stringWithFormat:@" %@",self.myPointModel.point] forState:UIControlStateNormal];
    self.detailLab.text = self.myPointModel.memo;
    self.timeLab.text = self.myPointModel.gmtCreated;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
