//
//  GYMyScoreVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyScoreVC.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "ZBMyScoreHeadView.h"
#import "GYScoreListVC.h"
#import "ZBMyCommissionHeaderView.h" //头部视图
#import "ZBMyPointFlowTbVC.h"//积分流水
#import "ZBPointsToBeIssuedVC.h" //待发放积分
#import "ZBEarnPointsModel.h"

@interface GYMyScoreVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,FSPageContentViewDelegate,FSSegmentTitleViewDelegate>

@property(nonatomic,strong)UITableView* tableView;

@property(nonatomic,strong)ZBMyScoreHeadView *headView;

@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@property(nonatomic,strong) NSMutableArray *segmentTitlesArr;
@property(nonatomic,strong) NSArray *childVCs;

@property (nonatomic,strong)ZBEarnPointsModel *myEarnPointsModel;

@end

@implementation GYMyScoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的积分";

    [self.view addSubview:self.tableView];
    ZBMyCommissionHeaderView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBMyCommissionHeaderView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 250);
    headView.btnWorn.hidden = NO;
    headView.titleLab.text = @"积分余额";
    headView.myCommissionMoneyLab.text = @"0";
    headView.myCommissionMoneyLab.font = [UIFont fontWithName:@"Geometric415BT-BlackA" size:34];
    headView.myCommissionMoneyLab.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBlack];
    headView.myCommissionMoneyLab.textColor = [UIColor getColor:@"353535"];
    [headView.applyMoneyBtn setTitle:@"积分流水" forState:UIControlStateNormal];
    [headView.grandTotalMoneyBtn setTitle:@"累计积分:0" forState:UIControlStateNormal];
    [headView.estimatedAeeivalMoneyBtn setTitle:@"待发放积分：5000" forState:UIControlStateNormal];
    [headView.estimatedAeeivalMoneyBtn setTitleColor:[UIColor getColor:@"FFBD34"] forState:UIControlStateNormal];
    [headView.grandTotalMoneyBtn addTarget:self action:@selector(grandTotalPointBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [headView.estimatedAeeivalMoneyBtn addTarget:self action:@selector(estimatedAeeivalPointBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [headView.applyMoneyBtn addTarget:self action:@selector(applyPointBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableHeaderView = headView;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.tableFooterView = [UIView new];
    
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
    [QSNetworkManager appPointFinddetailSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.myEarnPointsModel =  [ZBEarnPointsModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            headView.myCommissionMoneyLab.text = self.myEarnPointsModel.currentpoint;
            [headView.grandTotalMoneyBtn setTitle:[NSString stringWithFormat:@"累计积分:%@",self.myEarnPointsModel.totalpoint] forState:UIControlStateNormal];
              [headView.estimatedAeeivalMoneyBtn setTitle:[NSString stringWithFormat:@"待发放积分:%@",self.myEarnPointsModel.unsentpoint] forState:UIControlStateNormal];

            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        
    }];
}
//累计积分
- (void)grandTotalPointBtnClick{
    
}
//积分流水
- (void)applyPointBtnClick{
    ZBMyPointFlowTbVC *vc = [ZBMyPointFlowTbVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
//待发放积分
- (void)estimatedAeeivalPointBtnClick{
    ZBPointsToBeIssuedVC *vc = [ZBPointsToBeIssuedVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
//
//
//-(void)viewWillAppear:(BOOL)animated{
//
//    [super viewWillAppear:animated];
//
//     [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    //去掉透明后导航栏下边的黑边
//   // [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//}
//// 如果不想让其他页面的导航栏变为透明 需要重置
//- (void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//
//  //  [self.navigationController.navigationBar setShadowImage:nil];
//}

#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    }
    
    return _tableView;
}

-(NSMutableArray*)segmentTitlesArr{
    if (!_segmentTitlesArr) {
        _segmentTitlesArr = [[NSMutableArray alloc]initWithArray:@[@"获取记录",@"兑换记录"]];
    }
    return _segmentTitlesArr;
}

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    NSMutableArray *childVCs = [[NSMutableArray alloc]init];
    
    for (NSString *titleTT in self.segmentTitlesArr) {
        GYScoreListVC *vc = [[GYScoreListVC alloc]init];
        vc.currentSelectedTitle = titleTT;
        [childVCs addObject:vc];
    }
    self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-50-NavgationBarHeight) childVCs:childVCs parentVC:self delegate:self];
    //self.pageContentView.contentViewCurrentIndex = 2;
    [cell addSubview:self.pageContentView];
    
    self.childVCs = childVCs;
    
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 100) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:25];
    self.titleView.backgroundColor = [UIColor colorWithRed:254/255.0 green:254/255.0 blue:254/255.0 alpha:1.0];
    self.titleView.titleFont = [UIFont systemFontOfSize:23];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    //self.titleView.selectIndex = 2;
    
    return self.titleView;
}

#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
}


@end
