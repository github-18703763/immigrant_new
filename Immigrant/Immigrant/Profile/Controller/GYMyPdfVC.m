//
//  ZBViewController.m
//  Immigrant
//
//  Created by jlc on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyPdfVC.h"
#import "GYBaseWebVC.h"

@interface GYMyPdfVC ()

@property (weak, nonatomic) IBOutlet UIButton *checkPdfBtn;

@end

@implementation GYMyPdfVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的行程";
    
    self.checkPdfBtn.clipsToBounds = YES;
    self.checkPdfBtn.layer.cornerRadius = 25.0;
    [self.checkPdfBtn addTarget:self action:@selector(pushPdfWeb) forControlEvents:UIControlEventTouchUpInside];
}

-(void)pushPdfWeb{
    
    GYBaseWebVC *pdfWeb = [[GYBaseWebVC alloc]initWithUrl:nil];
    
    pdfWeb.title = @"我的行程";
    
    [self.navigationController pushViewController:pdfWeb animated:YES];
    
}
@end
