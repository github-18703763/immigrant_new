//
//  GYFeedBackVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYFeedBackVC.h"
#import "FSSegmentTitleView.h"

@interface GYFeedBackVC ()<FSSegmentTitleViewDelegate,UITextViewDelegate>

@property (nonatomic,strong) FSSegmentTitleView *titleView;
@property (weak, nonatomic) IBOutlet UILabel *content_Lab;
@property (weak, nonatomic) IBOutlet UILabel *detail_Lab;
@property (weak, nonatomic) IBOutlet UIButton *phone_btn;
@property (weak, nonatomic) IBOutlet UIButton *submit_btn;

@property (weak, nonatomic) IBOutlet UITextView *contentTxtView;
@property(nonatomic,strong) NSMutableArray *segmentTitlesArr;

@end

@implementation GYFeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title  = @"意见反馈";
    
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    self.titleView.selectIndex = 0;
    [self.view addSubview:self.titleView];
    
    //初始化一个约束
    self.contentTxtView.textContainerInset = UIEdgeInsetsMake(5, 10, 5, 10);
    self.contentTxtView.layer.cornerRadius = 15;
    self.contentTxtView.layer.masksToBounds = YES;
    self.contentTxtView.delegate = self;
    self.submit_btn.layer.cornerRadius = 25;
    self.submit_btn.layer.masksToBounds = YES;
    
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length != 0) {
        self.content_Lab.hidden = YES;
    }else{
        self.content_Lab.hidden = NO;
    }
    
}
- (BOOL) isValid {
    
    if (self.contentTxtView.text.length > 1) {
        [MBProgressHUD showError:@"反馈内容不能为空"];
        return NO;
    }
    return true;
}
-(NSMutableArray*)segmentTitlesArr{
    
    if (!_segmentTitlesArr) {
        _segmentTitlesArr = [[NSMutableArray alloc]initWithArray:@[@"我要投诉",@"我要点赞",@"产品建议"]];
    }
    
    return _segmentTitlesArr;
}

#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    if (endIndex == 0) {
        //投诉
        self.phone_btn.hidden = false;
        self.detail_Lab.hidden = true;
        self.content_Lab.text = @"您有什么想投诉的，快来告诉我吧！也可以直接拨打电话联系我哦！";
    }else if (endIndex == 1){
        //点赞
        self.phone_btn.hidden = true;
        self.detail_Lab.hidden = false;
        self.content_Lab.text = @"请输入您的赞美";
        self.detail_Lab.text = @"感谢您为我们点赞\n您的点赞是我们前进的动力！";
    }else{
        //产品建议
        self.phone_btn.hidden = true;
        self.detail_Lab.hidden = false;
        self.content_Lab.text = @"您有什么意见或建议，快来告诉我吧！";
        self.detail_Lab.text = @"请留下您宝贵的意见\n意见被采纳后会获得丰厚的奖励哦！";
    }
}
- (IBAction)btnSubmit:(id)sender {
    if (![self isValid]) {
        return;
    }
    NSString *strType = @"建议";
    if (self.titleView.selectIndex == 0 ) {
        strType = @"投诉";
    }else if (self.titleView.selectIndex == 1 ) {
        strType = @"赞美";
    }else{
        strType = @"建议";
    }
//    ZBLog(@"----%@,-------%@",self.contentTxtView.text,strType);
    [QSNetworkManager feedBackAddContent:self.contentTxtView.text withType:strType successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            [MBProgressHUD showSuccess:@"感谢您的反馈,我们会尽快处理！"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}
//打电话
- (IBAction)btnCallPhone:(id)sender {
    NSMutableString *str=[[NSMutableString alloc]initWithFormat:@"tel:%@",@"114"];
                          
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:str preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            }];
        UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"呼叫" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }];
        [alertController addAction:cancelAction];
       [alertController addAction:otherAction];
       [self presentViewController:alertController animated:YES completion:nil];
}
@end
