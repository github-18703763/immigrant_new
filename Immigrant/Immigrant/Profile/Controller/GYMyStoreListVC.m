//
//  GYMyStoreListVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyStoreListVC.h"
#import "ZBVisacenterChildCell.h"
#import "GYMyScoreListCell.h"
#import "ZBImmigrationProjectOneCell.h" //移民项目
#import "ZBInspectionRouteCell.h" //定制考察
#import "ZBOverseasStudySuccessListCell.h" //留学成功
#import "ZBOverseasStudyApplyListCell.h" //留学产品
#import "ZBOverseasStudySchoolChildTwoCell.h" //留学学院
#import "ZBMyCollectionModel.h"

@protocol GYMyStoreListVCDelegate<NSObject>

-(void)didSelectVisaModel:(id)model;

@end

@interface GYMyStoreListVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong) NSMutableArray *modelArr;

@property(nonatomic,weak)id<GYMyStoreListVCDelegate>delegate;
@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;

@end

static NSString *identifier = @"GYMyScoreListCell";

@implementation GYMyStoreListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
     [self.tableView registerNib:[UINib nibWithNibName:@"GYMyScoreListCell" bundle:nil] forCellReuseIdentifier:identifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBImmigrationProjectOneCell" bundle:nil] forCellReuseIdentifier:@"ZBImmigrationProjectOneCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"ZBVisacenterChildCell" bundle:nil] forCellReuseIdentifier:@"ZBVisacenterChildCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBInspectionRouteCell" bundle:nil] forCellReuseIdentifier:@"ZBInspectionRouteCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudySuccessListCell" bundle:nil] forCellReuseIdentifier:@"ZBOverseasStudySuccessListCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudyApplyListCell" bundle:nil] forCellReuseIdentifier:@"ZBOverseasStudyApplyListCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"ZBOverseasStudySchoolChildTwoCell" bundle:nil] forCellReuseIdentifier:@"ZBOverseasStudySchoolChildTwoCell"];
//    favoritesFindpageOuttype
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
}

#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    }
    
    return _tableView;
}

-(NSMutableArray*)modelArr{
    
    if (!_modelArr) {
        _modelArr = [NSMutableArray new];
    }
    return _modelArr;
}

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    [@"migrate",@"visa",@"house",@"line",@"camp",@"studysuccess",@"study",@"school"]
    if ([self.orderType isEqualToString:@"migrate"]) {
        //移民项目
        ZBImmigrationProjectOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBImmigrationProjectOneCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ZBMigrateDTOModel *model = self.newsArray[indexPath.row];
        cell.modelMigrate = model;
        return cell;
    }else if([self.orderType isEqualToString:@"visa"] || [self.orderType isEqualToString:@"house"]){
        //签证中心、海外房产
        ZBVisacenterChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBVisacenterChildCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([self.orderType isEqualToString:@"visa"]){
            ZBVisaDTOModel *model = self.newsArray[indexPath.row];
            cell.modelVisaDTO = model;
        }else{
           ZBHouseDTOModel *model = self.newsArray[indexPath.row];
            cell.modelHouseDTO = model;
        }

        return cell;
    }else if([self.orderType isEqualToString:@"line"] ||[self.orderType isEqualToString:@"camp"] ){
        //定制考察 、冬夏令营
        ZBInspectionRouteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBInspectionRouteCell" forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([self.orderType isEqualToString:@"line"]){
            ZBLineDTOModel *model = self.newsArray[indexPath.row];
            cell.modelLineDto = model;
        }else{
            ZBCampDTOModel *model = self.newsArray[indexPath.row];
            cell.modelCampDto = model;
        }
        return cell;
    }else if([self.orderType isEqualToString:@"studysuccess"]){
        //留学成功案例
        ZBOverseasStudySuccessListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBOverseasStudySuccessListCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ZBStudySuccessDTOModel *model = self.newsArray[indexPath.row];
        cell.modelStudySuccessDto = model;
        return cell;
    }else if([self.orderType isEqualToString:@"study"]){
        //留学产品
        ZBOverseasStudyApplyListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBOverseasStudyApplyListCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ZBStudyDTOModel *model = self.newsArray[indexPath.row];
        cell.modelStudyDto = model;
        return cell;
    }
    else{
        //学院列表
        ZBOverseasStudySchoolChildTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBOverseasStudySchoolChildTwoCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.orderType isEqualToString:@"migrate"]) {
        return 348.0;
    }else if ([self.orderType isEqualToString:@"visa"] || [self.orderType isEqualToString:@"house"]) {
        return 170.0;
    }else if ([self.orderType isEqualToString:@"line"] ||[self.orderType isEqualToString:@"camp"]) {
        return 145.0;
    }else if ([self.orderType isEqualToString:@"studysuccess"] ||[self.orderType isEqualToString:@"study"]) {
        return 140.0;
    }else{
        return 115.0;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (self.delegate&&[self.delegate respondsToSelector:@selector(didSelectVisaModel:)]) {
//
//        [self.delegate didSelectVisaModel:nil];
//    }
}
//数据的请求
-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager favoritesFindpageOuttype:self.orderType  withPage:self.pageIndex withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBMyCollectionModel mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBMyCollectionModel mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}
@end
