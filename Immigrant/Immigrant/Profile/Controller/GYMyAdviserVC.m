//
//  GYMyAdviserVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyAdviserVC.h"

@interface GYMyAdviserVC ()
@property (weak, nonatomic) IBOutlet UIButton *close_btn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation GYMyAdviserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的顾问";
    self.view.backgroundColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0];
    self.bgView.layer.cornerRadius = 15.0;
    self.bgView.layer.masksToBounds = YES;
    
    self.close_btn.layer.cornerRadius = 25.0;
    self.close_btn.layer.masksToBounds = YES;
    
//    self.imgPhoto.backgroundColor = UIColor.redColor;
    self.imgPhoto.layer.cornerRadius = 63.0;
    self.imgPhoto.layer.masksToBounds = YES;
    
    [self.close_btn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    //获取顾问信息
//    appUserFindSalesmanSuccessBlock
    WS(weakSELF);
    [QSNetworkManager appUserFindSalesmanSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSDictionary *dicAll = responseModel.data[@"bussData"];
            weakSELF.nameLab.text = dicAll[@"nickname"];
            weakSELF.phoneLab.text = dicAll[@"mobile"];
//            {
//                creator (string, optional),
//                gmtCreated (string, optional),
//                gmtModified (string, optional),
//                id (integer, optional),
//                idcard (string, optional): 身份证号码 ,
//                isDeleted (string, optional),
//                isblacklist (boolean, optional): 是否黑名单 ,
//                isblacklisttext (string, optional): 黑名单描述 ,
//                mobile (string, optional): 用户手机 ,
//                modifier (string, optional),
//                nickname (string, optional): 用户昵称 ,
//                password (string, optional): MD5密码 ,
//                salesmanlevel (string, optional): 等级 = ['four', 'five'],
//                salesmanleveltext (string, optional): 等级描述 ,
//                salesmanstatus (string, optional): 业务员状态 = ['enable', 'disable']
//                string
//            Enum:    "enable", "disable"
//                ,
//                salesmanstatustext (string, optional): 业务员状态描述 ,
//                userId (integer, optional): 用户主键（0 表示没有绑定） ,
//                userMobile (string, optional): 用户手机 ,
//                username (string, optional): 用户姓名
//            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}
//关闭
- (void)closeBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
