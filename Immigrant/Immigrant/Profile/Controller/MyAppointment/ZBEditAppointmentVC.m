//
//  ZBEditAppointmentVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/17.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBEditAppointmentVC.h"
#import "WSDatePickerView.h"
#define RGB(x,y,z) [UIColor colorWithRed:x/255.0 green:y/255.0 blue:z/255.0 alpha:1.0]

#define randomColor [UIColor colorWithRed:arc4random()%256/255.0 green:arc4random()%256/255.0 blue:arc4random()%256/255.0 alpha:1]

@interface ZBEditAppointmentVC ()
@property (weak, nonatomic) IBOutlet UIView *oneView;
@property (weak, nonatomic) IBOutlet UITextField *firstStartTimeTxt;
@property (weak, nonatomic) IBOutlet UITextField *firstEndTimeTxt;
@property (weak, nonatomic) IBOutlet UIView *twoView;
@property (weak, nonatomic) IBOutlet UITextField *secondStartTimeTxt;
@property (weak, nonatomic) IBOutlet UITextField *secondEndTimeLab;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnFisrtStart;
@property (weak, nonatomic) IBOutlet UIButton *btnFirstEnd;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondStart;

@property (weak, nonatomic) IBOutlet UIButton *btnSecondEnd;


@property (nonatomic,strong)NSDate *selectDateAll;//首先开始时间
@property (nonatomic,strong)NSDate *selectSecondDateAll;//次选开始时间
@end

@implementation ZBEditAppointmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"编辑预约信息";
    [self loadFrame];
}
//修改界面的UI
- (void)loadFrame{
    self.oneView.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.12].CGColor;
    self.oneView.layer.shadowOffset = CGSizeMake(0,0);
    self.oneView.layer.shadowOpacity = 1;
    self.oneView.layer.shadowRadius = 22;
    self.oneView.layer.cornerRadius = 15;
    
    self.twoView.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.12].CGColor;
    self.twoView.layer.shadowOffset = CGSizeMake(0,0);
    self.twoView.layer.shadowOpacity = 1;
    self.twoView.layer.shadowRadius = 22;
    self.twoView.layer.cornerRadius = 15;
    
    self.btnUpdate.layer.cornerRadius = 25.0;
    self.btnUpdate.layer.masksToBounds = YES;
}

- (IBAction)btnChooseTimeClick:(UIButton *)sender {
    //tag值11-14
    switch (sender.tag) {
        case 11:
        {
            //年-月-日-时-分
            WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
                self.selectDateAll = selectDate;
                NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
                NSLog(@"选择的日期：%@",dateString);
                self.firstStartTimeTxt.text = dateString;
                self.firstEndTimeTxt.text = @"";
            }];
            datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
            datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
            datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
            [datepicker show];
        }
            break;
        case 12:
        {
            if (self.firstStartTimeTxt.text.length>0) {
                //年-月-日-时-分
                WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
                    NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
                    NSLog(@"选择的日期：%@",dateString);
                    self.firstEndTimeTxt.text = dateString;
                }];
                datepicker.minLimitDate = [NSDate dateWithTimeInterval:60*60 sinceDate:self.selectDateAll];
                datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
                datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
                datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
                [datepicker show];
            }else{
                 [[UIApplication sharedApplication].delegate.window makeToast:@"请先选择开始时间" duration:0.8 position:CSToastPositionBottom];
            }
        }
            break;
        case 13:
        {
            //年-月-日-时-分
            WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
                self.selectSecondDateAll = selectDate;
                NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
                NSLog(@"选择的日期：%@",dateString);
                self.secondStartTimeTxt.text = dateString;
                self.secondEndTimeLab.text = @"";
            }];
            datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
            datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
            datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
            [datepicker show];
        }
            break;
        case 14:
        {
            if (self.secondStartTimeTxt.text.length>0) {
                //年-月-日-时-分
                WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDayHourMinute CompleteBlock:^(NSDate *selectDate) {
                    NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd HH:mm"];
                    NSLog(@"选择的日期：%@",dateString);
                    self.secondEndTimeLab.text = dateString;
                }];
                datepicker.minLimitDate = [NSDate dateWithTimeInterval:60*60 sinceDate:self.selectSecondDateAll];
                datepicker.dateLabelColor = [UIColor orangeColor];//年-月-日-时-分 颜色
                datepicker.datePickerColor = [UIColor blackColor];//滚轮日期颜色
                datepicker.doneButtonColor = [UIColor orangeColor];//确定按钮的颜色
                [datepicker show];
            }else{
                [[UIApplication sharedApplication].delegate.window makeToast:@"请先选择次选沟通开始时间" duration:0.8 position:CSToastPositionBottom];
            }
        }
            break;
        default:
            break;
    }
}

//确认修改
- (IBAction)btnUpdateClick:(id)sender {
    if (self.firstStartTimeTxt.text || self.firstEndTimeTxt.text || self.secondStartTimeTxt.text || self.secondEndTimeLab.text) {
        [[UIApplication sharedApplication].delegate.window makeToast:@"时间不能为空" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    WS(weakSELF);
    [QSNetworkManager appCallBackUpdateWithId:self.appointId withFirstsdate:self.firstStartTimeTxt.text withFirstedate:self.firstEndTimeTxt.text withSecondsdate:self.secondStartTimeTxt.text withSecondedate:self.secondEndTimeLab.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            [weakSELF.navigationController popViewControllerAnimated:YES];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];

        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
