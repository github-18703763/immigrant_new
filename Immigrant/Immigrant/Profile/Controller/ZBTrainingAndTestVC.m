//
//  ZBTrainingAndTestVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBTrainingAndTestVC.h"

@interface ZBTrainingAndTestVC ()
@property (weak, nonatomic) IBOutlet UIView *viewOne;
@property (weak, nonatomic) IBOutlet UIView *viewTwo;

@end

@implementation ZBTrainingAndTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"培训考核";
    self.viewOne.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.11].CGColor;
    self.viewOne.layer.shadowOffset = CGSizeMake(0,0);
    self.viewOne.layer.shadowOpacity = 1;
    self.viewOne.layer.shadowRadius = 26;
    self.viewOne.layer.cornerRadius = 7;
    
    self.viewTwo.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.11].CGColor;
    self.viewTwo.layer.shadowOffset = CGSizeMake(0,0);
    self.viewTwo.layer.shadowOpacity = 1;
    self.viewTwo.layer.shadowRadius = 26;
    self.viewTwo.layer.cornerRadius = 7;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
