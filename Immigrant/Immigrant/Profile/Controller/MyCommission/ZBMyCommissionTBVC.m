//
//  ZBMyCommissionTBVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyCommissionTBVC.h"
#import "ZBMyCommissionHeaderView.h"
#import "ZBZBMyCommissionTBCell.h"
#import "ZBCommissionFlowTBVC.h"
#import "ZBEstimatedAeeivalMoneyTBVC.h"
#import "ZBApplyMoneyVC.h"
#import "ZBBrokerageflowModel.h"

@interface ZBMyCommissionTBVC ()
@property (nonatomic,strong) NSMutableArray  *newsArray;
@end

@implementation ZBMyCommissionTBVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的佣金";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    ZBMyCommissionHeaderView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBMyCommissionHeaderView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 250);
    [headView.grandTotalMoneyBtn addTarget:self action:@selector(grandTotalMoneyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [headView.estimatedAeeivalMoneyBtn addTarget:self action:@selector(estimatedAeeivalMoneyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [headView.applyMoneyBtn addTarget:self action:@selector(applyMoneyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableHeaderView = headView;
    
     [self.tableView registerNib:[UINib nibWithNibName:@"ZBZBMyCommissionTBCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    //导航栏右边按钮--佣金流水
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"佣金流水" style:UIBarButtonItemStyleDone target:self action:@selector(rightBtnClick)];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    [QSNetworkManager appBrokerageFinddetailByUserSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSDictionary *dicAll = responseModel.data[@"bussData"];
            NSArray *array = dicAll[@"brokerageSends"];
              self.newsArray = [ZBBrokerageSendsModel mj_objectArrayWithKeyValuesArray:array];
            headView.myCommissionMoneyLab.text = dicAll[@"currentbrokerageyuan"];
            [headView.grandTotalMoneyBtn setTitle:dicAll[@"totalbrokerageyuan"] forState:UIControlStateNormal];
            [headView.estimatedAeeivalMoneyBtn setTitle:dicAll[@"unsendbrokerageyuan"] forState:UIControlStateNormal];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        
    }];
}
//累计佣金
- (void)grandTotalMoneyBtnClick{
    
}
//预计到账佣金
- (void)estimatedAeeivalMoneyBtnClick{
//    ZBEstimatedAeeivalMoneyTBVC *vc = [ZBEstimatedAeeivalMoneyTBVC new];
//    [self.navigationController pushViewController:vc animated:YES];
}
//申请提现
- (void)applyMoneyBtnClick{
    ZBApplyMoneyVC *vc = [ZBApplyMoneyVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
//佣金流水
- (void)rightBtnClick{
    ZBCommissionFlowTBVC *vc = [ZBCommissionFlowTBVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBZBMyCommissionTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.modelBrokerageSends = self.newsArray[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 73.0;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
