//
//  ZBEstimatedAeeivalMoneyTBVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBEstimatedAeeivalMoneyTBVC.h"
#import "ZBZBEstimatedAeeivalMoneyTBCell.h"
@interface ZBEstimatedAeeivalMoneyTBVC ()

@end

@implementation ZBEstimatedAeeivalMoneyTBVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预计到账佣金";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBZBEstimatedAeeivalMoneyTBCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBZBEstimatedAeeivalMoneyTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 73.0;
}

@end
