//
//  ZBApplyMoneyVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBApplyMoneyVC.h"

@interface ZBApplyMoneyVC ()
@property (weak, nonatomic) IBOutlet UITextField *applyMoneyTxt;
@property (weak, nonatomic) IBOutlet UIButton *applyMoneyBtn;

@end

@implementation ZBApplyMoneyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"申请提现";
    self.applyMoneyBtn.layer.cornerRadius = 25.0;
    self.applyMoneyBtn.layer.masksToBounds = YES;
}
- (IBAction)btnApplyMoneyClick:(id)sender {
    if (self.applyMoneyTxt.text.length == 0){
        [[UIApplication sharedApplication].delegate.window makeToast:@"请输入提现金额" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    double countMoney = [self.applyMoneyTxt.text doubleValue];
    [QSNetworkManager appBrokeragewithdrawAdd:countMoney successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSString *sesstionId = responseModel.data[@"bussData"];
            [QSNetworkManager postUserDetail:sesstionId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {
                     [[UIApplication sharedApplication].delegate.window makeToast:@"提现已提交，我们将尽快处理！" duration:0.8 position:CSToastPositionBottom];
                }else{
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
                
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
