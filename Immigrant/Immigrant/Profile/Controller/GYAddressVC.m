//
//  GYAddressVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYAddressVC.h"
#import "GYAddressListCell.h"
#import "ZBMineAddressAddOrUpdateController.h"

@interface GYAddressVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong) NSMutableArray *modelArr;

@end

@implementation GYAddressVC
static NSString *identifier = @"GYAddressListCell";

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"我的地址";
    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GYAddressListCell" bundle:nil] forCellReuseIdentifier:identifier];
    
    self.tableView.tableFooterView = [UIView new];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [[UIColor getColor:@"D5D5D5"] colorWithAlphaComponent:0.47];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.estimatedRowHeight = 120;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self loadData];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStylePlain target:self action:@selector(addAddressAction)];
}

-(void)addAddressAction{
    WS(weakSELF);
    ZBMineAddressAddOrUpdateController *detial = [ZBMineAddressAddOrUpdateController new];
    detial.typeEnum = AddAddressType;
    detial.commitBlock = ^{
        [weakSELF loadData];
    };
    [self.navigationController pushViewController:detial animated:YES];
}

-(void)loadData{
    WS(weakSELF);
    [QSNetworkManager mineAddressListsuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            self.modelArr = [ZBMineAddressObject mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            [weakSELF.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}


#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    }
    
    return _tableView;
}

-(NSMutableArray*)modelArr{
    
    if (!_modelArr) {
        _modelArr = [NSMutableArray new];
    }
    return _modelArr;
}

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.modelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GYAddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell =[[NSBundle mainBundle]loadNibNamed:@"GYAddressListCell" owner:self options:nil][0];
    }
    WS(weakSELF);
    cell.updateBlock = ^(ZBMineAddressObject *object) {
        ZBMineAddressAddOrUpdateController *detial = [ZBMineAddressAddOrUpdateController new];
        detial.commitBlock = ^{
            [weakSELF loadData];
        };
        detial.typeEnum = UpdateAddressType;
        detial.model = object;
        [weakSELF.navigationController pushViewController:detial animated:YES];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.modelArr[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isChoose) {
        if (self.chooseBlock) {
            self.chooseBlock(self.modelArr[indexPath.row]);
        }
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    WS(weakSELF);
    ZBMineAddressAddOrUpdateController *detial = [ZBMineAddressAddOrUpdateController new];
    detial.typeEnum = UpdateAddressType;
    detial.commitBlock = ^{
        [weakSELF loadData];
    };
    detial.model = self.modelArr[indexPath.row];
    [self.navigationController pushViewController:detial animated:YES];
}

- (NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    // delete action
    WS(weakSELF);
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              ZBMineAddressObject *object = weakSELF.modelArr[indexPath.row];
                                              [QSNetworkManager mineAddressDelete:object.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                                                  if (responseModel.status == 200) {//成功
                                                      [weakSELF.modelArr removeObjectAtIndex:indexPath.row];
                                                      [tableView reloadData];
                                                      [weakSELF.tableView reloadData];
                                                  }else{//失败
                                                      [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                                                  }
                                              } failBlock:^(NSError * _Nullable error) {
                                                  [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
                                              }];
                                              
                                          }];
    

    deleteAction.backgroundColor = [UIColor getColor:@"E8E8E8"];

    return @[deleteAction];

}



- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (UIView *subview in tableView.subviews) {
        if ([NSStringFromClass([subview class]) isEqualToString:@"UISwipeActionPullView"]) {
            if ([NSStringFromClass([subview.subviews[0] class]) isEqualToString:@"UISwipeActionStandardButton"]) {
                UIImageView * imgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shanchu"]];
                GYAddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GYAddressListCell"];
                imgV.x=27;
                imgV.centerY= cell.centerY-[UIImage imageNamed:@"shanchu"].size.height/2;
                [subview.subviews[0] addSubview:imgV];
            }
        }
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

@end
