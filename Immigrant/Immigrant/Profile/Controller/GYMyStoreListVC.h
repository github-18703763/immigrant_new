//
//  GYMyStoreListVC.h
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYBaseVC.h"

@interface GYMyStoreListVC : GYBaseVC
//@"移民项目",@"签证中心",@"海外房产",@"考察定制",@"冬夏令营",@"留学成功案例",@"留学产品",@"留学学院"
//[@"migrate",@"visa",@"house",@"line",@"camp",@"studysuccess",@"study",@"school"]
@property (nonatomic,assign)NSString *orderType;

@end
