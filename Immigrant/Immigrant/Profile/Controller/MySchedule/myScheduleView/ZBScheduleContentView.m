//
//  ZBScheduleContentView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBScheduleContentView.h"
#import "ZBContentViewCollectCell.h"
#import "ZBScheduleModel.h"

@interface ZBScheduleContentView()<UICollectionViewDelegate, UICollectionViewDataSource> {
    
    NSDate *_tempDate;
}

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *dayModelArray;
@property (strong, nonatomic) NSMutableArray *selectDayAry;
@end

@implementation ZBScheduleContentView
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addContentView];
    }
    
    return self;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        NSInteger width = self.frame.size.width / 7;
        NSInteger height = self.frame.size.height / 6;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.itemSize = CGSizeMake(width, width);
        //        flowLayout.headerReferenceSize = CGSizeMake(kScreenWidth - 60, height);
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, width * 7, self.frame.size.height) collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        [_collectionView registerClass:[ZBContentViewCollectCell class] forCellWithReuseIdentifier:@"ZBContentViewCollectCell"];
        
        
    }
    return _collectionView;
}

- (void)addContentView {
    
    [self addSubview:self.collectionView];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dayModelArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZBContentViewCollectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ZBContentViewCollectCell" forIndexPath:indexPath];
    
    cell.dayLabel.textColor = [UIColor blackColor];
    //    cell.yyyyMMStr = self.dateLabel.text;
    id mon = self.dayModelArray[indexPath.row];
    if ([mon isKindOfClass:[ZBScheduleModel class]]) {
        ZBScheduleModel *model = (ZBScheduleModel *)mon;
        
        if ([self.selectDayAry containsObject:[NSString stringWithFormat:@"%ld", (long)model.dayValue]]) {
            
            NSInteger lastday = model.dayValue - 1;
            NSInteger nextday = model.dayValue + 1;
            
            // 居中判断
            if ([self.selectDayAry containsObject:[NSString stringWithFormat:@"%ld", (long)lastday]] && [self.selectDayAry containsObject:[NSString stringWithFormat:@"%ld", (long)nextday]]) {
                cell.selectionType = SelectionTypeMiddle;
                
            }
            // 右边半圆
            else if ([self.selectDayAry containsObject:[NSString stringWithFormat:@"%ld", (long)lastday]] && [self.selectDayAry containsObject:[NSString stringWithFormat:@"%ld", (long)model.dayValue]]) {
                
                cell.selectionType = SelectionTypeRightBorder;
                
            }
            // 左边半圆
            else if ([self.selectDayAry containsObject:[NSString stringWithFormat:@"%ld", (long)nextday]]) {
                cell.selectionType = SelectionTypeLeftBorder;
                
            } else {
                cell.selectionType = SelectionTypeSingle;
                
            }
        }
        else {
            cell.selectionType = SelectionTypeNone;
            
        }
        
        cell.monthModel = model;
    }else{
        cell.dayLabel.text = @"";
        cell.bgView.backgroundColor = UIColor.whiteColor;
    }
    
    
    return cell;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
//    CalendarHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CalendarHeaderView" forIndexPath:indexPath];
//    return headerView;
//}

#pragma mark -- 刷新数据
- (void)reloadCalendarDataWithType:(NSInteger)type headView:(nonnull ZBScheduleHeaderView *)headView dataAry:(nonnull NSArray *)dataAry{
    
    self.selectDayAry = [NSMutableArray arrayWithArray:dataAry];
    
    // 判断当前需要显示的数据
    switch (type) {
        case 0:
        {
            _tempDate = [NSDate date];
            headView.calendarLab.text = [self yyyyMMByLineWithDate:_tempDate];
            [self getDataDayModel:_tempDate dataAry:dataAry];
        }
            break;
            
        case 1:
        {
            _tempDate = [self getLastMonth:_tempDate];
            headView.calendarLab.text = [self yyyyMMByLineWithDate:_tempDate];
            [self getDataDayModel:_tempDate dataAry:dataAry];
            
        }
            break;
            
        case 2:
        {
            _tempDate = [self getNextMonth:_tempDate];
            headView.calendarLab.text = [self yyyyMMByLineWithDate:_tempDate];
            [self getDataDayModel:_tempDate dataAry:dataAry];
        }
            break;
            
        default:
            break;
    }
}


- (void)getDataDayModel:(NSDate *)date dataAry:(NSArray *)dataAry{
    
    NSUInteger days = [self numberOfDaysInMonth:date];
    NSInteger week = [self startDayOfWeek:date];
    self.dayModelArray = [[NSMutableArray alloc] initWithCapacity:42];
    NSInteger day = 1;
    //    self.monthDays = days;
    for (int i= 1; i<days+week; i++) {
        if (i<week) {
            [self.dayModelArray addObject:@""];
        }else{
            
            ZBScheduleModel *mon = [ZBScheduleModel new];
            mon.dayValue = day;
            NSDate *dayDate = [self dateOfDay:day];
            mon.dateValue = dayDate;
            
            //            NSArray *aryone = [dayDate.yyyyMMddByLineWithDate componentsSeparatedByString:@"-"];
            //            NSArray *aryTwo = [[NSDate date].yyyyMMddByLineWithDate componentsSeparatedByString:@"-"];
            //            //得这个月还剩多少天
            //            NSInteger otherDay = days - [aryTwo[2] integerValue];
            NSString *str = [NSString stringWithFormat:@"%ld",(long)day];
            if ([dataAry containsObject:str]) {
                mon.isSelectedDay = YES;
            }
            
            [self.dayModelArray addObject:mon];
            day++;
        }
        [self.collectionView reloadData];
    }
}

#pragma mark -Private
- (NSUInteger)numberOfDaysInMonth:(NSDate *)date{
    NSCalendar *greCalendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [greCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date].length;
    
}

- (NSDate *)firstDateOfMonth:(NSDate *)date{
    NSCalendar *greCalendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *comps = [greCalendar
                               components:NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitWeekday | NSCalendarUnitDay
                               fromDate:date];
    comps.day = 1;
    return [greCalendar dateFromComponents:comps];
}

- (NSUInteger)startDayOfWeek:(NSDate *)date
{
    NSCalendar *greCalendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];//Asia/Shanghai
    NSDateComponents *comps = [greCalendar
                               components:NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitWeekday | NSCalendarUnitDay
                               fromDate:[self firstDateOfMonth:date]];
    return comps.weekday;
}

- (NSDate *)getLastMonth:(NSDate *)date{
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *comps = [greCalendar
                               components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                               fromDate:date];
    comps.month -= 1;
    return [greCalendar dateFromComponents:comps];
}

- (NSDate *)getNextMonth:(NSDate *)date{
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *comps = [greCalendar
                               components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                               fromDate:date];
    comps.month += 1;
    return [greCalendar dateFromComponents:comps];
}

- (NSDate *)dateOfDay:(NSInteger)day{
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *comps = [greCalendar
                               components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                               fromDate:_tempDate];
    comps.day = day;
    return [greCalendar dateFromComponents:comps];
}

//获取当前的月份
- (NSInteger)getNowMonth {
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM"];
    NSString *strMonth = [dateFormatter stringFromDate:date];
    
    return [strMonth integerValue];
}

//获取当前的年份
- (NSInteger)getNowYear {
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    NSString *strYear = [dateFormatter stringFromDate:date];
    
    return [strYear integerValue];
}

- (NSString *)yyyyMMByLineWithDate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy.MM"];
    return [formatter stringFromDate:date];
}


@end
