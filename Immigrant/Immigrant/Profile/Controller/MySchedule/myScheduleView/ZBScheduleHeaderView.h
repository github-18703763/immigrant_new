//
//  ZBScheduleHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBScheduleHeaderView : UIView

@property (nonatomic, strong) UILabel *calendarLab;
@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;

@end

NS_ASSUME_NONNULL_END
