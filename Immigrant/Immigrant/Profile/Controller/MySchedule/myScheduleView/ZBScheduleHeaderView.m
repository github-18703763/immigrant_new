//
//  ZBScheduleHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//
#define LL_SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define Iphone6Scale(x) ((x) * LL_SCREEN_WIDTH / 375.0f)
#import "ZBScheduleHeaderView.h"

@implementation ZBScheduleHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addContentView];
    }
    
    return self;
}

- (void)addContentView {
    
    // 顶部日历
    self.calendarLab = [[UILabel alloc] init];
    self.calendarLab.text = @"2019.1";
    self.calendarLab.textColor = UIColor.blackColor;
    self.calendarLab.font = [UIFont systemFontOfSize:20];
    [self addSubview:self.calendarLab];
    
    // 左右按钮
    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.leftBtn setTitle:@"左" forState:UIControlStateNormal];
    [self.leftBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    self.leftBtn.backgroundColor = UIColor.blueColor;
    [self addSubview:self.leftBtn];
    
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"右" forState:UIControlStateNormal];
    [self.rightBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    self.rightBtn.backgroundColor = UIColor.blueColor;
    [self addSubview:self.rightBtn];
    
    WS(ws);
    // 顶部视图布局
    [self.calendarLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(ws);
        make.top.equalTo(@20);
    }];
    
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(ws.calendarLab.mas_left).offset(-20);
        make.centerY.equalTo(ws.calendarLab);
        make.width.height.equalTo(@30);
    }];
    
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(ws.calendarLab.mas_right).offset(20);
        make.centerY.equalTo(ws.calendarLab);
        make.width.height.equalTo(@30);
    }];
    
    
    //创建周日和周一的显示title
    
    NSArray *weekArray = [[NSArray alloc] initWithObjects:@"日",@"一",@"二",@"三",@"四",@"五",@"六", nil];
    
    CGFloat width = self.frame.size.width / 7;
    
    
    for (int i=0; i<weekArray.count; i++) {
        UILabel *weekLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*width, 50, width, 30)];
        weekLabel.textAlignment = NSTextAlignmentCenter;
        weekLabel.textColor = [UIColor grayColor];
        weekLabel.font = [UIFont systemFontOfSize:15];
        weekLabel.text = weekArray[i];
        [self addSubview:weekLabel];
    }
    
}
@end
