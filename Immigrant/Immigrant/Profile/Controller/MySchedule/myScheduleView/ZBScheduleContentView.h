//
//  ZBScheduleContentView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBScheduleHeaderView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZBScheduleContentView : UIView
// 刷新数据 type: 1 上一个月 2下一个月 0当前月份
- (void)reloadCalendarDataWithType:(NSInteger )type headView:(ZBScheduleHeaderView *)headView dataAry:(NSArray *)dataAry;
@end

NS_ASSUME_NONNULL_END
