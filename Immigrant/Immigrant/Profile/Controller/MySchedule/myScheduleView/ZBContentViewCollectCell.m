//
//  ZBContentViewCollectCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBContentViewCollectCell.h"

@implementation ZBContentViewCollectCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        UIView *bgView = [[UIView alloc] initWithFrame:self.contentView. bounds];
        bgView.backgroundColor = UIColor.whiteColor;
        self.bgView = bgView;
        [self.contentView addSubview:bgView];
        
        CGFloat width = self.contentView.frame.size.width*0.6;
        CGFloat height = self.contentView.frame.size.height*0.6;
        UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake( self.contentView.frame.size.width*0.5-width*0.5,  self.contentView.frame.size.height*0.5-height*0.5, width, height )];
        dayLabel.textAlignment = NSTextAlignmentCenter;
        dayLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:dayLabel];
        self.dayLabel = dayLabel;
        
    }
    return self;
}

- (void)setMonthModel:(ZBScheduleModel *)monthModel{
    _monthModel = monthModel;
    self.dayLabel.text = [NSString stringWithFormat:@"%ld",(long)monthModel.dayValue];
    
    
    if (monthModel.isSelectedDay) {
        self.bgView.backgroundColor = [UIColor blueColor];
        self.dayLabel.textColor = [UIColor whiteColor];
//        ZBLog(@"%ld",(long)monthModel.dayValue);
        
        if (self.selectionType == SelectionTypeMiddle) {
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:self.bgView.bounds];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            self.bgView.layer.mask = maskLayer;
            
            //            self.bgView.backgroundColor = UIColor.blueColor;
            
        } else if (self.selectionType == SelectionTypeLeftBorder) {
            
            
            //            self.selectionLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.selectionLayer.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft cornerRadii:CGSizeMake(self.selectionLayer.frame.size.width/2, self.selectionLayer.frame.size.width/2)].CGPath;
            UIBezierPath *maskPath= [UIBezierPath bezierPathWithRoundedRect:self.bgView.bounds
                                                          byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft
                                                                cornerRadii:CGSizeMake(self.bgView.frame.size.width/2, self.bgView.frame.size.width/2)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            self.bgView.layer.mask = maskLayer;
            
        } else if (self.selectionType == SelectionTypeRightBorder) {
            UIBezierPath *maskPath= [UIBezierPath bezierPathWithRoundedRect:self.bgView.bounds
                                                          byRoundingCorners:UIRectCornerTopRight|UIRectCornerBottomRight
                                                                cornerRadii:CGSizeMake(self.bgView.frame.size.width/2, self.bgView.frame.size.width/2)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            self.bgView.layer.mask = maskLayer;
            
            
        } else if (self.selectionType == SelectionTypeSingle) {
            
            CGFloat diameter = MIN(self.bgView.frame.size.width, self.bgView.frame.size.width);
            UIBezierPath *maskPath=[UIBezierPath bezierPathWithOvalInRect:CGRectMake(self.contentView.frame.size.width/2-diameter/2, self.contentView.frame.size.height/2-diameter/2, diameter, diameter)];
            
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bgView.bounds;
            maskLayer.path = maskPath.CGPath;
            self.bgView.layer.mask = maskLayer;
            
        }
        else {
            self.bgView.backgroundColor = UIColor.blueColor;
            
        }
    }
    else {
        self.bgView.backgroundColor = [UIColor whiteColor];
        self.dayLabel.textColor = [UIColor blackColor];
    }
    
}


@end
