//
//  ZBContentViewCollectCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBScheduleModel.h"

typedef NS_ENUM(NSUInteger, SelectionType) {
    SelectionTypeNone,
    SelectionTypeSingle,
    SelectionTypeLeftBorder,
    SelectionTypeMiddle,
    SelectionTypeRightBorder
};
NS_ASSUME_NONNULL_BEGIN

@interface ZBContentViewCollectCell : UICollectionViewCell
@property (strong, nonatomic) UILabel *dayLabel;
@property (strong, nonatomic) ZBScheduleModel *monthModel;
@property (strong, nonatomic) UIView *bgView;

@property (assign, nonatomic) SelectionType selectionType;
@end

NS_ASSUME_NONNULL_END
