//
//  ZBScheduleVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBScheduleVC.h"
#import "ZBScheduleContentView.h"
#import "ZBScheduleHeaderView.h"
@interface ZBScheduleVC (){
  UILabel *_topDayLab; //上班天数，可变数量
}
@property (nonatomic, strong) ZBScheduleHeaderView *topHeadView;
@property (nonatomic, strong) ZBScheduleContentView *calendaryView;
@property (nonatomic, strong) NSMutableArray *dataAry;

@end
@implementation ZBScheduleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"排班表";
    self.view.backgroundColor = UIColor.cyanColor;
    self.view.backgroundColor = [UIColor getColor:@"fbfbfb"];
    self.navigationController.navigationBar.translucent = NO;
    
    [self loadViewFrame];
    [self loadViewDateSoucrce];
}

- (void)loadViewFrame {
    
    // 背景视图
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(30, 20, SystemScreenWidth - 60, 380)];
    bgView.backgroundColor = UIColor.whiteColor;
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 7.f;
    [self.view addSubview:bgView];
    
    //顶部视图
    self.topHeadView = [[ZBScheduleHeaderView alloc] initWithFrame:CGRectMake(0, 0, bgView.frame.size.width, 80)];
    [self.topHeadView.leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.topHeadView.rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [bgView addSubview:self.topHeadView];
    
    //中间日历
    self.calendaryView = [[ZBScheduleContentView alloc] initWithFrame:CGRectMake(0, 80, bgView.frame.size.width, 300)];
    //    [self.calendaryView reloadCalendarDataWithType:0 headView:self.topHeadView];
    [bgView addSubview:self.calendaryView];
    
    
    // 底部解释
    UILabel *topLab = [[UILabel alloc] init];
    topLab.text = @"本月上班天数";
    topLab.textColor = UIColor.blackColor;
    topLab.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:topLab];
    
    [topLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView.mas_bottom).offset(30);
        make.left.equalTo(@45);
    }];
    
    UILabel *topNumLab = [[UILabel alloc] init];
    topNumLab.text = @"4/30";
    topNumLab.textColor = UIColor.blackColor;
    topNumLab.font = [UIFont systemFontOfSize:20];
    _topDayLab = topNumLab;
    [self.view addSubview:topNumLab];
    
    [topNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topLab);
        make.right.equalTo(@-45);
    }];
    
    // 蓝底为上班时间
    UILabel *bottomLab = [[UILabel alloc] init];
    bottomLab.text = @"蓝底为本月上班时间";
    bottomLab.textColor = UIColor.blackColor;
    bottomLab.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:bottomLab];
    
    [bottomLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topLab.mas_bottom).offset(20);
        make.left.equalTo(@45);
    }];
    
    UIImageView *bottomImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    bottomImg.backgroundColor = UIColor.blueColor;
    [self.view addSubview:bottomImg];
    
    [bottomImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomLab);
        make.right.equalTo(@-45);
        make.width.equalTo(@50);
        make.height.equalTo(@20);
    }];
    
}

- (void)loadViewDateSoucrce {
    
    self.dataAry = [NSMutableArray arrayWithObjects:@"20",@"21",@"22",@"23",@"27", nil];
    [self.calendaryView reloadCalendarDataWithType:0 headView:self.topHeadView dataAry:self.dataAry];
}

#pragma mark -- 按钮点击事件
// 左边按钮 上一个月
- (void)leftBtnAction:(UIButton *)sender {
    
    [self.calendaryView reloadCalendarDataWithType:1 headView:self.topHeadView dataAry:self.dataAry];
    
}

// 右边按钮 下一个月
- (void)rightBtnAction:(UIButton *)sender {
    
    [self.calendaryView reloadCalendarDataWithType:2 headView:self.topHeadView dataAry:self.dataAry];
}

@end
