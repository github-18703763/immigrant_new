//
//  ZBMyRateDetailTbVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyRateDetailTbVC.h"
#import "ZBMyRateDetailHeaderView.h"
//#import "ZBInspectionRouteCell.h"
#import "ZBMyRateDetailTBCell.h"
#import "ZBMyRateModel.h"
@interface ZBMyRateDetailTbVC ()
@property (nonatomic,strong)ZBMyRateModel *modelRate;
@property (nonatomic,strong)NSMutableArray  *newsArray;
@end

@implementation ZBMyRateDetailTbVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"测评结果";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    ZBMyRateDetailHeaderView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBMyRateDetailHeaderView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 363);
    self.tableView.tableHeaderView = headView;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBMyRateDetailTBCell" bundle:nil] forCellReuseIdentifier:@"ZBMyRateDetailTBCell"];
    //请求接口
    [QSNetworkManager usermigratequestionDetailWithId:self.myRateID successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            // 将字典转为模型
            self.modelRate = [ZBMyRateModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            self.newsArray = [ZBMyRateDetailModel mj_objectArrayWithKeyValuesArray:self.modelRate.migrates];
            headView.matchingDegreeLab.text = [NSString stringWithFormat:@"%li",self.modelRate.score];
            headView.titleLab.text = [NSString stringWithFormat:@"测试项目:%@",self.modelRate.migreateName];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMyRateDetailTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBMyRateDetailTBCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.modelDetail = self.newsArray[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        return 256;
}

@end
