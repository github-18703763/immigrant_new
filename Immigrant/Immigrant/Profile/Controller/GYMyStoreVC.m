//
//  GYMyStoreVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyStoreVC.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "GYMyStoreListVC.h"

@interface GYMyStoreVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,FSPageContentViewDelegate,FSSegmentTitleViewDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property (nonatomic,strong) FSSegmentTitleView *titleView;

@property (nonatomic,strong) FSPageContentView *pageContentView;

@property(nonatomic,strong) NSMutableArray *segmentTitlesArr;
@property(nonatomic,strong) NSMutableArray *segmentEngArr; //英文缩写
@property(nonatomic,strong) NSArray *childVCs;

@end

@implementation GYMyStoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的收藏";

    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    }
    
    return _tableView;
}

-(NSMutableArray*)segmentTitlesArr{
    if (!_segmentTitlesArr) {
        _segmentTitlesArr = [[NSMutableArray alloc]initWithArray:@[@"移民项目",@"签证中心",@"海外房产",@"考察定制",@"冬夏令营",@"留学成功案例",@"留学产品",@"留学学院"]];
    }
    return _segmentTitlesArr;
}
-(NSMutableArray*)segmentEngArr{
    if (!_segmentEngArr) {
        _segmentEngArr = [[NSMutableArray alloc]initWithArray:@[@"migrate",@"visa",@"house",@"line",@"camp",@"studysuccess",@"study",@"school"]];
    }
    return _segmentEngArr;
}
#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
        NSMutableArray *childVCs = [[NSMutableArray alloc]init];
        for (int i=0; i<self.segmentEngArr.count;i++) {
            GYMyStoreListVC *vc = [[GYMyStoreListVC alloc]init];
            vc.orderType = _segmentEngArr[i];
            [childVCs addObject:vc];
        }
//        for (NSString *title in self.segmentTitlesArr) {
//            GYMyStoreListVC *vc = [[GYMyStoreListVC alloc]init];
////            vc.delgate = self;
////            vc.titleStr = title;
////            vc.tableView.scrollEnabled = NO;
//            [childVCs addObject:vc];
//        }
        self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-50) childVCs:childVCs parentVC:self delegate:self];
        self.pageContentView.contentViewCurrentIndex = 0;
        [cell addSubview:self.pageContentView];
      
        self.childVCs = childVCs;
    
    }
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    //self.titleView.selectIndex = 2;
    
    return self.titleView;
}

#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SystemScreenHeight-50;
}
@end
