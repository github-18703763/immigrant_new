//
//  ZBMyNetworkingVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyNetworkingVC.h"
#import "ZBMyNetworkingTBCell.h"
#import "ZBMyNetworkingHeaderView.h"
#import "ZBMyNetworkingTabHeadView.h"
#import "ZBMyNetworkingModel.h"
#import "ZBMyNetworkingGroupModel.h"
#import "ZBMyNetworkingGroupFriendModel.h"

@interface ZBMyNetworkingVC ()
@property (nonatomic, strong) NSMutableArray *flagAry;
@property (nonatomic, strong) ZBMyNetworkingModel *renmaiModel;
@property (nonatomic, strong) NSMutableArray *fenzuAry;
@property (nonatomic, strong) NSMutableArray *fenzuFriendAry;
@property (nonatomic, strong) ZBMyNetworkingTabHeadView *headView;

@end

@implementation ZBMyNetworkingVC

- (instancetype)initWithStyle:(UITableViewStyle)style {
    
    NSLog(@"1");
    
    return [super initWithStyle:UITableViewStyleGrouped];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.title = @"人脉关系网";
    
    [self loadViewFrame];
    [self loadViewDataSource];
}

- (void)loadViewFrame {
    
    ZBMyNetworkingTabHeadView *headView = [[ZBMyNetworkingTabHeadView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 140)];
    headView.backgroundColor = [UIColor getColor:@"f6f7fb"];
    self.headView = headView;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.backgroundColor = [UIColor getColor:@"f6f7fb"];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.tableHeaderView = headView;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBMyNetworkingTBCell" bundle:nil] forCellReuseIdentifier:@"ZBMyNetworkingTBCell"];
}

- (void)loadViewDataSource {
    
    self.flagAry = [NSMutableArray arrayWithObjects:@"0",@"0",@"0",@"0", nil];
    [self.tableView reloadData];
    //获取详情--也就是头部
//    socialFindDetailsuccessBlock
    [QSNetworkManager socialFindDetailsuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.renmaiModel = [ZBMyNetworkingModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            self.headView.model = self.renmaiModel;
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    //获取直接好友列表
    //     WS(weakSELF);
    [QSNetworkManager finFirstFriendFindpageNameormobile:@"" withPage:1 withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
             self.fenzuAry = [ZBMyNetworkingGroupModel mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}
//获取子集好友POST /app/social/findfirstfriendlist
- (void)requestSocialFindFirstFriendListByUserID:(NSString *)uID{
    [QSNetworkManager socialFindfirstfriendlistUserId:uID successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.fenzuFriendAry = [ZBMyNetworkingGroupFriendModel mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
         [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
   return self.fenzuAry.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   return self.fenzuFriendAry.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMyNetworkingTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBMyNetworkingTBCell" forIndexPath:indexPath];
    ZBMyNetworkingGroupModel *model = self.fenzuAry[indexPath.section];
    ZBMyNetworkingGroupFriendModel *fmodel = self.fenzuFriendAry[indexPath.row];
    if ([model.isOpen isEqualToString:@"1"] ) {
        
        cell.contentView.hidden = NO;
    }
    else {
        cell.contentView.hidden = YES;
    }
    cell.model = fmodel;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    ZBMyNetworkingHeaderView *headView = [[ZBMyNetworkingHeaderView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 75)];
    headView.userInteractionEnabled = YES;
    headView.tag = 100 + section;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sectionClick:)];
    [headView addGestureRecognizer:tap];
    ZBMyNetworkingGroupModel *model = self.fenzuAry[section];
    if ([model.isOpen isEqualToString:@"1"]) {//展开
        headView.rightImg.image = [UIImage imageNamed:@"renmai_shousuo"];
        
    } else { //收起
        headView.rightImg.image = [UIImage imageNamed:@"renmai_zhankai"];
        
    }
    headView.model = model;
    return headView;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMyNetworkingGroupModel *model = self.fenzuAry[indexPath.section];
    if ([model.isOpen isEqualToString:@"1"])
        return 90;
    else
        return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 100.f;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01f;
}

//手势点击事件
- (void)sectionClick:(UITapGestureRecognizer *)tap{
    int index = tap.view.tag % 100;
    ZBMyNetworkingGroupModel *model = self.fenzuAry[index];
    [self requestSocialFindFirstFriendListByUserID:model.userId];
    
    ZBMyNetworkingHeaderView *view = [self.view viewWithTag:tap.view.tag];
    
    NSMutableArray *indexArray = [[NSMutableArray alloc]init];
    //    ChatListModel *listModel = _dataAry[index - 1];
    for (int i=0; i<self.fenzuFriendAry.count; i++) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:index];
        [indexArray addObject:path];
    }
    if ([model.isOpen isEqualToString:@"1"]) {//收起
        view.rightImg.image = [UIImage imageNamed:@"renmai_zhankai"];
        model.isOpen = @"0";
        [self.fenzuAry replaceObjectAtIndex:index withObject:model];
        [self.tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationTop]; //使用下面注释的方法就 注释掉这一句
        
    } else {//展开
        view.rightImg.image = [UIImage imageNamed:@"renmai_shousuo"];
        model.isOpen = @"1";
        [self.fenzuAry replaceObjectAtIndex:index withObject:model];
        [self.tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationBottom];  //使用下面注释的方法就 注释掉这一句
    }
}

@end
