//
//  ZBMyMailListVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyMailListVC.h"
#import "ZBMyMailListTBCell.h"
#import "ZBMyMailListHeaderView.h"
#import "ZBMailListModel.h"
#import <Contacts/Contacts.h>
@interface ZBMyMailListVC ()<ZBMyMailListHeaderViewDelegate>
// 通讯录数组
@property (nonatomic, strong) NSMutableArray *addressListAry;
// 索引标题数组
@property (nonatomic, strong) NSMutableArray * indexArr;
// 分组数组
@property (nonatomic, strong) NSMutableArray *fzListAry;
// 搜索数组
@property (nonatomic, strong) NSMutableArray *searchAry;
// 保存最初的数组
@property (nonatomic, strong) NSMutableArray *oldAry;
// 保存搜索出来的数组， 数据结构都不一样，什么鬼设计
@property (nonatomic, strong) NSMutableArray *searchOtherAry;

// 数字数组， 用于数字搜索
@property (nonatomic, strong) NSMutableArray *numAry;
// 数字搜索数组
@property (nonatomic, strong) NSMutableArray *searchNumAry;
// 网络请求匹配数组
@property (nonatomic, strong) NSMutableArray *networkAry;
@end

@implementation ZBMyMailListVC

- (instancetype)initWithStyle:(UITableViewStyle)style {
    
    NSLog(@"1");
    
    return [super initWithStyle:UITableViewStyleGrouped];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"通讯录";
    
    [self loadViewFrame];
    
    [self requestContactAuthorAfterSystemVersion9];
    
    [self loadViewData];
}

// 加载视图
- (void)loadViewFrame {
    
    //数据
    self.addressListAry = [NSMutableArray array];
    self.numAry = [NSMutableArray array];
    
    self.tableView.rowHeight = 75.f;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBMyMailListTBCell" bundle:nil] forCellReuseIdentifier:@"ZBMyMailListTBCell"];
    
    //表格头视图
    ZBMyMailListHeaderView *view = [[ZBMyMailListHeaderView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 130)];
    self.tableView.tableHeaderView = view;
    view.delegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    
}

- (void)loadViewData {
    
    // 设置右侧索引数组
    _indexArr = [[NSMutableArray alloc]init];
    for(char c = 'A'; c<='Z'; c++) {
        [_indexArr addObject:[NSString stringWithFormat:@"%c", c]];
    }
    [_indexArr addObject:[NSString stringWithFormat:@"#"]]; //
    
    self.tableView.sectionIndexColor = [UIColor grayColor];
    self.tableView.sectionIndexBackgroundColor = [UIColor whiteColor];
    //加载数据
    [self loadSource];
}
- (void)loadSource{
//     WS(weakSELF);
    [QSNetworkManager socialFindfirstfriendmobilelistsuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
//            NSInteger count = [responseModel.data[@"count"] integerValue];
            NSArray *array = responseModel.data[@"bussData"];
           self.networkAry = [[NSMutableArray alloc] initWithArray:array];
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}
//请求通讯录权限
#pragma mark 请求通讯录权限
- (void)requestContactAuthorAfterSystemVersion9{
    
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status == CNAuthorizationStatusNotDetermined) {
        CNContactStore *store = [[CNContactStore alloc] init];
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError*  _Nullable error) {
            if (error) {
                NSLog(@"授权失败");
            }else {
                NSLog(@"成功授权");
            }
        }];
    }
    else if(status == CNAuthorizationStatusRestricted)
    {
        NSLog(@"用户拒绝");
        [self showAlertViewAboutNotAuthorAccessContact];
    }
    else if (status == CNAuthorizationStatusDenied)
    {
        NSLog(@"用户拒绝");
        [self showAlertViewAboutNotAuthorAccessContact];
    }
    else if (status == CNAuthorizationStatusAuthorized)//已经授权
    {
        //有通讯录权限-- 进行下一步操作
        [self openContact];
    }
    
}

//有通讯录权限-- 进行下一步操作
- (void)openContact{
    // 获取指定的字段,并不是要获取所有字段，需要指定具体的字段
    NSArray *keysToFetch = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
    CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    
    [contactStore enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
        NSLog(@"-------------------------------------------------------");
        
        NSString *givenName = contact.givenName;
        NSString *familyName = contact.familyName;
        NSLog(@"givenName=%@, familyName=%@", givenName, familyName);
        //拼接姓名
        NSString *nameStr = [NSString stringWithFormat:@"%@%@",contact.familyName,contact.givenName];
        
        NSArray *phoneNumbers = contact.phoneNumbers;
        
        //        CNPhoneNumber  * cnphoneNumber = contact.phoneNumbers[0];
        
        //        NSString * phoneNumber = cnphoneNumber.stringValue;
        
        for (CNLabeledValue *labelValue in phoneNumbers) {
            //遍历一个人名下的多个电话号码
            NSString *label = labelValue.label;
            //   NSString *    phoneNumber = labelValue.value;
            CNPhoneNumber *phoneNumber = labelValue.value;
            
            NSString * string = phoneNumber.stringValue ;
            
            //去掉电话中的特殊字符
            string = [string stringByReplacingOccurrencesOfString:@"+86" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"(" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSLog(@"姓名=%@, 电话号码是=%@", nameStr, string);
            
            ZBMailListModel *model = [[ZBMailListModel alloc] init];
            model.name = nameStr;
            model.phoneNum = string;
            [self.numAry addObject:string];
            [self.addressListAry addObject:model];
            
            
        }
        
        //    *stop = YES; // 停止循环，相当于break；
        
    }];
    
    self.fzListAry =  [self sortObjectsAccordingToInitialWith:self.addressListAry];
    self.oldAry = [NSMutableArray arrayWithArray:self.fzListAry];
    
}

// 按首字母分组排序数组
-(NSMutableArray *)sortObjectsAccordingToInitialWith:(NSArray *)arr {
    
    // 初始化UILocalizedIndexedCollation
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    //得出collation索引的数量，这里是27个（26个字母和1个#）
    NSInteger sectionTitlesCount = [[collation sectionTitles] count];
    //初始化一个数组newSectionsArray用来存放最终的数据，我们最终要得到的数据模型应该形如@[@[以A开头的数据数组], @[以B开头的数据数组], @[以C开头的数据数组], ... @[以#(其它)开头的数据数组]]
    NSMutableArray *newSectionsArray = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
    
    //初始化27个空数组加入newSectionsArray
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [newSectionsArray addObject:array];
    }
    
    //将每个名字分到某个section下
    for (ZBMailListModel *personModel in arr) {
        
        //获取name属性的值所在的位置，比如"林丹"，首字母是L，在A~Z中排第11（第一位是0），sectionNumber就为11
        NSInteger sectionNumber = [collation sectionForObject:personModel collationStringSelector:@selector(name)];
        //把name为“林丹”的p加入newSectionsArray中的第11个数组中去
        NSMutableArray *sectionNames = newSectionsArray[sectionNumber];
        [sectionNames addObject:personModel];
    }
    
    //对每个section中的数组按照name属性排序
    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *personArrayForSection = newSectionsArray[index];
        NSArray *sortedPersonArrayForSection = [collation sortedArrayFromArray:personArrayForSection collationStringSelector:@selector(name)];
        newSectionsArray[index] = sortedPersonArrayForSection;
    }
    
    //    //删除空的数组
    //    NSMutableArray *finalArr = [NSMutableArray new];
    //    for (NSInteger index = 0; index < sectionTitlesCount; index++) {
    //        if (((NSMutableArray *)(newSectionsArray[index])).count != 0) {
    //            [finalArr addObject:newSectionsArray[index]];
    //        }
    //    }
    //    return finalArr;
    
    return newSectionsArray;
}

//提示没有通讯录权限
- (void)showAlertViewAboutNotAuthorAccessContact{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"请授权通讯录权限"
                                          message:@"请在iPhone的\"设置-隐私-通讯录\"选项中,允许花解解访问你的通讯录"
                                          preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:OKAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.indexArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.fzListAry[section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMyMailListTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBMyMailListTBCell" forIndexPath:indexPath];
    
    NSArray *ary = self.fzListAry[indexPath.section];
    if (ary.count > 0) {
        ZBMailListModel *model = ary[indexPath.row];
        cell.dataAry = self.networkAry;
        cell.listModel = model;
    }
    else {
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if ([self.fzListAry[section] count] > 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, 20)];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 100, 20)];
        lab.text = self.indexArr[section];
        lab.textAlignment = NSTextAlignmentLeft;
        [view addSubview:lab];
        
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if ([self.fzListAry[section] count] > 0) {
        
        return 20.f;
    }
    return 0.01f;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark 右侧索引
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _indexArr;
}

//点击索引栏标题时执行
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    
//    ZBLog(@"t%@",title);
    //这里是为了指定索引index对应的是哪个section的，默认的话直接返回index就好。其他需要定制的就针对性处理
    if ([title isEqualToString:UITableViewIndexSearch])
    {
        [tableView setContentOffset:CGPointZero animated:NO];//tabview移至顶部
        return NSNotFound;
    }
    else
    {
        return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index] - 1; // -1 添加了搜索标识
    }
}

- (void)searchDataAryWithSuperDataAry:(NSMutableArray *)ary andTextStr:(NSString *)text {
    
    NSMutableArray *resutArray = [NSMutableArray array];
    self.searchOtherAry = [NSMutableArray array];
    
    if ([self isPureNum:text]) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            // 处理耗时操作的代码块...
            
            if (text!=nil && text.length>0) {
                // 判断阿拉伯数字
                NSArray *arr = [NSArray arrayWithArray:self.numAry];
                NSString *str = text;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@",str];
                NSLog(@"new arr: %@",[arr filteredArrayUsingPredicate:predicate]);
                
                self.searchNumAry = [NSMutableArray arrayWithArray:[arr filteredArrayUsingPredicate:predicate]];
                
                if (self.searchNumAry.count > 0) {
                    
                    [self.searchNumAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        NSString *numStr = obj;
                        NSMutableArray *numsAry = [NSMutableArray array];
                        
                        [self.addressListAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            
                            ZBMailListModel *listmodel = obj;
                            
                            if ([listmodel.phoneNum isEqualToString:numStr]) {
                                
                                [numsAry addObject:listmodel];
                                [self.searchOtherAry addObject:listmodel];
                            }
                        }];
                        
                        if (numsAry > 0) {
                            
                            [resutArray addObject:numsAry];
                        }
                    }];
                    
                    self.searchAry = [NSMutableArray arrayWithArray:resutArray];
                    self.fzListAry =  [[NSMutableArray alloc] initWithArray: [self sortObjectsAccordingToInitialWith:self.searchOtherAry]];
                }
            }else{
                self.searchAry = [NSMutableArray arrayWithArray:self.oldAry];
                self.fzListAry =  [[NSMutableArray alloc] initWithArray: [self sortObjectsAccordingToInitialWith:self.addressListAry]];
            }
            
            //回到主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView reloadData];
            });
            
        });
        
        
    }
    else {
        // 判断英文字母
        //加个多线程，否则数量量大的时候，有明显的卡顿现象
        //这里最好放在数据库里面再进行搜索，效率会更快一些
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            // 处理耗时操作的代码块...
            
            if (text!=nil && text.length>0) {
                //遍历需要搜索的所有内容，其中self.dataArray为存放总数据的数组
                for (NSArray *modelary in ary) {
                    NSMutableArray *ary = [NSMutableArray array];
                    for (ZBMailListModel *str in modelary) {
                        
                        NSString *tempStr = str.name;
                        //----------->把所有的搜索结果转成成拼音
                        NSString *pinyin = [self transformToPinyin:tempStr];
                        NSLog(@"pinyin--%@",pinyin);
                        
                        if ([pinyin rangeOfString:text options:NSCaseInsensitiveSearch].length >0 ) {
                            //把搜索结果存放self.resultArray数组
                            ZBMailListModel *smodel = [ZBMailListModel new];
                            smodel.name = str.name;
                            smodel.phoneNum = str.phoneNum;
                            [ary addObject:smodel];
                            [self.searchOtherAry addObject:smodel];
                        }
                    }
                    
                    if (ary.count > 0) {
                        
                        [resutArray addObject:ary];
                    }
                }
                
                self.searchAry = [NSMutableArray arrayWithArray:resutArray];
                self.fzListAry =  [[NSMutableArray alloc] initWithArray: [self sortObjectsAccordingToInitialWith:self.searchOtherAry]];
            }else{
                self.searchAry = [NSMutableArray arrayWithArray:self.oldAry];
                self.fzListAry =  [[NSMutableArray alloc] initWithArray: [self sortObjectsAccordingToInitialWith:self.addressListAry]];
            }
            
            //回到主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView reloadData];
            });
            
        });
    }
}

- (NSString *)transformToPinyin:(NSString *)aString
{
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:aString];
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    NSArray *pinyinArray = [str componentsSeparatedByString:@" "];
    NSMutableString *allString = [NSMutableString new];
    
    int count = 0;
    
    for (int  i = 0; i < pinyinArray.count; i++)
    {
        for(int i = 0; i < pinyinArray.count;i++)
        {
            if (i == count) {
                [allString appendString:@"#"];
                //区分第几个字母
            }
            [allString appendFormat:@"%@",pinyinArray[i]];
        }
        [allString appendString:@","];
        count ++;
    }
    NSMutableString *initialStr = [NSMutableString new];
    //拼音首字母
    for (NSString *s in pinyinArray)
    {
        if (s.length > 0)
        {
            [initialStr appendString:  [s substringToIndex:1]];
        }
    }
    [allString appendFormat:@"#%@",initialStr];
    [allString appendFormat:@",#%@",aString];
    return allString;
}

- (void)searchDelegate:(NSString *)str {
    
    [self searchDataAryWithSuperDataAry:self.oldAry andTextStr:str];
}

#pragma mark 判断输入的字符串是纯数字还是英文字母
/** 判断一个字符串是纯数字 */
- (BOOL)isPureNum:(NSString *)text {
    if (!text) {
        return NO;
    }
    NSScanner *scan = [NSScanner scannerWithString:text];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}



@end
