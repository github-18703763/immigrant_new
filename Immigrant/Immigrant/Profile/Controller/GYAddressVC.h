//
//  GYAddressVC.h
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYBaseVC.h"
#import "ZBMineAddressObject.h"

@interface GYAddressVC : GYBaseVC

@property (nonatomic,assign) BOOL  isChoose;

@property (nonatomic,copy) void(^chooseBlock)(ZBMineAddressObject *object);

@end
