//
//  ZBMineAddressAddOrUpdateController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMineAddressAddOrUpdateController.h"
#import "ZBMineAddressAddOrUpdateCell.h"

@interface ZBMineAddressAddOrUpdateController ()

@end

@implementation ZBMineAddressAddOrUpdateController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加新地址";
    if (self.typeEnum == UpdateAddressType) {
        self.title = @"地址详情";
    }
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth,0 , 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBMineAddressAddOrUpdateCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.rowHeight = KIsiPhoneX?SystemScreenHeight-64:SystemScreenHeight-88;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMineAddressAddOrUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    WS(weakSELF);
    cell.commitAcitonBlock = ^(NSDictionary * _Nonnull dict) {
        if (weakSELF.typeEnum == UpdateAddressType) {
            [QSNetworkManager mineAddressUpdateId:weakSELF.model.id withMsg:dict successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {//成功
                    if (weakSELF.commitBlock) {
                        weakSELF.commitBlock();
                    }
                    [weakSELF.navigationController popViewControllerAnimated:YES];
                }else{//失败
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];

        }else{
            [QSNetworkManager mineAddressAdd:dict successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
                if (responseModel.status == 200) {//成功
                    if (weakSELF.commitBlock) {
                        weakSELF.commitBlock();
                    }
                    [weakSELF.navigationController popViewControllerAnimated:YES];
                }else{//失败
                    [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
                }
            } failBlock:^(NSError * _Nullable error) {
                [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            }];
            
        }
    };
    if (self.typeEnum == UpdateAddressType) {
        cell.model = self.model;
    }
    return cell;
}


@end
