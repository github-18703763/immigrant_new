//
//  GYMyMessageDetailVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyMessageDetailVC.h"

@interface GYMyMessageDetailVC ()
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneConstaint; //197----25
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UIButton *youhuiquanBtn;
@property (weak, nonatomic) IBOutlet UILabel *disCountLab;
@property (weak, nonatomic) IBOutlet UILabel *dayTimeLab;

@end

@implementation GYMyMessageDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息详情";
    self.lineView.layer.masksToBounds = YES;
    self.lineView.layer.cornerRadius = 1.5;
        [self updateFrame:_modelMessage];
    [QSNetworkManager messageFindDetial:self.modelMessage.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
           self.modelMessage =  [ZBMessageModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            [self updateFrame:self.modelMessage];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];

    }];
}
- (void)updateFrame:(ZBMessageModel *)model{
    if ([_modelMessage.outtype isEqualToString:@"coupon"]) {
        self.oneConstaint.constant = 197;
        self.photoImg.hidden = false;
        self.youhuiquanBtn.hidden = false;
        self.disCountLab.hidden = false;
        self.dayTimeLab.hidden = false;
    }else{
        self.oneConstaint.constant = 25;
        self.photoImg.hidden = true;
        self.youhuiquanBtn.hidden = true;
        self.disCountLab.hidden = true;
        self.dayTimeLab.hidden = true;
    }
    self.titleLab.text = model.outtypetext;
    self.detailLab.text = [NSString stringWithFormat:@"%@\n\n\n\n%@",model.content,model.gmtCreated];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
