//
//  GYMyMessageDetailVC.h
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMessageModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GYMyMessageDetailVC : UIViewController
@property(nonatomic,strong)ZBMessageModel *modelMessage;
@end

NS_ASSUME_NONNULL_END
