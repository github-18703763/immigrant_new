//
//  ZBProfileController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBProfileController.h"
#import "ZBMyItemModel.h"
#import "ZBMyListItemCell.h"
#import "ZBMyHeadCell.h"

#import "GYMyScoreVC.h"
#import "GYMyOrderVC.h"
#import "GYMyScoreVC.h"
#import "GYMyStoreVC.h"
#import "GYMyRateVC.h"
#import "GyMyRecomendVC.h"
#import "GYMyMessageVC.h"
#import "GYAddressVC.h"
#import "GYMyPdfVC.h"
#import "GYFeedBackVC.h"
#import "ZBMyCommissionTBVC.h"
#import "ZBMyCRMTBVC.h"
#import "ZBTrainingAndTestVC.h"
#import "ZBMyAppointmentTBVC.h"
#import "GYMyAdviserVC.h"
#import "ZBScheduleVC.h"
#import "ZBUserMessageController.h" //个人信息
#import "ZBEarnPointsModel.h"

//#import "XLUserDetailModel.h"

@interface ZBProfileController ()<UIScrollViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,ZBMyHeadCellDelegate>{
    GYUserModel*_myUserDetailModel;
    
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic,strong) NSMutableArray *namesArr;

@property (nonatomic,strong) NSMutableArray *imageNamesArr;

@property (nonatomic,strong) NSMutableArray *itemsArr;


@end

static NSString *idendifier = @"ZBMyListItemCell";
static NSString *headIdentifier = @"ZBMyHeadCell";

@implementation ZBProfileController

#pragma mark - lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ZBMyListItemCell" bundle:nil] forCellWithReuseIdentifier:idendifier];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ZBMyHeadCell" bundle:nil] forCellWithReuseIdentifier:headIdentifier];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    UICollectionViewFlowLayout* layout1 = [[UICollectionViewFlowLayout alloc] init];
    
    self.collectionView.collectionViewLayout = layout1;
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
    self.collectionView.contentInset = UIEdgeInsetsMake(-StatuBarHeight, 0, 0, 0);
    //调用获取积分
    [self requestUserPoint];
    //获取用户详情
    if ([QSUserManager shareInstance].isLogin) {
        [self requestForUserDetail];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    //    self.navigationController.navigationBarHidden = YES;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    
}

#pragma mark -lazy load
-(NSMutableArray*)itemsArr{
    
    if (!_itemsArr) {
       _itemsArr = [[NSMutableArray alloc]initWithCapacity:12];
    }
    return _itemsArr;
}
-(void)addDataToItemsArr:(NSString *)roleStr{
    NSArray *titles =nil;
    
    NSArray *images  =nil;
    //签约会员
    if ([roleStr isEqualToString:@"signmember"]) {
        titles =@[@"我的预约",@"在办项目",@"我的行程",@"我的推荐",@"登陆定制",@"我的顾问",@"消息中心",@"我的地址",@"意见反馈"];
        
        images =@[@"mine_yuyue",@"mine_zaibanxiangmu",@"mine_xingcheng",@"mine_tuijian",@"mine_dengludingzhi",@"mine_guwen",@"mine_xiaoxi",@"mine_dizhi",@"mine_fankui"];
    }else if ([roleStr isEqualToString:@"partner"]){
        //合作伙伴
        titles =@[@"荐单详情",@"我的佣金",@"我的预约",@"在办项目",@"我的推荐",@"消息中心",@"我的地址",@"意见反馈"];
        
        images =@[@"wode_jiandanxiangqing",@"mine_yongjin",@"mine_yuyue",@"mine_zaibanxiangmu",@"mine_tuijian",@"mine_xiaoxi",@"mine_dizhi",@"mine_fankui"];
    }else if ([roleStr isEqualToString:@"salesman"]){
        //业务员
        titles =@[@"我的佣金",@"我的CRM",@"排班表",@"培训考核",@"我的预约",@"我的行程",@"我的推荐",@"在办项目",@"消息中心",@"我的地址",@"意见反馈"];
        
        images =@[@"mine_yongjin",@"mine_crm",@"mine_paibanbiao",@"mine_peixunkaohe",@"mine_yuyue",@"mine_xingcheng",@"mine_tuijian",@"mine_zaibanxiangmu",@"mine_xiaoxi",@"mine_dizhi",@"mine_fankui"];
    }else{
        //member 大众会员
        titles =@[@"我的预约",@"我的行程",@"我的推荐",@"我的顾问",@"消息中心",@"我的地址",@"意见反馈"];
        
        images =@[@"mine_yuyue",@"mine_xingcheng",@"mine_tuijian",@"mine_guwen",@"mine_xiaoxi",@"mine_dizhi",@"mine_fankui"];
    }

    NSMutableArray *arr = [NSMutableArray new];
    for (NSInteger i=0; i<titles.count; i++) {
        ZBMyItemModel *model = [ZBMyItemModel new];
        model.title =titles[i];
        model.imageName = images[i];
        [arr addObject:model];
    }
    [self.itemsArr removeAllObjects];
    [self.itemsArr addObjectsFromArray:arr];
}


#pragma mark - collectionView delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView
{
    return 2;
}

//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    
    return self.itemsArr.count;
}

//每个UICollectionView展示的内容
- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath
{
    
    if (indexPath.section==0) {
        
        ZBMyHeadCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:headIdentifier forIndexPath:indexPath];
        cell.userHeadImgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapUserDetail)];
        [cell.userHeadImgView addGestureRecognizer:tap];
        cell.leftLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"CURRENTPOINT"];
        cell.delegate = self;
        
        return cell;
    }
    
    else{
        
        ZBMyListItemCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:idendifier forIndexPath:indexPath];
        
        if (cell == nil) {
            cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(ZBMyListItemCell.class) owner:self options:nil][0];
        }
        
        if (self.itemsArr.count>0) {
            
            ZBMyItemModel *model = self.itemsArr[indexPath.row];
            
            cell.model = model;
            
            if ([model.title isEqualToString:@"消息中心"]) {
                
                cell.dotView.hidden = NO;
            }
            else{
                cell.dotView.hidden = YES;
                
            }
            
        }
        
        return cell;
        
    }
    
}

- (void)collectionView:(UICollectionView*)collectionView didSelectItemAtIndexPath:(NSIndexPath*)indexPath{
    
    if (indexPath.section==0) {
        return;
    }
    ZBMyItemModel *model = self.itemsArr[indexPath.row];

    if ([model.title isEqualToString:@"我的佣金"]) {
        //我的佣金
        ZBMyCommissionTBVC *vc = [ZBMyCommissionTBVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.title isEqualToString:@"我的CRM"]){
        //我的CRM
        ZBMyCRMTBVC *vc = [ZBMyCRMTBVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.title isEqualToString:@"排班表"]){
        //排班表
        ZBScheduleVC *vc = [ZBScheduleVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.title isEqualToString:@"培训考核"]){
        //培训考核
        ZBTrainingAndTestVC *vc = [ZBTrainingAndTestVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.title isEqualToString:@"我的预约"]){
        //我的预约
        ZBMyAppointmentTBVC *vc = [ZBMyAppointmentTBVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.title isEqualToString:@"我的行程"]){
        //我的行程
        GYMyPdfVC *ctrol = [GYMyPdfVC new];
        [self.navigationController pushViewController:ctrol animated:YES];
    }else if ([model.title isEqualToString:@"我的推荐"]){
        //我的推荐
        GyMyRecomendVC *ctrol = [GyMyRecomendVC new];
        [self.navigationController pushViewController:ctrol animated:YES];
        
    }else if ([model.title isEqualToString:@"我的顾问"]){
        //我的顾问
        GYMyAdviserVC  *vc = [GYMyAdviserVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.title isEqualToString:@"消息中心"]){
        //我的消息
        GYMyMessageVC *ctrol = [GYMyMessageVC new];
        
        [self.navigationController pushViewController:ctrol animated:YES];
    }else if ([model.title isEqualToString:@"我的地址"]){
        GYAddressVC *ctrol = [GYAddressVC new];
        
        [self.navigationController pushViewController:ctrol animated:YES];
    }else if ([model.title isEqualToString:@"意见反馈"]){
        GYFeedBackVC *ctrol = [GYFeedBackVC new];
        
        [self.navigationController pushViewController:ctrol animated:YES];
    }
    
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath*)indexPath{
    
    //    return CGSizeMake(collectionView.frame.size.width / 2, collectionView.frame.size.width/ 2);
    
    if (indexPath.section==0) {
        if (_myUserDetailModel.coupon) {
           return CGSizeMake(collectionView.frame.size.width, 400);
        }
        return CGSizeMake(collectionView.frame.size.width, 350);
    }
    
    return CGSizeMake(collectionView.frame.size.width / 2, 100);
    
}

//列间距
- (CGFloat)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}
//行间距
- (CGFloat)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

#pragma mark - ZBMyHeadCellDelegate
-(void)didSelectHeadBtn:(NSInteger)index{
    
    //    ZBLog(@"%d",index);
    
    switch (index) {
        case 0:{
            //我的测评
            GYMyRateVC *vc = [GYMyRateVC new];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }break;
        case 1:{
            //我的收藏
            GYMyStoreVC *vc =[GYMyStoreVC new];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }break;
        case 2:{
            //我的积分
            GYMyScoreVC *vc =[GYMyScoreVC new];
            
            [self.navigationController pushViewController:vc animated:YES];
        }break;
        case 3:{
            //我的订单
            GYMyOrderVC *vc =[GYMyOrderVC new];
            
            [self.navigationController pushViewController:vc animated:YES];
        }break;
        default:
            break;
    }
    
}
#pragma 用户资料
- (void)tapUserDetail{
    ZBUserMessageController *vc = [ZBUserMessageController new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma 获取用户积分
- (void)requestUserPoint{
    [QSNetworkManager appPointFinddetailSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            ZBEarnPointsModel *myEarnPointsModel =  [ZBEarnPointsModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            [[NSUserDefaults standardUserDefaults]setObject:myEarnPointsModel.currentpoint forKey:@"CURRENTPOINT"];
            [self.collectionView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        
    }];
}
//请求用户详情(区分会员种类)
-(void)requestForUserDetail{
    [QSNetworkManager postUserDetail:[QSUserManager shareInstance].sessionId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            //设置头部视图
            ZBMyHeadCell * cell = ( ZBMyHeadCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            
            GYUserModel *userDetailModel =  [GYUserModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            self->_myUserDetailModel=userDetailModel;
            cell.nickLabel.text=userDetailModel.nickname;
            
            [cell.userHeadImgView sd_setImageWithURL:[NSURL URLWithString:userDetailModel.avatar] placeholderImage:[UIImage imageNamed:@"icon_logo"]];
            
            cell.leftLabel.text=userDetailModel.point.currentpoint;
            cell.levelProgressView.progress=[userDetailModel.point.currentpoint intValue]/3000;
            
            if (userDetailModel.coupon) {
                cell.bottomView.backgroundColor=[UIColor clearColor];
                cell.couponImgView.hidden=NO;
                cell.discountLB.text=[NSString stringWithFormat:@"%.0f折优惠券",userDetailModel.coupon.discount*10];
                cell.quanTimeLB.text=[NSString stringWithFormat:@"%d天后过期",userDetailModel.coupon.remaindays];
                if (!userDetailModel.coupon.isvalid) {
                    if (userDetailModel.coupon.canactive) {
                        cell.useCouponBtn.backgroundColor=UIColorFromRGB(0xededed);
                    }else{
                        cell.useCouponBtn.enabled=NO;
                    }
                }
            }else{
                cell.bottomView.backgroundColor=[UIColor whiteColor];
                cell.couponImgView.hidden=YES;
            }
            
            
            if (userDetailModel.issalesman) {
                //是业务员
                cell.memberLabel.text=@"业务员";
                cell.levelImgView.image=[UIImage imageNamed:@"wode_huiyuan_b"];
                [self addDataToItemsArr:@"salesman"];
            }else{
                cell.memberLabel.text=userDetailModel.userroletext;
                //签约会员
                if ([userDetailModel.userrole isEqualToString:@"signmember"]) {
                    cell.levelImgView.image=[UIImage imageNamed:@"wode_huiyuan_d"];
                }else if ([userDetailModel.userrole isEqualToString:@"partner"]){
                    //合作伙伴
                    cell.levelImgView.image=[UIImage imageNamed:@"wode_huiyuan_c"];
                }else{//member 大众会员
                    cell.levelImgView.image=[UIImage imageNamed:@"wode_huiyuan_a"];
                }
                [self addDataToItemsArr:userDetailModel.userrole];
            }
            
            [self.collectionView reloadData];
            
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
}


@end
