//
//  GYMyMessageVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyMessageVC.h"
#import "GYMessageListCell.h"
#import "ZBMessageModel.h"
#import "GYMyMessageDetailVC.h"

@interface GYMyMessageVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property(nonatomic,strong)UITableView* tableView;
@property(nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;
@end

@implementation GYMyMessageVC

static NSString *mesID = @"GYMessageListCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的消息";
    
    [self.view addSubview:self.tableView];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
    }];
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GYMessageListCell" bundle:nil] forCellReuseIdentifier:mesID];
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    
    [self loadData:YES];
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager messageFindpageWithPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.newsArray = [ZBMessageModel mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.newsArray addObjectsFromArray:[ZBMessageModel mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}

#pragma mark - lazyload
-(UITableView*)tableView{
    
    if (!_tableView) {
        _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    }
    
    return _tableView;
}

-(NSMutableArray*)modelArr{
    
    if (!_modelArr) {
        _modelArr = [NSMutableArray new];
    }
    return _modelArr;
}

#pragma mark - UITableViewDataSource&Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 140;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GYMessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:mesID];
    if (!cell) {
        cell =[[NSBundle mainBundle]loadNibNamed:@"GYMessageListCell" owner:self options:nil][0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.modelMessage = self.newsArray[indexPath.row];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GYMyMessageDetailVC *vc = [GYMyMessageDetailVC new];
    vc.modelMessage = self.newsArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
