//
//  ZBFillReturnVisitVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBFillReturnVisitVC.h"

@interface ZBFillReturnVisitVC ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property (weak, nonatomic) IBOutlet UIButton *submintBtn;
@property (weak, nonatomic) IBOutlet UILabel *content_lab;

@end

@implementation ZBFillReturnVisitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"填写回访";
    
    //初始化一个约束
    self.txtView.textContainerInset = UIEdgeInsetsMake(5, 10, 5, 10);
    self.txtView.layer.cornerRadius = 15;
    self.txtView.layer.masksToBounds = YES;
    self.txtView.delegate = self;
    
    self.submintBtn.layer.cornerRadius = 25.0;
    self.submintBtn.layer.masksToBounds = YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length != 0) {
        self.content_lab.hidden = YES;
    }else{
        self.content_lab.hidden = NO;
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
