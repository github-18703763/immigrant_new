//
//  ZBMyCRMTBVC.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyCRMTBVC.h"
#import "FSSegmentTitleView.h"
#import "ZBMyCRMTBCell.h"
#import "ZBFillReturnVisitVC.h"
#import "ZBCommunicationLogTBVC.h"
#import "ZBBrokerageflowModel.h"

@interface ZBMyCRMTBVC ()<FSSegmentTitleViewDelegate>
@property(nonatomic,strong) NSMutableArray *segmentTitlesArr;
@property (nonatomic,strong) FSSegmentTitleView *titleView;
@property (nonatomic,assign)NSInteger currentTag;
@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *newsArray;
@property (nonatomic,strong)NSString *diffreentLoginId;
@end

@implementation ZBMyCRMTBVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的CRM";
    
    self.titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0,SystemScreenWidth, 50) titles:self.segmentTitlesArr delegate:self indicatorType:FSIndicatorTypeNone];
    self.titleView.titleSelectFont = [UIFont systemFontOfSize:19];
    self.titleView.backgroundColor = [UIColor whiteColor];
    self.titleView.titleFont = [UIFont systemFontOfSize:16];
    self.titleView.titleNormalColor = [UIColor getColor:@"A3A3A3"];
    self.titleView.titleSelectColor = [UIColor getColor:@"267BFF"];
    self.titleView.selectIndex = 0;
    self.currentTag = 0;
//    [self.view addSubview:self.titleView];
    [self.tableView setTableHeaderView:self.titleView];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBMyCRMTBCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //刷新
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer = footer;
    //切换用户角色
    [QSNetworkManager H2sSalesmaLoginByUserSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.diffreentLoginId = responseModel.data[@"bussData"];
            [self loadData:YES withUrl:@"/h5s/customer/findIntroducedPage"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];

        }
    } failBlock:^(NSError * _Nullable error) {
          [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

-(void)loadHeadData{
    if (_currentTag == 0) {
        [self loadData:YES withUrl:@"/h5s/customer/findIntroducedPage"];
    }else if (_currentTag == 1) {
        [self loadData:YES withUrl:@"/h5s/customer/findInterestedPage"];
    }else{
        [self loadData:YES withUrl:@"/h5s/customer/findSignedPage"];
    }

}

-(void)loadMoreData{
    self.pageIndex++;
    if (_currentTag == 0) {
        [self loadData:NO withUrl:@"/h5s/customer/findIntroducedPage"];
    }else if (_currentTag == 1) {
        [self loadData:NO withUrl:@"/h5s/customer/findInterestedPage"];
    }else{
        [self loadData:NO withUrl:@"/h5s/customer/findSignedPage"];
    }
}

-(void)loadData:(BOOL)isRefresh withUrl:(NSString *)urlStr{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager H5CustomerFindInterestedPageWithUrl:urlStr WithmobileLike:@"" withNameLike:@"" WithSessionId:self.diffreentLoginId WithPageIndex:self.pageIndex withPageSize:10 successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                //                weakSELF.newsArray = [ZBMycrmModel mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.tableView reloadData];
                if (weakSELF.newsArray.count>=count) {
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    //                    [weakSELF.newsArray addObjectsFromArray:[ZBMycrmModel mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}
-(NSMutableArray*)segmentTitlesArr{
    
    if (!_segmentTitlesArr) {
        _segmentTitlesArr = [[NSMutableArray alloc]initWithArray:@[@"新派客服",@"意向客户",@"已签约客户"]];
    }
    
    return _segmentTitlesArr;
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 13;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMyCRMTBCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.linkUpbtn addTarget:self action:@selector(linkUpbtnClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75.0;
}
#pragma mark -- FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.currentTag = endIndex;
}
//填写回访、、沟通日志
- (void)linkUpbtnClick:(UIButton *)btn{
    if ([btn.currentTitle isEqualToString:@"填写回访"]) {
        ZBFillReturnVisitVC *vc = [ZBFillReturnVisitVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        ZBCommunicationLogTBVC *vc = [ZBCommunicationLogTBVC new];
         [self.navigationController pushViewController:vc animated:YES];
    }
}
//数据
//POST /h5s/customer/findInterestedPage 获取意向客户分页
//POST /h5s/customer/findIntroducedPage 获取新派客户分页
//POST /h5s/customer/findSignedPage 获取签约客户分页
//*/
//+(void)H5CustomerFindInterestedPageWithUrl:
@end
