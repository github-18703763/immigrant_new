//
//  HomeListItemCell.m
//  Browser
//
//  Created by jlc on 2017/12/27.
//  Copyright © 2017年 xuexi. All rights reserved.
//

#import "ZBMyListItemCell.h"

@interface ZBMyListItemCell()

@property (nonatomic,strong)UILongPressGestureRecognizer *tap;

@end

@implementation ZBMyListItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //            make.width.height.mas_equalTo(30);
        //            make.centerX.equalTo(self.mas_centerX);
        //            make.centerY.equalTo(self.mas_centerY);
        
        make.left.top.mas_equalTo(5);
        make.right.bottom.mas_equalTo(-5);
        
    }];
    ViewShadowRadius(self.backView, 8, 0, 0, UIColorFromRGB(0xa9a9a9), 0.2, 20);
    
    [self.dotView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.mas_equalTo(10);
        
        make.right.mas_equalTo(-19);
        make.bottom.mas_equalTo(-19);
    }];
//    self.dotView.layer.cornerRadius =5;
//    self.dotView.clipsToBounds = YES;
//
//    self.dotView.backgroundColor = UIColorFromRGB(0x437DFF);
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.mas_equalTo(37);
        make.top.mas_equalTo(19);
        make.left.mas_equalTo(24);

    }];
    
    //
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.iconImageView.mas_bottom).offset(13);
        make.height.mas_equalTo(16);
        make.left.mas_equalTo(24);
        make.right.mas_equalTo(-5);
        
    }];
}

-(void)setModel:(ZBMyItemModel *)model{
    
    _model = model;
    
    self.nameLabel.text = model.title;
    
    self.iconImageView.image = [UIImage imageNamed:model.imageName];
    
}
@end
