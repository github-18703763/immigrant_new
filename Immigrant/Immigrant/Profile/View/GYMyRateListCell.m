//
//  GYMyRateListCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/22.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyRateListCell.h"

@implementation GYMyRateListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
 
    self.titleBtn.userInteractionEnabled = NO;
    self.titleBtn.layer.cornerRadius = 25;
    self.titleBtn.layer.borderWidth = 3;
    self.titleBtn.layer.borderColor = [UIColor getColor:@"2585F8"].CGColor;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
