//
//  ZBMyHeadCell.h
//  Immigrant
//
//  Created by jlc on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZBMyHeadCellDelegate<NSObject>

-(void)didSelectHeadBtn:(NSInteger)index;

@end

@interface ZBMyHeadCell : UICollectionViewCell

@property(nonatomic,weak)id<ZBMyHeadCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIImageView *backImgView;

@property (weak, nonatomic) IBOutlet UILabel *nickLabel;//昵称
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView;//等级图片
@property (weak, nonatomic) IBOutlet UILabel *memberLabel;//会员标签
@property (weak, nonatomic) IBOutlet UIImageView *userHeadImgView;//用户头像

@property (weak, nonatomic) IBOutlet UIProgressView *levelProgressView;

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

//优惠券
@property (weak, nonatomic) IBOutlet UIImageView *couponImgView;

@property (nonatomic,strong) UIImageView *quanImageV;
@property (nonatomic,strong) UILabel *discountLB;
@property (nonatomic,strong) UILabel *quanTimeLB;
@property (nonatomic,strong) UIButton *useCouponBtn;


@end
