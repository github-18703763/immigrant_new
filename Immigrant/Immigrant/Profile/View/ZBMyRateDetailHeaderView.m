//
//  ZBMyRateDetailHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyRateDetailHeaderView.h"

@implementation ZBMyRateDetailHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];

    self.titleLab.layer.cornerRadius = 25.0;
    self.titleLab.layer.masksToBounds = YES;
    
    self.viewbg.layer.cornerRadius = 64.0;
    self.viewbg.layer.masksToBounds = YES;
    
}

@end
