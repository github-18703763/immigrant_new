//
//  ZBMyGroupBuyReturnTVCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyGroupBuyReturnTVCell.h"
@interface ZBMyGroupBuyReturnTVCell()
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *statusTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *statusTimeLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneConstraint;//23---31
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
@implementation ZBMyGroupBuyReturnTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if (self.statusTimeLab.hidden == true) {
        self.oneConstraint.constant = 31;
    }else{
        self.oneConstraint.constant = 23;
    }
    self.photoImg.layer.cornerRadius = 25.0;
    self.photoImg.layer.masksToBounds = YES;
    self.photoImg.backgroundColor = [UIColor redColor];
    
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:6/255.0 green:6/255.0 blue:6/255.0 alpha:0.06].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,10);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 22;
}
-(void)setModel:(ZBMyGroupBuyReturnModel *)model{
    _model = model;
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.avatar] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _model.memo;
    self.timeLab.text = _model.gmtCreated;
    if (model.iscash == false) {
        self.statusTimeLab.hidden = true;
        self.statusTitleLab.text = _model.iscashtext;
    }else{
        self.statusTimeLab.hidden = false;
        self.statusTitleLab.text = _model.amountyuan;
        NSArray *p=[_model.gmtModified componentsSeparatedByString:@" "];
        self.statusTimeLab.text = [NSString stringWithFormat:@"返现时间:%@",p[0]];
    }
    if (self.statusTimeLab.hidden == true) {
        self.oneConstraint.constant = 31;
    }else{
        self.oneConstraint.constant = 23;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
