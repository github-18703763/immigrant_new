//
//  ZBMyGroupBuyReturnTVCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyGroupBuyReturnModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZBMyGroupBuyReturnTVCell : UITableViewCell

@property (strong,nonatomic)ZBMyGroupBuyReturnModel *model;

@end

NS_ASSUME_NONNULL_END
