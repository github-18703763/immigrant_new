//
//  ZBMyNetworkingTabHeadView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyNetworkingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBMyNetworkingTabHeadView : UIView
@property (nonatomic, strong) UIImageView *searchImg; //搜索图片
@property (nonatomic, strong) UILabel *searchLab; //搜索提示语

@property (nonatomic, strong) UILabel *renmaiLab; //我的人脉
@property (nonatomic, strong) UILabel *friendLab; //好友

@property (nonatomic, strong) ZBMyNetworkingModel *model;
@end

NS_ASSUME_NONNULL_END
