//
//  ZBMyNetworkingTabHeadView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyNetworkingTabHeadView.h"
@interface ZBMyNetworkingTabHeadView() <UITextFieldDelegate>

@end
@implementation ZBMyNetworkingTabHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.backgroundColor = UIColor.whiteColor;
        [self addContentView];
    }
    return self;
}

- (void)addContentView {
    
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(30, 20, SystemScreenWidth - 60, 50)];
    
    searchView.backgroundColor = [UIColor getColor:@"EAEAEE"];
    [self addSubview:searchView];
    
    //搜索文字
    self.searchLab = [[UILabel alloc] init];
    self.searchLab.text = @"搜索姓名或电话";
    self.searchLab.font = [UIFont systemFontOfSize:15];
    self.searchLab.textColor = [UIColor getColor:@"a6a6a8"];
    [searchView addSubview:self.searchLab];
    
    [self.searchLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(searchView);
    }];
    
    //搜索图片
    self.searchImg = [[UIImageView alloc]  initWithImage:[UIImage imageNamed:@""]];
    self.searchImg.image = [UIImage imageNamed:@"sousuo"];
    [searchView addSubview:self.searchImg];
    
    [self.searchImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.searchLab.mas_left);
        make.width.height.equalTo(@30);
        make.centerY.equalTo(self.searchLab);
    }];
    
    //搜索框
    UITextField *field = [[UITextField alloc] initWithFrame:searchView.bounds];
    field.delegate = self;
    field.layer.masksToBounds = YES;
    field.layer.cornerRadius = 5.f;
    [searchView addSubview:field];
    
    //下面的数字
    
    //人脉
    self.renmaiLab = [[UILabel alloc] init];
    self.renmaiLab.text = @"我的人脉：0位好友";
    self.renmaiLab.font = [UIFont systemFontOfSize:20];
    self.renmaiLab.textColor =  [UIColor getColor:@"414141"];;
    [self addSubview:self.renmaiLab];
    WS(ws);
    [self.renmaiLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(ws);
        make.top.equalTo(searchView.mas_bottom).offset(20);
    }];
    
    //图片
    UIImageView *friendImg = [[UIImageView alloc]  initWithImage:[UIImage imageNamed:@""]];
    friendImg.image = [UIImage imageNamed:@"renmai_haoyou"];
    friendImg.contentMode = UIViewContentModeScaleAspectFit;
    [searchView addSubview:friendImg];
    
    [friendImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.renmaiLab.mas_left).offset(-2);
        make.width.height.equalTo(@20);
        make.centerY.equalTo(self.renmaiLab);
    }];
    
    //好友
    self.friendLab = [[UILabel alloc] init];
    self.friendLab.text = @"0位直接好友，0位间接好友";
    self.friendLab.font = [UIFont systemFontOfSize:15];
    self.friendLab.textColor =  [UIColor getColor:@"919394"];
    [self addSubview:self.friendLab];
    [self.friendLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(ws);
        make.top.equalTo(self.renmaiLab.mas_bottom).offset(10);
    }];
}

#pragma mark 代理
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.searchImg.hidden = YES;
    self.searchLab.hidden = YES;
}
-(void)setModel:(ZBMyNetworkingModel *)model{
    _model = model;
    self.renmaiLab.text = [NSString stringWithFormat:@"我的人脉：%@位好友", model.totalfriendnum];
    self.friendLab.text = [NSString stringWithFormat:@"%@位直接好友，%@位间接好友", model.firstfriendnum,model.secondfriendnum];
}
@end
