//
//  ZBMyMailListTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyMailListTBCell.h"

@implementation ZBMyMailListTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.showRightBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.chatImgBtn
    .imageView.contentMode = UIViewContentModeScaleAspectFit;
//
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setListModel:(ZBMailListModel *)listModel{
    [self.dataAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *objstr = [obj stringByReplacingOccurrencesOfString:@" " withString:@""];
        //        NSString *phoneNum = [listModel.phoneNum stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *cleaned = [[listModel.phoneNum componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
        
        if ([objstr isEqualToString:cleaned]) {
            
            [self.showRightBtn setBackgroundColor:UIColor.redColor];// = UIColor.redColor;
        }
        else {
            //            self.showRightBtn.backgroundColor = UIColorFromHex(0x9986FB);
        }
    }];
    self.nameLab.text = listModel.name;
    self.iphoneLab.text = listModel.phoneNum;
}
@end
