//
//  ZBMyNetworkingHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyNetworkingGroupModel.h"
//人脉关系
@interface ZBMyNetworkingHeaderView : UIView
@property (nonatomic, strong) UIImageView *rightImg;
@property (nonatomic, strong) UIImageView *touImg;
@property (nonatomic, strong) UILabel *nameLab;
@property (nonatomic, strong) UILabel *jifenLab;
@property (nonatomic, strong) UILabel *friendLab;
@property (nonatomic,strong)ZBMyNetworkingGroupModel *model;
@end

