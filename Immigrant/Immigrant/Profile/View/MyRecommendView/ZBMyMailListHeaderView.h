//
//  ZBMyMailListHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ZBMyMailListHeaderViewDelegate <NSObject>

- (void)searchDelegate:(NSString *)str;

@end
@interface ZBMyMailListHeaderView : UIView

@property (nonatomic, strong) UIImageView *searchImg; //搜索图片
@property (nonatomic, strong) UILabel *searchLab; //搜索提示语

@property (nonatomic, strong) UILabel *totalLab; // 总共
@property (nonatomic, strong) UILabel *xiazLab; //下载
@property (nonatomic, strong) UILabel *wXiazLab; //未下载

@property (nonatomic, assign) id<ZBMyMailListHeaderViewDelegate> delegate;


@end

