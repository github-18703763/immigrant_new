//
//  ZBMyNetworkingTBCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZBMyNetworkingGroupFriendModel.h"
//人脉关系Cell
@interface ZBMyNetworkingTBCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *touImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIImageView *starImg;
@property (weak, nonatomic) IBOutlet UILabel *jifenLab;

@property (nonatomic,strong)ZBMyNetworkingGroupFriendModel *model;
@end
