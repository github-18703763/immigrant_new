//
//  ZBMyMailListTBCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMailListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZBMyMailListTBCell : UITableViewCell
/** 显示前面打勾 */
@property (weak, nonatomic) IBOutlet UIButton *showRightBtn;
/** 名称 */
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
/** 手机号码 */
@property (weak, nonatomic) IBOutlet UILabel *iphoneLab;
/** 打开聊天框 */
@property (weak, nonatomic) IBOutlet UIButton *chatImgBtn;

@property (nonatomic, strong) ZBMailListModel *listModel;

@property (nonatomic, strong) NSMutableArray *dataAry;

@end

NS_ASSUME_NONNULL_END
