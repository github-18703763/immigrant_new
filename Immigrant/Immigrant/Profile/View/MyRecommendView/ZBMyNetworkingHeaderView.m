//
//  ZBMyNetworkingHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyNetworkingHeaderView.h"
@implementation ZBMyNetworkingHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addContentView];
    }
    return self;
}

- (void)addContentView {
    
    self.backgroundColor =[UIColor getColor:@"f6f7fb"];
    
    //背景
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, SystemScreenWidth - 30, 75)];
    bgView.backgroundColor = UIColor.whiteColor;
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5.0f;
    [self addSubview:bgView];
    
    //右边视图
    UIImageView *rightImg = [[UIImageView alloc] initWithFrame:CGRectMake(bgView.frame.size.width - 25, 12.5, 10, 50)];
    rightImg.contentMode = UIViewContentModeScaleAspectFit;
    self.rightImg = rightImg;
    [bgView addSubview:rightImg];
    
    //头像
    UIImageView *touImg = [[UIImageView alloc] initWithFrame:CGRectMake(7.5, (75-55) / 2, 55, 55)];
    touImg.backgroundColor = UIColor.blueColor;
    touImg.layer.masksToBounds = YES;
    touImg.layer.cornerRadius = 55 / 2.f;
    self.touImg = touImg;
    [bgView addSubview:touImg];
    
    //昵称
    UILabel *nameLab = [[UILabel alloc] init];
    nameLab.text = @"逍客";
    nameLab.textColor = [UIColor getColor:@"363636"];
    nameLab.font = [UIFont systemFontOfSize:20];
    self.nameLab = nameLab;
    [bgView addSubview:nameLab];
    
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(touImg.mas_right).offset(20);
        make.top.equalTo(touImg.mas_top);
    }];
    
    //好友
    UIImageView *friendImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    friendImg.image = [UIImage imageNamed:@"renmai_haoyou"];
     friendImg.contentMode = UIViewContentModeScaleAspectFit;
    [bgView addSubview:friendImg];
    
    [friendImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(nameLab);
        make.bottom.equalTo(touImg);
        make.width.height.equalTo(@20);
    }];
    
    UILabel *friendLab = [[UILabel alloc] init];
    friendLab.text = @"2位直接好友";
    friendLab.textColor =  [UIColor getColor:@"646464"];
    friendLab.font = [UIFont systemFontOfSize:15];
    self.friendLab = friendLab;
    [bgView addSubview:friendLab];
    
    [friendLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(friendImg.mas_right).offset(3);
        make.bottom.equalTo(friendImg.mas_bottom);
    }];
    
    //优惠券
    UIImageView *jifenImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    jifenImg.image = [UIImage imageNamed:@"renmai_youhui"];
    jifenImg.contentMode = UIViewContentModeScaleAspectFit;
    [bgView addSubview:jifenImg];
    
    [jifenImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(rightImg.mas_left).offset(-30);
        make.top.equalTo(nameLab.mas_top);
        make.height.equalTo(@30);
        make.width.equalTo(@50);
    }];
    
    UILabel *jiFenLab = [[UILabel alloc] init];
    jiFenLab.text = @"贡献200积分";
    jiFenLab.textColor = [UIColor getColor:@"646464"];
    jiFenLab.font = [UIFont systemFontOfSize:15];
    self.jifenLab = jiFenLab;
    [bgView addSubview:jiFenLab];
    
    [jiFenLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(rightImg.mas_left).offset(0);
        make.bottom.equalTo(friendImg.mas_bottom);
    }];
    
    UIImageView *starImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    starImg.image = [UIImage imageNamed:@"wodejifen_jifen"];
    starImg.contentMode = UIViewContentModeScaleAspectFit;
    [bgView addSubview:starImg];
    
    [starImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(jiFenLab.mas_left).offset(-10);
        make.bottom.equalTo(touImg);
        make.width.height.equalTo(@20);
    }];
    
}
-(void)setModel:(ZBMyNetworkingGroupModel *)model{
    _model = model;
    self.nameLab.text = model.userName;
    self.friendLab.text = [NSString stringWithFormat:@"%@直接好友",model.firstfriendnum];
    self.jifenLab.text = [NSString stringWithFormat:@"贡献%@积分",model.devotepoint];
}
@end
