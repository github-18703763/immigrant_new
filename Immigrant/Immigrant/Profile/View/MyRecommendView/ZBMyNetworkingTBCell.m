//
//  ZBMyNetworkingTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyNetworkingTBCell.h"

@implementation ZBMyNetworkingTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.touImg.layer.masksToBounds = YES;
    self.touImg.layer.cornerRadius = 20.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(ZBMyNetworkingGroupFriendModel *)model{
    _model = model;
    self.nameLab.text = model.userName;
}
@end
