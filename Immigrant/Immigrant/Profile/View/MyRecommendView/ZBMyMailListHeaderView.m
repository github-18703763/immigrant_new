//
//  ZBMyMailListHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyMailListHeaderView.h"
@interface ZBMyMailListHeaderView() <UITextFieldDelegate>

@end

@implementation ZBMyMailListHeaderView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.backgroundColor = UIColor.whiteColor;
        [self addContentView];
    }
    return self;
}

- (void)addContentView {
    
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(30, 20, SystemScreenWidth - 60, 50)];
    searchView.backgroundColor =  [UIColor getColor:@"EAEAEE"];
    [self addSubview:searchView];
    
    //搜索文字
    self.searchLab = [[UILabel alloc] init];
    self.searchLab.text = @"搜索姓名或电话";
    self.searchLab.font = [UIFont systemFontOfSize:15];
    self.searchLab.textColor = [UIColor getColor:@"a6a6a8"];
    [searchView addSubview:self.searchLab];
    
    [self.searchLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(searchView);
    }];
    
    //搜索图片
    self.searchImg = [[UIImageView alloc]  initWithImage:[UIImage imageNamed:@""]];
    self.searchImg.image = [UIImage imageNamed:@"sousuo"];
    self.searchImg.contentMode = UIViewContentModeScaleAspectFit;
    [searchView addSubview:self.searchImg];
    
    [self.searchImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.searchLab.mas_left);
        make.width.height.equalTo(@30);
        make.centerY.equalTo(self.searchLab);
    }];
    
    //搜索框
    UITextField *field = [[UITextField alloc] initWithFrame:searchView.bounds];
    field.delegate = self;
    field.layer.masksToBounds = YES;
    field.layer.cornerRadius = 5.f;
    [searchView addSubview:field];
    
    //下面的数字
    //已下载
    self.xiazLab = [[UILabel alloc] init];
    self.xiazLab.text = @"已下载：20人";
    self.xiazLab.font = [UIFont systemFontOfSize:15];
    self.xiazLab.textColor = [UIColor getColor:@"6A6A6A"];
    [self addSubview:self.xiazLab];
    WS(ws);
    [self.xiazLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(ws);
        make.top.equalTo(searchView.mas_bottom).offset(20);
    }];
    
    //未下载
    self.wXiazLab = [[UILabel alloc] init];
    self.wXiazLab.text = @"未下载:16人";
    self.wXiazLab.font = [UIFont systemFontOfSize:15];
    self.wXiazLab.textColor = [UIColor getColor:@"6A6A6A"];
    [self addSubview:self.wXiazLab];
    
    [self.wXiazLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.xiazLab.mas_right).offset(20);
        make.top.equalTo(self.xiazLab.mas_top);
    }];
    
    //总共
    self.totalLab = [[UILabel alloc] init];
    self.totalLab.text = @"总共:36人";
    self.totalLab.font = [UIFont systemFontOfSize:15];
    self.totalLab.textColor =[UIColor getColor:@"6A6A6A"];
    [self addSubview:self.totalLab];
    
    [self.totalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.xiazLab.mas_left).offset(-20);
        make.top.equalTo(self.xiazLab.mas_top);
    }];
    
}

#pragma mark 代理
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.searchImg.hidden = YES;
    self.searchLab.hidden = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField.text == nil || [textField.text isEqualToString:@""]) {
        
        self.searchLab.hidden = NO;
        self.searchImg.hidden = NO;
    }
    else {
        
        
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchDelegate:)]) {
        
        [self.delegate searchDelegate:textField.text];
    }
}

@end
