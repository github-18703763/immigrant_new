//
//  ZBMyHeadView.m
//  Immigrant
//
//  Created by jlc on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyScoreHeadView.h"
#import "HTVerticalButton.h"

@interface ZBMyScoreHeadView()

@property (nonatomic,strong) NSMutableArray *namesArr;

@property (nonatomic,strong) NSMutableArray *imageNamesArr;

@property (nonatomic,strong) NSMutableArray *itemsArr;

@end

@implementation ZBMyScoreHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
}

#pragma mark -lazy load

-(NSMutableArray*)namesArr{
    
    if (!_namesArr) {
        
        _namesArr = [NSMutableArray arrayWithObjects:@"img_book_item",@"img_home_book_add",nil];
    }
    return _namesArr;
}
-(NSMutableArray*)itemsArr{
    
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray arrayWithObjects:@"img_book_item",@"img_home_book_add",nil];
    }
    return _itemsArr;
}

@end
