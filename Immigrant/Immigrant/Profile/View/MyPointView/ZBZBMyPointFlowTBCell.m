//
//  ZBZBMyPointFlowTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBZBMyPointFlowTBCell.h"
@interface ZBZBMyPointFlowTBCell()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;//标题
@property (weak, nonatomic) IBOutlet UILabel *timeLab;//时间
@property (weak, nonatomic) IBOutlet UIButton *btnPoint;//u多少积分
@property (weak, nonatomic) IBOutlet UILabel *detailLab;//积分来源

@end

@implementation ZBZBMyPointFlowTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.11].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 26;
    self.bgView.layer.cornerRadius = 7;
    self.bgView.layer.masksToBounds = YES;
}
//积分
- (void)setMyPointModel:(ZBMyPointModel *)myPointModel{
    _myPointModel = myPointModel;
    self.titleLab.text = _myPointModel.userName;
    self.detailLab.text = _myPointModel.memo;
    self.timeLab.text = _myPointModel.gmtCreated;
    [self.btnPoint setTitle:[NSString stringWithFormat:@" %@",_myPointModel.point] forState:UIControlStateNormal];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
