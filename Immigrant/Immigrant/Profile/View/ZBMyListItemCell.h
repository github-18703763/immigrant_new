//
//  HomeListItemCell.h
//  Browser
//
//  Created by jlc on 2017/12/27.
//  Copyright © 2017年 xuexi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZBMyItemModel.h"

@protocol ZBMyListItemCelllDelegate<NSObject>

-(void) selectItemByIndex:(NSInteger)index;

@end

@interface ZBMyListItemCell : UICollectionViewCell

@property (nonatomic,weak) id<ZBMyListItemCelllDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *dotView;

@property (nonatomic,assign)NSInteger index;

@property (nonatomic,strong)ZBMyItemModel *model;

@end
