//
//  GYAddressListCell.h
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMineAddressObject.h"

@interface GYAddressListCell : UITableViewCell

@property (nonatomic,strong) ZBMineAddressObject  *model;

@property (nonatomic,copy) void(^updateBlock)(ZBMineAddressObject *object);

@end
