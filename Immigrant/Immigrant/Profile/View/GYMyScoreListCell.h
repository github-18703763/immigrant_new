//
//  GYMyScoreListCell.h
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyPointModel.h"
@interface GYMyScoreListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UIImageView *scoreImgView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (nonatomic,strong)ZBMyPointModel *modelMyPoint;
@end
