//
//  GYMyScoreListCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMyScoreListCell.h"

@implementation GYMyScoreListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.bgView.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.11].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 26;
    self.bgView.layer.cornerRadius = 7;
    self.bgView.layer.masksToBounds = YES;
}
//积分
-(void)setModelMyPoint:(ZBMyPointModel *)modelMyPoint{
    _modelMyPoint = modelMyPoint;
    self.titleLabel.text = _modelMyPoint.memo;
    self.timeLabel.text = _modelMyPoint.gmtCreated;
    self.numLabel.text = [NSString stringWithFormat:@"+%@",_modelMyPoint.point];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
