//
//  ZBMyRateDetailHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//我的测评结果的头部视图
@interface ZBMyRateDetailHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgVbg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIView *viewbg;
@property (weak, nonatomic) IBOutlet UILabel *matchingDegreeLab;


@end

NS_ASSUME_NONNULL_END
