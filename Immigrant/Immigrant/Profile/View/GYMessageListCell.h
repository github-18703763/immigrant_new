//
//  GYMessageListCell.h
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMessageModel.h"

@interface GYMessageListCell : UITableViewCell

@property (nonatomic,strong)ZBMessageModel *modelMessage;

@end
