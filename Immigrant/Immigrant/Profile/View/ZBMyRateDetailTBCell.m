//
//  ZBMyRateDetailTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/7.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyRateDetailTBCell.h"
@interface ZBMyRateDetailTBCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *btnCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnweekTine;
@property (weak, nonatomic) IBOutlet UIButton *btnIdType;
@property (weak, nonatomic) IBOutlet UIButton *btnLongSet;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;

@end
@implementation ZBMyRateDetailTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModelDetail:(ZBMyRateDetailModel *)modelDetail{
    _modelDetail = modelDetail;
    self.titleLab.text = _modelDetail.name;
    [self.btnCountry setTitle:[NSString stringWithFormat:@" %@",_modelDetail.country] forState:UIControlStateNormal];
     [self.btnweekTine setTitle:[NSString stringWithFormat:@" %@",_modelDetail.servietime] forState:UIControlStateNormal];
      [self.btnIdType setTitle:[NSString stringWithFormat:@" %@",_modelDetail.cardtype] forState:UIControlStateNormal];
     [self.btnLongSet setTitle:[NSString stringWithFormat:@" ￥%@",_modelDetail.servicefeeyuan] forState:UIControlStateNormal];
     [self.btnRequest setTitle:[NSString stringWithFormat:@" %@",_modelDetail.residencerequires] forState:UIControlStateNormal];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
