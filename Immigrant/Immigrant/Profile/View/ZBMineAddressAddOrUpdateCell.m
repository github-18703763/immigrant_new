//
//  ZBMineAddressAddOrUpdateCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMineAddressAddOrUpdateCell.h"
#import "ZBAreaPickerView.h"
#import "ZBProviceObject.h"
#import "ZBCityObject.h"
#import "ZBAreaObject.h"

@interface ZBMineAddressAddOrUpdateCell ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameLab;
@property (weak, nonatomic) IBOutlet UITextField *phoneLab;
@property (weak, nonatomic) IBOutlet UITextField *addressLab;


@property (weak, nonatomic) IBOutlet UILabel *markLab;
@property (weak, nonatomic) IBOutlet UITextField *markLabReplace;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UISwitch *dfSwitch;

@property (nonatomic,strong) NSMutableArray  *addressArr;

@property (nonatomic,copy) NSString  *provice;
@property (nonatomic,copy) NSString  *city;
@property (nonatomic,copy) NSString  *area;

@end

@implementation ZBMineAddressAddOrUpdateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.saveBtn.layer.cornerRadius = 25;
    self.saveBtn.layer.masksToBounds = YES;
    self.dfSwitch.onTintColor = [UIColor getColor:@"267BFF"];
    
    self.nameLab.text = @"";
    self.nameLab.placeholder = @"请输入收货人姓名";
    self.phoneLab.text = @"";
    self.phoneLab.placeholder = @"请输入收货人电话";
    self.addressLab.text = @"";
    self.addressLab.placeholder = @"请选择地址";
    self.addressLab.delegate = self;
    
    self.markLab.text = @"";
    self.markLabReplace.text = @"";
    self.markLabReplace.placeholder = @"请输入详细地址";
    
    [self loadArear];
    
}

-(void)loadArear{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"province" ofType:@"json"];
    NSString *jsonStr = [NSString stringWithContentsOfFile:path usedEncoding:nil error:nil];
    self.addressArr = [NSJSONSerialization JSONObjectWithData:[((NSString *)jsonStr) dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    self.addressArr = [ZBProviceObject mj_objectArrayWithKeyValuesArray:self.addressArr];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.addressLab) {
        ZBAreaPickerView *picker = [[[NSBundle mainBundle] loadNibNamed:@"ZBAreaPickerView" owner:self options:nil] firstObject];
        picker.frame = [UIScreen mainScreen].bounds;
        picker.dataArray = self.addressArr;
        WS(weakSELF);
        picker.filishActionBlock = ^(NSString * _Nonnull proviceStr, NSString * _Nonnull cityStr, NSString * _Nonnull areaStr) {
            weakSELF.addressLab.text = [NSString stringWithFormat:@"%@-%@-%@",proviceStr,cityStr,areaStr];
            weakSELF.provice = proviceStr;
            weakSELF.city = cityStr;
            weakSELF.area = areaStr;
        };
        [[UIApplication sharedApplication].delegate.window addSubview:picker];
        return NO;
    }else{
        return YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)setModel:(ZBMineAddressObject *)model{
    _model = model;
    self.nameLab.text = _model.consignee;
    self.phoneLab.text = _model.mobile;
    self.addressLab.text = [NSString stringWithFormat:@"%@-%@-%@",_model.province,_model.city,_model.district];
    self.markLabReplace.text = _model.str;
    self.markLab.text = _model.str;
    [self.dfSwitch setOn:_model.isdefault];
    self.provice = _model.province;
    self.city = _model.city;
    self.area = _model.district;
    
}

- (IBAction)feildChange:(UITextField *)sender {
    self.markLab.text = sender.text;
}

- (IBAction)defataultAction:(id)sender {
    
}

- (IBAction)saveAction:(id)sender {
    /**
     id
     "address": "沛鸿大厦A2-506",
     "city": "深圳",
     "consignee": "一二三四",
     "district": "南山区",
     "isdefault": false,
     "mobile": "15814520421",
     "province": "广东"
     */
//    self.nameLab.text = @"";
//    self.nameLab.placeholder = @"请输入收货人姓名";
//    self.phoneLab.text = @"";
//    self.phoneLab.placeholder = @"请输入收货人电话";
//    self.addressLab.text = @"";
//    self.addressLab.placeholder = @"请选择地址";
//    self.addressLab.delegate = self;
//
//    self.markLab.text = @"";
//    self.markLabReplace.text = @"";
//    self.markLabReplace.placeholder = @"请输入详细地址";
    if (self.nameLab.text.length == 0 || self.phoneLab.text.length == 0|| self.addressLab.text.length == 0|| self.markLabReplace.text.length == 0){
        [[UIApplication sharedApplication].delegate.window makeToast:@"信息不能为空" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    
    NSString *isdefault = @"false";
    if (self.dfSwitch.on) {
        isdefault = @"true";
    }
    if (self.commitAcitonBlock) {
        NSDictionary *dict;
        if (self.model) {
            dict = @{@"id":self.model.id,@"address":self.markLab.text,@"consignee":self.nameLab.text,@"city":self.city,@"district":self.area,@"isdefault":isdefault,@"mobile":self.phoneLab.text,@"province":self.provice};
        }else{
            dict = @{@"address":self.markLab.text,@"consignee":self.nameLab.text,@"city":self.city,@"district":self.area,@"isdefault":isdefault,@"mobile":self.phoneLab.text,@"province":self.provice};
        }
        self.commitAcitonBlock(dict);
    }
}

@end
