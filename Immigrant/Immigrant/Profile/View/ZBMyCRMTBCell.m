//
//  ZBMyCRMTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyCRMTBCell.h"

@implementation ZBMyCRMTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //获取一个随机整数范围在：[2)包括0，不包括100
    int x = arc4random() % 2;
    if (x == 0) {
         [self.linkUpbtn setTitle:@"填写回访" forState:UIControlStateNormal];
    }else{
         [self.linkUpbtn setTitle:@"沟通日志" forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
