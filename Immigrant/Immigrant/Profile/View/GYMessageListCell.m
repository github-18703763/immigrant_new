//
//  GYMessageListCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYMessageListCell.h"
@interface GYMessageListCell()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@end

@implementation GYMessageListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:0.12].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 22;
    self.bgView.layer.cornerRadius = 15;
    self.bgView.layer.masksToBounds = YES;
//    #267BFF--------
    self.imgIcon.layer.cornerRadius = 6;
    self.imgIcon.layer.masksToBounds = YES;
}
//消息中心
- (void)setModelMessage:(ZBMessageModel *)modelMessage{
    _modelMessage = modelMessage;
    if (_modelMessage.isread == true) {
        self.imgIcon.backgroundColor = [UIColor colorWithRed:186/255.0 green:186/255.0 blue:186/255.0 alpha:1.0];
    }else{
       self.imgIcon.backgroundColor = [UIColor getColor:@"#267BFF"];
    }
    self.titleLab.text = _modelMessage.title;
    self.timLab.text = _modelMessage.gmtCreated;
    self.contentLab.text = _modelMessage.content;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
