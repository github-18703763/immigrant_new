//
//  ZBMyHeadCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/18.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyHeadCell.h"
#import "HTVerticalButton.h"

@implementation ZBMyHeadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(290);
        
    }];
    
    [self.nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(22);
        make.top.mas_equalTo(65);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(200);
    }];
    
    [self.userHeadImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.mas_equalTo(64);
        make.right.mas_equalTo(-22);
        make.top.mas_equalTo(64);
        //        make.centerY.equalTo(self.nickLabel.mas_centerY);
    }];
    self.userHeadImgView.layer.cornerRadius = 32;
    self.userHeadImgView.clipsToBounds = YES;
    
    [self.levelImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(25);
        make.height.mas_equalTo(22);
        make.top.equalTo(self.nickLabel.mas_bottom).offset(14);
        make.left.equalTo(self.nickLabel.mas_left);
        
    }];
    
    [self.memberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(100);
        make.centerY.equalTo(self.levelImgView.mas_centerY);
        make.left.equalTo(self.levelImgView.mas_right).offset(10);
    }];
    
    [self.levelProgressView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.nickLabel.mas_left);
        make.right.equalTo(self.userHeadImgView.mas_right);
        make.top.equalTo(self.levelImgView.mas_bottom).offset(30);
        make.height.mas_equalTo(14);
    }];
    ViewRadius(self.levelProgressView, 7);
    for (UIImageView * imageview in self.levelProgressView.subviews) {
        ViewRadius(imageview, 7);
    }
    self.levelProgressView.progress = 0;
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(12);
        make.left.equalTo(self.nickLabel.mas_left);
        make.top.equalTo(self.levelProgressView.mas_bottom).offset(10);
    }];
    
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(12);
        make.right.equalTo(self.userHeadImgView.mas_right);
        make.top.equalTo(self.levelProgressView.mas_bottom).offset(10);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.nickLabel.mas_left);
        make.right.equalTo(self.userHeadImgView.mas_right);
        make.height.mas_equalTo(100);
        make.top.equalTo(self.levelProgressView.mas_bottom).offset(59);
    }];
   
    
    self.bottomView.layer.cornerRadius= 8;
    self.bottomView.clipsToBounds = YES;
    //优惠券
    self.couponImgView.hidden=YES;
    [self.couponImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.nickLabel.mas_left);
        make.right.equalTo(self.userHeadImgView.mas_right);
        make.height.mas_equalTo(150);
        make.top.equalTo(self.levelProgressView.mas_bottom).offset(59);
    }];
    
    self.quanImageV=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wode_quan"]];
    [self.couponImgView addSubview:self.quanImageV];
    [self.quanImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(32);
        make.height.width.mas_equalTo(8);
        make.bottom.mas_equalTo(-24);
    }];
    
    
    self.discountLB=[[UILabel alloc]init];
    self.discountLB.textColor=UIColorFromRGB(0x437DFF);
    self.discountLB.font=[UIFont systemFontOfSize:15];
    self.discountLB.text=@"8折优惠券";
    [self.couponImgView addSubview:self.discountLB];
    [self.discountLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.quanImageV.mas_right).offset(5);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(80);
        make.bottom.mas_equalTo(-20);
    }];
    
    
    self.quanTimeLB=[[UILabel alloc]init];
    self.quanTimeLB.textColor=UIColorFromRGB(0x404040);
    self.quanTimeLB.font=[UIFont systemFontOfSize:15];
    self.quanTimeLB.text=@"29天后过期";
    [self.couponImgView addSubview:self.quanTimeLB];
    [self.quanTimeLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.discountLB.mas_right).offset(5);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(80);
        make.bottom.mas_equalTo(-20);
    }];
    
    self.useCouponBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [self.useCouponBtn setTitle:@"立即使用" forState:UIControlStateNormal];
    self.useCouponBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    self.useCouponBtn.backgroundColor=UIColorFromRGB(0x437DFF);
    [self.couponImgView addSubview:self.useCouponBtn];
    ViewShadowRadius(self.useCouponBtn, 15, 0, 4, UIColorFromRGB(0x437dff), 0.23, 7);
    [self.useCouponBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-27);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(85);
        make.bottom.mas_equalTo(-13);
    }];
    
    NSArray *titles =@[@"我的测评",@"我的收藏",@"我的积分",@"我的订单"];
    NSArray *images =@[@"mine_ceping",@"mine_shoucang",@"mine_jifen",@"mine_dingdan"];
    CGFloat btnW = (SystemScreenWidth-44)/4;
    
    //CGFloat margin = 10;
    
    for (NSInteger i=0; i<titles.count; i++) {
        
        HTVerticalButton *button =[HTVerticalButton buttonWithType:UIButtonTypeSystem];
        
        [button setTitle:titles[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:images[i]] forState:UIControlStateNormal];
        //        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
        button.tag =i;
        
        [button addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.bottomView addSubview:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(0);
            make.width.mas_equalTo(btnW);
            make.left.mas_equalTo(i*btnW);
            make.height.mas_equalTo(100);
        }];
    }
    
    
}

-(void)btnAction:(UIButton*)btn{
    
    if (self.delegate&&[self.delegate respondsToSelector:@selector(didSelectHeadBtn:)]) {
        
        [self.delegate didSelectHeadBtn:btn.tag];
    }
}

@end
