//
//  GYAddressListCell.m
//  Immigrant
//
//  Created by jlc on 2018/12/23.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYAddressListCell.h"

@interface GYAddressListCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *defaultLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *markLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneLeftCons;

@end


@implementation GYAddressListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.defaultLab.layer.cornerRadius = 17/2;
    self.defaultLab.layer.masksToBounds = YES;
}

-(void)setModel:(ZBMineAddressObject *)model{
    _model = model;
    if (_model.isdefault) {
        self.defaultLab.hidden = NO;
        self.phoneLeftCons.constant = 57;
    }else{
        self.defaultLab.hidden = YES;
        self.phoneLeftCons.constant = 15;
    }
    self.nameLab.text = _model.consignee;
    self.markLab.text = _model.str;
    
}

- (IBAction)updateAction:(id)sender {
    if (self.updateBlock) {
        self.updateBlock(self.model);
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
