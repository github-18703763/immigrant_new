//
//  ZBMyAppointmentTBCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBCallBackModel.h"
//我的预约cell
@interface ZBMyAppointmentTBCell : UITableViewCell
//编辑
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

//回访数据
@property (nonatomic,strong)ZBCallBackModel *model;
@end

