//
//  ZBMyAppointmentTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyAppointmentTBCell.h"
@interface ZBMyAppointmentTBCell()
@property (weak, nonatomic) IBOutlet UIView *viewCircle;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *communicateModeLab;
@property (weak, nonatomic) IBOutlet UILabel *firstComunicTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *secondComnuicTimelab;

@end

@implementation ZBMyAppointmentTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.viewCircle.layer.borderColor = [UIColor getColor:@"2585F8"].CGColor;
    self.viewCircle.layer.borderWidth=3.0;
    self.viewCircle.layer.cornerRadius = 7.5;
    self.viewCircle.layer.masksToBounds = YES;
    
    self.viewBg.layer.cornerRadius = 15;
    self.viewBg.layer.masksToBounds = YES;
    self.viewBg.layer.shadowColor = [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:1].CGColor;
    self.viewBg.layer.shadowOffset = CGSizeMake(0,0);
    self.viewBg.layer.shadowOpacity = 1;
    self.viewBg.layer.shadowRadius = 22;
    
}
- (void)setModel:(ZBCallBackModel *)model{
    _model = model;
    if ([_model.outtype isEqualToString:@"migrate"]) {
         self.titleLab.text = _model.outName;
        self.titleLab.hidden = NO;
    }else{
        self.titleLab.hidden = YES;
    }
    self.communicateModeLab.text = _model.callbacktypetext;
    self.firstComunicTimeLab.text = [NSString stringWithFormat:@"%@到%@",_model.firstsdate,_model.firstedate];
    self.secondComnuicTimelab.text = [NSString stringWithFormat:@"%@到%@",_model.secondsdate,_model.secondedate];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
