//
//  ZBMineAddressAddOrUpdateCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMineAddressObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBMineAddressAddOrUpdateCell : UITableViewCell

@property (nonatomic,strong) ZBMineAddressObject  *model;

@property (nonatomic,copy) void(^commitAcitonBlock)(NSDictionary *dict);

@end

NS_ASSUME_NONNULL_END
