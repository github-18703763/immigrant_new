//
//  ZBZBEstimatedAeeivalMoneyTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBZBEstimatedAeeivalMoneyTBCell.h"
@interface ZBZBEstimatedAeeivalMoneyTBCell()
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
@implementation ZBZBEstimatedAeeivalMoneyTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.cornerRadius = 7.0;
    self.bgView.layer.masksToBounds = YES;
    self.contentView.backgroundColor = COLOR(252, 252, 252, 1);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
