//
//  ZBMyCommissionHeaderView.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMyCommissionHeaderView.h"
@interface ZBMyCommissionHeaderView()
@property (weak, nonatomic) IBOutlet UIView *bgView;




@end

@implementation ZBMyCommissionHeaderView

-(void)awakeFromNib{
      [super awakeFromNib];
    self.bgView.layer.cornerRadius = 7.0;
    self.bgView.layer.masksToBounds = YES;
    
    self.applyMoneyBtn.layer.cornerRadius = 17.0;
    self.applyMoneyBtn.layer.masksToBounds = YES;
}

@end
