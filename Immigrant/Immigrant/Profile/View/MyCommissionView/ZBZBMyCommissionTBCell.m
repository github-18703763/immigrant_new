//
//  ZBZBMyCommissionTBCell.m
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBZBMyCommissionTBCell.h"
@interface ZBZBMyCommissionTBCell()
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moneyTopConstraint;//16----如果detail隐藏就是23

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@end
@implementation ZBZBMyCommissionTBCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.cornerRadius = 7.0;
    self.bgView.layer.masksToBounds = YES;
    self.contentView.backgroundColor = COLOR(252, 252, 252, 1);
}
//佣金流水
-(void)setModelFlow:(ZBBrokerageflowModel *)modelFlow{
    _modelFlow = modelFlow;
    self.detailLab.hidden = YES;
    self.moneyTopConstraint.constant = 23;
    self.moneyLab.text = _modelFlow.amountyuan;
    self.detailLab.text = _modelFlow.memo;
    self.timeLab.text = _modelFlow.gmtCreated;
}
-(void)setModelBrokerageSends:(ZBBrokerageSendsModel *)modelBrokerageSends{
    _modelBrokerageSends = modelBrokerageSends;
    self.detailLab.text = _modelBrokerageSends.currentprogress;
    self.moneyLab.text = _modelBrokerageSends.currentprogressamountyuan;
    self.detailLab.text = _modelBrokerageSends.outName;
    self.timeLab.text = _modelBrokerageSends.gmtCreated;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
