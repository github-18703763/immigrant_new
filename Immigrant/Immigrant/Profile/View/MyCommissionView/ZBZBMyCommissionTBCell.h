//
//  ZBZBMyCommissionTBCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBBrokerageflowModel.h"
//我的佣金
NS_ASSUME_NONNULL_BEGIN

@interface ZBZBMyCommissionTBCell : UITableViewCell

//假设佣金流水的显示
@property (nonatomic,strong)ZBBrokerageflowModel *modelFlow;

//我的佣金
@property (nonatomic,strong)ZBBrokerageSendsModel *modelBrokerageSends;
@end

NS_ASSUME_NONNULL_END
