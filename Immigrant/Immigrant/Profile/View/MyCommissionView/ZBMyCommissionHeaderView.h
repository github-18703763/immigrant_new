//
//  ZBMyCommissionHeaderView.h
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/31.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
//我的佣金的头部视图
NS_ASSUME_NONNULL_BEGIN

@interface ZBMyCommissionHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *myCommissionMoneyLab;//我的佣金
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UIButton *grandTotalMoneyBtn;//累计佣金
@property (weak, nonatomic) IBOutlet UIButton *estimatedAeeivalMoneyBtn;//预计到账佣金
@property (weak, nonatomic) IBOutlet UIButton *applyMoneyBtn;//申请提现
@property (weak, nonatomic) IBOutlet UIButton *btnWorn;//只有积分才显示。。。我的佣金不显示
@end

NS_ASSUME_NONNULL_END
