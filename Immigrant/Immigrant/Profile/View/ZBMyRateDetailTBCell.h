//
//  ZBMyRateDetailTBCell.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/7.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMyRateModel.h"
@interface ZBMyRateDetailTBCell : UITableViewCell
@property (nonatomic,strong)ZBMyRateDetailModel *modelDetail;
@end

