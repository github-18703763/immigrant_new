//
//  ZBMyPointModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBMyPointModel : NSObject

@property (nonatomic,copy) NSString  *creator;//
@property (nonatomic,copy) NSString  *gmtCreated;//
@property (nonatomic,copy) NSString  *gmtModified;//
@property (nonatomic,copy) NSString  *id;//
@property (nonatomic,copy) NSString  *isDeleted;//
@property (nonatomic,copy) NSString  *issend;//是否发放 ,
@property (nonatomic,copy) NSString  *issendtext;//是否发放描述 ,
@property (nonatomic,copy) NSString  *memo;//说明 ,
@property (nonatomic,copy) NSString  *modifier;//
@property (nonatomic,copy) NSString  *point;//积分数量 ,
@property (nonatomic,copy) NSString  *sendtime;//发放时间 ,
@property (nonatomic,copy) NSString  *userId;// 用户主键 ,
@property (nonatomic,copy) NSString  *userName;// 用户姓名

@end

NS_ASSUME_NONNULL_END
