//
//  ZBMessageModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//消息中心
NS_ASSUME_NONNULL_BEGIN

@interface ZBMessageModel : NSObject
/*
 "content": "预约成功内容",
 */
@property (nonatomic,strong)NSString *content;
/*
 "creator": "[SYS]",
 */
@property (nonatomic,strong)NSString *creator;
/*
 "gmtCreated": "2018-12-30 15:38:23",
 */
@property (nonatomic,strong)NSString *gmtCreated;
/*
"gmtModified": "2018-12-30 15:38:23",
 */
@property (nonatomic,strong)NSString *gmtModified;
/*
 "id": 2,
 */
@property (nonatomic,strong)NSString *id;
/*
 "isDeleted": "n",
 */
@property (nonatomic,strong)NSString *isDeleted;
/*
"isread": true,
 */
@property (nonatomic,assign)BOOL isread;
/*
 "modifier": "[SYS]",
 */
@property (nonatomic,strong)NSString *modifier;
/*
 "outId": 1,
 */
@property (nonatomic,strong)NSString *outId;
/*
 "outtype": "reserve"--不是优惠券，"coupon"--显示优惠券
 */
@property (nonatomic,strong)NSString *outtype;
/*
 "outtypetext": "预约",
 */
@property (nonatomic,strong)NSString *outtypetext;
/*
 "title": "预约成功",
 */
@property (nonatomic,strong)NSString *title;
/*
 "userId": 2
 */
@property (nonatomic,strong)NSString *userId;
@end

NS_ASSUME_NONNULL_END
