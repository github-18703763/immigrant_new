//
//  ZBMyOrderModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//夏令营
@interface ZBMyOrderCampModel : NSObject
//orderCamp
@property (nonatomic,strong)NSString *campId;//冬夏令营主键 ,
@property (nonatomic,strong)NSString *country;//国家
@property (nonatomic,strong)NSString *countryId;// 国家主键
@property (nonatomic,strong)NSString *cover;//封面
@property (nonatomic,strong)NSString *creator;
@property (nonatomic,strong)NSString *currentnum;//当前人数
@property (nonatomic,strong)NSString *datestr;//字符串格式的日期 ,
@property (nonatomic,strong)NSString *depositfee;//定金（分）
@property (nonatomic,strong)NSString *depositfeeyuan;//定金（元）
@property (nonatomic,strong)NSString *edate;//结束日期

@property (nonatomic,strong)NSString *gmtCreated;
@property (nonatomic,strong)NSString *gmtModified;
@property (nonatomic,strong)NSString *id;
@property (nonatomic,strong)NSString *images;//图集
@property (nonatomic,strong)NSString *imageskey;//图集KEY ）
//@property (nonatomic,strong)NSString *introduce;//活动介绍
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,strong)NSString *isactivity;//是否揽分产品
//@property (nonatomic,strong)NSString *isfull;//是否满人
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name;//名称
//@property (nonatomic,strong)NSString *notice;//报名须知
@property (nonatomic,strong)NSString *orderId;//订单主键
//@property (nonatomic,strong)NSString *plan;// 日程安排
@property (nonatomic,strong)NSString *sdate;//开始日期
@property (nonatomic,strong)NSString *totalnum;//总人数
@property (nonatomic,strong)NSString *totalprice;//总售价（分）
@property (nonatomic,strong)NSString *totalpriceyuan;//总售价（元）
@end


//商品
@interface ZBMyOrderGoodsModel : NSObject
//orderGoods
@property (nonatomic,strong)NSString *cateId;//商品分类主键 ,
//@property (nonatomic,strong)NSString *cateName;//分类名称
//@property (nonatomic,strong)NSString *content;// 详情
@property (nonatomic,strong)NSString *cover;//封面
@property (nonatomic,strong)NSString *creator;
@property (nonatomic,strong)NSString *gmtCreated;
@property (nonatomic,strong)NSString *gmtModified;
@property (nonatomic,strong)NSString *id;
@property (nonatomic,strong)NSString *goodsContent;//(string, optional): 商品内容 ,
@property (nonatomic,strong)NSString *goodsId;//商品主键
@property (nonatomic,strong)NSString *images;//图集
@property (nonatomic,strong)NSString *imageskey;//图集KEY ）
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,strong)NSString *ison;//是否上架
@property (nonatomic,strong)NSString *isontext;//是否上架描述
@property (nonatomic,strong)NSString *logisticCode;//(string, optional): 物流单号 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name;//名称
@property (nonatomic,strong)NSString *orderId;//订单主键
@property (nonatomic,strong)NSString *paypoint;// 支付积分
@property (nonatomic,strong)NSString *point;//所需积分
@property (nonatomic,strong)NSString *price;//所需价格（分）
@property (nonatomic,strong)NSString *priceyuan;//所需价格（元）
@property (nonatomic,strong)NSString *shipperCode;//(string, optional): 快递公司编码 ,
@property (nonatomic,strong)NSString *userAddress;//收货地址
@end

//海外房产
@interface ZBMyOrderHouseModel : NSObject
//orderHouse
@property (nonatomic,strong)NSString *address;//地点 ,
@property (nonatomic,strong)NSString *annuprofit;//年收益（%）
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;//国家主键
@property (nonatomic,strong)NSString *cover;//封面
@property (nonatomic,strong)NSString *gmtCreated;
@property (nonatomic,strong)NSString *gmtModified;
@property (nonatomic,strong)NSString *id;
@property (nonatomic,strong)NSString *creator;
@property (nonatomic,strong)NSString *deliverystandard;//交房标准
@property (nonatomic,strong)NSString *deliverytime;//交房时间
@property (nonatomic,strong)NSString *depositfee;//定金（分）
@property (nonatomic,strong)NSString *depositfeeyuan;//定金（元）
@property (nonatomic,strong)NSString *downpayment;//首付比例（%）
@property (nonatomic,strong)NSString *facility;//周边设施
@property (nonatomic,strong)NSString *feature;//项目特色
@property (nonatomic,strong)NSString *floorage;//使用面积（平方米）
@property (nonatomic,strong)NSString *houseId ;//海外房产主键 ,
@property (nonatomic,strong)NSString *images ; //图集 ,
@property (nonatomic,strong)NSString *imageskey ;// 图集KEY ,
@property (nonatomic,strong)NSString *increaserate; //涨幅（%） ,
@property (nonatomic,strong)NSString *investment ; //投资评估 ,
@property (nonatomic,strong)NSString *isDeleted ;
@property (nonatomic,strong)NSString *isactivity;//是否揽分产品 ,
@property (nonatomic,strong)NSString *management;//物业类型 ,
@property (nonatomic,strong)NSString *modifier;
@property (nonatomic,strong)NSString *name ; //名称 ,
@property (nonatomic,strong)NSString *orderId ; //订单主键 ,
@property (nonatomic,strong)NSString *ownerrights;//业主权益 ,
@property (nonatomic,strong)NSString *ownershipyear;// 产权年限 ,
@property (nonatomic,strong)NSString *process;//购房流程 ,
@property (nonatomic,strong)NSString *servicefee;//办理费（分） ,
@property (nonatomic,strong)NSString *servicefeeyuan;//办理费（元） ,
@property (nonatomic,strong)NSString *totalprice;//总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;//总售价（元） ,
@property (nonatomic,strong)NSString *totalpriceyuanstr;// 总售价（元）（文字描述） ,
@property (nonatomic,strong)NSString *type;//在售户型

@end


//经典路线
@interface ZBMyOrderLineModel : NSObject
//orderLine
//@property (nonatomic,strong)NSString *country;// 国家 ,
//@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover ;// 封面 ,
@property (nonatomic,strong)NSString *creator ;//
@property (nonatomic,strong)NSString *datestr;// 字符串格式的日期 ,
@property (nonatomic,strong)NSString *currentnum ;//当前参与人数 ,
@property (nonatomic,strong)NSString *depositfee ;//定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan ;// 定金（元） ,
@property (nonatomic,strong)NSString *edate ;// 结束日期 ,
//@property (nonatomic,strong)NSString *feeinclude;//费用包含 ,
@property (nonatomic,strong)NSString *gmtCreated ;//
@property (nonatomic,strong)NSString *gmtModified;//
@property (nonatomic,strong)NSString *goal;// 出游目的 ,
@property (nonatomic,strong)NSString *id ;//
@property (nonatomic,strong)NSString *images ;// 图集 ,
@property (nonatomic,strong)NSString *imageskey ;// 图集KEY ,
//@property (nonatomic,strong)NSString *info;//项目信息 ,
//@property (nonatomic,strong)NSString *introduce ;// 行程介绍 ,
@property (nonatomic,strong)NSString *isDeleted ;//
@property (nonatomic,strong)NSString *isactivity;//是否揽分产品 ,
@property (nonatomic,strong)NSString *isfull;//是否满人 ,
@property (nonatomic,strong)NSString *lineId;//冬夏令营主键 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name;//名称 ,
@property (nonatomic,strong)NSString *orderId ;// 订单主键 ,
@property (nonatomic,strong)NSString *sdate ;//开始日期 ,
@property (nonatomic,strong)NSString *totalnum;//总人数 ,
@property (nonatomic,strong)NSString *totalprice;//总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;//总售价（分） ,
@property (nonatomic,strong)NSString *userGoal;//用户出游目的
@end


//签证办理
@interface ZBMyOrderVisaModel : NSObject
//orderVisa
@property (nonatomic,assign)NSInteger booknum;
@property (nonatomic,strong)NSString *country;//国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;//封面 ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *entriesnum;// 入境次数 ,
@property (nonatomic,strong)NSString *expirydate;// 有效期 ,
@property (nonatomic,strong)NSString *gmtCreated;//
@property (nonatomic,strong)NSString *gmtModified;//
@property (nonatomic,strong)NSString *id;//
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *interviewrequires;// 面试要求 ,
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,strong)NSString *isactivity;// 是否揽分产品 ,
@property (nonatomic,strong)NSString *material;//所需材料 ,
@property (nonatomic,strong)NSString *maxstaytime;//最长停留时间 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name ;//名称 ,
@property (nonatomic,strong)NSString *notice ;//办理须知 ,
@property (nonatomic,strong)NSString *orderId ;//订单主键 ,
@property (nonatomic,strong)NSString *process;//办理流程 ,
@property (nonatomic,strong)NSString *remark;// 办理说明 ,
@property (nonatomic,strong)NSString *servicefee;// 办理费（分） ,
@property (nonatomic,strong)NSString *servicefeeyuan;//办理费（元） ,
@property (nonatomic,strong)NSString *servietime ;//办理周期 ,
@property (nonatomic,strong)NSString *userAddress ;//收货地址 ,
@property (nonatomic,strong)NSString *userTraveldate;// 出行时间 ,
@property (nonatomic,strong)NSString *visaId;//签证办理主键 ,
@property (nonatomic,strong)NSString *visatype;// 签证办理类型
@end


//回访------------------------
@interface ZBMyOrderCallbackModel : NSObject
//orderCallback
@property (nonatomic,strong)NSString *callbackId;//回访主键 ,
@property (nonatomic,strong)NSString *callbackstatus;//回访状态 = ['wait', 'finish'],
@property (nonatomic,strong)NSString *callbackstatustex;//回访状态描述 ,
@property (nonatomic,strong)NSString *callbacktype;// 回访方式 = ['visit', 'online', 'company'],
@property (nonatomic,strong)NSString *callbacktypetext;//回访方式描述 ,
@property (nonatomic,strong)NSString *companyaddress;// 面谈公司的公司地址（callbackype = company） ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *firstedate;//首选沟通结束时间 ,
@property (nonatomic,strong)NSString *firstsdate;//首选沟通开始时间 ,
@property (nonatomic,strong)NSString *gmtCreated;//
@property (nonatomic,strong)NSString *gmtModified;//
@property (nonatomic,strong)NSString *id;//
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,strong)NSString *islocationcity;// 是否同城 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *orderId;//订单主键 ,
@property (nonatomic,strong)NSString *outId;//项目主键 ,
@property (nonatomic,strong)NSString *outName;// 项目名称 ,
@property (nonatomic,strong)NSString *outtype;// 项目类型 = ['other', 'migrate'],
@property (nonatomic,strong)NSString *outtypetext;// 项目类型描述 ,
@property (nonatomic,strong)NSString *secondedate;//次选沟通结束时间 ,
@property (nonatomic,strong)NSString *secondsdate;//次选沟通开始时间 ,
@property (nonatomic,strong)NSString *userId;// 用户主键 ,
@property (nonatomic,strong)NSString *userMobile;//用户手机 ,
@property (nonatomic,strong)NSString *userName;//用户姓名 ,
@property (nonatomic,strong)NSString *visitcity;//上面拜访所在的城市（callbackype = visit）
@property (nonatomic,strong)NSString *priceyuan;//价格
@end

//移民项目
@interface ZBMyOrderMigrateModel : NSObject
//orderMigrate
@property (nonatomic,strong)NSString *cardtype;//身份证类型 ,
@property (nonatomic,strong)NSString *country;//国家 ,
@property (nonatomic,strong)NSString *countryId;//国家主键 ,
@property (nonatomic,strong)NSString *cover;//封面 ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *depositfee;//定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan;//定金（元） ,
@property (nonatomic,strong)NSString *gmtCreated;//
@property (nonatomic,strong)NSString *gmtModified;//
@property (nonatomic,strong)NSString *id;//
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;//图集KEY ,
@property (nonatomic,strong)NSString *investment;//投资金额（分） ,
@property (nonatomic,strong)NSString *investmentyuan;//投资金额（元） ,
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,strong)NSString *isactivity;//是否揽分产品 ,
@property (nonatomic,strong)NSString *migrateId;//移民项目主键 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name ;// 名称 ,
@property (nonatomic,strong)NSString *orderId;//订单主键 ,
@property (nonatomic,strong)NSString *questionIds;//条件测试主键集合 ,
@property (nonatomic,strong)NSString *require0;//移民方案——国家地区（从地址库获取ID） ,
@property (nonatomic,strong)NSString *require1 ;//移民方案——最高学历（0-高中以下 1-高中或中专 2-大专 3-本科或硕士 4-博士） ,
@property (nonatomic,strong)NSString *require2 ;//移民方案——移民目的（0-子女教育 1-海外生育 2-养老储备 3-出行便利 4-海外置业 5-投资理财 6-旅游度假 7-税务筹划） ,
@property (nonatomic,strong)NSString *require3 ;//移民方案——资金要求（0-100万以内 1-100万-300万 2-300万-500万 3-500万-1000万 4-1000万-3000万 5-3000万以上） ,
@property (nonatomic,strong)NSString *require4;//移民方案——居住要求（0-不方便居住 1-每年入境一次 2-每年住7天 3-每年住30天 4-每年住半年 5-累计住满2年 ） ,
@property (nonatomic,strong)NSString *require5 ;//移民方案——外语要求（0-完全不会 1-大学英语4级以下 2-大学英语6级 3-大学英语6级优秀 4-专业英语8级） ,
@property (nonatomic,strong)NSString *residencerequires ;//居住要求 ,
@property (nonatomic,strong)NSString *servicefee;//服务费（分） ,
@property (nonatomic,strong)NSString *servicefeeyuan;// 服务费（元） ,
@property (nonatomic,strong)NSString *servietime ;//办理周期

@end

//orderStudy海外留学
@interface ZBMyOderStudyModel : NSObject
//orderStudy
@property (nonatomic,strong)NSString *cover;//封面 ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *depositfee;//定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan;//定金（元） ,
@property (nonatomic,strong)NSString *gmtCreated;//
@property (nonatomic,strong)NSString *gmtModified;//
@property (nonatomic,strong)NSString *id;//
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;//图集KEY ,
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,strong)NSString *isactivity;//是否揽分产品 ,
@property (nonatomic,strong)NSString *modifier ;//
@property (nonatomic,strong)NSString *name;//名称 ,
@property (nonatomic,strong)NSString *orderId;//订单主键 ,
@property (nonatomic,strong)NSString *studyId;//海外留学主键 ,
@property (nonatomic,strong)NSString *totalprice ;// 总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;//总售价（元）

@end
//冬夏令营后补
@interface ZBMyOrderCampStandbyModel : NSObject
//orderCampStandby
@property (nonatomic, assign) BOOL              isactivity;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, assign) NSInteger              totalprice;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *standbystatustext;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, assign) CGFloat              depositfeeyuan;
@property (nonatomic, copy) NSString *imageskey;
@property (nonatomic, assign) CGFloat              totalpriceyuan;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger              countryId;
@property (nonatomic, assign) NSInteger              campId;
@property (nonatomic, assign) NSInteger              id;
@property (nonatomic, assign) NSInteger              depositfee;
@property (nonatomic, assign) NSInteger              totalnum;
@property (nonatomic, copy) NSString *datestr;
@property (nonatomic, copy) NSString *edate;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, copy) NSString *sdate;
@property (nonatomic, assign) NSInteger              orderId;
@property (nonatomic, copy) NSString *standbystatus;
@end
//经典路线后补orderLineStandby
@interface ZBMyOrderLineStandbyModel : NSObject
//orderLineStandby
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *datestr;// 字符串格式的日期 ,
@property (nonatomic,strong)NSString *depositfee;//定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan;//定金（元） ,
@property (nonatomic,strong)NSString *edate;//结束日期 ,
@property (nonatomic,strong)NSString *gmtCreated;//
@property (nonatomic,strong)NSString *gmtModified;//
@property (nonatomic,strong)NSString *goal;//出游目的 ,
@property (nonatomic,strong)NSString *id ;//
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;//图集KEY ,
@property (nonatomic,strong)NSString *isDeleted;//
@property (nonatomic,assign)BOOL isactivity;//是否揽分产品 ,
@property (nonatomic,strong)NSString *lineId;//冬夏令营主键 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name;//名称 ,
@property (nonatomic,strong)NSString *orderId;// 订单主键 ,
@property (nonatomic,strong)NSString *sdate;//开始日期 ,
@property (nonatomic,strong)NSString *standbystatus;//候补状态 = ['wait', 'failure', 'success'],
@property (nonatomic,strong)NSString *standbystatustext;//候补状态描述 ,
@property (nonatomic,strong)NSString *totalnum;//总人数 ,
@property (nonatomic,strong)NSString *totalprice;// 总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;//总售价（分） ,
@property (nonatomic,strong)NSString *userGoal;//用户出游目的

@end


//我的订单的数据模型
@interface ZBMyOrderModel : NSObject
//OrderDTO
@property (nonatomic,assign)NSInteger booknum;//booknum (integer, optional): 预定数量 ,
@property (nonatomic,strong)NSString *creator;
@property (nonatomic,strong)NSString *gmtCreated;
@property (nonatomic,strong)NSString *gmtModified;
@property (nonatomic,strong)NSString *id;
@property (nonatomic,assign)BOOL isfromap;//是否APP下单 ,
@property (nonatomic,strong)NSString *isfromapptext;//是否APP下单描述 ,
@property (nonatomic,assign)BOOL issigned;//是否签约成功 ,
@property (nonatomic,strong)NSString *issignedtext;//是否签约成功描述 ,
@property (nonatomic,strong)NSString *isDeleted;
@property (nonatomic,strong)NSString *modifier;
@property (nonatomic,strong)NSString *name;//订单名称
@property (nonatomic,strong)NSString *no;//订单编号
@property (nonatomic,strong)ZBMyOrderCallbackModel *orderCallback;//回访
@property (nonatomic,strong)ZBMyOrderCampModel *orderCamp;//冬夏令营
@property (nonatomic,strong)ZBMyOrderCampStandbyModel *orderCampStandby;//冬夏令营候补
@property (nonatomic,strong)ZBMyOrderGoodsModel *orderGoods;//商品
@property (nonatomic,strong)ZBMyOrderHouseModel *orderHouse;//海外房产
@property (nonatomic,strong)ZBMyOrderLineModel *orderLine;//经典路线
@property (nonatomic,strong)ZBMyOrderLineStandbyModel *orderLineStandby;//经典路线候补
@property (nonatomic,strong)ZBMyOrderVisaModel *orderVisa;//签证办理
@property (nonatomic,strong)ZBMyOrderMigrateModel *orderMigrate;//移民项目
@property (nonatomic,strong)ZBMyOderStudyModel *orderStudy;//海外留学
@property (nonatomic,strong)NSString *orderstatus;//订单状态 = ['unpaid', 'undelivered', 'unreceived', 'finished', 'user_cancelled', 'timeout_cancelled'],
@property (nonatomic,strong)NSString *orderstatustext;//订单状态描述
@property (nonatomic,strong)NSString *ordertype;//['visa', 'camp', 'line', 'study', 'house', 'migrate', 'campstandby', 'linestandby', 'goods', 'callback'],
@property (nonatomic,strong)NSString *ordertypetext;//订单类型描述
@property (nonatomic,strong)NSString *payamout;//支付金额（分）
@property (nonatomic,strong)NSString *payamoutyuan;//支付金额（元）
@property (nonatomic,strong)NSString *paytype;//支付方式 = ['weixin', 'alipay']
@property (nonatomic,strong)NSString *userEmail;//用户电子邮箱
@property (nonatomic,strong)NSString *userId;//用户主键
@property (nonatomic,strong)NSString *userIdcard;//用户身份证号码
@property (nonatomic,strong)NSString *userMobile;//用户手机
@property (nonatomic,strong)NSString *userName;//用户姓名

@end

