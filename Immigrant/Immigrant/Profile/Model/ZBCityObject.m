//
//  ZBCityObject.m
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBCityObject.h"

@implementation ZBCityObject

-(void)setAreaList:(NSArray *)areaList{
    _areaList = areaList;
    _areaList = [ZBAreaObject mj_objectArrayWithKeyValuesArray:areaList];
}

@end
