//
//  ZBScheduleModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/8.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//排班表
NS_ASSUME_NONNULL_BEGIN

@interface ZBScheduleModel : NSObject
@property (assign, nonatomic) NSInteger dayValue;
@property (strong, nonatomic) NSDate *dateValue;
@property (assign, nonatomic) BOOL isSelectedDay;
@end

NS_ASSUME_NONNULL_END
