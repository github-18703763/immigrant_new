//
//  ZBMineAddressObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBMineAddressObject : NSObject

/**
 "address": "沛鸿大厦A2-506",
 */
@property (nonatomic,copy) NSString  *address;
/**
"city": "深圳",
*/
@property (nonatomic,copy) NSString *city;
/**
 "consignee": "一二三四",
 */
@property (nonatomic,copy) NSString  *consignee;
/**
 "county": "南山区",
 */
@property (nonatomic,copy) NSString  *district;
/**
 "creator": "string",
 */
@property (nonatomic,copy) NSString  *creator;
/**
 "gmtCreated": "2018-12-29T15:37:53.607Z",
 */
@property (nonatomic,copy) NSString  *gmtCreated;
/**
 "gmtModified": "2018-12-29T15:37:53.607Z",
 */
@property (nonatomic,copy) NSString  *gmtModified;
/**
 "id": 0,
 */
@property (nonatomic,copy) NSString  *id;
/**
 "isDeleted": "string",
 */
@property (nonatomic,copy) NSString  *isDeleted;
/**
 "isdefault": false,
 */
@property (nonatomic,assign) BOOL  isdefault;
/**
 "mobile": "15814520421",
 */
@property (nonatomic,copy) NSString  *mobile;
/**
 "modifier": "string",
 */
@property (nonatomic,copy) NSString  *modifier;
/**
 "province": "广东",
 */
@property (nonatomic,copy) NSString  *province;
/**
 "str": "广东省 深圳市 南山区 科技园沛鸿大厦 张三收",
 */
@property (nonatomic,copy) NSString  *str;
/**
 "userId": 0
 */
@property (nonatomic,copy) NSString  *userId;

@end

NS_ASSUME_NONNULL_END
