//
//  ZBMailListModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/6.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//通讯录模型
@interface ZBMailListModel : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phoneNum;
@end

NS_ASSUME_NONNULL_END
