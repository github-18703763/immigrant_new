//
//  ZBMyItemModel.h
//  Immigrant
//
//  Created by jlc on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBMyItemModel : NSObject

@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString  *imageName;


@end
