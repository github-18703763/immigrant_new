//
//  ZBAreaObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBAreaObject : NSObject
/**
"code":"110000",
*/
@property (nonatomic,copy) NSString  *code;
/**
 "name":"北京市",
 */
@property (nonatomic,copy) NSString  *name;

@end

NS_ASSUME_NONNULL_END
