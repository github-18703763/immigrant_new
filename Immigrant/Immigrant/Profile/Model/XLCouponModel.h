//
//  XLCouponModel.h
//  Immigrant
//
//  Created by 王小林 on 20/2/19.
//  Copyright © 2019 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XLCouponModel : NSObject

/*
 activeexpiredday (string, optional): 激活过期日期 ,
 canactive (boolean, optional): 是否可激活 ,
 creator (string, optional),
 discount (number, optional): 折扣（0-1 0.8-八折 0.9-九折 1-无折扣） ,
 discounttext (string, optional): 折扣描述 ,
 gmtCreated (string, optional),
 gmtModified (string, optional),
 id (integer, optional),
 isDeleted (string, optional),
 isuse (boolean, optional): 是否已使用 ,
 isvalid (boolean, optional): 是否有效 ,
 modifier (string, optional),
 remaindays (integer, optional): 过期天数 ,
 sendUserId (integer, optional): 发送人主键 ,
 sendUserName (string, optional): 发送人姓名 ,
 sendexpiredday (string, optional): 赠送过期日期 ,
 userId (integer, optional): 用户主键 ,
 userName (string, optional): 用户姓名
 */
@property (nonatomic,copy) NSString *activeexpiredday;
@property (nonatomic,assign) BOOL canactive;
@property (nonatomic,assign) float discount;
@property (nonatomic,copy) NSString *discounttext;
@property (nonatomic,assign) int remaindays;
@property (nonatomic,assign) BOOL isvalid;


@end

NS_ASSUME_NONNULL_END
