//
//  ZBMyRateModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/7.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//我的测评结果
@interface ZBMyRateDetailModel : NSObject

@property (nonatomic, copy) NSString *cardtype;//"永久居民",
@property (nonatomic, copy) NSString *country;// "美国",
@property (nonatomic, copy) NSString *countryId;// 2,
@property (nonatomic, copy) NSString *cover;// "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1546850345&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=zjvlc0TevauQmX6ugfH5GabLbko%3D",
@property (nonatomic, copy) NSString *creator;// "2",
@property (nonatomic, copy) NSString *depositfee;// 200000,
@property (nonatomic, copy) NSString *depositfeeyuan;// 2000,
@property (nonatomic, copy) NSString *gmtCreated;// "2019-01-03 15:32:41",
@property (nonatomic, copy) NSString *gmtModified;// "2019-01-05 17:18:13",
@property (nonatomic, copy) NSString *id;// 2,
@property (nonatomic, copy) NSString *imageskey;// "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg",
@property (nonatomic, copy) NSString *investment;// 50000000,
@property (nonatomic, copy) NSString *investmentyuan;// 500000,
@property (nonatomic, copy) NSString *isDeleted;// "n",
@property (nonatomic, copy) NSString *isactivity;// false,
@property (nonatomic, copy) NSString *modifier;// "[SYS]",
@property (nonatomic, copy) NSString *name;// "美国EB5投资移民",
@property (nonatomic, copy) NSString *questionIds;// "1,3",
@property (nonatomic, copy) NSString *require0;// 2,
@property (nonatomic, copy) NSString *require1;// 3,
@property (nonatomic, copy) NSString *require2;// 3,
@property (nonatomic, copy) NSString *require3;// 3,
@property (nonatomic, copy) NSString *require4;// 3,
@property (nonatomic, copy) NSString *require5;// 3,
@property (nonatomic, copy) NSString *residencerequires;//"无居住要求",
@property (nonatomic, copy) NSString *servicefee;//5000000,
@property (nonatomic, copy) NSString *servicefeeyuan;// 50000,
@property (nonatomic, copy) NSString *servietime;//"4-6个月"

@end
//我的测评
@interface ZBMyRateModel : NSObject
@property (nonatomic, copy) NSString *creator;//: "2",
@property (nonatomic, copy) NSString *gmtCreated;// "2019-01-05 17:28:17",
@property (nonatomic, copy) NSString *gmtModified;// "2019-01-05 17:28:17",
@property (nonatomic, copy) NSString *id;// 2,
@property (nonatomic, copy) NSString *isDeleted;//"n",
@property (nonatomic, copy) NSString *migrateCountryId;// 2,
@property (nonatomic, copy) NSString *migrateId;//2,
@property (nonatomic, copy) NSString *migreateName;//"美国EB5投资移民",
@property (nonatomic, copy) NSString *modifier;// "[SYS]",
@property (nonatomic, assign) NSInteger score;// 0,
@property (nonatomic, copy) NSString *userId;// 2

//详情有下面这个字段
@property (nonatomic, strong)NSArray<ZBMyRateDetailModel *> *migrates;
@end
