//
//  ZBLogisticFrameModel.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/21.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBLogisticFrameModel.h"
#import "CZHGlobalHeader.h"
#import "NSString+CZHSizeExtension.h"
#import "YYText.h"

#define MARGIN_LEFT  CZH_ScaleWidth(45)
#define MARGIN_RIGHT  CZH_ScaleWidth(15)
#define MAX_WIDTH   (ScreenWidth - MARGIN_LEFT - MARGIN_RIGHT)

@implementation ZBLogisticFrameModel
- (void)setModel:(ZBLookLogisticsModel *)model {
    _model = model;
    
    _contentString = model.AcceptStation;
    _timeString = model.AcceptTime;
    
    ///判断颜色
    if (model.indexCount == 0) {
        //        _contentColor = CZHColor(0xff0000);
        //        _timeColor = CZHColor(0xff0000);
        _leftRoundColor = CZHColor(0xff0000);
    } else {
        _contentColor = CZHColor(0x999999);
        _timeColor = CZHColor(0x999999);
        _leftRoundColor = CZHColor(0xdfdfdf);
    }
    
    
    _contentAttributedString = [[NSMutableAttributedString alloc] initWithString:_contentString];
    
    _contentAttributedString.yy_font = CZHGlobelNormalFont(14);
    
    _contentAttributedString.yy_color = _contentColor;
    
    _contentAttributedString.yy_lineSpacing = 10;
    
    
    
    ///匹配电话号码
    NSString *string = _contentString;
    NSError *error = nil;
    NSDataDetector * detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    
    __block NSUInteger count = 0;
    __block NSString *phoneNumber;
    [detector enumerateMatchesInString:string options:0 range:NSMakeRange(0, [string length]) usingBlock:^(NSTextCheckingResult * _Nullable match, NSMatchingFlags flags, BOOL * _Nonnull stop) {
        
        if (count == 0) *stop = YES;
        
        if ([match resultType] == NSTextCheckingTypePhoneNumber) {
            phoneNumber = [match phoneNumber];
            //            NSLog(@"phoneNumber:%@", phoneNumber);
        }
    }];
    
    if (phoneNumber.length > 0) {
        [_contentAttributedString yy_setTextHighlightRange:[_contentString rangeOfString:phoneNumber]
                                                     color:[UIColor blueColor]
                                           backgroundColor:nil
                                                 tapAction:^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect){
                                                     
                                                     NSLog(@"----拨打电话--%@", phoneNumber);
                                                     
                                                     NSMutableString *str=[[NSMutableString alloc]initWithFormat:@"tel:%@",phoneNumber];
                                                     
                                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                                                     
                                                 }];
    }
    CGSize timeLabelSize = [_timeString czh_sizeWithFont:CZHGlobelNormalFont(13) maxW:110];
    //    CGFloat timeLabelX = MARGIN_LEFT;
    //    CGFloat timeLabelY = CGRectGetMaxY(_contentLabelF) + CZH_ScaleWidth(10);
    CGFloat timeLabelX = 12;
    CGFloat timeLabelY =  CZH_ScaleWidth(15);
    CGFloat timeLabelW = 105;
    CGFloat timeLabelH = timeLabelSize.height;
    _timeLabelF = CGRectMake(timeLabelX, timeLabelY, timeLabelW, timeLabelH);
    
    CGFloat leftRoundViewX = 0;
    CGFloat leftRoundViewY = 0;
    CGFloat leftRoundViewW = 0;
    CGFloat leftRoundViewH = 0;
    
    if (model.indexCount == 0) {
        
        leftRoundViewY = CZH_ScaleWidth(15);
        leftRoundViewW = CZH_ScaleWidth(15);
        leftRoundViewH = CZH_ScaleWidth(15);
        leftRoundViewX = 127;
    } else {
        leftRoundViewY = CZH_ScaleWidth(19);
        leftRoundViewW = CZH_ScaleWidth(7);
        leftRoundViewH = CZH_ScaleWidth(7);
        leftRoundViewX = 132;
    }
    
    _leftRoundViewF = CGRectMake(leftRoundViewX, leftRoundViewY, leftRoundViewW, leftRoundViewH);
    
    
    
    ///计算控件frame
    
    CGSize maxSize = CGSizeMake(ScreenWidth - CZH_ScaleWidth(155), MAXFLOAT);
    CGSize contentLabelSize = [YYTextLayout layoutWithContainerSize:maxSize text:_contentAttributedString].textBoundingSize;
    CGFloat contentLabelX = CGRectGetMaxX(_leftRoundViewF) + CZH_ScaleWidth(10);;
    CGFloat contentLabelY = CZH_ScaleWidth(15);
    CGFloat contentLabelW = contentLabelSize.width;
    CGFloat contentLabelH = contentLabelSize.height;
    _contentLabelF = CGRectMake(contentLabelX, contentLabelY, contentLabelW, contentLabelH);
    
    
    
    
    _cellHeight = CGRectGetMaxY(_contentLabelF) + CZH_ScaleWidth(15);
    
    
    if (model.indexCount == model.totalCount - 1) {
        _bottomLineF = CGRectZero;
    } else {
        CGFloat bottomLineX = MARGIN_LEFT;
        CGFloat bottomLineY = _cellHeight - 0.5;
        CGFloat bottomLineW = ScreenWidth - bottomLineX;
        CGFloat bottomLineH = 0.5;
        _bottomLineF = CGRectMake(bottomLineX, bottomLineY, bottomLineW, bottomLineH);
    }
    
    
    CGFloat leftLineViewW = 1;
    CGFloat leftLineViewX = 132+leftRoundViewW/2;
    CGFloat leftLineViewY = 0;
    CGFloat leftLineViewH = 0;
    
    if (model.indexCount == 0) {//第一个
        leftLineViewY = CGRectGetMaxY(_leftRoundViewF);
        leftLineViewH = _cellHeight - leftLineViewY;
        leftLineViewX = 132+leftRoundViewW/5;
    } else if (model.indexCount == model.totalCount - 1) {//最后一个
        leftLineViewY = 0;
        leftLineViewH = CGRectGetMinY(_leftRoundViewF);
    } else {
        leftLineViewY = 0;
        leftLineViewH = _cellHeight;
    }
    _leftLineViewF = CGRectMake(leftLineViewX, leftLineViewY, leftLineViewW, leftLineViewH);
}
@end
