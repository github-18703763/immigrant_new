//
//  ZBMyCollectionModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface ZBFavoritesDTOModel : NSObject
////FavoritesDTO
//@property (nonatomic,strong)NSString *cardtype;//身份证类型
//@property (nonatomic,strong)NSString *camp ;//冬夏令营 ,
//@property (nonatomic,strong)NSString *creator;//
//@property (nonatomic,strong)NSString *gmtCreated;//
//@property (nonatomic,strong)NSString *gmtModified ;//
//@property (nonatomic,strong)NSString *house;//海外房产 ,
//@property (nonatomic,strong)NSString *id;//
//@property (nonatomic,strong)NSString *isDeleted ;//
//@property (nonatomic,strong)NSString *line  ;//经典路线 ,
//@property (nonatomic,strong)NSString *migrate  ;// 移民项目 ,
//@property (nonatomic,strong)NSString *modifier  ;//,
//@property (nonatomic,strong)NSString *outId ;//业务主键 ,
//@property (nonatomic,strong)NSString *outtype;//业务类型 = ['visa', 'camp','line', 'study', 'studysuccess', 'house', 'school', 'migrate'],
//@property (nonatomic,strong)NSString *outtypetext ;//业务类型描述 ,
//@property (nonatomic,strong)NSString *school  ;//学院 ,
//@property (nonatomic,strong)NSString *study ;//海外留学 ,
//@property (nonatomic,strong)NSString *studySuccess  ;// 海外留学（成功案例） ,
//@property (nonatomic,strong)NSString *userId ;//用户主键 ,
//@property (nonatomic,strong)NSString *visa  ;//签证办理
//@end


@interface ZBCampDTOModel : NSObject
//CampDTO
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *currentnum;// 当前人数 ,
@property (nonatomic,strong)NSString *datestr;// 字符串格式的日期 ,
@property (nonatomic,strong)NSString *depositfee;// 定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan ;// 定金（元） ,
@property (nonatomic,strong)NSString *edate;// 结束日期 ,
@property (nonatomic,strong)NSString *gmtCreated ;//
@property (nonatomic,strong)NSString *gmtModified ;//
@property (nonatomic,strong)NSString *id;//
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *introduce;// 活动介绍 ,
@property (nonatomic,strong)NSString *isDeleted ;//
@property (nonatomic,strong)NSString *isactivity ;// 是否揽分产品 ,
@property (nonatomic,strong)NSString *isfavorites ;// 是否被收藏 ,
@property (nonatomic,strong)NSString *isfull;//是否满人 ,
@property (nonatomic,strong)NSString *modifier;//
@property (nonatomic,strong)NSString *name;// 名称 ,
@property (nonatomic,strong)NSString *notice;// 报名须知 ,
@property (nonatomic,strong)NSString *plan;// 日程安排 ,
@property (nonatomic,strong)NSString *sdate;// 开始日期 ,
@property (nonatomic,strong)NSString *totalnum;// 总人数 ,
@property (nonatomic,strong)NSString *totalprice;// 总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;// 总售价（分）

@end




@interface ZBHouseDTOModel : NSObject
//HouseDTO
@property (nonatomic,strong)NSString *address ;// 地点 ,
@property (nonatomic,strong)NSString *annuprofit ;// 年收益（%） ,
@property (nonatomic,strong)NSString *country ;// 国家 ,
@property (nonatomic,strong)NSString *countryId ;// 国家主键 ,
@property (nonatomic,strong)NSString *cover ;// 封面 ,
@property (nonatomic,strong)NSString *creator ;//,
@property (nonatomic,strong)NSString *deliverystandard ;// 交房标准 ,
@property (nonatomic,strong)NSString *deliverytime ;// 交房时间 ,
@property (nonatomic,strong)NSString *depositfee ;// 定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan ;// 定金（元） ,
@property (nonatomic,strong)NSString *downpayment ;// 首付比例（%） ,
@property (nonatomic,strong)NSString *facility ;// 周边设施 ,
@property (nonatomic,strong)NSString *feature ;// 项目特色 ,
@property (nonatomic,strong)NSString *floorage ;// 使用面积（平方米） ,
@property (nonatomic,strong)NSString *gmtCreated ;//,
@property (nonatomic,strong)NSString *gmtModified ;//,
@property (nonatomic,strong)NSString *id ;//
@property (nonatomic,strong)NSString *images  ;//图集 ,
@property (nonatomic,strong)NSString *imageskey ;// 图集KEY ,
@property (nonatomic,strong)NSString *increaserate ;// 涨幅（%） ,
@property (nonatomic,strong)NSString *investment ;// 投资评估 ,
@property (nonatomic,strong)NSString *isDeleted ;//,
@property (nonatomic,strong)NSString *isactivity ;// 是否揽分产品 ,
@property (nonatomic,strong)NSString *isfavorites ;// 是否被收藏 ,
@property (nonatomic,strong)NSString *management ;// 物业类型 ,
@property (nonatomic,strong)NSString *modifier ;//,
@property (nonatomic,strong)NSString *name ;// 名称 ,
@property (nonatomic,strong)NSString *ownerrights ;// 业主权益 ,
@property (nonatomic,strong)NSString *ownershipyear ;// 产权年限 ,
@property (nonatomic,strong)NSString *process ;// 购房流程 ,
@property (nonatomic,strong)NSString *servicefee ;// 办理费（分） ,
@property (nonatomic,strong)NSString *servicefeeyuan ;// 办理费（元） ,
@property (nonatomic,strong)NSString *totalprice ;// 总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan ;// 总售价（元） ,
@property (nonatomic,strong)NSString *totalpriceyuanstr ;// 总售价（元）（文字描述） ,
@property (nonatomic,strong)NSString *type ;// 在售户型

@end


@interface ZBLineDTOModel : NSObject
//LineDTO
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *currentnum;// 当前参与人数 ,
@property (nonatomic,strong)NSString *datestr;// 字符串格式的日期 ,
@property (nonatomic,strong)NSString *depositfee;// 定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan;// 定金（元） ,
@property (nonatomic,strong)NSString *edate;// 结束日期 ,
@property (nonatomic,strong)NSString *feeinclude;// 费用包含 ,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *goal;// 出游目的 ,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *images ;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *info;// 项目信息 ,
@property (nonatomic,strong)NSString *introduce;// 行程介绍 ,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *isactivity;//是否揽分产品 ,
@property (nonatomic,strong)NSString *isfavorites ;//是否被收藏 ,
@property (nonatomic,strong)NSString *isfull;// 是否满人 ,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *name;// 名称 ,
@property (nonatomic,strong)NSString *sdate;// 开始日期 ,
@property (nonatomic,strong)NSString *totalnum;// 总人数 ,
@property (nonatomic,strong)NSString *totalprice;// 总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;// 总售价（分）

@end


@interface ZBMigrateDTOModel : NSObject
//MigrateDTO
@property (nonatomic,strong)NSString *cardtype;// 身份证类型 ,
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *depositfee;// 定金（分） ,
@property (nonatomic,strong)NSString *depositfeeyuan;// 定金（元） ,
@property (nonatomic,strong)NSString *feature;// 项目优势 ,
@property (nonatomic,strong)NSString *fee;// 费用详情 ,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *info;// 项目信息 ,
@property (nonatomic,strong)NSString *investment;// 投资金额（分） ,
@property (nonatomic,strong)NSString *investmentyuan;// 投资金额（元） ,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *isactivity;// 是否揽分产品 ,
@property (nonatomic,strong)NSString *isfavorites;// 是否被收藏 ,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *name;// 名称 ,
@property (nonatomic,strong)NSString *process;// 申请流程 ,
@property (nonatomic,strong)NSString *questionIds;// 条件测试主键集合 ,
@property (nonatomic,strong)NSString *questions;//条件测试集合 ,
@property (nonatomic,strong)NSString *require0;// 移民方案——国家地区（从地址库获取ID） ,
@property (nonatomic,strong)NSString *require0fit;// 移民方案——国家地区（是否达标） ,
@property (nonatomic,strong)NSString *require1;// 移民方案——最高学历（0-高中以下 1-高中或中专 2-大专 3-本科或硕士 4-博士） ,
@property (nonatomic,strong)NSString *require1fit;// 移民方案——最高学历（是否达标） ,
@property (nonatomic,strong)NSString *require2;// 移民方案——移民目的（0-子女教育 1-海外生育 2-养老储备 3-出行便利 4-海外置业 5-投资理财 6-旅游度假 7-税务筹划） ,
@property (nonatomic,strong)NSString *require2fit;// 移民方案——移民目的（是否达标） ,
@property (nonatomic,strong)NSString *require3;// 移民方案——资金要求（0-100万以内 1-100万-300万 2-300万-500万 3-500万-1000万 4-1000万-3000万 5-3000万以上） ,
@property (nonatomic,strong)NSString *require3fit;// 移民方案——资金要求（是否达标） ,
@property (nonatomic,strong)NSString *require4;// 移民方案——居住要求（0-不方便居住 1-每年入境一次 2-每年住7天 3-每年住30天 4-每年住半年 5-累计住满2年 ） ,
@property (nonatomic,strong)NSString *require4fit;// 移民方案——居住要求（是否达标） ,
@property (nonatomic,strong)NSString *require5;// 移民方案——外语要求（0-完全不会 1-大学英语4级以下 2-大学英语6级 3-大学英语6级优秀 4-专业英语8级） ,
@property (nonatomic,strong)NSString *require5fit;// 移民方案——外语要求（是否达标） ,
@property (nonatomic,strong)NSString *requirefitscore;// 移民方案——匹配度（百分比） ,
@property (nonatomic,strong)NSString *requires;// 申请条件 ,
@property (nonatomic,strong)NSString *residencerequires;// 居住要求 ,
@property (nonatomic,strong)NSString *servicefee;// 服务费（分） ,
@property (nonatomic,strong)NSString *servicefeeyuan;// 服务费（元） ,
@property (nonatomic,strong)NSString *servietime;// 办理周期

@end


@interface ZBSchoolDTOModel : NSObject
//SchoolDTO
@property (nonatomic,strong)NSString *chinesename;// 中文名 ,
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *englishname;// 英文名 ,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *introduce;// 学院简介 ,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *isfavorites;// 是否被收藏 ,
@property (nonatomic,strong)NSString *isrecommend;// 是否推荐 ,
@property (nonatomic,strong)NSString *isrecommendtext;// 是否推荐描述 ,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *profess;// 专业，多个用逗号分隔 ,
@property (nonatomic,strong)NSString *qs;// QS排名 ,
@property (nonatomic,strong)NSString *region;// 地区 ,
@property (nonatomic,strong)NSString *regionId;// 地区主键 ,
@property (nonatomic,strong)NSString *threshold;// 申请门槛 ,
@property (nonatomic,strong)NSString *tuimes;// TUIMES排名


@end

@interface ZBStudyDTOModel : NSObject
//StudyDTO
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *introduce;// 留学介绍 ,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *isactivity;// 是否揽分产品 ,
@property (nonatomic,strong)NSString *isfavorites;// 是否被收藏 ,
@property (nonatomic,strong)NSString *material;// 所需材料 ,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *name;// 名称 ,
@property (nonatomic,strong)NSString *notice;// 办理须知 ,
@property (nonatomic,strong)NSString *process;// 服务流程 ,
@property (nonatomic,strong)NSString *service;// 服务详情 ,
@property (nonatomic,strong)NSString *totalprice;// 总售价（分） ,
@property (nonatomic,strong)NSString *totalpriceyuan;// 总售价（元）

@end


@interface ZBStudySuccessDTOModel : NSObject
//StudySuccessDTO
@property (nonatomic,strong)NSString *content;// 内容 ,
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *degree;// 学习阶段 ,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *isfavorites;// 是否被收藏 ,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *school;// 学校 ,
@property (nonatomic,strong)NSString *schoolId;// 学院主键 ,
@property (nonatomic,strong)NSString *title;// 标题

@end

@interface ZBVisaDTOModel : NSObject
//VisaDTO
@property (nonatomic,strong)NSString *country;// 国家 ,
@property (nonatomic,strong)NSString *countryId;// 国家主键 ,
@property (nonatomic,strong)NSString *cover;// 封面 ,
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *entriesnum;// 入境次数 ,
@property (nonatomic,strong)NSString *expirydate;// 有效期 ,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *images;// 图集 ,
@property (nonatomic,strong)NSString *imageskey;// 图集KEY ,
@property (nonatomic,strong)NSString *interviewrequires;// 面试要求 ,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *isactivity;//: 是否揽分产品 ,
@property (nonatomic,strong)NSString *isfavorites;//: 是否被收藏 ,
@property (nonatomic,strong)NSString *material;// 所需材料 ,
@property (nonatomic,strong)NSString *maxstaytime;// 最长停留时间 ,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *name;// 名称 ,
@property (nonatomic,strong)NSString *notice;// 办理须知 ,
@property (nonatomic,strong)NSString *process;// 办理流程 ,
@property (nonatomic,strong)NSString *remark;// 办理说明 ,
@property (nonatomic,strong)NSString *servicefee;// 办理费（分） ,
@property (nonatomic,strong)NSString *servicefeeyuan;// 办理费（元） ,
@property (nonatomic,strong)NSString *servietime;// 办理周期 ,
@property (nonatomic,strong)NSString *visatype;// 签证办理类型
@end


//展示没用
@interface ZBMigrateQuestionDTOModel : NSObject
//MigrateQuestionDTO
@property (nonatomic,strong)NSString *creator;//,
@property (nonatomic,strong)NSString *gmtCreated;//,
@property (nonatomic,strong)NSString *gmtModified;//,
@property (nonatomic,strong)NSString *id;//,
@property (nonatomic,strong)NSString *isDeleted;//,
@property (nonatomic,strong)NSString *modifier;//,
@property (nonatomic,strong)NSString *optiona;// 选项A ,
@property (nonatomic,strong)NSString *optionb;// 选项B ,
@property (nonatomic,strong)NSString *optionc;// 选项C ,
@property (nonatomic,strong)NSString *optiond;// 选项D ,
@property (nonatomic,strong)NSString *question;// 题目 ,
@property (nonatomic,strong)NSString *scorea;// 分数A ,
@property (nonatomic,strong)NSString *scoreb;// 分数B ,
@property (nonatomic,strong)NSString *scorec;// 分数C ,
@property (nonatomic,strong)NSString *scored;// 分数D

@end


//我的收藏FavoritesDTO
@interface ZBMyCollectionModel : NSObject
@property (nonatomic,strong)NSString *cardtype;//身份证类型
@property (nonatomic,strong)ZBCampDTOModel *camp ;//冬夏令营 ,
@property (nonatomic,strong)NSString *creator;//
@property (nonatomic,strong)NSString *gmtCreated;//
@property (nonatomic,strong)NSString *gmtModified ;//
@property (nonatomic,strong)ZBHouseDTOModel *house;//海外房产 ,
@property (nonatomic,strong)NSString *id;//
@property (nonatomic,strong)NSString *isDeleted ;//
@property (nonatomic,strong)ZBLineDTOModel *line  ;//经典路线 ,
@property (nonatomic,strong)ZBMigrateDTOModel *migrate  ;// 移民项目 ,
@property (nonatomic,strong)NSString *modifier  ;//,
@property (nonatomic,strong)NSString *outId ;//业务主键 ,
@property (nonatomic,strong)NSString *outtype;//业务类型 = ['visa', 'camp','line', 'study', 'studysuccess', 'house', 'school', 'migrate'],
@property (nonatomic,strong)NSString *outtypetext ;//业务类型描述 ,
@property (nonatomic,strong)ZBSchoolDTOModel *school  ;//学院 ,
@property (nonatomic,strong)ZBStudyDTOModel *study ;//海外留学 ,
@property (nonatomic,strong)ZBStudySuccessDTOModel *studySuccess  ;// 海外留学（成功案例） ,
@property (nonatomic,strong)NSString *userId ;//用户主键 ,
@property (nonatomic,strong)ZBVisaDTOModel *visa  ;//签证办理
@end
