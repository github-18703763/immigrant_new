//
//  ZBArearObject.m
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBProviceObject.h"

@implementation ZBProviceObject

-(void)setCityList:(NSArray *)cityList{
    _cityList = cityList;
    _cityList = [ZBCityObject mj_objectArrayWithKeyValuesArray:cityList];
}

@end
