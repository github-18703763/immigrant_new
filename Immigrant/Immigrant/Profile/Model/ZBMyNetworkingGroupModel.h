//
//  ZBMyNetworkingGroupModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/15.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBMyNetworkingGroupModel : NSObject

@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *devotepoint;
@property (nonatomic, copy) NSString *existscoupon;
@property (nonatomic, copy) NSString *firstfriendnum;
@property (nonatomic, strong) NSArray *firstfriends;
@property (nonatomic, copy) NSString *gmtCreated;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *isDeleted;
@property (nonatomic, copy) NSString *modifier;
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, copy) NSString *secondfriendnum;
@property (nonatomic, copy) NSString *totalfriendnum;
@property (nonatomic, copy) NSString *userAvatarkey;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userMobile;
@property (nonatomic, copy) NSString *userName;

@property (nonatomic, copy) NSString *isOpen;


@end

NS_ASSUME_NONNULL_END
