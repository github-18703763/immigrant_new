//
//  ZBMyCollectionModel.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/5.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyCollectionModel.h"

@implementation ZBMyCollectionModel

@end

//冬夏令营 ,
@implementation ZBCampDTOModel

@end
//海外房产
@implementation ZBHouseDTOModel

@end
//经典路线
@implementation ZBLineDTOModel

@end
//// 移民项目
@implementation ZBMigrateDTOModel

@end
//学院
@implementation ZBSchoolDTOModel

@end
//海外留学
@implementation ZBStudyDTOModel

@end
// 海外留学（成功案例）
@implementation ZBStudySuccessDTOModel

@end
//签证办理
@implementation ZBVisaDTOModel

@end
