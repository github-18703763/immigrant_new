//
//  ZBCallBackModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/17.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//回访
NS_ASSUME_NONNULL_BEGIN

@interface ZBCallBackModel : NSObject

@property (nonatomic, copy) NSString *callbackstatus;//(string, optional): 回访状态 = ['wait', 'finish'],
@property (nonatomic, copy) NSString *callbackstatustext;//(string, optional): 回访状态描述 ,
@property (nonatomic, copy) NSString *callbacktype;//(string, optional): 回访方式 = ['visit', 'online', 'company'],
@property (nonatomic, copy) NSString *callbacktypetext;//(string, optional): 回访方式描述 ,
@property (nonatomic, copy) NSString *companyaddress;//(string, optional): 面谈公司的公司地址（callbackype = company） ,
@property (nonatomic, copy) NSString *creator;//(string, optional),
@property (nonatomic, copy) NSString *firstedate;//(string, optional): 首选沟通结束时间 ,
@property (nonatomic, copy) NSString *firstsdate;//(string, optional): 首选沟通开始时间 ,
@property (nonatomic, copy) NSString *gmtCreated;//(string, optional),
@property (nonatomic, copy) NSString *gmtModified;//(string, optional),
@property (nonatomic, copy) NSString *id;//(integer, optional),
@property (nonatomic, copy) NSString *isDeleted;//(string, optional),
@property (nonatomic, copy) NSString *islocationcity;//(boolean, optional): 是否同城 ,
@property (nonatomic, copy) NSString *islocationcitytext;//(string, optional): 是否同城描述 ,
@property (nonatomic, copy) NSString *modifier;//(string, optional),
@property (nonatomic, copy) NSString *orderId;//(integer, optional): 订单主键（callbackype = visit） ,
@property (nonatomic, copy) NSString *orderstatustext;//(string, optional): 订单状态描述 ,
@property (nonatomic, copy) NSString *outId;//(integer, optional): 项目主键 ,
@property (nonatomic, copy) NSString *outName;//(string, optional): 项目名称 ,
@property (nonatomic, copy) NSString *outtype;//(string, optional): 项目类型 = ['other', 'migrate'],
@property (nonatomic, copy) NSString *outtypetext;//(string, optional): 项目类型描述 ,
@property (nonatomic, copy) NSString *secondedate;//(string, optional): 次选沟通结束时间 ,
@property (nonatomic, copy) NSString *secondsdate;//(string, optional): 次选沟通开始时间 ,
@property (nonatomic, copy) NSString *userId;//(integer, optional): 用户主键 ,
@property (nonatomic, copy) NSString *userMobile;//(string, optional): 用户手机 ,
@property (nonatomic, copy) NSString *userName;//(string, optional): 用户姓名 ,
@property (nonatomic, copy) NSString *visitcity;//(string, optional): 上面拜访所在的城市（callbackype = visit）
@end

NS_ASSUME_NONNULL_END
