//
//  ZBLogisticFrameModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/21.
//  Copyright © 2019年 张波. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ZBLookLogisticsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZBLogisticFrameModel : NSObject

///左边的远
@property (nonatomic, assign, readonly) CGRect leftRoundViewF;
///左边的线
@property (nonatomic, assign, readonly) CGRect leftLineViewF;
///内容
@property (nonatomic, assign, readonly) CGRect contentLabelF;
///时间
@property (nonatomic, assign, readonly) CGRect timeLabelF;
///底部线
@property (nonatomic, assign, readonly) CGRect bottomLineF;


///物流文本
@property (nonatomic, copy, readonly) NSString *contentString;
///时间文本
@property (nonatomic, copy, readonly) NSString *timeString;
///内容富文本
@property (nonatomic, strong, readonly) NSMutableAttributedString *contentAttributedString;
///物流文本颜色
@property (nonatomic, strong, readonly) UIColor *contentColor;
///时间文本颜色
@property (nonatomic, strong, readonly) UIColor *timeColor;
///左边圆圈颜色
@property (nonatomic, strong, readonly) UIColor *leftRoundColor;

///cell高度
@property (nonatomic, assign, readonly) CGFloat cellHeight;

///模型
@property (nonatomic, strong) ZBLookLogisticsModel *model;

@end

NS_ASSUME_NONNULL_END
