//
//  ZBMyGroupBuyReturnModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/15.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//团购返现
NS_ASSUME_NONNULL_BEGIN

@interface ZBMyGroupBuyReturnModel : NSObject
@property (nonatomic, copy) NSString *amount;//(integer, optional): 返现金额（分） ,
@property (nonatomic, copy) NSString *amountyuan;// (number, optional): 返现金额（元） ,
@property (nonatomic, copy) NSString *avatar;// (string, optional): 头像 ,
@property (nonatomic, copy) NSString *creator;// (string, optional),
@property (nonatomic, copy) NSString *gmtCreated;// (string, optional),
@property (nonatomic, copy) NSString *gmtModified ;//(string, optional),
@property (nonatomic, copy) NSString *id;// (integer, optional),
@property (nonatomic, copy) NSString *isDeleted;// (string, optional),
@property (nonatomic, assign)BOOL iscash;// (boolean, optional): 是否返现 ,
@property (nonatomic, copy) NSString *iscashtext;// (string, optional): 是否返现描述 ,
@property (nonatomic, copy) NSString *memo;// (string, optional): 备注 ,
@property (nonatomic, copy) NSString *modifier;// (string, optional),
@property (nonatomic, copy) NSString *recommendAvatarkey;// (string, optional): 被邀请人头像KEY ,
@property (nonatomic, copy) NSString *recommendId;// (integer, optional): 被邀请人主键 ,
@property (nonatomic, copy) NSString *recommendName;// (string, optional): 被邀请人姓名 ,
@property (nonatomic, copy) NSString *userId;// (integer, optional): 邀请人主键 ,
@property (nonatomic, copy) NSString *userName;// (string, optional): 邀请人姓名

@end

NS_ASSUME_NONNULL_END
