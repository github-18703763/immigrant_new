//
//  ZBMyOrderModel.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/4.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBMyOrderModel.h"

@implementation ZBMyOrderModel

@end

//夏令营
@implementation ZBMyOrderCampModel

@end

//商品
@implementation ZBMyOrderGoodsModel

@end

//海外房产
@implementation ZBMyOrderHouseModel

@end

//经典路线
@implementation ZBMyOrderLineModel

@end

//签证办理
@implementation ZBMyOrderVisaModel

@end

//回访
@implementation ZBMyOrderCallbackModel

@end

//移民项目
@implementation ZBMyOrderMigrateModel

@end

//海外留学
@implementation ZBMyOderStudyModel

@end
//经典路线后补orderLineStandby
@implementation ZBMyOrderLineStandbyModel
@end
//冬夏令营后补
@implementation ZBMyOrderCampStandbyModel
@end
