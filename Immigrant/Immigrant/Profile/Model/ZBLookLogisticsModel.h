//
//  ZBLookLogisticsModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/21.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//查看物流
NS_ASSUME_NONNULL_BEGIN

@interface ZBLookLogisticsModel : NSObject
//【广东深圳公司福田区天安数码城分部】正在进行【分发】扫描
@property (nonatomic,strong)NSString *AcceptStation;
//\"2019-01-20 14:20:44\"
@property (nonatomic,strong)NSString *AcceptTime;

///模型下标
@property (nonatomic, assign) NSInteger indexCount;
///模型总数量
@property (nonatomic, assign) NSInteger totalCount;

@end

NS_ASSUME_NONNULL_END
