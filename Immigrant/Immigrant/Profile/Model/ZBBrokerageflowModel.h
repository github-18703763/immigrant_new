//
//  ZBBrokerageflowModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/2/17.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
//佣金流水的数据模型
@interface ZBBrokerageflowModel : NSObject
//amount (integer, optional): 操作金额（分） ,
//amountyuan (number, optional): 操作金额（元） ,
//creator (string, optional),
//gmtCreated (string, optional),
//gmtModified (string, optional),
//id (integer, optional),
//isDeleted (string, optional),
//memo (string, optional): 备注说明 ,
//modifier (string, optional),
//userId (integer, optional): 用户主键（只针对业务员和合伙人）
@property (nonatomic, copy) NSString *amount;//操作金额（分） ,
@property (nonatomic, copy) NSString *amountyuan;//操作金额（元） ,
@property (nonatomic, copy) NSString *creator;//
@property (nonatomic, copy) NSString *gmtCreated;//
@property (nonatomic, copy) NSString *gmtModified;//
@property (nonatomic, copy) NSString *id;//(string, optional),
@property (nonatomic, copy) NSString *isDeleted;//
@property (nonatomic, copy) NSString *memo;//
@property (nonatomic, copy) NSString *modifier;//
@property (nonatomic, copy) NSString *userId;//用户主键（只针对业务员和合伙人）
@end

//我的佣金
@interface ZBBrokerageSendsModel : NSObject

@property (nonatomic, copy) NSString *creator;//
@property (nonatomic, copy) NSString *currentprogress;//"收款中",
@property (nonatomic, copy) NSString *currentprogressId;//
@property (nonatomic, copy) NSString *currentprogressamountyuan;//当前项目阶段发放佣金（元） ,
@property (nonatomic, copy) NSString *currentprogressissend;//当前项目阶段是否发放 ,
@property (nonatomic, copy) NSString *currentprogresssendtime;//(string, optional): 当前项目阶段发放时间 ,
@property (nonatomic, copy) NSString *gmtCreated;//(string, optional),
@property (nonatomic, copy) NSString *gmtModified;//
@property (nonatomic, copy) NSString *id;//
@property (nonatomic, copy) NSString *isDeleted;//
@property (nonatomic, copy) NSString *issend;//是否全部发放完毕发放 ,
@property (nonatomic, copy) NSString *modifier;//
@property (nonatomic, copy) NSString *outId;
@property (nonatomic, copy) NSString *outName;//项目名称 ,
@property (nonatomic, copy) NSString *outtype;//
@property (nonatomic, copy) NSString *outtypetext;//
@property (nonatomic, copy) NSString *userId;//(string, optional),
@property (nonatomic, copy) NSString *userNickname;//用户昵称

@end

//我的CRM
@interface ZBMycrmModel : NSObject

@property (nonatomic, copy) NSString *userId;//(string, optional),
@property (nonatomic, copy) NSString *singedcountry;//签约国家主键 ,
@property (nonatomic, copy) NSString *singedcountryId;//签约国家主键 ,
@property (nonatomic, copy) NSString *salesmanId;//负责的业务员主键 ,
@property (nonatomic, copy) NSString *name;//
@property (nonatomic, copy) NSString *modifier;//
@property (nonatomic, copy) NSString *mobile;//
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *istimeout;//是否超时（acceptance=false未接单时，如果为istimeout=true则无法接单） ,
@property (nonatomic, copy) NSString *isDeleted;//
@property (nonatomic, copy) NSString *introducedtime;//派单时间 ,
@property (nonatomic, copy) NSString *interestedcountryId;//(string, optional),
@property (nonatomic, copy) NSString *id;//用户昵称

@property (nonatomic, copy) NSString *gmtModified;//(string, optional),
@property (nonatomic, copy) NSString *gmtCreated;//
@property (nonatomic, copy) NSString *datafromtext;//数据来源 ,
@property (nonatomic, copy) NSString *datafrom;// 数据来源 = ['app', 'manual'],
@property (nonatomic, copy) NSString *customertypetext;//客户类型 ,
@property (nonatomic, copy) NSString *customertype;//客户类型 = ['signed', 'interested', 'introduced']

//客户来源 = ['personal', 'introduced']
@property (nonatomic, copy) NSString *customerfrom;
@property (nonatomic, copy) NSString *customerfromtext;//客户来源 ,
@property (nonatomic, copy) NSString *creator;//
@property (nonatomic, copy) NSString *acceptancetext;//(string, optional): 接单描述 ,
@property (nonatomic, assign)BOOL acceptance;//是否接单 ,

@end
