//
//  OrderPayTool.m
//  RVProject
//
//  Created by Gao on 2018/2/1.
//  Copyright © 2018年 icebartech. All rights reserved.
//

#import "OrderPayTool.h"

@implementation OrderPayTool

SINGLETON_IMPLE(OrderPayTool)

#pragma mark 支付宝支付
- (void)alipayResult:(NSDictionary *)payDic {
    //支付完成发送通知
    NSInteger  resultStatus = [[payDic objectForKey:@"resultStatus"] integerValue];
    if (resultStatus == 9000){
        //支付成功
        [[NSNotificationCenter defaultCenter] postNotificationName:NotifactionAlipayPaySuccess object:nil];
    }else{
        //支付成功
        [[NSNotificationCenter  defaultCenter] postNotificationName:NotifactionAlipayPayFail object:nil];
    }
}

/**
 根据返回的订单信息，发起微信支付
 */
- (void)sendWeixinPayReqWith:(NSDictionary *)dic {
    PayReq *req = [[PayReq alloc] init];
    //由用户微信号和AppID组成的唯一标识，用于校验微信用户
//    req.openID = [dic objectForKey:@"appid"];
            
    // 商家id，在注册的时候给的
    req.partnerId = [dic objectForKey:@"partnerid"];
    
    // 预支付订单这个是后台跟微信服务器交互后，微信服务器传给你们服务器的，你们服务器再传给你
    req.prepayId  = [dic objectForKey:@"prepayid"];
    
    // 根据财付通文档填写的数据和签名
    //这个比较特殊，是固定的，只能是即req.package = Sign=WXPay
    req.package   = [dic objectForKey:@"packageDesc"];
    
    // 随机编码，为了防止重复的，在后台生成
    req.nonceStr  = [dic objectForKey:@"noncestr"];
    
    // 这个是时间戳，也是在后台生成的，为了验证支付的
    NSString * stamp = [dic objectForKey:@"timestamp"];
    req.timeStamp = stamp.intValue;
    
    // 这个签名也是后台做的
    req.sign = [dic objectForKey:@"sign"];
    
    [WXApi sendReq:req];
}

#pragma mark - WXApiDelegate
-(void)onReq:(BaseReq*)req {
}

-(void)onResp:(BaseResp*)resp {
    if ([resp isKindOfClass:[PayResp class]]) {
        if (resp.errCode == 0) { //支付成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifactionWeixinPaySuccess object:nil];
        }else {
            [[NSNotificationCenter defaultCenter] postNotificationName:NotifactionWeixinPayFail object:nil];
        }
    } else if ([resp isKindOfClass:[SendAuthResp class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wxAuth" object:nil userInfo:@{@"resp":resp}];
    }
    
}

@end
