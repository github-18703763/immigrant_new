//
//  OrderPayTool.h
//  RVProject
//
//  Created by Gao on 2018/2/1.
//  Copyright © 2018年 icebartech. All rights reserved.
//  处理第三方支付的工具类

#import "WXApi.h"
#import <Foundation/Foundation.h>
//单例宏
#define SINGLETON_INTERFACE(className) +(className*)shared;

#define SINGLETON_IMPLE(className) \
+ (className *)shared {\
static dispatch_once_t once;\
static className* instance;\
dispatch_once(&once, ^{instance = self.new; instance = [instance init];});\
return instance;\
}

//支付宝支付结果通知
static NSString * const  NotifactionAlipayPaySuccess  = @"alipayPaySuccess";
static NSString * const  NotifactionAlipayPayFail  = @"alipayPayFail";

//微信支付结果通知
static NSString * const  NotifactionWeixinPaySuccess =  @"weixinPaySuccess";
static NSString * const  NotifactionWeixinPayFail  = @"weixinPayFail";

@interface OrderPayTool : NSObject <WXApiDelegate>

SINGLETON_INTERFACE(OrderPayTool)

/**根据预支付返回的订单信息调起微信支付*/
- (void)sendWeixinPayReqWith:(NSDictionary *)dic;

/**处理支付宝支付结果*/
- (void)alipayResult:(NSDictionary *)payDic;



@end
