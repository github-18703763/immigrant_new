//
//  QSUserManager.m
//  Immigrant
//
//  Created by jlc on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "QSUserManager.h"

@implementation QSUserManager

static QSUserManager  * userVC = nil;

+ (QSUserManager *)shareInstance
{
    static dispatch_once_t oncetoken;
    dispatch_once(&oncetoken, ^{
        
    userVC = [[QSUserManager alloc] init];
        
    });
    return userVC;
}

-(void)userLoginWithsessionId:(NSString *)sessionId user:(NSDictionary*_Nullable) userDic{
    GYUserModel *userModel=[GYUserModel mj_objectWithKeyValues:userDic];
    [QSUserManager shareInstance].userModel = userModel;
    [QSUserManager shareInstance].isLogin=YES;
    [QSUserManager shareInstance].sessionId = sessionId;
    [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:@"sessionId"];
    [[NSUserDefaults standardUserDefaults]setObject:@(YES) forKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults]setObject:userDic forKey:@"userMessageDic"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(GYUserModel *)userModel{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userMessageDic"]) {
        _userModel = [GYUserModel mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:@"userMessageDic"]];
        return _userModel;
    }else{
        return nil;
    }
}

-(NSString *)sessionId{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"sessionId"]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionId"];
    }else{
        return @"";
    }
}

-(BOOL)isLogin{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"];
    }else{
        return NO;
    }
}

-(void)exitLogin{
    [QSUserManager shareInstance].userModel = nil;
    [QSUserManager shareInstance].isLogin=NO;
    [QSUserManager shareInstance].sessionId = nil;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey: @"sessionId"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userMessageDic"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

@end
