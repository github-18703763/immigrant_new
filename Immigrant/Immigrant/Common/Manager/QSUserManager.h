//
//  QSUserManager.h
//  Immigrant
//
//  Created by jlc on 2018/12/25.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GYUserModel.h"

@interface QSUserManager : NSObject
+ (QSUserManager *_Nullable)shareInstance;

@property (nonatomic,copy) NSString  *sessionId;

@property(nonatomic,copy) NSString * _Nullable wechatCode;//微信登录code

@property(nonatomic,strong)GYUserModel * _Nullable userModel;

@property(nonatomic,assign)BOOL isLogin;

-(void)userLoginWithsessionId:(NSString *)sessionId user:(NSDictionary*_Nullable) userDic;

-(void)exitLogin;

@end
