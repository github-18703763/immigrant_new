//
//  UITableViewCell+identifier.h
//  instrument
//
//  Created by 徐盖粦 on 2018/3/9.
//  Copyright © 2018年 徐盖粦. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (identifier)

+ (nonnull instancetype)cellForTableView: (nonnull UITableView *)tableView;

+ (nonnull instancetype)cellForTableView:(nonnull UITableView *)tableView reuserIdentifier:(nonnull NSString *)identifier;
@end
