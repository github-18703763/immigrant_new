//
//  UITableViewCell+Extension.h
//  IndustryArchive
//
//  Created by icecream on 2018/7/6.
//  Copyright © 2018年 Icebar tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Extension)
- (void)setHideTopSeparator:(BOOL)hide;
- (void)setTopSeparatorInsets:(UIEdgeInsets)edgeInsets;

- (void)setHideBottomSeparator:(BOOL)hide;
- (void)setBottomSeparatorInsets:(UIEdgeInsets)edgeInsets;
@end

@interface UITableViewHeaderFooterView (Extension)
- (void)setHideTopSeparator:(BOOL)hide;
- (void)setTopSeparatorInsets:(UIEdgeInsets)edgeInsets;

- (void)setHideBottomSeparator:(BOOL)hide;
- (void)setBottomSeparatorInsets:(UIEdgeInsets)edgeInsets;
@end
