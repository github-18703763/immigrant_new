//
//  UIColor+Category.h
//  健步行
//
//  Created by mac on 16/4/11.
//  Copyright © 2016年 Mystery. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UIColor (Category)

+(UIColor *) getColor:(NSString *)hexColor;

+(UIColor *) randomColor;

+ (UIColor *)getColor:(NSString *)hexColor alpha:(CGFloat)alpha;
@end
