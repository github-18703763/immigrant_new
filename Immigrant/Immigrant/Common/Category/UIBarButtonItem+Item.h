//
//  UIBarButtonItem+Item.h
//
//  Created by apple on 15-3-5.
//  Copyright (c) 2013年 junnpyStrong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Item)

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)image highImage:(UIImage *)highImage target:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;

/**
返回按钮  叉号按钮

 @param image <#image description#>
 @param highImage <#highImage description#>
 @param twoimage <#twoimage description#>
 @param twohighImage <#twohighImage description#>
 @param target <#target description#>
 @param action <#action description#>
 @param controlEvents <#controlEvents description#>
 @param twotarget <#twotarget description#>
 @param twoaction <#twoaction description#>
 @param twocontrolEvents <#twocontrolEvents description#>
 @return <#return value description#>
 */
+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)image highImage:(UIImage *)highImage ButtonItemTwoWithImage:(UIImage *)twoimage highImage:(UIImage *)twohighImage target:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents twotarget:(id)twotarget twoaction:(SEL)twoaction fortwoControlEvents:(UIControlEvents)twocontrolEvents;
@end
