//
//  UIViewController+login.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/20.
//  Copyright © 2019 张波. All rights reserved.
//

#import "UIViewController+login.h"
#import "GYLoginRegisterVC.h"
#import <objc/runtime.h>
@implementation UIViewController (login)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalSelector = @selector(viewDidLoad);
        SEL swizzledSelector = @selector(my_viewDidLoad);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
        
        //交换实现
        method_exchangeImplementations(originalMethod, swizzledMethod);
    });
}

#pragma mark - Method Swizzling
- (void)my_viewDidLoad {
    [self my_viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentLogin) name:NeedLogin object:nil];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.navigationController.navigationBar setBackgroundImage: [UIImage createImageWithColor:UIColor.whiteColor withRect:CGRectMake(0, 0, 1, 1)] forBarMetrics:UIBarMetricsDefault];
}
- (void)presentLogin {
    GYLoginRegisterVC *vc = [[GYLoginRegisterVC alloc] init];
    [self presentViewController:[[XKNavigationController alloc] initWithRootViewController:vc] animated:true completion:nil];
}

@end
