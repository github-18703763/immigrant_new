//
//  UITableViewCell+Extension.m
//  IndustryArchive
//
//  Created by icecream on 2018/7/6.
//  Copyright © 2018年 Icebar tech. All rights reserved.
//

#import "UITableViewCell+Extension.h"
#import <objc/runtime.h>

@implementation UITableViewCell (Extension)

- (UIView *)topSeparator {
    return objc_getAssociatedObject(self, @selector(topSeparator));

}

- (UIView *)bottomSeparator {
    return objc_getAssociatedObject(self, @selector(bottomSeparator));
}

- (void)setHideTopSeparator:(BOOL)hide {
    UIView *sep = [self topSeparator];
    sep.hidden = hide;
}

- (void)setTopSeparatorInsets:(UIEdgeInsets)edgeInsets {
    
    UIView *topSeparator = [self topSeparator];
    if (nil == topSeparator) {
        UIView *sep = [[UIView alloc] init];
        sep.backgroundColor = [UIColor getColor:@"E1E1E1"];
        [self.contentView addSubview:sep];
        [sep mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5);
            make.top.equalTo(self.contentView).offset(edgeInsets.top);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
        objc_setAssociatedObject(self, @selector(topSeparator), sep, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        [topSeparator mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(edgeInsets.top);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
    }
}

- (void)setHideBottomSeparator:(BOOL)hide {
    UIView *sep = [self bottomSeparator];
    sep.hidden = hide;
}

- (void)setBottomSeparatorInsets:(UIEdgeInsets)edgeInsets {
    
    UIView *bottomSeparator = [self bottomSeparator];
    if (nil == bottomSeparator) {
        UIView *sep = [[UIView alloc] init];
        sep.backgroundColor = [UIColor getColor:@"E1E1E1"];
        [self.contentView addSubview:sep];
        [sep mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5);
            make.bottom.equalTo(self.contentView).offset(-edgeInsets.bottom);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
        objc_setAssociatedObject(self, @selector(bottomSeparator), sep, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        [bottomSeparator mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView).offset(-edgeInsets.bottom);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
    }
}

@end

@implementation UITableViewHeaderFooterView (Extension)

- (UIView *)topSeparator {
    return objc_getAssociatedObject(self, @selector(topSeparator));
    
}

- (UIView *)bottomSeparator {
    return objc_getAssociatedObject(self, @selector(bottomSeparator));
}

- (void)setHideTopSeparator:(BOOL)hide {
    UIView *sep = [self topSeparator];
    sep.hidden = hide;
}

- (void)setTopSeparatorInsets:(UIEdgeInsets)edgeInsets {
    
    UIView *topSeparator = [self topSeparator];
    if (nil == topSeparator) {
        UIView *sep = [[UIView alloc] init];
        sep.backgroundColor = [UIColor getColor:@"E1E1E1"];
        [self.contentView addSubview:sep];
        [sep mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5);
            make.top.equalTo(self.contentView).offset(edgeInsets.top);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
        objc_setAssociatedObject(self, @selector(topSeparator), sep, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        [topSeparator mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(edgeInsets.top);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
    }
}

- (void)setHideBottomSeparator:(BOOL)hide {
    UIView *sep = [self bottomSeparator];
    sep.hidden = hide;
}

- (void)setBottomSeparatorInsets:(UIEdgeInsets)edgeInsets {
    
    UIView *bottomSeparator = [self bottomSeparator];
    if (nil == bottomSeparator) {
        UIView *sep = [[UIView alloc] init];
        sep.backgroundColor = [UIColor getColor:@"E1E1E1"];
        [self.contentView addSubview:sep];
        [sep mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5);
            make.bottom.equalTo(self.contentView).offset(-edgeInsets.bottom);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
        objc_setAssociatedObject(self, @selector(bottomSeparator), sep, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        [bottomSeparator mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView).offset(-edgeInsets.bottom);
            make.left.equalTo(self.contentView).offset(edgeInsets.left);
            make.right.equalTo(self.contentView).offset(-edgeInsets.right);
        }];
    }
}

@end
