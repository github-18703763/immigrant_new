//
//  UITableViewCell+identifier.m
//  instrument
//
//  Created by 徐盖粦 on 2018/3/9.
//  Copyright © 2018年 徐盖粦. All rights reserved.
//

#import "UITableViewCell+identifier.h"

@implementation UITableViewCell (identifier)

+ (nonnull NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

+ (nonnull instancetype)cellForTableView:(nonnull UITableView *)tableView
{
    NSString *cellId = [self cellIdentifier];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    return cell;
}

+ (nonnull instancetype)cellForTableView:(nonnull UITableView *)tableView reuserIdentifier:(nonnull NSString *)identifier
{
//    NSString *cellId = [self cellIdentifier];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}

@end
