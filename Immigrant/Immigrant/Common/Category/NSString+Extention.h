//
//  NSString+Extention.h
//  eHealthCare
//
//  Created by mac on 16/11/10.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extention)

/**传入浮点型数据返回是否含所有小数点之后的数据**/
+(NSString *)formatterNum:(CGFloat)number;

/*根据身份证号获取生日*/
+(NSString *)birthdayStrFromIdentityCard:(NSString *)numberStr;

/*根据身份证号性别*/
+(NSString *)sexFromIdentityCard:(NSString *)numberStr;

/*根据省份证号获取年龄*/
+(NSString *)ageFromIdentityCard:(NSString *)numberStr;

+ (NSString *) compareCurrentTime:(NSString *) strDate;

/**
 判断数据是否在这之间
 
 @param urlString <#urlString description#>
 @return <#return value description#>
 */
+(float)scaneInteger:(NSString *)urlString;
/**
 传入一个字符串验证是否含有特殊支付
 */
+(BOOL)JudgeTheillegalCharacter:(NSString *)content;

/**
 判断是否含有特殊字符
 */
+(BOOL)isIncludeSpecialCharact: (NSString *)str;


/**
 截取指定字符串

 @param characString <#characString description#>
 @param start <#start description#>
 @param end <#end description#>
 @return <#return value description#>
 */
+(NSString *)loadCharcterView:(NSString *)characString startS:(NSString *)start endS:(NSString *)end;

@end
