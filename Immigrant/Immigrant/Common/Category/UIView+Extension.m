//
//  UIView+Extension.m

//
//  Created by apple on 14-10-7.
//  Copyright (c) 2014年 heima. All rights reserved.
//

#import "UIView+Extension.h"

@implementation UIView (Extension)



- (UIViewController *)parentController{
    UIResponder *responder = [self nextResponder];
    while (responder) {
        if ([responder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)responder;
        }
        responder = [responder nextResponder];
    }
    return nil;
}


- (UIViewController *)currentViewController
{
    
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
   
    UIView *firstView = [keyWindow.subviews firstObject];
    
    UIView *secondView = [firstView.subviews firstObject];
   
    UIViewController *vc = [secondView parentController];
    
    if ([vc isKindOfClass:[UITabBarController class]]) {
       
        UITabBarController *tab = (UITabBarController *)vc;
       
        if ([tab.selectedViewController isKindOfClass:[UINavigationController class]]) {
         
            UINavigationController *nav = (UINavigationController *)tab.selectedViewController;
          
            return [nav.viewControllers lastObject];
       
        } else {
          
            return tab.selectedViewController;
       
        }
    } else if ([vc isKindOfClass:[UINavigationController class]]) {
       
        UINavigationController *nav = (UINavigationController *)vc;
        
        return [nav.viewControllers lastObject];
    
    } else {
        
        return vc;
    }
    return nil;
}





- (void)setLeft:(CGFloat)left
{
    CGRect frame = self.frame;
    frame.origin.x = left;
    self.frame = frame;
}

- (void)setTop:(CGFloat)top
{
    CGRect frame = self.frame;
    frame.origin.y = top;
    self.frame = frame;
}

- (CGFloat)left
{
    return self.frame.origin.x;
}

- (CGFloat)top
{
    return self.frame.origin.y;
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX
{
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY
{
    return self.center.y;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGFloat)right
{
    return self.frame.origin.x + self.frame.size.width;
}


- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}


-(void)addShadow{
    self.layer.cornerRadius = 15;
    self.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.1].CGColor;
    self.layer.shadowRadius = 15;
    self.layer.shadowOpacity = 1;
}



- (CGSize)size
{
    return self.frame.size;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin
{
    return self.frame.origin;
}
@end
