//
//  UIImage+ZBImage.m
//
//  Created by junnpy Strong on 15/12/31.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import "UIImage+Image.h"

@implementation UIImage (Image)

#pragma mark 让图片保持不被渲染
+ (instancetype)imageWithOriginalName:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}
#pragma make 图片拉伸
+ (instancetype)imageWithStretchableName:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    return [image stretchableImageWithLeftCapWidth:image.size.width * 0.5 topCapHeight:image.size.height * 0.5];
}
- (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize{
    //数据源图片
    UIImage *sourceImage = self;
    //新图片
    UIImage *newImage = nil;
    //数据源图片的size
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    //压缩指定的size
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    //按照比例压缩
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    //等size不压缩
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    //开始绘制
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    //从图层获取新的图片对象
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
    //结束绘制
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(UIImage*) createImageWithColor:(UIColor*)color withRect:(CGRect)rect
{
//    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

@end
