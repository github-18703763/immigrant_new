
//
//  UIImage+ZBImage.h
//
//  Created by junnpy Strong on 15/12/31.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Image)

+ (instancetype)imageWithOriginalName:(NSString *)imageName;
+ (instancetype)imageWithStretchableName:(NSString *)imageName;

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;

+(UIImage*) createImageWithColor:(UIColor*)color withRect:(CGRect)rect;

@end
