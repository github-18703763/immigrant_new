//
//  UILabel+Category.h
//  Immigrant
//
//  Created by jlc on 2018/12/21.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Category)

-(void)textAlignmentLeftAndRight;

@end
