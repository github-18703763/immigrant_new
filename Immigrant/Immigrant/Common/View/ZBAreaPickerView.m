//
//  ZBAreaPickerView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBAreaPickerView.h"
#import "ZBAreaPickerCell.h"

@interface ZBAreaPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *pick;

@property (nonatomic,assign) NSInteger  oneRow;

@property (nonatomic,assign) NSInteger  twoRow;

@property (nonatomic,copy) NSString  *proviceStr;

@property (nonatomic,copy) NSString  *cityStr;

@property (nonatomic,copy) NSString  *areaStr;

@end

@implementation ZBAreaPickerView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.pick.delegate = self;
}

-(void)setDataArray:(NSArray *)dataArray{
    _dataArray = dataArray;
    [self.pick reloadAllComponents];
    [self.pick selectRow:0 inComponent:0 animated:NO];
    ZBProviceObject *pObj = _dataArray[0];
    self.proviceStr = pObj.name;
    ZBCityObject *cObj = pObj.cityList[0];
    self.cityStr = cObj.name;
    ZBAreaObject *aObj = cObj.areaList[0];
    self.areaStr = aObj.name;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return self.dataArray.count;
    }else if (component == 1) {
        ZBProviceObject *provice = self.dataArray[self.oneRow];
        return provice.cityList.count;
    }else{
        ZBProviceObject *provice = self.dataArray[self.oneRow];
        ZBCityObject *city = provice.cityList[self.twoRow];
        return city.areaList.count;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view{
    //设置分割线颜色
    for (UIView *singleLine in pickerView.subviews) {
        if (singleLine.frame.size.height < 1) {
            singleLine.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        }
    }
    ZBAreaPickerCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"ZBAreaPickerCell" owner:self options:nil] firstObject];
    cell.frame = CGRectMake(0, 0, SystemScreenWidth/3, 50);
    if (component == 0) {
        ZBProviceObject *provice = self.dataArray[row];
        cell.markLab.text = provice.name;
    }
    if (component == 1) {
        ZBProviceObject *provice = self.dataArray[self.oneRow];
        ZBCityObject *city = provice.cityList[row];
        cell.markLab.text = city.name;
    }
    if (component == 2) {
        ZBProviceObject *provice = self.dataArray[self.oneRow];
        ZBCityObject *city = provice.cityList[self.twoRow];
        ZBAreaObject *area = city.areaList[row];
        cell.markLab.text = area.name;
    }
    return cell;
}


//每一行的高度
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        self.oneRow = row;
        self.twoRow = 0;
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
        ZBProviceObject *pObj = self.dataArray[row];
        self.proviceStr = pObj.name;
        ZBCityObject *cObj = pObj.cityList[0];
        self.cityStr = cObj.name;
        ZBAreaObject *aObj = cObj.areaList[0];
        self.areaStr = aObj.name;
    }
    if (component == 1) {
        self.twoRow = row;
        ZBProviceObject *pObj = self.dataArray[self.oneRow];
        self.proviceStr = pObj.name;
        ZBCityObject *cObj = pObj.cityList[row];
        self.cityStr = cObj.name;
        ZBAreaObject *aObj = cObj.areaList[0];
        self.areaStr = aObj.name;
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    if (component == 2) {
        ZBProviceObject *pObj = self.dataArray[self.oneRow];
         self.proviceStr = pObj.name;
        ZBCityObject *cObj = pObj.cityList[self.twoRow];
        self.cityStr = cObj.name;
        ZBAreaObject *aObj = cObj.areaList[row];
        self.areaStr = aObj.name;
    }
  
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self removeFromSuperview];
}

- (IBAction)concalAction:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)sureCation:(id)sender {
    if (self.filishActionBlock) {
        self.filishActionBlock(self.proviceStr, self.cityStr, self.areaStr);
    }
    [self removeFromSuperview];
}

@end
