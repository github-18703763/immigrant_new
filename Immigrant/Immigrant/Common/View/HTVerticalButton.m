//
//  HTVerticalButton.m
//  Immigrant
//
//  Created by jlc on 2018/12/17.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "HTVerticalButton.h"

@implementation HTVerticalButton

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;

}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = contentRect.size.height *0.5;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    return CGRectMake(0, titleY , titleW, titleH);
}
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
//    CGFloat imageW = CGRectGetWidth(contentRect);
//    CGFloat imageH = contentRect.size.height * 0.6;
    return CGRectMake(CGRectGetWidth(contentRect)/2-15, 30, 25, 25);
}

@end
