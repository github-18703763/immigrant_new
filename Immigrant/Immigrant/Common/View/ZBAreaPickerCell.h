//
//  ZBAreaPickerCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBAreaPickerCell : UIView
@property (weak, nonatomic) IBOutlet UILabel *markLab;

@end

NS_ASSUME_NONNULL_END
