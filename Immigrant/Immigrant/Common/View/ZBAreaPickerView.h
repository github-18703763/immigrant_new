//
//  ZBAreaPickerView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/30.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBProviceObject.h"
#import "ZBCityObject.h"
#import "ZBAreaObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBAreaPickerView : UIView

@property (nonatomic,strong) NSArray  *dataArray;

@property (nonatomic,copy) void(^filishActionBlock)(NSString *proviceStr,NSString *cityStr,NSString *areaStr);

@end

NS_ASSUME_NONNULL_END
