//
//  ZBIMCalendarCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KJMonthModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBIMCalendarCell : UITableViewCell

@property (nonatomic,strong) KJMonthModel  *model;

@property (nonatomic,copy) void(^selectTimeBlock)(NSString *time,NSInteger selectDay);

@end

NS_ASSUME_NONNULL_END
