//
//  ZBIMCalendarCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBIMCalendarCell.h"
#import "KJCalendarUtility.h"

@interface ZBIMCalendarCell ()

@property (weak, nonatomic) IBOutlet UIView *containerV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCons;
@property (weak, nonatomic) IBOutlet UILabel *monthLab;
@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnThree;
@property (weak, nonatomic) IBOutlet UIButton *btnFour;
@property (weak, nonatomic) IBOutlet UIButton *btnFive;
@property (weak, nonatomic) IBOutlet UIButton *btnSix;
@property (weak, nonatomic) IBOutlet UIButton *btnSeven;

@property (weak, nonatomic) IBOutlet UIButton *btnTwoOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoThree;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoFour;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoFive;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoSix;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoSeven;

@property (weak, nonatomic) IBOutlet UIButton *btnThreeOne;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeThree;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeFour;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeFive;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeSix;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeSeven;

@property (weak, nonatomic) IBOutlet UIButton *btnFourOne;
@property (weak, nonatomic) IBOutlet UIButton *btnFourTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnFourThree;
@property (weak, nonatomic) IBOutlet UIButton *btnFourFive;
@property (weak, nonatomic) IBOutlet UIButton *btnFourSix;
@property (weak, nonatomic) IBOutlet UIButton *btnFourSeven;
@property (weak, nonatomic) IBOutlet UIButton *btnFourFour;

@property (weak, nonatomic) IBOutlet UIButton *btnFiveOne;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveThree;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveFour;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveFive;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveSix;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveSeven;

@property (weak, nonatomic) IBOutlet UIButton *btnSixOne;
@property (weak, nonatomic) IBOutlet UIButton *btnSixTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnSixThree;
@property (weak, nonatomic) IBOutlet UIButton *btnSixFour;
@property (weak, nonatomic) IBOutlet UIButton *btnSixFive;
@property (weak, nonatomic) IBOutlet UIButton *btnSixSix;
@property (weak, nonatomic) IBOutlet UIButton *btnSixSeven;

@property (nonatomic,strong) NSArray  *btnArray;

@end

@implementation ZBIMCalendarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerV.layer.cornerRadius = 15;
    self.containerV.layer.masksToBounds = YES;
    self.heightCons.constant = (SystemScreenWidth-48)/7;
    
    self.btnArray = @[self.btnOne,self.btnTwo,self.btnThree,self.btnFour,self.btnFive,self.btnSix,self.btnSeven,self.btnTwoOne,self.btnTwoTwo,self.btnTwoThree,self.btnTwoFour,self.btnTwoFive,self.btnTwoSix,self.btnTwoSeven,self.btnThreeOne,self.btnThreeTwo,self.btnThreeThree,self.btnThreeFour,self.btnThreeFive,self.btnThreeSix,self.btnThreeSeven,self.btnFourOne,self.btnFourTwo,self.btnFourThree,self.btnFourFour,self.btnFourFive,self.btnFourSix,self.btnFourSeven,self.btnFiveOne,self.btnFiveTwo,self.btnFiveThree,self.btnFiveFour,self.btnFiveFive,self.btnFiveSix,self.btnFiveSeven,self.btnSixOne,self.btnSixTwo,self.btnSixThree,self.btnSixFour,self.btnSixFive,self.btnSixSix,self.btnSixSeven];
    
    for (UIButton *btn in self.btnArray) {
        [btn addTarget:self action:@selector(chooseAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.cornerRadius =(SystemScreenWidth-48)/7/2;
        btn.layer.masksToBounds = YES;
    }
    
}

-(void)chooseAction:(UIButton *)btn{
    if (btn.titleLabel.text.length<=0) {
        return;
    }
    if (([btn.titleLabel.text integerValue]<[KJCalendarUtility day:[NSDate date]])&&([KJCalendarUtility month:[NSDate date]] == _model.month)) {
        return;
    }
    for (UIButton *bt in self.btnArray) {
        if (btn != bt){
            bt.backgroundColor = [UIColor whiteColor];
            [bt setTitleColor:[UIColor getColor:@"727272"] forState:UIControlStateNormal];
        }else{
            bt.backgroundColor = [UIColor getColor:@"437DFF"];
            [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    if (self.selectTimeBlock) {
        NSString *month = [NSString stringWithFormat:@"%li",_model.month];
        if (month.length == 1) {
            month = [NSString stringWithFormat:@"0%li",_model.month];
        }
        NSString *day = btn.titleLabel.text;
        if (day.length == 1) {
            day = [NSString stringWithFormat:@"0%@",btn.titleLabel.text];
        }
        self.selectTimeBlock([NSString stringWithFormat:@"%li-%@-%@",_model.year,month,day],[btn.titleLabel.text integerValue]);
    }
}

-(void)setModel:(KJMonthModel *)model{
    _model = model;
    
    self.monthLab.text = [NSString stringWithFormat:@"%li.%li",_model.year,_model.month];
    for (UIButton * btn in self.btnArray) {
        [btn setTitle:@"" forState:UIControlStateNormal];
    }
    
    for (UIButton *btn in self.btnArray) {
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitleColor:[UIColor getColor:@"727272"] forState:UIControlStateNormal];
    }
    if (_model.hasSelect) {
        UIButton *btn = self.btnArray[_model.selectDay-1+_model.week-1];
        btn.backgroundColor = [UIColor getColor:@"437DFF"];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    NSInteger week = model.week-1;
    for (NSInteger i=0; i<_model.totalDay; i++) {
        UIButton *btn = self.btnArray[week];
        [btn setTitle:[NSString stringWithFormat:@"%li",i+1] forState:UIControlStateNormal];
        week = week+1;
    }
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
