//
//  ZBCalendarController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBCalendarController.h"
#import "ZBIMCalendarCell.h"
#import "KJCalendarModel.h"
#import "KJCalendarUtility.h"
#import "KJMonthModel.h"

@interface ZBCalendarController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView  *tableView;

@property (nonatomic,strong) NSArray  *timeArray;

@property (nonatomic,copy) NSString  *currentTime;

@end

@implementation ZBCalendarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"时间选择";
    self.view.backgroundColor = COLOR(252, 252, 252, 1);
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight-(KIsiPhoneX?88:64)) style:UITableViewStylePlain];
    self.tableView.bounces = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = COLOR(252, 252, 252, 1);
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBIMCalendarCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    NSDate *date = [NSDate date];
    KJCalendarModel *calendarModel = [[KJCalendarModel alloc] init];
    calendarModel.year = [KJCalendarUtility year:date];
    calendarModel.month = [KJCalendarUtility month:date];
    calendarModel.day = [KJCalendarUtility day:date];
    
    KJMonthModel *model1 = [[KJMonthModel alloc] init];
    model1.year = [KJCalendarUtility year:date];
    model1.hasSelect = YES;
    model1.month = [KJCalendarUtility month:date];
    model1.week = [KJCalendarUtility firstWeekdayForMonth:date];
    model1.totalDay = [KJCalendarUtility totaldaysForMonth:date];
    model1.selectDay = [KJCalendarUtility day:date];
    self.currentTime = [NSString stringWithFormat:@"%li-%li-%li",model1.year,model1.month,model1.selectDay];
    
    NSInteger nextYear = [KJCalendarUtility year:date];
    NSInteger nextMoth = [KJCalendarUtility month:date]+1;
    if ([KJCalendarUtility month:date]>=12) {
        nextYear = nextYear+1;
        nextMoth = 1;
    }
    NSString *dateStr = [NSString stringWithFormat:@"%ld-%ld-01 00:00:00.000",nextYear,nextMoth];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
    NSDate *birthdayDate = [dateFormatter dateFromString:dateStr];
    KJMonthModel *model2 = [[KJMonthModel alloc] init];
    model2.year = [KJCalendarUtility year:birthdayDate];
    model2.month = [KJCalendarUtility month:birthdayDate];
    model2.week = [KJCalendarUtility firstWeekdayForMonth:birthdayDate];
    model2.totalDay = [KJCalendarUtility totaldaysForMonth:birthdayDate];
    model2.hasSelect = NO;
    self.timeArray = @[model1,model2];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (self.chooseTimeBlock) {
        self.chooseTimeBlock(self.currentTime, self.selectDay, self.path);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.timeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBIMCalendarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    WS(weakSELF);
    cell.selectTimeBlock = ^(NSString * _Nonnull time, NSInteger selectDay) {
//        ZBLog(@"%@",time);
        weakSELF.currentTime = time;
        weakSELF.selectDay = selectDay;
        weakSELF.path = indexPath;
        [self.navigationController popViewControllerAnimated:YES];
//        if (indexPath.row == 0) {
//            KJMonthModel *model = self.timeArray[0];
//            model.hasSelect = YES;
//            model.selectDay = selectDay;
//
//            KJMonthModel *model1 = self.timeArray[1];
//            model1.hasSelect = NO;
//        }else{
//            KJMonthModel *model = self.timeArray[0];
//            model.hasSelect = NO;
//
//            KJMonthModel *model1 = self.timeArray[1];
//            model1.hasSelect = YES;
//            model1.selectDay = selectDay;
//        }
        [tableView reloadData];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.timeArray[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 113+6*(SystemScreenWidth-28)/7;
}

@end
