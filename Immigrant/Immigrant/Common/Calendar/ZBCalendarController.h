//
//  ZBCalendarController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBCalendarController : UIViewController

@property (nonatomic,copy) void(^chooseTimeBlock)(NSString *time,NSInteger selectDay,NSIndexPath *path);

@property (nonatomic,assign) NSInteger  selectDay;

@property (nonatomic,strong) NSIndexPath  *path;

@end

NS_ASSUME_NONNULL_END
