

typedef NS_ENUM(NSInteger, EMLoginEn) {
    kLoginEn_dev = 0, //开发环境带默认帐号
    kLoginEn_test, //测试环境带默认帐号
    kLoginEn_preRelease, //预生产环境
    kLoginEn_release, //生产环境
};

#import <Foundation/Foundation.h>

@interface GYLoginEn : NSObject

@property (nonatomic, assign) EMLoginEn loginLine;

+ (GYLoginEn*)sharedInstance;

- (NSString*)getLoginUrl;
-(NSString*)getWeatherIP;

//获取用户登录系统url地址
-(NSString*)getUserLoginUrl;

//获取评论系统host url
-(NSString*)getReviewSystemUrl;


-(NSString*)getDataAsyncUrl;

+ (EMLoginEn)getInitLoginLine;

+ (BOOL)isReleaseEn;//是否为生产发布环境 否：NO 是：YES

-(NSString*)getWeatherStatic;

-(NSString*)getReportErrorUrl;


@end
