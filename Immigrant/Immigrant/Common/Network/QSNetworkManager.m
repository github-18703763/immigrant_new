//
//  QSNetworkManager.m
//  QuickBidSmart
//
//  Created by MacLw on 17/8/10.
//  Copyright © 2017年 gogobids. All rights reserved.
//

#import "QSNetworkManager.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "GYLoginEn.h"
#import <AdSupport/AdSupport.h>

#import <CoreTelephony/CTTelephonyNetworkInfo.h>  //为判断网络制式的主要文件

static NSMutableArray *tasks;

@interface QSNetworkManager()
@property (nonatomic,strong)AFHTTPSessionManager *sessionManager;
@end

@implementation QSNetworkManager
static AFURLSessionManager  *sessionURLManager=nil;

+(AFURLSessionManager*)sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionURLManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        AFJSONResponseSerializer *serializer=[AFJSONResponseSerializer serializer];
        serializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",nil];
        sessionURLManager.responseSerializer = serializer;
        AFSecurityPolicy *securityPolicy;
        
        //此处不要其他pingMode,因后台没有给出证书 from jianglincen
        securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        securityPolicy.allowInvalidCertificates = YES;
        securityPolicy.validatesDomainName = NO;
        sessionURLManager.securityPolicy = securityPolicy;
 

    });
    return sessionURLManager;

}

+ (instancetype)sharedNetworkManager
{
    /*! 为单例对象创建的静态实例，置为nil，因为对象的唯一性，必须是static类型 */
    static id sharedNetworkManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedNetworkManager = [[super allocWithZone:NULL] init];
    });
    return sharedNetworkManager;
}

+ (void)initialize
{
    [self setupNetworkManager];
}

+ (void)setupNetworkManager
{
    QSNetWorkManager.sessionManager = [AFHTTPSessionManager manager];
    
     QSNetWorkManager.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    QSNetWorkManager.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];

    QSNetWorkManager.requestSerializer = QSRequestSerializerJSON;
    QSNetWorkManager.responseSerializer = QSResponseSerializerJSON;

//    QSNetWorkManager.responseSerializer = QSResponseSerializerHTTP;

    
    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    [securityPolicy setAllowInvalidCertificates:YES];

#warning 不加此句无法访问 by jianglincen
    
    securityPolicy.validatesDomainName = NO;//验证证书绑定的域
    //这里进行设置；
    [  QSNetWorkManager.sessionManager setSecurityPolicy:securityPolicy];

    
  QSNetWorkManager.sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", @"text/json", @"text/javascript",@"text/plain", nil];
    
    /*! 设置请求超时时间，默认：15秒 */
    QSNetWorkManager.timeoutInterval = 10;
    /*! 打开状态栏的等待菊花 */
    //    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    /*! 复杂的参数类型 需要使用json传值-设置请求内容的类型*/
    [ QSNetWorkManager.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    

    
    /*! 设置响应数据的基本类型 */

//    QSNetWorkManager.sessionManager.requestSerializer.acceptableContentTypes =[];

//    NSString * head =
//@"{\"accessToken\":\"token_001\",,\"channel\":\"BING\",\"contextAccessToken\":{\"accountId\":\"test_c76e15dbcf494949a3b2d11fc0b3e543\",\"expirationTime\":\"2017-12-28T12:35:16.453Z\",\"startTime\":\"2017-12-27T12:35:16.451Z\",\"system\":0,\"token\":\"token_001\"},\"contextAccount\":{\"authId\":\"8d4aaf76860745de8173688129b67cc8\",\"email\":\"test@test.com\",\"id\":\"test_c76e15dbcf494949a3b2d11fc0b3e543\",\"mobile\":\"18881513201\",\"openid\":\"ed6a08943d71462181dd45b062c9a090\",\"state\":0},\"language\":\"CH\",\"network\":1,\"sdk\":\"IOS 1.12.3\",\"terminal\":1,\"udid\":\"udid8087a6f2b18e49ae886e0d9046f08f97\",\"version\":1}";
//    
//    [ QSNetWorkManager.sessionManager.requestSerializer setValue:head forHTTPHeaderField:@"System-Context"];
//    [ QSNetWorkManager.sessionManager.requestSerializer setValue:@"1" forHTTPHeaderField:@"NO-DE"];


}

#pragma mark - 网络请求方法

+(QSURLSessionTask *)basicRequestFullPath:(NSString *)path
                                 requestType:(QSRequestType)type
                                  parameters:(NSDictionary *)parameters
                                 isNeedCache:(BOOL)isNeedCache
                                successBlock:(QSResponseSuccess)successBlock
                                failureBlock:(QSResponseFail)failureBlock
                                    progress:(QSDownloadProgress)progress{
    
    
    if(path==nil){
        return nil;
    }
    
    WS(weakSELF);
    
    /*! 检查地址中是否有中文 */
    NSString *urlString = [NSURL URLWithString:path] ? path : [self strUTF8Encoding:path];
    
    QSURLSessionTask *sessionTask = nil;
    
  //  NSString *urlPath=[NSString stringWithFormat:@"%@%@",[[GYLoginEn sharedInstance]getLoginUrl],urlString];
    
     NSString *urlPath=urlString;
    
  //  SZLog(@"请求地址：%@，请求参数：%@",urlPath,parameters);
    
    switch (type) {
        case QSRequestTypeGet:{
            
            
            [QSNetWorkManager.sessionManager GET:urlPath parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
                if (successBlock)
                {
                    [weakSELF successManageWithUrl:urlPath
                                    responseObject:responseObject
                                           success:successBlock];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                if (failureBlock)
                {
                    failureBlock(error);
                }
                
            }];
            
            
        }break;
            
            
        case QSRequestTypePost:{
            
            [QSNetWorkManager.sessionManager POST:urlPath parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
                if (successBlock)
                {
                    [weakSELF successManageWithUrl:urlPath
                                    responseObject:responseObject
                                           success:successBlock];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                if (failureBlock)
                {
                    failureBlock(error);
                }
                
            }];
            
            
            
        }
        default:
            break;
    }
    
    return sessionTask;
}


/**
 网络请求的实例方法
 
 @param host         服务器主机地址
 @param path         接口请求 url
 @param type         get / post / put / delete
 @param parameters   请求的参数
 @param isNeedCache 是否需要缓存，只有 get / post 请求有缓存配置
 @param successBlock 请求成功的回调
 @param failureBlock 请求失败的回调
 @param progress     进度
 
 @return 方便管理请求任务。执行取消，暂停，继续等任务.
 */

+ (NSURLSessionDataTask *)networkRequestWithHost:(NSString*)host Path:(NSString *)path
                                 requestType:(QSRequestType)type
                                  parameters:(NSDictionary *)parameters
                                 isNeedCache:(BOOL)isNeedCache
                                successBlock:(QSResponseSuccess)successBlock
                                failureBlock:(QSResponseFail)failureBlock
                                    progress:(QSDownloadProgress)progress{
    
    
    NSString *fullPath  = [NSString stringWithFormat:@"%@%@",host,path];
    
   // if(![fullPath containsString:@"taobao.com"]){
        fullPath  = [NSString stringWithFormat:@"%@%@",fullPath,[self appendDeviceString]];
  //  }
    
    
    return  [self networkRequestWithPath:fullPath requestType:type parameters:parameters isNeedCache:isNeedCache successBlock:successBlock failureBlock:failureBlock progress:progress];
    
    
  
}

#pragma mark - 获取系统信息字符串
+(NSString*)appendDeviceString{
    
    return @"";
}


+ (QSURLSessionTask *)networkRequestWithPath:(NSString *)path
                                 requestType:(QSRequestType)type
                                  parameters:(NSDictionary *)parameters
                                 isNeedCache:(BOOL)isNeedCache
                                successBlock:(QSResponseSuccess)successBlock
                                failureBlock:(QSResponseFail)failureBlock
                                    progress:(QSDownloadProgress)progress{
    if(path==nil){
        return nil;
    }
    
    WS(weakSELF);
    
    /*! 检查地址中是否有中文 */
    NSString *urlPath = [NSURL URLWithString:path] ? path : [self strUTF8Encoding:path];

    QSURLSessionTask *sessionTask = nil;
//    NSString *urlPath=[NSString stringWithFormat:@"%@%@",[[GYLoginEn sharedInstance]getLoginUrl],urlString];
//    SZLog(@"请求地址：%@，请求参数：%@",urlPath,parameters);
    
    switch (type) {
        case QSRequestTypeGet:{
            
          //  SZLog(@"header:%@",QSNetWorkManager.sessionManager.requestSerializer.HTTPRequestHeaders);
            
            sessionTask = [QSNetWorkManager.sessionManager GET:urlPath parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
               // NSLog(@"%@",task.allHTTPHeaderFields);

              
                    if (successBlock)
                    {
                        [weakSELF successManageWithUrl:urlPath
                                        responseObject:responseObject
                                               success:successBlock];
                    }
              
               
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                /*
                 NSURLErrorDomain Code=-999 "已取消"
                 已取消 的不是错误，不要处理
                 */
                if (error.code != -999 ) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        
                        if (failureBlock)
                        {
                            failureBlock(error);
                        }
                    });
                }

            }];
            
            
        }break;
            
            
        case QSRequestTypePost:{
            
            //modi by jianglincen 临时处理post请求签名失效问题，待整体优化
            QSNetworkManager *qsManager = [self defaultManager];
            
            sessionTask = [qsManager.sessionManager POST:urlPath parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
                if ([responseObject[@"status"] integerValue] == 401) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NeedLogin object:nil];
                }
                
                if (successBlock)
                {
                    [weakSELF successManageWithUrl:urlPath
                                    responseObject:responseObject
                                           success:successBlock];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                /*
                 NSURLErrorDomain Code=-999 "已取消"
                 已取消 的不是错误，不要处理
                 */
                if (error.code != -999 ) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        
                        if (failureBlock)
                        {
                            failureBlock(error);
                        }
                    });
                }
                
            }];

            
            
    }
        default:
            break;
    }
    
    return sessionTask;
}

+ (QSURLSessionTask *)weatherNetworkRequestWithPath:(NSString *)path
                                 requestType:(QSRequestType)type
                                  parameters:(NSDictionary *)parameters
                                 isNeedCache:(BOOL)isNeedCache
                                successBlock:(QSResponseSuccess)successBlock
                                failureBlock:(QSResponseFail)failureBlock
                                    progress:(QSDownloadProgress)progress{
    if(path==nil){
        return nil;
    }
    
    WS(weakSELF);
    
    /*! 检查地址中是否有中文 */
    NSString *urlString = [NSURL URLWithString:path] ? path : [self strUTF8Encoding:path];
    
    QSURLSessionTask *sessionTask = nil;
    NSString *urlPath=[NSString stringWithFormat:@"%@%@",[[GYLoginEn sharedInstance]getWeatherIP],urlString];
//    SZLog(@"请求地址：%@，请求参数：%@",urlPath,parameters);
    
    switch (type) {
        case QSRequestTypeGet:{
            
            //  SZLog(@"header:%@",QSNetWorkManager.sessionManager.requestSerializer.HTTPRequestHeaders);
            
            [QSNetWorkManager.sessionManager GET:urlPath parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
                // NSLog(@"%@",task.allHTTPHeaderFields);
                
                
                if (successBlock)
                {
                    [weakSELF successManageWithUrl:urlPath
                                    responseObject:responseObject
                                           success:successBlock];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                if (failureBlock)
                {
                    failureBlock(error);
                }
                
            }];
            
            
        }break;
            
            
        case QSRequestTypePost:{
            
            //modi by jianglincen 临时处理post请求签名失效问题，待整体优化
            
            [QSNetWorkManager.sessionManager POST:urlPath parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
                if (successBlock)
                {
                    [weakSELF successManageWithUrl:urlPath
                                    responseObject:responseObject
                                           success:successBlock];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                if (failureBlock)
                {
                    failureBlock(error);
                }
                
            }];
            
            
            
        }
        default:
            break;
    }
    
    return sessionTask;
}


#pragma mark - 网络状态监测
+ (void)startNetWorkMonitoringWithBlock:(QSNetworkStatusBlock)networkStatus{
    /*! 1.获得网络监控的管理者 */
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    /*! 当使用AF发送网络请求时,只要有网络操作,那么在状态栏(电池条)wifi符号旁边显示  菊花提示 */
    //    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    /*! 2.设置网络状态改变后的处理 */
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        /*! 当网络状态改变了, 就会调用这个block */
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown:
//                ZBLog(@"未知网络");
                networkStatus ? networkStatus(QSNetworkStatusUnknown) : nil;
                break;
            case AFNetworkReachabilityStatusNotReachable:
//                ZBLog(@"没有网络");
                networkStatus ? networkStatus(QSNetworkStatusNotReachable) : nil;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
//                ZBLog(@"手机自带网络");
                networkStatus ? networkStatus(QSNetworkStatusReachableViaWWAN) : nil;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
//                ZBLog(@"wifi 网络");
                networkStatus ? networkStatus(QSNetworkStatusReachableViaWiFi) : nil;
                break;
        }
    }];
}

#pragma mark - 取消请求
//取消所有的请求
+ (void)cancelAllRequest{
    // 锁操作
    @synchronized(self)
    {
        [[self tasks] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            [task cancel];
        }];
        [[self tasks] removeAllObjects];
    }
}

//取消指定URL的请求
+ (void)cancelRequestWithURL:(NSString *)URL{
    if (!URL)
    {
        return;
    }
    @synchronized (self)
    {
        [[self tasks] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([task.currentRequest.URL.absoluteString hasPrefix:URL])
            {
                [task cancel];
                [[self tasks] removeObject:task];
                *stop = YES;
            }
        }];
    }
}

#pragma mark - 删除请求头
+ (void)clearAuthorizationHeader{
    [QSNetWorkManager.sessionManager.requestSerializer clearAuthorizationHeader];
}

#pragma mark - setter / getter
/**
 存储着所有的请求task数组
 
 @return 存储着所有的请求task数组
 */
+ (NSMutableArray *)tasks
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //        NSLog(@"创建数组");
        tasks = [[NSMutableArray alloc] init];
    });
    return tasks;
}

- (void)setTimeoutInterval:(NSTimeInterval)timeoutInterval
{
    _timeoutInterval = timeoutInterval;
    QSNetWorkManager.sessionManager.requestSerializer.timeoutInterval = timeoutInterval;
}

- (void)setRequestSerializer:(QSRequestSerializer)requestSerializer
{
    _requestSerializer = requestSerializer;
    switch (requestSerializer) {
        case QSRequestSerializerJSON:
        {
            QSNetWorkManager.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer] ;
        }
            break;
        case QSRequestSerializerHTTP:
        {
            QSNetWorkManager.sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer] ;
        }
            break;
            
        default:
            break;
    }
}

- (void)setResponseSerializer:(QSResponseSerializer)responseSerializer
{
    _responseSerializer = responseSerializer;
    switch (responseSerializer) {
        case QSResponseSerializerJSON:
        {
            QSNetWorkManager.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer] ;
        }
            break;
        case QSResponseSerializerHTTP:
        {
            QSNetWorkManager.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer] ;
        }
            break;
            
        default:
            break;
    }
}


/**
 请求成功数据处理
 
 @param urlStr         接口url
 @param responseObject 接口响应的数据
 @param success        成功回调
 */
+ (void)successManageWithUrl:(NSString *)urlStr responseObject:(id)responseObject success:(QSResponseSuccess _Nullable)success {
    //    QSNetResponseModel *model = [QSNetResponseModel mj_objectWithKeyValues:responseObject];
    QSNetResponseModel * model = [QSNetResponseModel objectWithobject:responseObject];
//    QSNetResponseModel * model=[QSNetResponseModel mj_objectWithKeyValues:responseObject];
    if (model.status !=200) {
//        ZBLog(@"***************************数据错误**********************************");
//        ZBLog(@"url:%@",urlStr);
//        ZBLog(@"message:%@",model.msg);
//        ZBLog(@"***************************数据错误**********************************");
    //处理异常code
    }
      dispatch_async(dispatch_get_main_queue(), ^(){
      //正常处理
        success(model);
      
      });
}


#pragma mark - 返回临时manager

+(QSNetworkManager*)defaultManager{
    
    QSNetworkManager *qsManager =[QSNetworkManager new];
    
    qsManager.sessionManager = [self sharedAFManager];
    //获取当前时间
    [qsManager.sessionManager.requestSerializer setValue:@"" forHTTPHeaderField:@"userid"];
    
    qsManager.requestSerializer = QSRequestSerializerJSON;
    qsManager.responseSerializer = QSResponseSerializerJSON;
    
    /*! 设置请求超时时间，默认：15秒 */
    qsManager.timeoutInterval = 15;

    
    [qsManager.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    [qsManager.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [qsManager.sessionManager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"os"];
    [qsManager.sessionManager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"mobileos"];

    [qsManager.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemVersion]] forHTTPHeaderField:@"mobileosversion"];
  
    qsManager.sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", @"text/json", @"text/javascript", nil];
    
    return qsManager;
    
}


static AFHTTPSessionManager *afManager;

+ (AFHTTPSessionManager *)sharedAFManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // 初始化请求管理类
            afManager = [AFHTTPSessionManager manager];
            afManager.requestSerializer = [AFJSONRequestSerializer serializer];
            afManager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
            afManager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
        
        [afManager.requestSerializer setValue:@"application/json"
                         forHTTPHeaderField:@"Accept"];

        [afManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
       
        
    });
    return afManager;
    
}

#pragma mark - url 中文格式化
+ (NSString *)strUTF8Encoding:(NSString *)str
{
    //! ios9适配的话 打开第一个
    if ([[UIDevice currentDevice] systemVersion].floatValue >= 9.0)
    {
        return [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    }
    else
    {
        return [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
}

@end
