//
//  QSNetworkManager+Order.h
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "QSNetworkManager.h"
#import "OrderCampAddModel.h"
#import "OrderHouseAddModel.h"
#import "OrderLineAddModel.h"
#import "OrderMigrateAddModel.h"
#import "OrderStudyAddModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QSNetworkManager (Order)

+(void)addCampOrder:(OrderCampAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+(void)addCampOrderStandby:(OrderCampAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)addHouseOrder:(OrderHouseAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)addLineOrderStandby:(OrderLineAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)addLineOrder:(OrderLineAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)addMigrateOrder:(OrderMigrateAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)addStudyOrder:(OrderStudyAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)studyFindDetail:(NSInteger)studyId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)studyFindPage:(NSInteger)pageIndex nameLike:(NSString *)nameLike successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

/**
 新增收藏

 @param outId 收藏物品id
 @param outType [visa,camp,line,study,studysuccess,house,school,migrate]
 @param block 成功block
 @param failBlock 失败block
 */
+ (void)favoriteAdd:(NSInteger)outId outType:(NSString *)outType successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)favoriteDelete:(NSInteger)outId outType:(NSString *)outType successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

/**
 移民条件测试

 @param ids 题目id
 */
+ (void)migratequestionList:(NSString *)ids successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;

+ (void)migrateFindlistByrequire:(NSArray *)indexArray successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock;
@end

NS_ASSUME_NONNULL_END
