

#import "GYLoginEn.h"

@implementation GYLoginEn

+ (GYLoginEn*)sharedInstance
{
    static GYLoginEn* _s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _s = [[self alloc] init];
    });
    return _s;
}

- (instancetype)init
{
    if (self = [super init]) {
        //默认开发环境
        self.loginLine = [[self class] getInitLoginLine];
    }
    return self;
}

- (void)setLoginLine:(EMLoginEn)l
{
    _loginLine = l;
}


/**
 *  获取登录接口
 *
 *  @return 接口
 */
- (NSString*)getLoginUrl
{
    switch (self.loginLine) {
            
        case kLoginEn_dev:
            
            return @"https://migrate.letsqyq.com/api";
        
            break;
        case kLoginEn_release: //发布环境 登录url

            return @"https://migrate.letsqyq.com/api";
            
            break;
      
        default:
            break;
    }
    
    return @"https://migrate.letsqyq.com/api";

}

-(NSString*)getReportErrorUrl{
    
  
    return nil;
    
    
}

#pragma mark - 设置默认的登录环境
+ (EMLoginEn)getInitLoginLine
{
  
        return kLoginEn_release;
}



@end
