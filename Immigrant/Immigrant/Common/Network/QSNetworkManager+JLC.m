//
//  QSNetworkManager+JLC.m
//  Immigrant
//
//  Created by jlc on 2018/12/24.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "QSNetworkManager+JLC.h"
#import "GYLoginEn.h"

@implementation QSNetworkManager (JLC)

+(void)getMobileCodeByRegister:(NSString*)mobile successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    
//    NSDictionary *parms = @{
//                            @"mobile":mobile,
//                            @"mobilecodetype":@"register"
//                          };
    
    NSString *path = [NSString stringWithFormat:@"/mobile/code/sendcode?mobile=%@&mobilecodetype=register",mobile];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+(void)getMobileCodeByResetPwd:(NSString*)mobile successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    
    
//    NSDictionary *parms = @{
//                            @"mobile":mobile,
//                            @"mobilecodetype":@"register"
//                            };
     NSString *path = [NSString stringWithFormat:@"/mobile/code/sendcode?mobile=%@&mobilecodetype=resetpassword",mobile];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/user/login 用户登录
 */
+(void)postUserLogin:(NSString*)mobile pwd:(NSString *)password successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
     [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/user/login?mobile=%@&password=%@",mobile,password] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
    POST /app/user/register 用户注册
 avatarkey (string, optional): 用户头像KEY ,
 email (string, optional): 电子邮箱 ,
 mobile (string, optional): 用户手机 ,
 nickname (string, optional): 用户昵称 ,
 password (string, optional): 密码 ,
 tag (string, optional): 用户签名 ,
 username (string, optional): 用户姓名 ,
 weixin (string, optional): 用户微信
 */
+(void)postUserRegister:(NSString*)mobile pwd:(NSString *)password nickname:(NSString *)nickname username:(NSString *)username weixin:(NSString *)weixin email:(NSString *)email successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
        NSDictionary *parms = @{
                                @"mobile":mobile,
                                @"password":password,
                                @"nickname":nickname,
                                @"username":username,
                                @"weixin":weixin,
                                @"email":email
                                };
        
        NSString *path = @"/app/user/register";
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
 POST /app/user/resetpassword 重置用户密码
 */
+(void)postUserResetPassword:(NSString*)mobile pwd:(NSString *)password successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/user/resetpassword?sessionId=%@&mobile=%@&password=%@",[QSUserManager shareInstance].sessionId,mobile,password];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 /app/user/finddetail 获取用户信息
 */
+(void)postUserDetail:(NSString *)sesstionId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
     [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/user/finddetail?sessionId=%@",sesstionId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 首页数据接口 /app/slider/findhouselist
 */
+(void)homeDatasuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/index/findindex" requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 移民项目列表 /app/migrate/findpage
 */
+(void)migrateNameLinek:(NSString*)countryLike withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"countryId":countryLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    NSString *path = @"/app/migrate/findpage";
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/migrate/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    }
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 移民项目风险评估列表 /app/migratedanger/findpage
 */
+(void)migrateRiskWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    NSString *path = @"/app/migratedanger/findpage";
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/migratedanger/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    }
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 移民项目列表详情页  /app/migrate/finddetai
 */
+(void)migrateDetial:(NSString *)migrateId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    
    NSString *path = [NSString stringWithFormat:@"/app/migrate/finddetail?id=%@",migrateId];
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/migrate/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,migrateId];
    }
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/country/findVisaCoutryList
 sessionId admin:5e5838d1-c4fb-44d2-9f78-26a1bb519420
 */
+(void)visaCoutryListsuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = @"/app/country/findVisaCoutryList";
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/country/findVisaCoutryList?sessionId=%@",[QSUserManager shareInstance].sessionId];
    }
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 签证中心列表 /app/Visa/findpage
 {
 "ascOrderBy": "servicefee",
 "countryIdIn": [
 0
 ],
 "descOrderBy": "string",
 "nameLike": "美国探亲访友签证",
 "pageIndex": 1,
 "pageSize": 10
 }
 */
+(void)visaIsSearch:(BOOL)isSearch ascOrderBy:(NSString *)ascOrderBy countryIdIn:(NSArray *)countryIdIn descOrderBy:(NSString *)descOrderBy nameLike:(NSString *)nameLike withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = @"/app/Visa/findpage";
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/Visa/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    }
    NSDictionary *parms;
    if (isSearch) {
        parms = @{
                  @"ascOrderBy":ascOrderBy,
                  @"countryIdIn":countryIdIn,
                  @"descOrderBy":descOrderBy,
                  @"nameLike":nameLike,
                  @"pageIndex":@(pageIndex),
                  @"pageSize":@(pageSize)
                  };
    }else{
        parms = @{
                  @"nameLike":nameLike,
                  @"pageIndex":@(pageIndex),
                  @"pageSize":@(pageSize)
                  };
    }
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 签证中心表详情页  /app/Visa/finddetail
 */
+(void)visaDetial:(NSString *)visaId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/Visa/finddetail?id=%@",visaId];
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/Visa/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,visaId];
    }
        
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 定制考察列表 /app/line/findpage
 */
+(void)inspectionNameLinek:(NSString*)nameLike withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"nameLike":nameLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/line/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 定制考察详情页  /app/line/finddetail
 */
+(void)inspectionDetial:(NSString *)insPectionId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
//    NSDictionary *parms = @{
//                            @"id":insPectionId
//                            };
//
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/line/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,insPectionId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 pp/policy/finddetail 政策咨询详情
 */
+(void)plicyId:(NSString*)plicyId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
//    NSDictionary *parms = @{
//                            @"id":@([plicyId longLongValue])
//                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/policy/finddetail?id=%@",plicyId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /pp/policy/findpage 政策咨询列表
 */
+(void)plicyPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/policy/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
/app/new/finddetail 趣闻详情页
 */
+(void)newsId:(NSString*)newsId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/new/finddetail?id=%@",newsId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/new/findpage 趣闻列表
 */
+(void)newsPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/new/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 app/slider/finddetial banner详情
 */
+(void)bannerId:(NSString*)bannerId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/slider/finddetail?id=%@",bannerId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/cate/findIndexList 商城首页接口
 */
+(void)mallsuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
     [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/cate/findIndexList"] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/migratesuccess/findpage 移民成功案列接口
 */
+(void)migratesuccess:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/migratesuccess/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/migratesuccess/finddetail 一名成功案例详情
 */
+(void)migrateDetailSuccess:(NSString *)detialId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/migratesuccess/finddetail?id=%@",detialId];
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/migratesuccess/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,detialId];
    }
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/goods/findpage 商城分类列表 搜索对应的id不给
 "cateId": 0,
 "nameLike": "Beats 蓝牙耳机",
 "pageIndex": 1,
 "pageSize": 10
 */
+(void)mallpaging:(NSString *)cateId nameLike:(NSString *)nameLike isSearch:(BOOL)search withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"cateId":cateId,
                            @"nameLike":nameLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    if (search) {
        parms = @{
                  @"cateId":cateId,
                  @"nameLike":nameLike,
                  @"pageIndex":@(pageIndex),
                  @"pageSize":@(pageSize)
                  };
    }
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/goods/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/goods/finddetail 商城商品详情
 id
 */
+(void)mallDetail:(NSString *)cateId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
      [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/goods/finddetail?id=%@",cateId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 /app/camp/findpage 夏令营列表
 "nameLike": "美国探亲访友签证",
 "pageIndex": 1,
 "pageSize": 10,
 */
+(void)campFindpageNameLinek:(NSString*)nameLike withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"nameLike":nameLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/camp/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 夏令营详情页  /app/camp/finddetail
 */
+(void)campDetial:(NSString *)campId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/camp/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId, campId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/house/findpage 海外房产列表
 "nameLike": "美国探亲访友签证",
 "pageIndex": 1,
 "pageSize": 10,
 */
+(void)houseFindpageNameLinek:(NSString*)nameLike withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"nameLike":nameLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/house/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 海外房产详情页  /app/house/finddetail
 */
+(void)houseDetial:(NSString *)houseId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/house/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId, houseId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/slider/findlist 获取轮播图
 sessionId
 slidertype
 index("首页轮播图"),
 visa("签证办理轮播图"),
 study("海外留学轮播图"),
 house("海外房产轮播图");
 */
+(void)takeBannerPhoto:(NSString *)type successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/slider/findlist?slidertype=%@",type];
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/slider/findlist?sessionId=%@&slidertype=%@",[QSUserManager shareInstance].sessionId,type];
    }
      [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 海外留学首页/app/index/findstudyindex
 */
+(void)findStudyIndexSuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/index/findstudyindex" requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 学院列表/app/school/findpage
 {
 ---ascOrderBy (string, optional): TUIMES升序则填：tuimes，QS升序则填：qs，。两者都升序则填：tuimes,qs ,
 chinesenameLike (string, optional): 中文名 ,
 countryId (integer, optional): 国家主键 ,
 ---descOrderBy (string, optional): TUIMES降序则填：tuimes，QS升序则填：qs，。两者都降降序则填：tuimes,qs ,
 isrecommend (boolean, optional): 是否推荐 ,
 pageIndex (integer, optional): 第几页，默认第一页 ,
 pageSize (integer, optional): 每页大小，默认10 ,
 professLike (string, optional): 专业 ,
 regionIdIn (Array[integer], optional): 地区主键集合
 }
 */
+(void)schoolFindpageChinesenameLike:(NSString*)chinesenameLike withAscOrderBy:(NSString*)ascOrderBy withDescOrderBy:(NSString*)descOrderBy withIsrecommend:(BOOL)isrecommend withCountryId:(NSString *)countryId withProfessLike:(NSString *)professLike withRegionIdIn:(NSArray *)regionIdInArray withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"ascOrderBy":ascOrderBy,
                            @"descOrderBy":descOrderBy,
                            @"chinesenameLike":chinesenameLike,
                            @"isrecommend":@(isrecommend),
                            @"professLike":professLike,
                            @"regionIdIn":regionIdInArray,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/school/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 学院详情页  /app/school/finddetail
 */
+(void)schoolDetial:(NSString *)schoolId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/school/finddetail?id=%@",schoolId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 APP海外留学（留学咨询）列表  /app/studyinfo/findpage
 countryIdIn (Array[integer], optional): 国家主键集合 ,
 pageIndex (integer, optional): 第几页，默认第一页 ,
 pageSize (integer, optional): 每页大小，默认10
 */
+(void)studyinfoFindpageCountryIdIn:(NSArray *)countryIdIn withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"countryIdIn":countryIdIn,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/studyinfo/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
APP海外留学（留学咨询）详情页  /app/studyinfo/finddetail
 */
+(void)studyinfoDetial:(NSString *)schoolId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/studyinfo/finddetail?id=%@",schoolId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
//APP海外留学（成功案例）接口列表  /app/studysuccess/findpage
 countryId (integer, optional): 国家主键 ,
 degreeLike (string, optional): 学习阶段 ,
 pageIndex (integer, optional): 第几页，默认第一页 ,
 pageSize (integer, optional): 每页大小，默认10
 */
+(void)studysuccessFindpageCountryIdIn:(NSString*)countryIdIn withDegreeLike :(NSString *)degreeLike withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"countryIdIn":countryIdIn,
                            @"degreeLike":degreeLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/studysuccess/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
//APP海外留学（成功案例）接口详情页  /app/studysuccess/finddetail
 */
+(void)studysuccessDetial:(NSString *)schoolId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/studysuccess/finddetail?id=%@",schoolId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/useraddress/findlist  我的地址列表查询
 sessionId
 */
+(void)mineAddressListsuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/useraddress/findlist?sessionId=%@",[QSUserManager shareInstance].sessionId];
     [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/useraddress/finddetial 我的地址详情
 sessionId
 id
 */
+(void)mineAddressDetial:(NSString *)addressId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/useraddress/finddetial?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,addressId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/useraddress/add 添加我的地址
 sessionId
 "address": "沛鸿大厦A2-506",
 "city": "深圳",
 "consignee": "一二三四",
 "county": "南山区",
 "isdefault": false,
 "mobile": "15814520421",
 "province": "广东"
 */
+(void)mineAddressAdd:(NSDictionary *)dict successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/useraddress/add?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:dict isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/useraddress/delete 删除我的地址
 sessionId
 id
 */
+(void)mineAddressDelete:(NSString *)addressId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/useraddress/delete?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,addressId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/useraddress/update 更新我的地址
 sessionId
 id
 "address": "沛鸿大厦A2-506",
 "city": "深圳",
 "consignee": "一二三四",
 "county": "南山区",
 "isdefault": false,
 "mobile": "15814520421",
 "province": "广东"
 */
+(void)mineAddressUpdateId:(NSString *)addressId withMsg:(NSDictionary *)dict successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/useraddress/update?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:dict isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
    反馈接口/app/feedback/add
 {
 content (string, optional): 内容 ,
 type (string, optional): 类别
 }
 */
+(void)feedBackAddContent:(NSString *)content withType:(NSString *)type successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"content":content,
                            @"type":type
                            };
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/feedback/add" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 APP消息列表  /app/message/findpage
 pageIndex (integer, optional): 第几页，默认第一页 ,
 pageSize (integer, optional): 每页大小，默认10
 */
+(void)messageFindpageWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:@"/app/message/findpage" requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 //APP消息列表接口详情页  /app/message/finddetail
 */
+(void)messageFindDetial:(NSString *)msgdId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:[NSString stringWithFormat:@"/app/message/finddetail?id=%@",msgdId] requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/index/findactivityindex app活动分类列表
 sessionId
 */
+(void)activityHomeListsuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/index/findactivityindex"];
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/index/findactivityindex?sessionId=%@",[QSUserManager shareInstance].sessionId];
    }
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/order/addOrderVisa 签证中心下单
 sessionId
 {
 "booknum": 3,
 "userAddressId": 0,
 "userEmail": "1106988868@qq.com",
 "userIdcard": "15814520421",
 "userTraveldate": "2019-01-03T18:08:11.096Z",
 "visaId": 0
 }
 */
+(void)visaCommitOrder:(NSDictionary *)dict successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderVisa?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:dict isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}


/**
 /app/order/finddetail 订单详情接口
 */
+(void)orderDetail:(NSString *)orderId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/order/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,orderId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/country/findMigrateCoutryList
 sessionId
 获取移民项目 所有的国家
 */
+(void)migrateContrysuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = @"/app/country/findMigrateCoutryList";
    if ([QSUserManager shareInstance].isLogin) {
        path = [NSString stringWithFormat:@"/app/country/findMigrateCoutryList?sessionId=%@",[QSUserManager shareInstance].sessionId];
    }
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}


//订单列表/app/order/findpage
+(void)orderFindpageOrderstatus:(NSString *)orderstatus withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"orderstatus":orderstatus,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
     NSString *path = [NSString stringWithFormat:@"/app/order/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//POST/app/pointflow/findpage积分列表operate (string, optional): 操作 = ['in', 'out'],
+(void)pointflowFindpageOperate:(NSString *)operate WithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"operate":operate,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/pointflow/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//待发放积分列表POST /app/pointsend/findpage
+(void)pointsendFindpageWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/pointsend/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//POST /app/favorites/findpage 我的收藏
+(void)favoritesFindpageOuttype:(NSString *)outtype withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"outtype":outtype,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
      NSString *path = [NSString stringWithFormat:@"/app/favorites/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//通讯录的好友匹配POST /app/social/findfirstfriendmobilelist
+(void)socialFindfirstfriendmobilelistsuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/social/findfirstfriendmobilelist?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//人脉关系最外层--POST /app/social/findfirstfriendpage
//第一个参数是用来搜索的姓名或者手机号
+(void)finFirstFriendFindpageNameormobile:(NSString *)nameormobile withPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                @"nameormobile":nameormobile,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/social/findfirstfriendpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//人脉关系的第二层数据POST /app/social/findfirstfriendlist
+(void)socialFindfirstfriendlistUserId:(NSString *)userId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/social/findfirstfriendlist?sessionId=%@&userId=%@",[QSUserManager shareInstance].sessionId,userId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//社交里面的获取详情 /app/social/finddetail
+(void)socialFindDetailsuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/social/finddetail?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

//个人中心---我的测评列表POST /app/usermigratequestion/findpage
+(void)usermigratequestionFindpageWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/usermigratequestion/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//个人中心---我的测评对应的详情POST /app/usermigratequestion/finddetail
+(void)usermigratequestionDetailWithId:(NSString *)usermigratequestionId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/usermigratequestion/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,usermigratequestionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
    
/**
/app/migratedanger/finddetail 移民评估详情
     sessionId
     id
*/
+(void)ImmigrationAssessmentDetail:(NSString *)detialId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/migratedanger/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,detialId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/**
 /app/usermigratequestion/add 移民条件测试提交
 */
+(void)ImmigrationAdd:(NSDictionary *)pram successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/usermigratequestion/add?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:pram isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//新增商品订单----积分商城里面的确认兑换哪里/app/order/addOrderGoods
+(void)appOrderaddOrderGoodsWithBookNum:(NSInteger )booknum withGoodsId:(NSString *)goodsId withAddressId:(NSString *)userAddressId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"booknum":@(booknum),
                            @"goodsId":goodsId,
                            @"userAddressId":userAddressId
                            };
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderGoods?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//团购返现POST /app/cashback/findpage
+(void)appCashBackFindpageWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/cashback/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

//个人中心--我的预约POST /app/callback/findpage
+(void)appCallbackkFindpageWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/callback/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//个人中心--我的预约POST /app/callback/update编辑
+(void)appCallBackUpdateWithId:(NSString *)callBackId withFirstsdate:(NSString *)firstsdate withFirstedate:(NSString *)firstedate withSecondsdate:(NSString *)secondsdate withSecondedate:(NSString *)secondedate successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"id":callBackId,
                            @"firstsdate":firstsdate,
                            @"firstedate":firstedate,
                            @"secondsdate":secondsdate,
                            @"secondedate":secondedate
                            };
    NSString *path = [NSString stringWithFormat:@"/app/callback/update?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//个人中心--我的预约POST /app/callback/finddetail详情
+(void)appCallBackFindDetailWithId:(NSString *)callBackId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/callback/update?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId,callBackId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//POST /app/callback/add 新增
/*
 callbacktype (string, optional): 回访方式 = ['visit', 'online', 'company'],
 companyaddress (string, optional): 面谈公司的公司地址（callbackype = company） ,
 firstedate (string, optional): 首选沟通结束时间 ,
 firstsdate (string, optional): 首选沟通开始时间 ,
 outId (integer, optional): 项目主键（针对outtype!=other的回访） ,
 outtype (string, optional): 项目类型 = ['other', 'migrate'],
 secondedate (string, optional): 次选沟通结束时间 ,
 secondsdate (string, optional): 次选沟通开始时间 ,
 userMobile (string, optional): 用户手机 ,
 userName (string, optional): 用户姓名 ,
 visitcity (string, optional): 上面拜访所在的城市（callbackype = visit）
 */
+(void)appCallBackAddCallbacktype:(NSString *)callbacktype withCompanyaddress:(NSString *)companyaddress withFirstsdate:(NSString *)firstsdate withFirstedate:(NSString *)firstedate withSecondsdate:(NSString *)secondsdate withSecondedate:(NSString *)secondedate withOutId:(NSString *)outId withOuttype:(NSString *)outtype withUserMobile:(NSString *)userMobile withUserName:(NSString *)userName withVisitcity:(NSString *)visitcity successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"callbacktype":callbacktype,
                            @"companyaddress":companyaddress,
                            @"firstsdate":firstsdate,
                            @"firstedate":firstedate,
                            @"secondsdate":secondsdate,
                            @"secondedate":secondedate,
                            @"outId":outId,
                            @"outtype":outtype,
                            @"userMobile":userMobile,
                            @"userName":userName,
                            @"visitcity":visitcity
                            };
    NSString *path = [NSString stringWithFormat:@"/app/callback/add?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//获取我的顾问POST /app/user/findSalesman
+(void)appUserFindSalesmanSuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/user/findSalesman?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//赚取积分 POST /app/point/finddetail
+(void)appPointFinddetailSuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/point/finddetail?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/* 领取任务--领取每日任务奖励（task1currentcount >= task1totalcount && isreceivetask1 = false 才能领取）POST /app/point/receivetask1
 --领取下载任务（task2currentcount >= task2totalcount && isreceivetask2 = false 才能领取）POST /app/point/receivetask2
 --领取下载任务（task3currentcount >= task3totalcount && isreceivetask3 = false 才能领取）POST /app/point/receivetask3
 */
+(void)appPointReceivetaskWithIndex:(NSInteger)index successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/point/receivetask%li?sessionId=%@",index,[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//要求回访---公司地址POST /app/companyaddress/findlist
+(void)appCompanyaddressFindListSuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/companyaddress/findlist?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 微信支付接口
 sessionId
 orderId
 */
+(void)appWeChatPay:(NSString *)orderId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/weixin/pay/payOrder?sessionId=%@&orderId=%@",[QSUserManager shareInstance].sessionId,orderId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 支付宝支付接口
 sessionId
 orderId
 */
+(void)appAliPay:(NSString *)orderId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/ali/pay/payOrder?sessionId=%@&orderId=%@",[QSUserManager shareInstance].sessionId,orderId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
 佣金提现接口
 POST /app/brokeragewithdraw/add
 */
+(void)appBrokeragewithdrawAdd:(double)amountyuan successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/brokeragewithdraw/add?sessionId=%@&amountyuan=%lf",[QSUserManager shareInstance].sessionId,amountyuan];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
 佣金流水
 POST /app/brokerageflow/findpage
 */
+(void)appBrokeragewithdrawFindpageWithPage:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"/app/brokerageflow/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
 获取当前用户的佣金详情
 POST /app/brokerage/finddetailByUser
 */
+(void)appBrokerageFinddetailByUserSuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/brokerage/finddetailByUser?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
//切换用户身份
//POST /h2s/salesman/loginByUser
+(void)H2sSalesmaLoginByUserSuccessBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/h2s/salesman/loginByUser?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
/*
 我的CRM
 mobileLike (string, optional): 客户电话 ,
 nameLike (string, optional): 客户姓名 ,
 pageIndex (integer, optional): 第几页，默认第一页 ,
 pageSize (integer, optional): 每页大小，默认10
 POST /h5s/customer/findInterestedPage 获取意向客户分页
  POST /h5s/customer/findIntroducedPage 获取新派客户分页
  POST /h5s/customer/findSignedPage 获取签约客户分页
 */
+(void)H5CustomerFindInterestedPageWithUrl:(NSString *)urlStr WithmobileLike:(NSString *)mobileLike withNameLike:(NSString *) nameLike WithSessionId:(NSString *)sessionId WithPageIndex:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"mobileLike":mobileLike,
                            @"nameLike":nameLike,
                            @"pageIndex":@(pageIndex),
                            @"pageSize":@(pageSize)
                            };
    NSString *path = [NSString stringWithFormat:@"%@?sessionId=%@",urlStr,sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
///*
// 我的CRM
// mobileLike (string, optional): 客户电话 ,
// nameLike (string, optional): 客户姓名 ,
// pageIndex (integer, optional): 第几页，默认第一页 ,
// pageSize (integer, optional): 每页大小，默认10
// POST /h5s/customer/findIntroducedPage 获取新派客户分页
// */
//+(void)H5CustomerFindIntroducedPageWithmobileLike:(NSString *)mobileLike withNameLike:(NSString *) nameLike  WithSessionId:(NSString *)sessionId WithPageIndex:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
//    NSDictionary *parms = @{
//                            @"mobileLike":mobileLike,
//                            @"nameLike":nameLike,
//                            @"pageIndex":@(pageIndex),
//                            @"pageSize":@(pageSize)
//                            };
//    NSString *path = [NSString stringWithFormat:@"/h5s/customer/findIntroducedPage?sessionId=%@",sessionId];
//    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
//}
///*
// 我的CRM
// mobileLike (string, optional): 客户电话 ,
// nameLike (string, optional): 客户姓名 ,
// pageIndex (integer, optional): 第几页，默认第一页 ,
// pageSize (integer, optional): 每页大小，默认10
// POST /h5s/customer/findSignedPage 获取签约客户分页
// */
//+(void)H5CustomerFindSignedPageWithmobileLike:(NSString *)mobileLike withNameLike:(NSString *) nameLike WithSessionId:(NSString *)sessionId WithPageIndex:(NSInteger)pageIndex withPageSize:(NSInteger)pageSize successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
//    NSDictionary *parms = @{
//                            @"mobileLike":mobileLike,
//                            @"nameLike":nameLike,
//                            @"pageIndex":@(pageIndex),
//                            @"pageSize":@(pageSize)
//                            };
//    NSString *path = [NSString stringWithFormat:@"/h5s/customer/findSignedPage?sessionId=%@",sessionId];
//    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
//}

/**
 /app/callcenter/add 在线客服添加绘画
 sessionId
 {
 "imagekey": "img1.png",
 "message": "你好，我想了解一些问题",
 "userId": 0
 }
 */
+(void)addOnlineASession:(NSString *)userId imagekey:(NSString *)imagekey message:(NSString *)message successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSDictionary *parms = @{
                            @"userId":userId,
                            @"imagekey":imagekey,
                            @"message":message
                            };
    NSString *path = [NSString stringWithFormat:@"/app/callcenter/add?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 /app/callcenter/findlist
 获取会话列表
 sessionId
 */
+(void)lookForcallcentergmtCreatedGreaterThan:(NSString *)gmtCreatedGreaterThan successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/app/callcenter/findlist?sessionId=%@",[QSUserManager shareInstance].sessionId];
    NSDictionary *dict = @{@"gmtCreatedGreaterThan":gmtCreatedGreaterThan};
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:dict isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

/**
 获取图片上传链接
 /base/sys/app/getOSSUploadUrl
 sessionId
 suffix
 contentType
 */
+(void)getOSSUploadUrl:(NSString *)suffix contentType:(NSString *)contentType successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock{
    NSString *path = [NSString stringWithFormat:@"/base/sys/app/getOSSUploadUrl?sessionId=%@&suffix=%@&contentType=%@",[QSUserManager shareInstance].sessionId,suffix,contentType];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance]getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}


@end
