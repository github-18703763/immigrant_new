//
//  QSNetworkManager+Order.m
//  Immigrant
//
//  Created by 徐盖粦 on 2019/2/16.
//  Copyright © 2019 张波. All rights reserved.
//

#import "QSNetworkManager+Order.h"
#import "GYLoginEn.h"
#import <YYModel.h>
@implementation QSNetworkManager (Order)

+ (void)addCampOrder:(OrderCampAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderCamp?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)addCampOrderStandby:(OrderCampAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderCampStandby?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)addHouseOrder:(OrderHouseAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderHouse?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)addLineOrder:(OrderLineAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderLine?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)addLineOrderStandby:(OrderLineAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderLineStandby?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)addMigrateOrder:(OrderMigrateAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderMigrate?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)addStudyOrder:(OrderStudyAddModel *)model successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSDictionary *parms = [model yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"/app/order/addOrderStudy?sessionId=%@",[QSUserManager shareInstance].sessionId];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}
+ (void)studyFindDetail:(NSInteger)studyId successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSString *path = [NSString stringWithFormat:@"/app/study/finddetail?sessionId=%@&id=%@",[QSUserManager shareInstance].sessionId, @(studyId)];
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)studyFindPage:(NSInteger)pageIndex nameLike:(NSString *)nameLike successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSString *path = [NSString stringWithFormat:@"/app/study/findpage?sessionId=%@",[QSUserManager shareInstance].sessionId];
    NSMutableDictionary *parms = @{
        @"pageIndex" : @(pageIndex),
        @"pageSize" : @(10),
    }.mutableCopy;
    
    parms[@"nameLike"] = nameLike;
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:parms isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)favoriteAdd:(NSInteger)outId outType:(NSString *)outType successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSString *path = [NSString stringWithFormat:@"/app/favorites/add?sessionId=%@&outId=%@&outtype=%@",[QSUserManager shareInstance].sessionId,@(outId),outType];
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)favoriteDelete:(NSInteger)outId outType:(NSString *)outType successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSString *path = [NSString stringWithFormat:@"/app/favorites/delete?sessionId=%@&outId=%@&outtype=%@",[QSUserManager shareInstance].sessionId,@(outId),outType];
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)migratequestionList:(NSString *)ids successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSString *path = [NSString stringWithFormat:@"/app/migratequestion/findlist?sessionId=%@&ids=%@",[QSUserManager shareInstance].sessionId,ids];
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}

+ (void)migrateFindlistByrequire:(NSArray *)indexArray successBlock:(QSResponseSuccess)block failBlock:(QSResponseFail)failBlock {
    NSString *path = [NSString stringWithFormat:@"/app/migrate/findlistByrequire?sessionId=%@&require0=%@&require1=%@&require2=%@&require3=%@&require4=%@&require5=%@",[QSUserManager shareInstance].sessionId,indexArray[0],indexArray[1],indexArray[2],indexArray[3],indexArray[4],indexArray[5]];
    
    [self networkRequestWithHost:[[GYLoginEn sharedInstance] getLoginUrl] Path:path requestType:QSRequestTypePost parameters:nil isNeedCache:NO successBlock:block failureBlock:failBlock progress:nil];
}


@end
