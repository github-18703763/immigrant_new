//
//  QSNetResponseModel.m
//  QuickBidSmart
//
//  Created by user001 on 2016/12/27.
//  Copyright © 2016年 gogobids. All rights reserved.
//

#import "QSNetResponseModel.h"

@implementation QSNetResponseModel


+ (instancetype)objectWithobject:(id)responseObject {
    
    QSNetResponseModel *model = [QSNetResponseModel new];
    
   
    if ([responseObject objectForKey:@"msg"]) {
        
        model.msg = [responseObject objectForKey:@"msg"];
    }
    
    if ([responseObject objectForKey:@"status"]) {
        
        model.status = [[responseObject objectForKey:@"status"]integerValue];
    }
    
    if ([responseObject objectForKey:@"data"]) {
        
        model.data = [responseObject objectForKey:@"data"];
    }
    return model;
}

@end
