//
//  QSNetworkManager.h
//  QuickBidSmart
//
//  Created by MacLw on 17/8/10.
//  Copyright © 2017年 gogobids. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QSNetResponseModel.h"

@class AFURLSessionManager;

#define kSucsessCode 0
#define QSNetWorkManager [QSNetworkManager sharedNetworkManager]

typedef NS_ENUM(NSUInteger, QSNetworkStatus)
{
    /*! 未知网络 */
    QSNetworkStatusUnknown           = 0,
    /*! 没有网络 */
    QSNetworkStatusNotReachable,
    /*! 手机 3G/4G 网络 */
    QSNetworkStatusReachableViaWWAN,
    /*! wifi 网络 */
    QSNetworkStatusReachableViaWiFi
};

/*！定义请求类型的枚举 */
typedef NS_ENUM(NSUInteger, QSRequestType)
{
    /*! get请求 */
    QSRequestTypeGet = 0,
    /*! post请求 */
    QSRequestTypePost,
};

typedef NS_ENUM(NSUInteger, QSRequestSerializer) {
    /** 设置请求数据为JSON格式*/
    QSRequestSerializerJSON,
    /** 设置请求数据为HTTP格式*/
    QSRequestSerializerHTTP,
};

typedef NS_ENUM(NSUInteger, QSResponseSerializer) {
    /** 设置响应数据为JSON格式*/
    QSResponseSerializerJSON,
    /** 设置响应数据为HTTP格式*/
    QSResponseSerializerHTTP,
};

typedef NS_ENUM(NSUInteger, QSHostLocation){
    WebServerDomain,        //API
    BidServerDomain,        //竞拍
    QNServerDomain,         //七牛
    PayServerDomain,        //支付
    ActivateAppDomain,      //激活统计
    TempServerDomin,        //临时
    LogServerDomain         //日志
};

/*! 实时监测网络状态的 block */
typedef void(^QSNetworkStatusBlock)(QSNetworkStatus status);

/*! 定义请求成功的 block */
typedef void( ^ QSResponseSuccess)(QSNetResponseModel* _Nonnull responseModel);
/*! 定义请求失败的 block */
typedef void( ^ QSResponseFail)(NSError *_Nullable error);

/*! 定义上传进度 block */
typedef void( ^ QSUploadProgress)(int64_t bytesProgress,
int64_t totalBytesProgress);
/*! 定义下载进度 block */
typedef void( ^ QSDownloadProgress)(int64_t bytesProgress,
int64_t totalBytesProgress);

/*!
 *  方便管理请求任务。执行取消，暂停，继续等任务.
 *  - (void)cancel，取消任务
 *  - (void)suspend，暂停任务
 *  - (void)resume，继续任务
 */
typedef NSURLSessionTask QSURLSessionTask;
static NSString * const  NeedLogin  = @"needLogin";

@interface QSNetworkManager : NSObject

/**
 创建的请求的超时间隔（以秒为单位），此设置为全局统一设置一次即可，默认超时时间间隔为30秒。
 */
@property (nonatomic, assign) NSTimeInterval timeoutInterval;

/**
 设置网络请求参数的格式，此设置为全局统一设置一次即可，默认：QSRequestSerializerJSON
 */
@property (nonatomic, assign) QSRequestSerializer requestSerializer;

/**
 设置服务器响应数据格式，此设置为全局统一设置一次即可，默认：QSResponseSerializerJSON
 */
@property (nonatomic, assign) QSResponseSerializer responseSerializer;

/*!
 *  获得全局唯一的网络请求实例单例方法
 *
 *  @return 网络请求类QSNetManager单例
 */
+ (instancetype _Nullable)sharedNetworkManager;

+(AFURLSessionManager *) sharedManager;

/**
 网络请求的实例方法 get
 
 @param host         服务器主机地址
 @param path         接口请求 url
 @param parameters   请求的参数
 @param successBlock 请求成功的回调
 @param failureBlock 请求失败的回调
 
 @return 方便管理请求任务。执行取消，暂停，继续等任务.
 */
+ (QSURLSessionTask *_Nullable)getRequestTypeWithHost:(QSHostLocation)host
                                                 path:(NSString *_Nullable)path
                                           parameters:(NSDictionary *_Nullable)parameters
                                         successBlock:(QSResponseSuccess _Nullable)successBlock
                                         failureBlock:(QSResponseFail _Nullable)failureBlock;

/**
 网络请求的实例方法 post
 
 @param host         服务器主机地址
 @param path         接口请求 url
 @param parameters   请求的参数
 @param successBlock 请求成功的回调
 @param failureBlock 请求失败的回调
 
 @return 方便管理请求任务。执行取消，暂停，继续等任务.
 */
+ (QSURLSessionTask *_Nullable)postRequestTypeWithHost:(QSHostLocation)host
                                                  path:(NSString *_Nullable)path
                                            parameters:(NSDictionary *_Nullable)parameters
                                          successBlock:(QSResponseSuccess _Nullable)successBlock
                                          failureBlock:(QSResponseFail _Nullable)failureBlock;

/**
 网络请求的实例方法
 
 @param host         服务器主机地址
 @param path         接口请求 url
 @param type         get / post / put / delete
 @param parameters   请求的参数
 @param isNeedCache 是否需要缓存，只有 get / post 请求有缓存配置
 @param successBlock 请求成功的回调
 @param failureBlock 请求失败的回调
 @param progress     进度
 
 @return 方便管理请求任务。执行取消，暂停，继续等任务.
 */

+ (QSURLSessionTask *_Nullable)networkRequestWithHost:(NSString*)host Path:(NSString *)path
                                 requestType:(QSRequestType)type
                                           parameters:(NSDictionary *_Nullable)parameters
                                 isNeedCache:(BOOL)isNeedCache
                                         successBlock:(QSResponseSuccess _Nullable )successBlock
                                         failureBlock:(QSResponseFail _Nullable )failureBlock
                                             progress:(QSDownloadProgress)progress;


+ (QSURLSessionTask *_Nullable)networkRequestWithPath:(NSString *_Nullable)path
                                          requestType:(QSRequestType)type
                                           parameters:(NSDictionary *_Nullable)parameters
                                          isNeedCache:(BOOL)isNeedCache
                                         successBlock:(QSResponseSuccess _Nullable)successBlock
                                         failureBlock:(QSResponseFail _Nullable)failureBlock
                                             progress:(QSDownloadProgress _Nullable)progress;


+ (QSURLSessionTask *_Nullable)weatherNetworkRequestWithPath:(NSString *_Nullable)path
                                        requestType:(QSRequestType)type
                                         parameters:(NSDictionary *_Nullable)parameters
                                        isNeedCache:(BOOL)isNeedCache
                                       successBlock:(QSResponseSuccess _Nullable)successBlock
                                       failureBlock:(QSResponseFail _Nullable)failureBlock
                                           progress:(QSDownloadProgress _Nullable)progress;

+(QSURLSessionTask *_Nullable)basicRequestFullPath:(NSString *_Nonnull)path
                              requestType:(QSRequestType)type
                                        parameters:(NSDictionary *_Nullable)parameters
                              isNeedCache:(BOOL)isNeedCache
                                      successBlock:(QSResponseSuccess _Nullable )successBlock
                                      failureBlock:(QSResponseFail _Nullable )failureBlock
                                          progress:(QSDownloadProgress _Nonnull )progress;

//取消所有的请求
+ (void)cancelAllRequest;

//取消指定URL的请求
+ (void)cancelRequestWithURL:(NSString *_Nullable)URL;

//生成请求完整路径
+ (NSString *_Nullable)createRequestURLWithHost:(QSHostLocation)host path:(NSString *_Nullable)path;

//删除请求头
+ (void)clearAuthorizationHeader;

#pragma mark - 网络状态监测
+ (void)startNetWorkMonitoringWithBlock:(QSNetworkStatusBlock _Nullable)networkStatus;

+(NSString*)appendDeviceString;

@end
