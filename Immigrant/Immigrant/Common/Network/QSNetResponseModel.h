//
//  QSNetResponseModel.h
//  QuickBidSmart
//
//  Created by user001 on 2016/12/27.
//  Copyright © 2016年 gogobids. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QSNetResponseModel : NSObject

@property (nonatomic, strong)id data;
@property (nonatomic, copy)NSString * msg;
@property (nonatomic, assign)NSInteger status;

+ (instancetype)objectWithobject:(id)responseObject;

@end
