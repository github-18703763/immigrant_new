//
//  ListViewController.h
//  BingProject
//
//  Created by JordanCZ on 2017/1/10.
//  Copyright © 2017年 冰棍科技. All rights reserved.
//


@interface ListViewController : UIViewController

/**
 列表样式
 */
@property (nonatomic, assign) UITableViewStyle tableViewStyle;

/**
 *  列表数据
 */
@property (nonatomic, strong) NSMutableArray *tableData;

/**
 *  列表
 */
@property (nonatomic, strong, readonly) UITableView *tableView;

/**
 *  需要子类重写，用于发送请求
 *
 *  @param isRefresh YES为下拉刷新 NO为上拉更多
 */
- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh;

@end

