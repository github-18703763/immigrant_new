//
//  GYBaseWebVCViewController.h
//  Immigrant
//
//  Created by jlc on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface GYBaseWebVC : UIViewController

-(id)initWithUrl:(NSString*)urlString;

@end
