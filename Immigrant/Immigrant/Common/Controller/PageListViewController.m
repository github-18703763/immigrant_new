//
//  PageListViewController.m
//  OCBase
//
//  Created by 徐盖粦 on 2018/4/17.
//  Copyright © 2018年 徐盖粦. All rights reserved.
//

#import "PageListViewController.h"
#import <MJRefresh.h>
@interface PageListViewController ()



@end

@implementation PageListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pageIndex = 1;
    
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestHandlerWishIsRefresh:NO];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark: - Private Methods


/**
 接口调用失败时停止刷新

 @param msg 失败信息
 */
- (void)apiFailed:(NSString *)msg {
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
    [MBProgressHUD showError:msg];
//    [self showHudWithText:msg isNeedLoading:false autoHideAfterDelay:2];
}


/**
 使用该方法接受返回数据

 @param data 返回数据
 @param isRefresh 是否是刷新
 */
- (void)handleResponseData:(NSArray *)data isRefresh:(BOOL)isRefresh {
    if (isRefresh) {
        [self.tableView.mj_header endRefreshing];
        self.pageIndex += 1;
        [self.tableData removeAllObjects];
        [self.tableData addObjectsFromArray:data];
        if (self.tableData.count != 10) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } else {
        self.pageIndex += 1;
        
        [self.tableData addObjectsFromArray:data];
        [self.tableView reloadData];
        if (self.tableData.count != 10) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
    }
}

#pragma mark - DZNEmptyDataSetDelegate

- (void)emptyDataSetWillAppear:(UIScrollView *)scrollView;
{
    scrollView.mj_footer.hidden = YES;
}

- (void)emptyDataSetDidDisappear:(UIScrollView *)scrollView;
{
    scrollView.mj_footer.hidden = NO;
}

@end
