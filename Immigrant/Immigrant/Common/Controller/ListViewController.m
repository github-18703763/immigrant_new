//
//  ListViewController.m
//  BingProject
//
//  Created by JordanCZ on 2017/1/10.
//  Copyright © 2017年 冰棍科技. All rights reserved.
//

#import "ListViewController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <Masonry.h>
#import <MJRefresh.h>
#import "UITableViewCell+identifier.h"

@interface ListViewController ()<UITableViewDataSource, UITableViewDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.tableViewStyle == UITableViewStyleGrouped) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    } else {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
        if (@available(iOS 11, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        
    }
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.separatorColor = [UIColor colorWithHexString:@"#e1e1e1"];
//    self.tableView.backgroundColor = [UIColor colorWithHexString:@"#f7f8fe"];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];

    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestHandlerWishIsRefresh:YES];
    }];
    
    
}

#pragma mark - Public Methods
- (void)requestHandlerWishIsRefresh:(BOOL)isRefresh
{
    
}

- (void)beginRefreshing
{
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshing
{
    [self.tableView.mj_header endRefreshing];
}

- (void)endLoadMore
{
    [self.tableView.mj_footer endRefreshing];
}

- (void)endLoadMoreWithNoMoreData
{
    [self.tableView.mj_footer endRefreshingWithNoMoreData];
}
#pragma mark - Private Methods

#pragma mark - UITableView DataSource
/**
 *  必须实现，如果子类没有实现这个方法，调用[self.tableView.dataSource numberOfSectionsInTableView:self.tableView]会crach
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UITableViewCell cellForTableView:tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView;
{
    return [[NSAttributedString alloc] initWithString:@"暂时没有数据哟" attributes:nil];
}

//- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView;
//{
//    return [UIImage imageNamed:@"consulting_bg"];
//}

#pragma mark - DZNEmptyDataSetDelegate

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView;
{
    return YES;
}

#pragma mark -- Getter And Setter

- (NSMutableArray *)tableData
{
    if (!_tableData) {
        _tableData = [NSMutableArray array];
    }
    return _tableData;
}

@end 
    
