//
//  PageListViewController.h
//  OCBase
//
//  Created by 徐盖粦 on 2018/4/17.
//  Copyright © 2018年 徐盖粦. All rights reserved.
//

#import "ListViewController.h"

@interface PageListViewController : ListViewController

@property (nonatomic, assign) NSInteger pageIndex;
/**
 接口调用失败时停止刷新
 
 @param msg 失败信息
 */
- (void)apiFailed:(NSString *)msg;

/**
 使用该方法接受返回数据
 
 @param data 返回数据
 @param isRefresh 是否是刷新
 */
- (void)handleResponseData:(NSArray *)data isRefresh:(BOOL)isRefresh;

@end
