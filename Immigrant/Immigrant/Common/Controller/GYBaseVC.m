//
//  GYBaseVC.m
//  Immigrant
//
//  Created by jlc on 2018/12/19.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYBaseVC.h"
#import "GYLoginRegisterVC.h"

@interface GYBaseVC ()

@end

@implementation GYBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentLogin) name:NeedLogin object:nil];
}

- (void)presentLogin {
    GYLoginRegisterVC *vc = [[GYLoginRegisterVC alloc] init];
    [self presentViewController:[[XKNavigationController alloc] initWithRootViewController:vc] animated:true completion:nil];
}

@end


