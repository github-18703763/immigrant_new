//
//  GYBaseWebVCViewController.m
//  Immigrant
//
//  Created by jlc on 2018/12/29.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "GYBaseWebVC.h"
#import "MQGradientProgressView.h"

@interface GYBaseWebVC ()<WKUIDelegate,WKNavigationDelegate,UIScrollViewDelegate>

@property (strong,nonatomic) WKWebView  *webView;

@property (nonatomic,strong) NSString *urlString;

@property (nonatomic, strong)  MQGradientProgressView *gradProgressView;

@end

@implementation GYBaseWebVC

#pragma mark - lifecyle

-(id)initWithUrl:(NSString*)urlString{
    
    self  = [super init];
    
    if (self) {
        
        self.urlString = urlString;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.webView];
    
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
//        make.top.mas_equalTo(StatusbarHeight);
          make.top.mas_equalTo(0);
        
        make.bottom.equalTo(self.view.mas_bottom).offset(-(KIsiPhoneX?28:0));
        
    }];
    
    self.gradProgressView =  [[MQGradientProgressView alloc] initWithFrame:CGRectMake(0, StatusbarHeight, SystemScreenWidth, 2)];
    
    [self.view addSubview:self.gradProgressView];
    
    [self.gradProgressView setProgress:0];
    
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    
    
    if (!self.urlString) {
        
        self.urlString = @"http://g.hiphotos.baidu.com/image/pic/item/d4628535e5dde711e61a706caaefce1b9c16615b.jpg";
        
    }
    
    [self.webView loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    
}

-(void)dealloc{
    
    self.webView.scrollView.delegate = nil;

}

#pragma mark - lazy

-(WKWebView *)webView{
    if(!_webView){

        _webView=[[WKWebView alloc]initWithFrame:CGRectZero];
        
        _webView.UIDelegate=self;
        _webView.navigationDelegate=self;
        _webView.scrollView.delegate = self;
        _webView.allowsBackForwardNavigationGestures = NO;
        _webView.opaque =NO;
        _webView.contentScaleFactor = 1;
        
        _webView.backgroundColor=[UIColor clearColor];
        
        if (@available(iOS 11.0, *)){
            [_webView.scrollView setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        }
        
    }
    
    return _webView;
}

#pragma mark- -KVO监听WebView
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        
        [self.gradProgressView setProgress:self.webView.estimatedProgress];
        
        
        if (self.webView.estimatedProgress  >= 1.0f ) {
            
            [UIView animateWithDuration:0.2f animations:^{
                self.gradProgressView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                if (finished) {
                    self.gradProgressView.progress = 0.0f;
                }
                
            }];
        }

    }
}

#pragma mark - WKWebViewDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    
}

@end
