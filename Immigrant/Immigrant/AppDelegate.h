//
//  AppDelegate.h
//  Immigrant
//
//  Created by 张波 on 2018/12/10.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

