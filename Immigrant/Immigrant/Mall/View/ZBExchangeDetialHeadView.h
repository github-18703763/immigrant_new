//
//  ZBExchangeDetialHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMallProjectObject.h"

@interface ZBExchangeDetialHeadView : UIView

@property (nonatomic,strong)ZBMallProjectObject *modelMallProject;

@end
