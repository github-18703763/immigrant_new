//
//  ZBMallCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMallCell.h"
#import "ZBSureExchangeController.h"
#import "ZBMallController.h"

@interface ZBMallCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *exchangeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLab;

@end 

@implementation ZBMallCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.exchangeBtn.layer.cornerRadius = 19;
    self.exchangeBtn.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
}

-(void)setModel:(ZBMallProjectObject *)model{
    _model = model;
    [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@"dingzhikaocha_bga"]];
    self.titleLab.text = _model.cateName;
    self.numberLab.text = [NSString stringWithFormat:@"%@积分",_model.point];
    self.payMoneyLab.text = @"缺字段";
}

- (IBAction)exchangeAction:(id)sender {
//    ZBLog(@"立即兑换");
    ZBSureExchangeController *sure = [ZBSureExchangeController new];
//    sure.model = self.model;
    if (![[self currentViewController] isKindOfClass:[ZBMallController class]]) {
        [[self currentViewController].navigationController pushViewController:sure animated:YES];
        return;
    }
    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:sure];
    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
