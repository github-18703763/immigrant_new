//
//  ZBPointRuleView.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/14.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBPointRuleView.h"
@interface ZBPointRuleView()
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;

@end

@implementation ZBPointRuleView
-(void)awakeFromNib{
    [super awakeFromNib];
    self.viewContent.layer.cornerRadius = 15;

    //客服
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    NSMutableAttributedString *one = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"积分满"]];
     [text appendAttributedString:one];
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"wodejifen_jifen"];
    attach.bounds = CGRectMake(-5, -2, 12, 12);
    NSAttributedString *zero_1 = [NSAttributedString attributedStringWithAttachment:attach];
    [text appendAttributedString:zero_1];
    NSMutableAttributedString *two = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"1000积分可兑换KIindle \n"]];
    [text appendAttributedString:two];
    
    NSMutableAttributedString *three = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"积分满"]];
    [text appendAttributedString:three];
    NSTextAttachment *attach2 = [[NSTextAttachment alloc] init];
    attach2.image = [UIImage imageNamed:@"wodejifen_jifen"];
    attach2.bounds = CGRectMake(-5, -2, 12, 12);
    NSAttributedString *zero_2 = [NSAttributedString attributedStringWithAttachment:attach2];
    [text appendAttributedString:zero_2];
    NSMutableAttributedString *four = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"2000积分可兑换ipad\n"]];
    [text appendAttributedString:four];
    
    NSMutableAttributedString *five = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"积分满"]];
    [text appendAttributedString:five];
    NSTextAttachment *attach3 = [[NSTextAttachment alloc] init];
    attach3.image = [UIImage imageNamed:@"wodejifen_jifen"];
    attach3.bounds = CGRectMake(-5, -2, 12, 12);
    NSAttributedString *zero_3 = [NSAttributedString attributedStringWithAttachment:attach3];
    [text appendAttributedString:zero_3];
    NSMutableAttributedString *six = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"5000积分可兑换相机\n"]];
    [text appendAttributedString:six];

    self.detailLab.attributedText = text;
}
- (IBAction)btnCancle:(id)sender {
      [self removeFromSuperview];
}

@end
