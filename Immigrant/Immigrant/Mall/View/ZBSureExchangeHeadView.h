//
//  ZBSureExchangeHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMallProjectObject.h"

@interface ZBSureExchangeHeadView : UIView

@property (nonatomic,strong) ZBMallProjectObject  *model;
@property (weak, nonatomic) IBOutlet UILabel *returnNameTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddress;

@property (weak, nonatomic) IBOutlet UILabel *returnName;
@property (weak, nonatomic) IBOutlet UILabel *returnPhone;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
//默认的文案
@property (weak, nonatomic) IBOutlet UILabel *defaultAddressLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgFollowDefault;
@property (weak, nonatomic) IBOutlet UIView *contianerTwo;

@end
