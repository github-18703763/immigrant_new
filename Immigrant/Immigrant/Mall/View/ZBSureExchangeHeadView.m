//
//  ZBSureExchangeHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSureExchangeHeadView.h"

@interface ZBSureExchangeHeadView()
@property (weak, nonatomic) IBOutlet UIView *contianerOne;
@property (weak, nonatomic) IBOutlet UIImageView *photoOne;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *countLab;

@property (weak, nonatomic) IBOutlet UILabel *pointLab;


@end

@implementation ZBSureExchangeHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.contianerOne.layer.cornerRadius = 15;
    self.contianerOne.layer.masksToBounds = YES;
    self.contianerTwo.layer.cornerRadius = 15;
    self.contianerTwo.layer.masksToBounds = YES;
    self.photoOne.layer.cornerRadius = 8;
    self.photoOne.layer.masksToBounds = YES;
}
//
-(void)setModel:(ZBMallProjectObject *)model{
    _model = model;
     [self.photoOne sd_setImageWithURL:[NSURL URLWithString:_model.cover] placeholderImage:[UIImage imageNamed:@"dingzhikaocha_bga"]];
    self.titleLab.text = _model.name;
    self.countLab.text = [NSString stringWithFormat:@"x1"];
    self.pointLab.text = [NSString stringWithFormat:@"%@积分",_model.point];
    
}
@end
