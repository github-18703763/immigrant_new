//
//  ZBExchangeAlertView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBExchangeAcceptanceAlertView : UIView

@property (nonatomic, copy) void(^sureBlock)(void);
@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;
@property (weak, nonatomic) IBOutlet UITextField *codeTxt;
@end
