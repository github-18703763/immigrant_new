//
//  ZBExchangeAlertView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBExchangeAlertView.h"

@interface ZBExchangeAlertView()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIButton *concalBtn;
@property (nonatomic,weak) IBOutlet UILabel *noticeLB;
@end

@implementation ZBExchangeAlertView
-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
    self.concalBtn.layer.cornerRadius = 25;
    self.concalBtn.layer.masksToBounds = YES;
}
- (IBAction)closeAction:(id)sender {
    [self removeFromSuperview];
}
- (IBAction)sureAction:(id)sender {
    if (self.sureBlock) {
        self.sureBlock();
        [self removeFromSuperview];
    }
}

- (IBAction)concalAction:(id)sender {
    [self removeFromSuperview];
}
-(void)setNoticeStr:(NSString *)noticeStr{
    _noticeStr=noticeStr;
    _noticeLB.text=noticeStr;
}


@end
