//
//  ZBExchangeDetialHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBExchangeDetialHeadView.h"

@interface ZBExchangeDetialHeadView()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *pointLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;

@end

@implementation ZBExchangeDetialHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 15;
    self.photoImg.layer.masksToBounds = YES;
    self.pointView.layer.cornerRadius = 4;
    self.pointView.layer.masksToBounds = YES;
}
//积分兑换
-(void)setModelMallProject:(ZBMallProjectObject *)modelMallProject{
    _modelMallProject = modelMallProject;
     [self.photoImg sd_setImageWithURL:[NSURL URLWithString:_modelMallProject.cover] placeholderImage:[UIImage imageNamed:@"xueyuanliebiao_bg"]];
    self.titleLab.text = _modelMallProject.name;
    self.pointLab.text = [NSString stringWithFormat:@"%@积分",_modelMallProject.point];
    self.detailLab.text = _modelMallProject.content;
}
@end
