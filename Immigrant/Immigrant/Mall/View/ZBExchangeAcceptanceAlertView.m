//
//  ZBExchangeAlertView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBExchangeAcceptanceAlertView.h"

@interface ZBExchangeAcceptanceAlertView()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (nonatomic, assign)NSUInteger codeNumberInt;
@property (strong,nonatomic)NSTimer *timer;
@property (strong,nonatomic)NSString *strMsgCode;//手机验证码

@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@end

@implementation ZBExchangeAcceptanceAlertView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
}


- (IBAction)closeAction:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)sureAction:(id)sender {
    if (![self.codeTxt.text isEqualToString:self.strMsgCode]) {
         [[UIApplication sharedApplication].delegate.window makeToast:@"验证码不正确" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    if (self.sureBlock) {
        self.sureBlock();
        [self removeFromSuperview];
    }
}
//获取验证码
- (IBAction)btnCode:(UIButton *)sender {
    //1、判断手机号码的格式是否正确
    if (![self checkPhoneNumInput:self.phoneTxt.text]) {
        return;
    }
    sender.userInteractionEnabled = NO;
    //按钮开始倒计时
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshBtnTitle:) userInfo:nil repeats:YES];
    
    [self requestCode];
    
}
//请求验证码
- (void)requestCode{
    WS(weakSELF);
    [QSNetworkManager getMobileCodeByRegister:self.phoneTxt.text successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.strMsgCode = responseModel.data[@"bussData"];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            self.codeBtn.userInteractionEnabled = YES;
            self.codeNumberInt = 0;
            [self refreshBtnTitle:weakSELF.timer];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        self.codeBtn.userInteractionEnabled = YES;
        self.codeNumberInt = 0;
        [self refreshBtnTitle:weakSELF.timer];
    }];
}

//检查是否为手机号的方法
-(BOOL)checkPhoneNumInput:(NSString *)phoneStr
{
    NSString *photoRange = @"^1(3[0-9]|4[0-9]|5[0-9]|7[0-9]|8[0-9])\\d{8}$";//正则表达式
    NSPredicate *regexMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",photoRange];
    BOOL result = [regexMobile evaluateWithObject:phoneStr];
    if (result) {
        return YES;
    } else {
        [[UIApplication sharedApplication].delegate.window makeToast:@"手机号码格式不正确" duration:0.8 position:CSToastPositionBottom];
        return NO;
    }
}
//获取验证码
- (void)refreshBtnTitle:(NSTimer *)aTimer{
    if (self.codeNumberInt <= 0) {
        [aTimer invalidate];
        [self.codeBtn setEnabled:YES];
        self.codeNumberInt = 59;
        [self.codeBtn setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
        [self.codeBtn setTitle:[NSString stringWithFormat:@"重新获取"] forState:UIControlStateNormal];
        self.codeBtn.userInteractionEnabled = YES;
        return;
    }
    [self.codeBtn setEnabled:NO];
    [self.codeBtn setTitle:[NSString stringWithFormat:@"%zdS", self.codeNumberInt] forState:UIControlStateDisabled];
    self.codeNumberInt--;
}

@end
