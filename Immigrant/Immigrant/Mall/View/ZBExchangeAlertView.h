//
//  ZBExchangeAlertView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBExchangeAlertView : UIView

//提示语
@property (nonatomic,copy) NSString *noticeStr;

@property (nonatomic, copy) void(^sureBlock)(void);

@end
