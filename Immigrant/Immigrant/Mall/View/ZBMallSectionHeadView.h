//
//  ZBMallSectionHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBMallSectionHeadView : UIView

@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (nonatomic,copy) void(^lookAllBlock)(void);


@end
