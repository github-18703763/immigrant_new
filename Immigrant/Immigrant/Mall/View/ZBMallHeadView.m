//
//  ZBMallHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMallHeadView.h"
#import "ZBMallSearchListController.h"

@interface ZBMallHeadView()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;

@end

@implementation ZBMallHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.headImg.layer.cornerRadius = 37.5;
    self.headImg.layer.masksToBounds = YES;
    self.searchTxt.delegate = self;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    ZBMallSearchListController *search = [ZBMallSearchListController new];
    XKNavigationController *nav = [[XKNavigationController alloc] initWithRootViewController:search];
    [[self currentViewController] presentViewController:nav animated:YES completion:nil];
    return NO;
}

@end
