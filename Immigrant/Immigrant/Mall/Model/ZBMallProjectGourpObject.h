//
//  ZBMallProdectGourpObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBMallProjectObject.h"

@interface ZBMallProjectGourpObject : NSObject

/**
 creator = 2;
 */
@property (nonatomic,copy) NSString  *creator;

/**
 gmtCreated = "2018-12-27 09:21:50";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-27 09:21:50";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 1;
 */
@property (nonatomic,copy) NSString  *id;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 isreal = 1;
 */
@property (nonatomic,copy) NSString  *isreal;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 name = "\U7535\U5b50\U4ea7\U54c1";
 */
@property (nonatomic,copy) NSString  *name;

/**
 goodss
 */
@property (nonatomic,strong) NSArray  *goodss;

@end
