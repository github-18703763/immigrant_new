//
//  ZBMallProjectObject.h
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBMallProjectObject : NSObject

/**
 cateId = 3;
 */
@property (nonatomic,copy) NSString  *cateId;

/**
 cateName = "\U624b\U673a\U5145\U503c";
 */
@property (nonatomic,copy) NSString  *cateName;

/**
 cover = "http://letsqyq.oss-cn-shenzhen.aliyuncs.com/icebartech-migrate/9.jpg?Expires=1545881124&OSSAccessKeyId=LTAImojeJLzKug9e&Signature=4K09q6HQP8h7eQKYLemTse1HbwQ=";
 */
@property (nonatomic,copy) NSString  *cover;

/**
 creator = 2;
 */
@property (nonatomic,copy) NSString  *creator;

/**
 gmtCreated = "2018-12-27 09:25:29";
 */
@property (nonatomic,copy) NSString  *gmtCreated;

/**
 gmtModified = "2018-12-27 09:25:29";
 */
@property (nonatomic,copy) NSString  *gmtModified;

/**
 id = 7;
 */
@property (nonatomic,copy) NSString  *id;

/**
 imageskey = "icebartech-migrate/9.jpg,icebartech-migrate/9.jpg";
 */
@property (nonatomic,copy) NSString  *imageskey;

/**
 isDeleted = n;
 */
@property (nonatomic,copy) NSString  *isDeleted;

/**
 ison = 0;
 */
@property (nonatomic,copy) NSString  *ison;

/**
 isontext = "\U4e0b\U67b6";
 */
@property (nonatomic,copy) NSString  *isontext;

/**
 modifier = "[SYS]";
 */
@property (nonatomic,copy) NSString  *modifier;

/**
 name = "\U624b\U673a\U5145\U503c200\U5143";
 */
@property (nonatomic,copy) NSString  *name;

/**
 point = 2000;
 */
@property (nonatomic,copy) NSString  *point;

/**
 price = 10000000;
 */
@property (nonatomic,copy) NSString  *price;

/**
 priceyuan = 100000;
 */
@property (nonatomic,copy) NSString  *priceyuan;

/**
 content
 */
@property (nonatomic,copy) NSString *content;

/**
 images
 */
@property (nonatomic,strong) NSArray  *images;

@end
