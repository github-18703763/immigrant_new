//
//  ZBMallSearchListController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/27.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMallSearchListController.h"
#import "ZBExchangeDetialController.h"
#import "ZBMallCell.h"
#import "ZBMallProjectObject.h"

@interface ZBMallSearchListController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *searchTxt;
@property (weak, nonatomic) IBOutlet UITableView *listTable;

@property (nonatomic,assign) NSInteger  pageIndex;

@property (nonatomic,assign) NSInteger  pageSize;

@property (nonatomic,strong) NSMutableArray  *mallArray;

@end

@implementation ZBMallSearchListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"积分商城";
    
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.searchTxt.layer.cornerRadius = 6;
    self.searchTxt.layer.masksToBounds = YES;
    self.listTable.delegate = self;
    self.listTable.dataSource = self;
    
    self.listTable.showsVerticalScrollIndicator = NO;
    self.listTable.showsHorizontalScrollIndicator = NO;
    self.listTable.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.listTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.listTable.bounces = NO;
    self.listTable.contentInset = UIEdgeInsetsMake(-StatuBarHeight, 0, 0, 0);
    [self.listTable registerNib:[UINib nibWithNibName:@"ZBMallCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    self.pageSize = 10;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHeadData)];
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    self.listTable.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.listTable.mj_footer = footer;
    [self loadData:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


- (IBAction)searchtxtDidChange:(UITextField *)sender {
    [self loadData:YES];
}


-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadHeadData{
    [self loadData:YES];
}

-(void)loadMoreData{
    self.pageIndex++;
    [self loadData:NO];
}

-(void)loadData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageIndex = 1;
    }
    WS(weakSELF);
    [QSNetworkManager mallpaging:@"" nameLike:self.searchTxt.text isSearch:YES withPage:self.pageIndex withPageSize:self.pageSize successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            NSInteger count = [responseModel.data[@"count"] integerValue];
            [weakSELF.listTable.mj_footer endRefreshing];
            [weakSELF.listTable.mj_header endRefreshing];
            NSArray *array = responseModel.data[@"bussData"];
            if (isRefresh) {
                weakSELF.mallArray = [ZBMallProjectObject mj_objectArrayWithKeyValuesArray:array];
                [weakSELF.listTable reloadData];
                if (weakSELF.mallArray.count>=count) {
                    [weakSELF.listTable.mj_footer endRefreshingWithNoMoreData];
                }
            }else{
                if (array.count>0) {
                    [weakSELF.mallArray addObjectsFromArray:[ZBMallProjectObject mj_objectArrayWithKeyValuesArray:array]];
                }else{
                    [weakSELF.listTable.mj_footer endRefreshingWithNoMoreData];
                }
            }
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.listTable.mj_footer endRefreshing];
            [weakSELF.listTable.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.listTable.mj_footer endRefreshing];
        [weakSELF.listTable.mj_header endRefreshing];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mallArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMallCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ZBMallProjectObject *model = self.mallArray[indexPath.row];
    cell.model = model;
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 135;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBExchangeDetialController *detail = [ZBExchangeDetialController new];
    ZBMallProjectObject *model = self.mallArray[indexPath.row];
    detail.model = model;
    [self.navigationController pushViewController:detail animated:YES];
}

@end
