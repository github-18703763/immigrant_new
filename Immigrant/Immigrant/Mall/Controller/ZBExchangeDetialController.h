//
//  ZBExchangeDetialController.h
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMallProjectObject.h"

@interface ZBExchangeDetialController : UIViewController

@property (nonatomic,strong) ZBMallProjectObject  *model;

@end
