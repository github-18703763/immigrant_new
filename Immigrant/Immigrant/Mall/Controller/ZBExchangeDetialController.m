//
//  ZBExchangeDetialController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBExchangeDetialController.h"
#import "ZBExchangeDetialHeadView.h"
#import "ZBSureExchangeController.h"

@interface ZBExchangeDetialController ()

@property (weak, nonatomic) IBOutlet UIButton *exchangeBtn;
@property (weak, nonatomic) IBOutlet UITableView *exchangeTable;

@property (nonatomic,strong) ZBMallProjectObject  *currentModel;
@property (nonatomic,strong)ZBExchangeDetialHeadView *headView;
@end

@implementation ZBExchangeDetialController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"积分兑换";
    
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.exchangeBtn.layer.cornerRadius = 25;
    self.exchangeBtn.layer.masksToBounds = YES;
    
    self.headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBExchangeDetialHeadView" owner:self options:nil] firstObject];
    self.headView.frame = CGRectMake(0, 0, SystemScreenWidth, 400);
    
    self.exchangeTable.tableHeaderView = self.headView;
    
    self.exchangeTable.tableFooterView = [UIView new];
    
    [self loadData];
    
}

-(void)loadData{
    WS(weakSELF);
    [QSNetworkManager mallDetail:self.model.id successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            weakSELF.currentModel = [ZBMallProjectObject mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            self.headView.modelMallProject = [ZBMallProjectObject mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            [weakSELF.exchangeTable reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];
    
}

- (IBAction)exchangeAction:(id)sender {
//    ZBLog(@"立即兑换");
    ZBSureExchangeController *sure = [ZBSureExchangeController new];
    sure.modelMallProject = self.currentModel;
    [self.navigationController pushViewController:sure animated:YES];
}

-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
