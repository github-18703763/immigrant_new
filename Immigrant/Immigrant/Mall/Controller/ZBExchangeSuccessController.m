//
//  ZBExchangeSuccessController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/16.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBExchangeSuccessController.h"

@interface ZBExchangeSuccessController ()
@property (weak, nonatomic) IBOutlet UIButton *exchangeBtn;
@property (weak, nonatomic) IBOutlet UIButton *orderBtn;

@end

@implementation ZBExchangeSuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"兑换成功";
    self.orderBtn.layer.cornerRadius = 25;
    self.orderBtn.layer.masksToBounds = YES;
    self.exchangeBtn.layer.cornerRadius = 25;
    self.exchangeBtn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)orderAction:(id)sender {
    
}
- (IBAction)exchangeAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}



@end
