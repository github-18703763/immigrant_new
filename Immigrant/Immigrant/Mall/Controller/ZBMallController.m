//
//  ZBMallController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBMallController.h"
#import "ZBMallHeadView.h"
#import "ZBMallSectionHeadView.h"
#import "ZBMallCell.h"
#import "ZBExchangeDetialController.h"
#import "ZBMallProjectGourpObject.h"
#import "ZBMallProjectObject.h"
#import "ZBMallListController.h"

@interface ZBMallController ()

@property (nonatomic,strong) NSMutableArray  *groupArray;

@end

@implementation ZBMallController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationController.navigationBar.hidden = YES;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    self.tableView.contentInset = UIEdgeInsetsMake(-StatuBarHeight, 0, 0, 0);
    ZBMallHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBMallHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 271);
    headView.labCurrentPoint.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"CURRENTPOINT"];
    self.tableView.tableHeaderView = headView;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBMallCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [self loadData];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)loadData{
    WS(weakSELF);
    [QSNetworkManager mallsuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            
            weakSELF.groupArray = [ZBMallProjectGourpObject mj_objectArrayWithKeyValuesArray:responseModel.data[@"bussData"]];
            
            [weakSELF.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            [weakSELF.tableView.mj_footer endRefreshing];
            [weakSELF.tableView.mj_header endRefreshing];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        [weakSELF.tableView.mj_footer endRefreshing];
        [weakSELF.tableView.mj_header endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.groupArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ZBMallProjectGourpObject *group = self.groupArray[section];
    return group.goodss.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBMallCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ZBMallProjectGourpObject *group = self.groupArray[indexPath.section];
    ZBMallProjectObject *model = group.goodss[indexPath.row];
    cell.model = model;
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 135;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ZBMallSectionHeadView *head = [[[NSBundle mainBundle] loadNibNamed:@"ZBMallSectionHeadView" owner:self options:nil] firstObject];
    ZBMallProjectGourpObject *object = self.groupArray[section];
    head.nameLab.text = object.name;
    WS(weakSELF);
    head.lookAllBlock = ^{
        ZBMallListController *list = [ZBMallListController new];
        XKNavigationController *nav = [[XKNavigationController alloc]  initWithRootViewController:list];
        list.model = weakSELF.groupArray[section];
        [weakSELF presentViewController:nav animated:YES completion:nil];
    };
    return head;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 56;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZBExchangeDetialController *detail = [ZBExchangeDetialController new];
    XKNavigationController *nav = [[XKNavigationController alloc]  initWithRootViewController:detail];
    ZBMallProjectGourpObject *group = self.groupArray[indexPath.section];
    ZBMallProjectObject *model = group.goodss[indexPath.row];
    detail.model = model;
    [self presentViewController:nav animated:YES completion:nil];
}

@end
