
//
//  ZBSureExchangeController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/15.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBSureExchangeController.h"
#import "ZBSureExchangeHeadView.h"
#import "ZBExchangeAlertView.h"
#import "ZBExchangeAcceptanceAlertView.h"
#import "ZBExchangeSuccessController.h"
#import "GYAddressVC.h" //我的地址
@interface ZBSureExchangeController ()
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UITableView *exchangeTable;
@property (nonatomic,strong)ZBSureExchangeHeadView *headView;
@property (nonatomic,strong)NSString *strAddressId;//地址ID
@end

@implementation ZBSureExchangeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"确认兑换";
    
    if (!self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"back"] highImage:[UIImage imageNamed:@"back"] target:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.sureBtn.layer.cornerRadius = 25;
    self.sureBtn.layer.masksToBounds = YES;
    
    self.headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBSureExchangeHeadView" owner:self options:nil] firstObject];
    self.headView.frame = CGRectMake(0, 0, SystemScreenWidth, 267);
    self.headView.model = self.modelMallProject;
    //选择地址
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapChooseAddressClick)];
    [self.headView.contianerTwo addGestureRecognizer:tap];
    
    self.exchangeTable.tableHeaderView = self.headView;
    self.exchangeTable.tableFooterView = [UIView new];
    
}
#pragma 选择地址
- (void)tapChooseAddressClick{
      WS(weakSELF);
    GYAddressVC *vc = [GYAddressVC new];
    vc.isChoose = YES;
    vc.chooseBlock = ^(ZBMineAddressObject *object) {
        weakSELF.headView.defaultAddressLab.hidden = YES;
        weakSELF.headView.imgFollowDefault.hidden = YES;
        //
        weakSELF.headView.returnNameTitle.hidden = NO;
        weakSELF.headView.imgAddress.hidden = NO;
        weakSELF.headView.returnName.hidden = NO;
        weakSELF.headView.returnPhone.hidden = NO;
        weakSELF.headView.addressLab.hidden = NO;
        weakSELF.headView.returnName.text = object.consignee;
       weakSELF.headView.returnPhone.text = object.mobile;
        weakSELF.headView.addressLab.text = object.str;
        self.strAddressId = object.id;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)sureAction:(id)sender {
//    ZBLog(@"确认提交");
    if ([self.strAddressId isEqualToString:@""] || self.strAddressId == nil) {
          [[UIApplication sharedApplication].delegate.window makeToast:@"请选择收货地址" duration:0.8 position:CSToastPositionBottom];
        return;
    }
    WS(weakSELF);
    ZBExchangeAlertView *alert = [[[NSBundle mainBundle] loadNibNamed:@"ZBExchangeAlertView" owner:self options:nil] firstObject];
    alert.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:alert];
    alert.sureBlock = ^{
        //新增商品订单--数量为1
        [QSNetworkManager appOrderaddOrderGoodsWithBookNum:1 withGoodsId:self.modelMallProject.id withAddressId:self.strAddressId successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {
                //发送短信验证码15.3
                        ZBExchangeAcceptanceAlertView *accept = [[[NSBundle mainBundle] loadNibNamed:@"ZBExchangeAcceptanceAlertView" owner:self options:nil] firstObject];
                        accept.frame = [UIScreen mainScreen].bounds;
                        [[UIApplication sharedApplication].delegate.window addSubview:accept];
                
                        accept.sureBlock = ^{
                            ZBExchangeSuccessController *success = [ZBExchangeSuccessController new];
                            [weakSELF.navigationController pushViewController:success animated:YES];
                        };
            }else{//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
        } failBlock:^(NSError * _Nullable error) {
             [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        }];

    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
