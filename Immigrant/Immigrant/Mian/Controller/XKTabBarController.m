//
//  ZBTabBarController.m
//
//  Created by junnpy Strong on 15/12/31.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import "XKTabBarController.h"
#import "XKTabBar.h"
#import "ZBProfileController.h"
#import "XKTabBarButton.h"
#import "ZBHomeControlle.h"
#import "ZBActivityController.h"
#import "ZBMallController.h"
#import "ZBActivityController.h"
#import "ZBConsultingView.h"
#import "ZBReturnVisitController.h"
#import "GYLoginRegisterVC.h"
#import "ZBOnLineController.h"

@interface XKTabBarController ()<UINavigationControllerDelegate,XKTabBarDelegate>


@property (nonatomic,weak)ZBHomeControlle *home;

@property (nonatomic,weak)ZBMallController *mall;

@property (nonatomic,weak)ZBActivityController *activity;

@property (nonatomic,weak)ZBProfileController *profile;

@property (nonatomic,weak)UIViewController *empty;

@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation XKTabBarController
#pragma mark 懒加载按钮模型数组
- (NSMutableArray *)items{
    
    if (!_items) {
        _items = [NSMutableArray array];
    }
    return _items;
}

- (void)viewDidLoad {
    
//    self.enterforgroudCount=1;
    
    [super viewDidLoad];
        
    [self addChildViewControllers];
    
    //2.设置tab
    [self setUpTab];

    
}


-(void)changeIndxeToZero{
    
    self.selectedIndex=0;
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
}
- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
}

#pragma mark 添加所有的子视图控制器
- (void)addChildViewControllers{
    
    ZBHomeControlle *home = [[ZBHomeControlle alloc]init];
    [self addOneChildViewController:home image:@"home" selectedImage:@"home_selected" barTitle:@"首页" navTitle:@"首页"];
    _home = home;
    
    
    ZBActivityController *activity = [[ZBActivityController alloc]init];
   [self addOneChildViewController:activity image:@"home_credit_activity" selectedImage:@"home_credit_activity_selected" barTitle:@"揽分活动" navTitle:@"揽分活动"];
    _activity = activity;
    
    UIViewController *empty = [[UIViewController alloc]init];
    [self addOneChildViewController:empty image:@"" selectedImage:@"" barTitle:@"" navTitle:@""];
    _empty = empty;

    ZBMallController *mall = [[ZBMallController alloc] init];
    [self addOneChildViewController:mall image:@"home_points_mall" selectedImage:@"home_points_mall_selected" barTitle:@"积分商城" navTitle:@"积分商城"];
    _mall = mall;
    

    ZBProfileController *profile = [[ZBProfileController alloc]init];
    [self addOneChildViewController:profile image:@"home_me" selectedImage:@"home_me_slelcted" barTitle:@"我的" navTitle:@""];
    _profile = profile;
   
    
}

- (void)addOneChildViewController:(UIViewController *)vc image:(NSString *)imageName selectedImage:(NSString *)selectedImageName barTitle:(NSString *)barTitle navTitle:(NSString *)navTitle{
    
    vc.navigationItem.title=navTitle;
    
    //标题
    vc.tabBarItem.title = barTitle;
    
    //常规图片
    vc.tabBarItem.image=[UIImage imageNamed:imageName];
    
    //选中图片
    vc.tabBarItem.selectedImage=[UIImage imageWithOriginalName:selectedImageName];

    XKNavigationController *nav=[[XKNavigationController alloc]initWithRootViewController:vc];
    [self addChildViewController:nav];
    [self.items addObject:vc.tabBarItem];
    self.tabBar.translucent =false;
    self.tabBar.backgroundColor = [UIColor getColor:@"E1E0FF"];
}

-(void)zbTabBar:(XKTabBar *)zbTabBar clickEmpty:(NSInteger)index{
    ZBConsultingView *consulting = [[[NSBundle mainBundle] loadNibNamed:@"ZBConsultingView" owner:self options:nil] firstObject];
    consulting.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:consulting];
    consulting.phoneBlock = ^{
//        ZBLog(@"电话咨询");
        NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"tel:%@",@"4007888798"];UIWebView * callWebview = [[UIWebView alloc] init];[callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
        [self.view addSubview:callWebview];
    };
    consulting.onlineBlock = ^{
//        ZBLog(@"在线咨询");
        ZBOnLineController *onLine = [ZBOnLineController new];
        switch (self.selectedIndex) {
            case 0:
                [self.home.navigationController pushViewController:onLine animated:YES];
                break;
            case 1:
                [self.activity.navigationController pushViewController:onLine animated:YES];
                break;
            case 3:
                [self.mall.navigationController pushViewController:onLine animated:YES];
                break;
            case 4:
                [self.profile.navigationController pushViewController:onLine animated:YES];
                break;
                
            default:
                break;
        }
    };
    consulting.visitiBlock = ^{
//        ZBLog(@"要求回访");
        ZBReturnVisitController *visit = [[ZBReturnVisitController alloc] init];
        switch (self.selectedIndex) {
            case 0:
                [self.home.navigationController pushViewController:visit animated:YES];
                break;
            case 1:
                [self.activity.navigationController pushViewController:visit animated:YES];
                break;
            case 3:
                [self.mall.navigationController pushViewController:visit animated:YES];
                break;
            case 4:
                [self.profile.navigationController pushViewController:visit animated:YES];
                break;
                
            default:
                break;
        }
    };
}

- (void)setUpTab{
    
    for (UIView *v in self.tabBar.subviews) {
        [v removeFromSuperview];
    }
    self.tabBar.backgroundColor = [UIColor redColor];
    
    XKTabBar *tabBar = [[XKTabBar alloc] init];
    tabBar.left = 0;
    tabBar.width = SystemScreenWidth;

    tabBar.height = KIsiPhoneX?84:TABBARHEIGHT;
    tabBar.top = 0;
    //设置tabBar背景颜色
    tabBar.backgroundColor = [UIColor colorWithCGColor:[UIColor getColor:@"E1E0FF"].CGColor];
    CGRect rect = CGRectMake(0, 0, SystemScreenWidth, SystemScreenHeight);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBar setBackgroundImage:img];
    [self.tabBar setShadowImage:img];

    // 设置代理
    tabBar.delegate = self;

    // 给tabBar传递tabBarItem模型
    tabBar.Items = self.items;

    // 将自定义的Tab添加到系统的Tab上
    [self.tabBar addSubview:tabBar];
    self.tabBar.backgroundColor = [UIColor redColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)zbTabBar:(XKTabBar *)zbTabBar didClickButton:(NSInteger)index{

        [[NSUserDefaults standardUserDefaults]setObject:@(index) forKey:@"selectedIndex"];
        
        self.selectedIndex = index;
    
    if (index>0&&index<=4&&![QSUserManager shareInstance].isLogin) {
        
        XKNavigationController *nav = [[XKNavigationController alloc]  initWithRootViewController:[GYLoginRegisterVC new]];
        //                    GYLoginRegisterVC *login = [GYLoginRegisterVC new];
        [self presentViewController:nav animated:YES completion:nil];
        
    }
}




@end
