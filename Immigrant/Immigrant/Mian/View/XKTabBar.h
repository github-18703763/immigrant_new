//
//  ZBTabBar.h
//
//  Created by junnpy Strong on 16/1/1.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import <UIKit/UIKit.h>
@class XKTabBar;
@protocol XKTabBarDelegate <NSObject>
@optional

//告诉控制器点击了那个选项卡
- (void)zbTabBar:(XKTabBar *)zbTabBar didClickButton:(NSInteger)index;

- (void)zbTabBar:(XKTabBar *)zbTabBar clickEmpty:(NSInteger)index;


@end

@interface XKTabBar : UIView

@property (nonatomic,strong)NSArray *Items;

@property (nonatomic)id<XKTabBarDelegate>delegate;

@end
