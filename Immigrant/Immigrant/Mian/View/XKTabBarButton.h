//
//  ZBTabBarButton.h
//
//  Created by junnpy Strong on 16/1/1.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XKBadgeView.h"
@interface XKTabBarButton : UIButton

@property (nonatomic,strong)UITabBarItem *item;

@end
