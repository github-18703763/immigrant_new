//
//  ZBBadgeView.m
//
//  Created by junnpy Strong on 16/1/1.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import "XKBadgeView.h"
#import "UIView+Frame.h"
#define ZBBadgeViewFont [UIFont systemFontOfSize:11.5]
#define BigBadgeViewFont [UIFont systemFontOfSize:8]
@implementation XKBadgeView
#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor blackColor];
        self.userInteractionEnabled = NO;
        
        [self setBackgroundImage:[UIImage imageNamed:@"main_badge"] forState:UIControlStateNormal];
        
        // 设置字体大小
        self.titleLabel.font = ZBBadgeViewFont;
        [self sizeToFit];
        
    }
    return self;
}
#pragma mark 设置提示值
- (void)setBadgeValue:(NSString *)badgeValue
{
    _badgeValue = badgeValue;
    
    // 判断badgeValue是否有内容
    if (badgeValue.length == 0 || [badgeValue isEqualToString:@"0"]) { // 没有内容或者空字符串,等于0
        self.hidden = YES;
    }else{
        self.hidden = NO;
    }
    
    NSDictionary* attributes;
    if ([badgeValue integerValue]>99) {
        badgeValue = @"99+";
        self.titleLabel.font = BigBadgeViewFont;
        attributes = @{NSFontAttributeName : BigBadgeViewFont};
    }else{
        self.titleLabel.font = ZBBadgeViewFont;
        attributes = @{NSFontAttributeName : ZBBadgeViewFont};
    }
    
//    NSDictionary *attributes = @{NSFontAttributeName : ZBBadgeViewFont};
    CGSize size = [badgeValue  sizeWithAttributes:attributes];
    
    if (size.width > self.width) {
        // 文字的尺寸大于控件的宽度
        [self setImage:[UIImage imageNamed:@"badge_dot"] forState:UIControlStateNormal];
        [self setTitle:badgeValue forState:UIControlStateNormal];
        [self setImage:nil forState:UIControlStateNormal];
    }else{
        [self setBackgroundImage:[UIImage imageNamed:@"main_badge"] forState:UIControlStateNormal];
        [self setTitle:badgeValue forState:UIControlStateNormal];
        [self setImage:nil forState:UIControlStateNormal];
    }
    
}

@end
