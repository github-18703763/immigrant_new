//
//  ZBBadgeView.h
//
//  Created by junnpy Strong on 16/1/1.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XKBadgeView : UIButton

@property (nonatomic, copy) NSString *badgeValue;

@end
