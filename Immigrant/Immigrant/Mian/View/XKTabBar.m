//
//  ZBTabBar.m
//
//  Created by junnpy Strong on 16/1/1.
//  Copyright © 2013年 Mystery. All rights reserved.
//

#import "XKTabBar.h"
#import "XKTabBarButton.h"

@interface XKTabBar()

@property (nonatomic, strong) NSMutableArray *buttons;

@property (nonatomic, weak) UIButton *selectedButton;

@property (nonatomic, weak) UIButton *advisoryButton;

@end

@implementation XKTabBar

#pragma mark 按钮数组懒加载
- (NSMutableArray *)buttons
{
    if (_buttons == nil) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}

#pragma mark 重写模型数组且创建自定义按钮
- (void)setItems:(NSArray *)Items{
    _Items = Items;
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, -0.7, SystemScreenWidth, 0.5)];
    
    v.backgroundColor= [UIColor colorWithWhite:0.85 alpha:1.0];
    
    [self addSubview:v];
    
    // 遍历模型数组，创建对应tabBarButton
    for (UITabBarItem *item in _Items) {
        
       XKTabBarButton *btn = [XKTabBarButton buttonWithType:UIButtonTypeCustom];
        
        // 给按钮赋值模型，按钮的内容由模型对应决定
        btn.item = item;
        
        btn.tag = self.buttons.count;
        
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];
        
        if (btn.tag == 0) { // 选中第0个
            [self btnClick:btn];
            
        }
        
        [self addSubview:btn];
        
        // 把按钮添加到按钮数组
        [self.buttons addObject:btn];
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"home_advisory"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"home_advisory"] forState:UIControlStateSelected];
    [btn setImage:[UIImage imageNamed:@"home_advisory"] forState:UIControlStateHighlighted];
    btn.tag = 100;
    [btn addTarget:self action:@selector(advisoryButtonAction:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:btn];
    self.advisoryButton = btn;
    [self bringSubviewToFront:btn];
    
}

-(void)advisoryButtonAction:(UIButton *)button{
    [self.delegate zbTabBar:self clickEmpty:100];
}


#pragma mark 点击tabBarButton调用
-(void)btnClick:(UIButton *)button
{
    
        button.selected = YES;
        _selectedButton.selected = NO;
    if (_selectedButton == button) {//11-20 为了强制选中第一个而加上的判断
        _selectedButton.selected = YES;
    }
        _selectedButton = button;
//    }
    
    // 通知tabBarVc切换控制器，
    if ([_delegate respondsToSelector:@selector(zbTabBar:didClickButton:)]) {
       
        [_delegate zbTabBar :self didClickButton:_selectedButton.tag];
       
    }
}

#pragma mark 自动布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat w = SystemScreenWidth;
    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = w / (self.Items.count);
    CGFloat btnH = KIsiPhoneX?49:self.bounds.size.height;
    
    
    int i = 0;
    // 设置tabBarButton的frame
    for (UIView *tabBarButton in self.buttons) {

        btnX = i * btnW;
        tabBarButton.frame = CGRectMake(btnX,btnY, btnW, btnH);
        i++;
    }
    
    self.advisoryButton.frame =CGRectMake(btnW*2, -10, btnW, btnH+10);
    
    
}


@end
