//
//  ZBActivityHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBActivityHeadView.h"
#import "ZBIntegralRulesAlert.h"

@interface ZBActivityHeadView()
@property (weak, nonatomic) IBOutlet UIView *voiceView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *pointOne;
@property (weak, nonatomic) IBOutlet UIView *pointTwo;
@property (weak, nonatomic) IBOutlet UIView *progressView;

@end

@implementation ZBActivityHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = COLOR(253, 253, 253, 1);
    self.voiceView.layer.cornerRadius = 20;
    self.voiceView.layer.masksToBounds = YES;
    self.bottomView.layer.cornerRadius = 15;
    self.bottomView.layer.masksToBounds = YES;
    self.pointOne.layer.cornerRadius = 4;
    self.pointOne.layer.masksToBounds = YES;
    self.pointTwo.layer.cornerRadius = 5;
    self.pointTwo.layer.masksToBounds= YES;
    self.progressView.layer.cornerRadius = 2.5;
    self.progressView.layer.masksToBounds = YES;
}

- (IBAction)tapAction:(id)sender {
    ZBIntegralRulesAlert *alert = [[[NSBundle mainBundle] loadNibNamed:@"ZBIntegralRulesAlert" owner:self options:nil] firstObject];
    alert.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:alert];
}


@end
