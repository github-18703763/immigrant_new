//
//  ZBIntegralRulesCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBEarnPointsModel.h"

@interface ZBIntegralRulesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UILabel *pointCountLab;
//@property (nonatomic,strong)ZBPointFlowsModel *myEarnPoint;

@end
