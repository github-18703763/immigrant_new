//
//  ZBActivitySectionHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBActivitySectionHeadView.h"

@implementation ZBActivitySectionHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
}
- (IBAction)lookAllAction:(id)sender {
    if (self.lookAllBlock) {
        self.lookAllBlock();
    }
}

@end
