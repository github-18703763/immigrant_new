//
//  ZBIntegralRulesHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBIntegralRulesHeadView.h"

@interface ZBIntegralRulesHeadView()
@property (weak, nonatomic) IBOutlet UIView *sliderView;

@end

@implementation ZBIntegralRulesHeadView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.sliderView.layer.cornerRadius = 7.5;
    self.sliderView.layer.masksToBounds = YES;
}
- (IBAction)backAction:(id)sender {
    [[self currentViewController].navigationController popViewControllerAnimated:YES];
}

@end
