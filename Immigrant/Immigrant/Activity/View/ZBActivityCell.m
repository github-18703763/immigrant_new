//
//  ZBActivityCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBActivityCell.h"

@interface ZBActivityCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIView *pointOne;
@property (weak, nonatomic) IBOutlet UIView *pointTwo;
@property (weak, nonatomic) IBOutlet UIView *pointThree;
@property (weak, nonatomic) IBOutlet UIView *pointFour;
@property (weak, nonatomic) IBOutlet UIButton *shardBtn;

@end

@implementation ZBActivityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.photoImg.layer.cornerRadius = 8;
    self.photoImg.layer.masksToBounds = YES;
    self.pointOne.layer.cornerRadius = 3;
    self.pointOne.layer.masksToBounds = YES;
    self.pointTwo.layer.cornerRadius = 3;
    self.pointTwo.layer.masksToBounds = YES;
    self.pointThree.layer.cornerRadius = 3;
    self.pointThree.layer.masksToBounds = YES;
    self.pointFour.layer.cornerRadius = 3;
    self.pointFour.layer.masksToBounds = YES;
    self.shardBtn.layer.cornerRadius = 19;
    self.shardBtn.layer.masksToBounds = YES;
}
- (IBAction)sharedAction:(id)sender {
//    ZBLog(@"分享");
}

-(void)setMigrateObject:(ZBMigrateObject *)migrateObject{
    _migrateObject = migrateObject;
    
}

-(void)setVisa:(ZBVisaObject *)visa{
    _visa = visa;
}

-(void)setHouse:(ZBHouseObject *)house{
    _house = house;
}

-(void)setCamp:(ZBCampObject *)camp{
    _camp = camp;
}

-(void)setLine:(ZBInspectionObject *)line{
    _line = line;
}

-(void)setStudy:(ZBStudyObject *)study{
    _study = study;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
