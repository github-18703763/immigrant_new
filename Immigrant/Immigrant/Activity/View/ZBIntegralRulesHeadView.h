//
//  ZBIntegralRulesHeadView.h
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBIntegralRulesHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *allPointLab;
@property (weak, nonatomic) IBOutlet UILabel *currentPointLab;
@property (weak, nonatomic) IBOutlet UILabel *achievePointLab;

@end
