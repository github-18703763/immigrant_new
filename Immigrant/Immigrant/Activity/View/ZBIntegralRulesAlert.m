//
//  ZBIntegralRulesAlert.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBIntegralRulesAlert.h"

@interface ZBIntegralRulesAlert()

@property (weak, nonatomic) IBOutlet UIView *contianerView;


@end

@implementation ZBIntegralRulesAlert
- (IBAction)closeAction:(id)sender {
    [self removeFromSuperview];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.contianerView.layer.cornerRadius = 15;
    self.contianerView.layer.masksToBounds = YES;
}

@end
