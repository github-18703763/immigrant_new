//
//  ZBActivityCell.h
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBMigrateObject.h"
#import "ZBVisaObject.h"
#import "ZBHouseObject.h"
#import "ZBCampObject.h"
#import "ZBInspectionObject.h"
#import "ZBStudyObject.h"

@interface ZBActivityCell : UITableViewCell

@property (nonatomic,strong) ZBMigrateObject  *migrateObject;

@property (nonatomic,strong) ZBVisaObject  *visa;

@property (nonatomic,strong) ZBHouseObject  *house;

@property (nonatomic,strong) ZBCampObject  *camp;

@property (nonatomic,strong) ZBInspectionObject  *line;

@property (nonatomic,strong) ZBStudyObject  *study;

@end
