//
//  ZBIntegralRulesCell.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBIntegralRulesCell.h"

@interface ZBIntegralRulesCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation ZBIntegralRulesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.cornerRadius = 15;
    self.containerView.layer.masksToBounds = YES;
    self.actionBtn.layer.cornerRadius = 19;
    self.actionBtn.layer.masksToBounds = YES;
    
    //操作按钮状态
    //1.未领取 字体颜色437DFF  边框颜色437DFF 2px 背景色F7F7F7
    //2.未完成 背景色FDBC3F 字体颜色白色
    //3.已完成 字体颜色7E7E7E 背景颜色F7F7F7
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
