//
//  ZBActivityHeadView.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBActivityFooterView.h"

@interface ZBActivityFooterView()
@property (weak, nonatomic) IBOutlet UIView *voiceView;
@property (weak, nonatomic) IBOutlet UIImageView *pointOne;

@end

@implementation ZBActivityFooterView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.voiceView.layer.cornerRadius = 20;
    self.voiceView.layer.masksToBounds = YES;
    self.pointOne.layer.cornerRadius = 4;
    self.pointOne.layer.masksToBounds = YES;
}

@end
