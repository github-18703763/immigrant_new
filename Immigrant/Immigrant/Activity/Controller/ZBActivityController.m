//
//  ZBActivityController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/12.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBActivityController.h"
#import "ZBActivityHeadView.h"
#import "ZBActivityFooterView.h"
#import "ZBActivitySectionHeadView.h"
#import "ZBActivityCell.h"
#import "ZBIntegralRulesController.h"
#import "ZBActivityHomeModel.h"
#import "ZBEarnPointsModel.h"

@interface ZBActivityController ()

@property (nonatomic,strong) ZBActivityHomeModel  *activityModel;

@end

@implementation ZBActivityController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"去赚积分" style:UIBarButtonItemStylePlain target:self action:@selector(integralAction)];
    self.navigationController.navigationBar.tintColor = [UIColor getColor:@"FDBC3F"];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    [self configTabHeadAndFooter];
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBActivityCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [self loadData];
    
}

-(void)loadData{
    WS(weakSELF);
    [QSNetworkManager activityHomeListsuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {//成功
            weakSELF.activityModel = [ZBActivityHomeModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            [weakSELF.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)configTabHeadAndFooter{
    
    ZBActivityHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBActivityHeadView" owner:self options:nil] firstObject];
    headView.left = 0;
    headView.top = 0;
    headView.width = SystemScreenWidth;
    headView.height = 140;
    self.tableView.tableHeaderView = headView;
    
    ZBActivityFooterView *footerView = [[[NSBundle mainBundle] loadNibNamed:@"ZBActivityFooterView" owner:self options:nil] firstObject];
    footerView.left = 0;
    footerView.top = 0;
    footerView.width = SystemScreenWidth;
    footerView.height = 73;
    self.tableView.tableFooterView = footerView;
    
    [QSNetworkManager appPointFinddetailSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            ZBEarnPointsModel *myEarnPointsModel =  [ZBEarnPointsModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            headView.labCurrentPoint.text = myEarnPointsModel.currentpoint;
            headView.labAllPoint.text = @"3000";
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        
    }];
}


-(void)integralAction{
    ZBIntegralRulesController *rules = [[ZBIntegralRulesController alloc] init];
    [self.navigationController pushViewController:rules animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.activityModel) {
        return 6;
    }else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.activityModel) {
        return 0;
    } else{
        if (section == 0) {
            return self.activityModel.migrates.count;
        }else if (section == 1) {
            return self.activityModel.visas.count;
        }else if (section == 2) {
            return self.activityModel.hosues.count;
        }else if (section == 3) {
            return self.activityModel.camps.count;
        }else if (section == 4) {
            return self.activityModel.lines.count;
        }else if (section == 5) {
            return self.activityModel.studys.count;
        }else{
            return 0;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    /**
     #import "ZBMigrateObject.h"
     #import "ZBVisaObject.h"
     #import "ZBHouseObject.h"
     #import "ZBCampObject.h"
     #import "ZBInspectionObject.h"
     #import "ZBStudyObject.h"
     */
    switch (indexPath.section) {
        case 0:{
            ZBMigrateObject *object = self.activityModel.migrates[indexPath.row];
            cell.migrateObject = object;
        }
            break;
        case 1:{
             ZBVisaObject *object = self.activityModel.visas[indexPath.row];
            cell.visa = object;
        }
            break;
        case 2:{
               ZBHouseObject *object = self.activityModel.hosues[indexPath.row];
            cell.house = object;
        }
            break;
        case 3:{
               ZBCampObject *object = self.activityModel.camps[indexPath.row];
            cell.camp = object;
        }
            break;
        case 4:{
               ZBInspectionObject *object = self.activityModel.lines[indexPath.row];
            cell.line = object;
        }
            break;
        case 5:{
               ZBStudyObject *object = self.activityModel.studys[indexPath.row];
            cell.study = object;
        }
            break;
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ZBActivitySectionHeadView *hv = [[[NSBundle mainBundle] loadNibNamed:@"ZBActivitySectionHeadView" owner:self options:nil] firstObject];
    switch (section) {
        case 0:
            hv.titleLab.text = @"移民项目";
            break;
        case 1:
            hv.titleLab.text = @"签证中心";
            break;
        case 2:
            hv.titleLab.text = @"海外房产";
            break;
        case 3:
            hv.titleLab.text = @"冬夏令营";
            break;
        case 4:
            hv.titleLab.text = @"定制考察";
            break;
        case 5:
            hv.titleLab.text = @"海外留学";
            break;
        default:
            
            break;
    }
    hv.lookAllBlock = ^{
        
    };
    return hv;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 174;
}

@end
