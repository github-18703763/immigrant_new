//
//  ZBIntegralRulesController.m
//  Immigrant
//
//  Created by 张波 on 2018/12/13.
//  Copyright © 2018年 张波. All rights reserved.
//

#import "ZBIntegralRulesController.h"
#import "ZBIntegralRulesHeadView.h"
#import "ZBIntegralRulesCell.h"
#import "ZBPointRuleView.h"
#import "ZBEarnPointsModel.h"
@interface ZBIntegralRulesController ()

@property (nonatomic,strong)ZBEarnPointsModel *myEarnPointsModel;

@end

@implementation ZBIntegralRulesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"赚积分";
    
    self.tableView.contentInset = UIEdgeInsetsMake(-NavgationBarHeight, 0, 0, 0);
    
    ZBIntegralRulesHeadView *headView = [[[NSBundle mainBundle] loadNibNamed:@"ZBIntegralRulesHeadView" owner:self options:nil] firstObject];
    headView.frame = CGRectMake(0, 0, SystemScreenWidth, 360);
    self.tableView.tableHeaderView = headView;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, SystemScreenWidth, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"ZBIntegralRulesCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //兑换规则
    ZBPointRuleView *accept = [[[NSBundle mainBundle] loadNibNamed:@"ZBPointRuleView" owner:self options:nil] firstObject];
    accept.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].delegate.window addSubview:accept];
    
    [QSNetworkManager appPointFinddetailSuccessBlock:^(QSNetResponseModel * _Nonnull responseModel) {
        if (responseModel.status == 200) {
            self.myEarnPointsModel =  [ZBEarnPointsModel mj_objectWithKeyValues:responseModel.data[@"bussData"]];
            headView.allPointLab.text = self.myEarnPointsModel.currentpoint;
            headView.currentPointLab.text = self.myEarnPointsModel.currentpoint;
            headView.achievePointLab.text = @"3000";
            [self.tableView reloadData];
        }else{//失败
            [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
        }
        
    } failBlock:^(NSError * _Nullable error) {
        [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
        
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.myEarnPointsModel.pointFlows.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBIntegralRulesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ZBPointFlowsModel *dtModel = self.myEarnPointsModel.pointFlows[indexPath.row];
    cell.titleLab.text = [NSString stringWithFormat:@"%@ (%@/%@)",dtModel.operatetext,self.myEarnPointsModel.task1currentcount,self.myEarnPointsModel.task1totalcount];
    cell.detailLab.text = dtModel.operatememo;
    cell.pointCountLab.text = [NSString stringWithFormat:@"%@积分",self.myEarnPointsModel.task1point];
    //1.未领取 字体颜色437DFF  边框颜色437DFF 2px 背景色F7F7F7
    //2.未完成 背景色FDBC3F 字体颜色白色
    //3.已完成 字体颜色7E7E7E 背景颜色F7F7F7
    if (indexPath.row == 0) {
        if (self.myEarnPointsModel.task1currentcount == self.myEarnPointsModel.task1totalcount && self.myEarnPointsModel.isreceivetask1==false) {
            [cell.actionBtn setTitle:@"领取" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"437DFF"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"F7F7F7"];
            cell.actionBtn.layer.borderColor = [UIColor getColor:@"437DFF"].CGColor;
            cell.actionBtn.layer.borderWidth = 2.0;
        }else if (self.myEarnPointsModel.task1currentcount == self.myEarnPointsModel.task1totalcount && self.myEarnPointsModel.isreceivetask1==true) {
            [cell.actionBtn setTitle:@"已完成" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"7E7E7E"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"F7F7F7"];
        }else{
            [cell.actionBtn setTitle:@"去完成" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"FDBC3F"];
        }
    }else  if (indexPath.row == 1) {
        if (self.myEarnPointsModel.task2currentcount == self.myEarnPointsModel.task2totalcount && self.myEarnPointsModel.isreceivetask2==false) {
            [cell.actionBtn setTitle:@"领取" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"437DFF"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"F7F7F7"];
            cell.actionBtn.layer.borderColor = [UIColor getColor:@"437DFF"].CGColor;
            cell.actionBtn.layer.borderWidth = 2.0;
        }else if (self.myEarnPointsModel.task2currentcount == self.myEarnPointsModel.task2totalcount && self.myEarnPointsModel.isreceivetask2==true) {
            [cell.actionBtn setTitle:@"已完成" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"7E7E7E"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"F7F7F7"];
        }else{
            [cell.actionBtn setTitle:@"去完成" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"FDBC3F"];
        }
    }else{
        if (self.myEarnPointsModel.task3currentcount == self.myEarnPointsModel.task3totalcount && self.myEarnPointsModel.isreceivetask3==false) {
            [cell.actionBtn setTitle:@"领取" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"437DFF"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"F7F7F7"];
            cell.actionBtn.layer.borderColor = [UIColor getColor:@"437DFF"].CGColor;
            cell.actionBtn.layer.borderWidth = 2.0;
        }else if (self.myEarnPointsModel.task3currentcount == self.myEarnPointsModel.task3totalcount && self.myEarnPointsModel.isreceivetask3==true) {
            [cell.actionBtn setTitle:@"已完成" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"7E7E7E"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"F7F7F7"];
        }else{
            [cell.actionBtn setTitle:@"去完成" forState:UIControlStateNormal];
            [cell.actionBtn setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
            cell.actionBtn.backgroundColor = [UIColor getColor:@"FDBC3F"];
        }
        cell.actionBtn.tag = indexPath.row;
        [cell.actionBtn addTarget:self action:@selector(cellActionbBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}
- (void)cellActionbBtnClick:(UIButton *)btn{
    if ([btn.currentTitle isEqualToString:@"领取"]) {
        NSInteger index = btn.tag +1;
        [QSNetworkManager appPointReceivetaskWithIndex:index successBlock:^(QSNetResponseModel * _Nonnull responseModel) {
            if (responseModel.status == 200) {
                [btn setTitle:@"去完成" forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor getColor:@"ffffff"] forState:UIControlStateNormal];
                btn.backgroundColor = [UIColor getColor:@"FDBC3F"];
            }else{//失败
                [[UIApplication sharedApplication].delegate.window makeToast:responseModel.msg duration:0.8 position:CSToastPositionBottom];
            }
            
        } failBlock:^(NSError * _Nullable error) {
            [[UIApplication sharedApplication].delegate.window makeToast:error.description duration:0.8 position:CSToastPositionBottom];
            
        }];
    }else if ([btn.currentTitle isEqualToString:@"去完成"]){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        
    }
    
}
#pragma 领取每日任务1\领取下载任务2\领取下载任务3

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115;
}

@end
