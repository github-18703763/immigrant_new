//
//  ZBEarnPointsModel.m
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/21.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBEarnPointsModel.h"

@implementation ZBPointFlowsModel

@end

@implementation ZBEarnPointsModel
-(void)setPointFlows:(NSArray *)pointFlows{
    _pointFlows = pointFlows;
    _pointFlows = [ZBPointFlowsModel mj_objectArrayWithKeyValuesArray:pointFlows];
}
@end
