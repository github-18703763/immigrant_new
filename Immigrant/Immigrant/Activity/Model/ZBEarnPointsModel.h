//
//  ZBEarnPointsModel.h
//  Immigrant
//
//  Created by meifute-iOS on 2019/1/21.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZBPointFlowsModel : NSObject
@property (nonatomic,copy) NSString  *creator;//"2",
@property (nonatomic,copy) NSString  *gmtCreated;// "2019-01-15 17:05:28",
@property (nonatomic,copy) NSString  *gmtModified;// "2019-01-15 17:05:28",
@property (nonatomic,copy) NSString  *id;// 5,
@property (nonatomic,copy) NSString  *isDeleted;// "n",
@property (nonatomic,copy) NSString  *modifier;//"[SYS]",
@property (nonatomic,copy) NSString  *operate;// "in",
@property (nonatomic,copy) NSString  *operatememo;// "完成每日任务获取积分",
@property (nonatomic,copy) NSString  *operatepoint;// 1,
@property (nonatomic,copy) NSString  *operatetext;// "获得积分",
@property (nonatomic,copy) NSString  *userId;// 2
@end

//赚积分
@interface ZBEarnPointsModel : NSObject

@property (nonatomic,copy) NSString  *creator;//": "2",
@property (nonatomic,copy) NSString  *currentpoint;// 4001,
@property (nonatomic,copy) NSString  *gmtCreated;// "2019-01-11 14:08:23",
@property (nonatomic,copy) NSString  *gmtModified;// "2019-01-15 17:05:27",
@property (nonatomic,copy) NSString  *id;// 4,
@property (nonatomic,copy) NSString  *incount;// 1,
@property (nonatomic,copy) NSString  *isDeleted;// "n",
@property (nonatomic,assign)BOOL isreceivefirstgoods;// false,
@property (nonatomic,assign)BOOL  isreceivesecondgoods;// false,
@property (nonatomic,assign)BOOL  isreceivetask1;// false,
@property (nonatomic,assign)BOOL  isreceivetask1date;// true,
@property (nonatomic,assign)BOOL  isreceivetask2;//false,
@property (nonatomic,assign)BOOL  isreceivetask3;// false,
@property (nonatomic,assign)BOOL  isreceivethirdgoods;// false,
@property (nonatomic,copy) NSString  *modifier;// "2",
@property (nonatomic,copy) NSString  *outcount;// 0,
@property (nonatomic, strong)NSArray *pointFlows;
@property (nonatomic,copy) NSString  *task1currentcount;// 0,
@property (nonatomic,copy) NSString  *task1date;//"2019-01-15 00:00:00",
@property (nonatomic,copy) NSString  *task1datecount;// 4,
@property (nonatomic,copy) NSString  *task1point;// 2,
@property (nonatomic,copy) NSString  *task1totalcount;// 1,
@property (nonatomic,copy) NSString  *task2currentcount;// 3,
@property (nonatomic,copy) NSString  *task2point;// 4,
@property (nonatomic,copy) NSString  *task2totalcount;//3,
@property (nonatomic,copy) NSString  *task3currentcount;// 1,
@property (nonatomic,copy) NSString  *task3point;// 6,
@property (nonatomic,copy) NSString  *task3totalcount;// 5,
@property (nonatomic,copy) NSString  *totalpoint;//0,
@property (nonatomic,copy) NSString  *unsentpoint;//0,
@property (nonatomic,copy) NSString  *userId;// 2
@end

