//
//  ZBActivityHomeModel.h
//  Immigrant
//
//  Created by 张波 on 2019/1/2.
//  Copyright © 2019年 张波. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZBMigrateObject.h"
#import "ZBVisaObject.h"
#import "ZBHouseObject.h"
#import "ZBCampObject.h"
#import "ZBInspectionObject.h"
#import "ZBStudyObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBActivityHomeModel : NSObject

/**
 migrates 移民项目
 */
@property (nonatomic,strong) NSArray  *migrates;
/**
 visas 签证中心
 */
@property (nonatomic,strong) NSArray  *visas;
/**
 hosues 海外房产
 */
@property (nonatomic,strong) NSArray  *hosues;
/**
 camps 夏令营集合
 */
@property (nonatomic,strong) NSArray  *camps;
/**
 lines 定制考察
 */
@property (nonatomic,strong) NSArray  *lines;
/**
 studys 海外留学
 */
@property (nonatomic,strong) NSArray  *studys;
/**
 taskmsgs 广播
 */
@property (nonatomic,strong) NSArray  *taskmsgs;

@end

NS_ASSUME_NONNULL_END
