//
//  ZBActivityHomeModel.m
//  Immigrant
//
//  Created by 张波 on 2019/1/2.
//  Copyright © 2019年 张波. All rights reserved.
//

#import "ZBActivityHomeModel.h"

@implementation ZBActivityHomeModel

/**
 #import "ZBMigrateObject.h"
 #import "ZBVisaObject.h"
 #import "ZBHouseObject.h"
 #import "ZBCampObject.h"
 #import "ZBInspectionObject.h"
 #import "ZBStudyObject.h"
 */

/**
 migrates 移民项目
 */
-(void)setMigrates:(NSArray *)migrates{
    _migrates = migrates;
    _migrates = [ZBMigrateObject mj_objectArrayWithKeyValuesArray:migrates];
}
/**
 visas 签证中心
 */
-(void)setVisas:(NSArray *)visas{
    _visas = visas;
    _visas = [ZBVisaObject mj_objectArrayWithKeyValuesArray:visas];
}
/**
 hosues 海外房产
 */
-(void)setHosues:(NSArray *)hosues{
    _hosues = hosues;
    _hosues = [ZBHouseObject mj_objectArrayWithKeyValuesArray:hosues];
}
/**
 camps 夏令营集合
 */
-(void)setCamps:(NSArray *)camps{
    _camps = camps;
    _camps = [ZBCampObject mj_objectArrayWithKeyValuesArray:camps];
}
/**
 lines 定制考察
 */
-(void)setLines:(NSArray *)lines{
    _lines = lines;
    _lines = [ZBInspectionObject mj_objectArrayWithKeyValuesArray:lines];
}
/**
 studys 海外留学
 */
-(void)setStudys:(NSArray *)studys{
    _studys = studys;
    _studys = [ZBStudyObject mj_objectArrayWithKeyValuesArray:studys];
}

@end
